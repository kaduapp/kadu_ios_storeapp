//
//  Extenstions.swift
//  DelivX Store
//
//  Created by 3Embed on 25/04/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension UIColor {
    /// Get new color from hashstring
    ///
    /// - Parameter color: hashtsring (#FFFFFF)
    /// - Returns: UIColor
    class func getUIColor(color:String) -> UIColor {
        var cString:String = color.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    static let customGrayColor = getUIColor(color: "EEEEEE")
    //E0E0E0
    //F9F9F9
}



extension UIViewController {
    
    /// Add the action sheet to ipad screen
    ///
    /// - Parameter actionSheet: UIAlertController
    
    public func addActionSheetForiPad(actionSheet: UIAlertController) {
        if let popoverPresentationController = actionSheet.popoverPresentationController {
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverPresentationController.permittedArrowDirections = []
        }
    }
}


extension UIImage {
    
    /// Resize the Image
    ///
    /// - Parameter size: New Size
    /// - Returns: UIImage
    func resizeImage(size: CGSize) -> UIImage {
        UIGraphicsBeginImageContext(size)
        self.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}

