//
//  ProgressHUD.swift
//  DelivX Store
//
//  Created by 3Embed on 03/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//



import UIKit
/// show the Custom AtivityIndicator
class ProgressHUD: UIVisualEffectView {
    
    var text: String? {
        didSet {
            label.text = text
            label.numberOfLines = 0
        }
    }
    
    let activityIndictor: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    let label: UILabel = UILabel()
    let blurEffect = UIBlurEffect(style: .light)
    let vibrancyView: UIVisualEffectView
    var vc  = UIViewController()
    
    init(vc : UIViewController) {
        self.vc = vc
        self.text = "Please Wait..." // default msg
        self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
        super.init(effect: blurEffect)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.text = ""
        self.vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
        super.init(coder: aDecoder)
        
    }
    
    func setup() {
        contentView.addSubview(vibrancyView)
        contentView.addSubview(activityIndictor)
        contentView.addSubview(label)
        activityIndictor.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        if let superview = self.superview {
            
            let width = superview.frame.size.width  /// 2.3
            let height: CGFloat = superview.frame.size.height
            
            self.frame = CGRect(x: superview.frame.size.width / 2 - width / 2,
                                y: superview.frame.height / 2 - height / 2,
                                width: width,
                                height: height)
            vibrancyView.frame = self.bounds
            
            let activityIndicatorSize: CGFloat = 60
            activityIndictor.frame = CGRect(x: 5,
                                            y: height / 2 - activityIndicatorSize / 2,
                                            width: activityIndicatorSize,
                                            height: activityIndicatorSize)
            
            layer.cornerRadius = 8.0
            layer.masksToBounds = true
            label.text = text
            label.numberOfLines = 0
            label.textAlignment = NSTextAlignment.center
            label.frame = CGRect(x: activityIndicatorSize + 5,
                                 y: 10,
                                 width: width - activityIndicatorSize - 15,
                                 height: height)
            label.textColor = .black
            label.font = UIFont.boldSystemFont(ofSize: 16)
        }
    }
    
    // remove the Activity indicator 
    func dismiss()  {
        self.isHidden = true
        self.removeFromSuperview()
        activityIndictor.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func show(text : String){
        self.text = text
        vc.view.addSubview(self)
        self.isHidden = false
        self.setup()
    }
    
   
}

