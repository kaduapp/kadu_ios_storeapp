//
//  Configurations.swift
//
//
//  Created by CodeX on 31/10/2018
//  Copyright © 2018 Dev_iOS. All rights reserved.
//

import UIKit



protocol ConfigurableCell {
    /// DataType , this is a associatedtype variable , you can send any kind of data with the help of this variable , you can use this variable as array , disctionary , touples , any class type ctc.
    associatedtype DataType
    
    
    /// This method is used for set data to the tableview cell or collectionview cell
    /// associatedtype DataType
    
    func configure(data: DataType)
    
}


protocol CellConfigurator {
    static var reuseId: String { get }
    func configure(cell: UIView)
    func updateItem(item: Any)
}

/// TableCellConfigurator , This is a genric type class , inherite this class to make your tableview cell class genric type it will help you to configur your cell in table view , it also inherited the CellConfigurator protocol



class TableCellConfigurator<CellType: ConfigurableCell, DataType>: CellConfigurator where CellType.DataType == DataType, CellType: UITableViewCell {
    static var reuseId: String { return String(describing: CellType.self) }
    
    var item: DataType
    
    init(item: DataType) {
        self.item = item
    }
    
    func configure(cell: UIView) {
        (cell as! CellType).configure(data: item)
    }
    
    func updateItem(item: Any) {
        self.item = item as! DataType
    }
}

/// CollectionCellConfigurator , This is a genric type class , inherite this class to make your collectionView cell class genric type it will help you to configur your cell in collection view it also inherited the CellConfigurator protocol
class CollectionCellConfigurator<CellType: ConfigurableCell, DataType>: CellConfigurator where CellType.DataType == DataType, CellType: UICollectionViewCell {
    static var reuseId: String { return String(describing: CellType.self) }
    
    var item: DataType
    
    init(item: DataType) {
        self.item = item
    }
    
    func updateItem(item: Any) {
        self.item = item as! DataType
    }
    
    func configure(cell: UIView) {
        (cell as! CellType).configure(data: item)
    }
}
