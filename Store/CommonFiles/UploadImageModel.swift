//
//  UploadImageModel.swift
//  DayRunner
//
//  Created by Vasant Hugar on 27/06/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

struct UploadImage {
    var image = UIImage()
    var path = ""
    
    init(image: UIImage, path: String) {
        self.image = image
        self.path = path
    }
}


protocol UploadImageModelDelegate : class{
    func callBackWithURL(isSuccess : Bool , url : String )
}

class UploadImageModel: NSObject {
    
    weak var delegate : UploadImageModelDelegate?
    private static var manager: UploadImageModel? = nil
    static var shared: UploadImageModel {
        if manager == nil {
            manager = UploadImageModel()
        }
        return manager!
    }
    
    override init() {
        super.init()
        
    }
    
    var uploadImages: [UploadImage] = []
    var imagesKey : [String] = []
    
    private var amazon = AmazonWrapper.sharedInstance()
    private var imageIndexa = 0
    private var deleteIndex = 0
    /// start delete image
    func startDelete(){
        if deleteIndex < imagesKey.count{
        
        }
    }
    
    
    /// Start Uploading Image
    func start() {
        
        if imageIndexa < uploadImages.count {
            uploadImage(uploadImg: uploadImages[imageIndexa])
            imageIndexa += 1
        }
        else {
            uploadImages.removeAll()
            imageIndexa = 0
            print("\n*****************************\nUploading Completed\n*****************************\n")
        }
    }
    
    
    /// delete Images in Background
    ///
    private func deleteImage(keyName : String){
        amazon.deleteImageFrom(keyName: keyName) { (success) in
            print("\n*****************************\n Deleted Image: \n*****************************\n")
        }
    }
    
    
    
    /// Uplad Images in Background
    ///
    /// - Parameter uploadImg: Upload Image Object
    private func uploadImage(uploadImg: UploadImage) {
        amazon.upload(withImage: uploadImg.image,
                      imgPath: uploadImg.path,
                      completion: {(success, url) in
                        self.delegate?.callBackWithURL(isSuccess: success, url: url)
                        print("\n*****************************\nUploaded Image: \(url)\n*****************************\n")
                        self.start()
        })
    }
}

