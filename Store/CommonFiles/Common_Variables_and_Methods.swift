//
//  Common_Methods_And_Variables.swift
//  DelivX Store
//
//  Created by 3Embed on 25/04/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
import SystemConfiguration
import CoreLocation


/// Common_Variables_and_Methods , this is a singleton class , some important and common methods and variables are here

/// it will take memory oone time only.
/// for creating the instance of the class use the getInstance method
/// ### Example ###
/// - let object = Common_Variables_and_Methods.getInstance()


class Common_Variables_and_Methods{
    fileprivate static var Obj_Commom_Variables_and_Methods : Common_Variables_and_Methods!
    let buttonViewBottomAnchor : CGFloat = 40.0
    let hookValue : CGFloat = 48.0
    let defualthookValue : CGFloat = 20.0
    // AIzaSyAepaYaaKl_4W3V0D9gK0FLz75LXi9pY4s
    let key_ALLOWED_LOCATION_ACCESS = "location_Access"
    // com.GoPato.GopatoHome
    let Google_Place_API_KEY = "AIzaSyCAgpxEYmbjiC6xDi-BXwNfutAZrb64jdE"
    
    let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    let taskIndicatorYes = "Yes"
    let taskIndicatorNo = "No"
    
    
    private init(){
        
    }
    
    
    func dialNumber(number : String ) {
        
        if let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            
        }
    }
    
    
    
    
    func setBorderAndcorners(view : UIView){
        view.layer.backgroundColor = UIColor.white.cgColor
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.getUIColor(color: "4C82EE").cgColor
        view.layer.cornerRadius = view.frame.height / 2
    }
    
    
    func getShadwoView_10radius(view : UIView){
        view.layer.cornerRadius = (view.frame.height / 2 )
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowRadius = 3.0
        view.layer.shadowOpacity = 2.0
        view.layer.shadowOffset = CGSize(width: 0, height: 0)
    }
    
    
    func getRoundedCorner( view : UIView , shadowEffect : Bool){
        view.layer.cornerRadius = view.frame.height/2
        if shadowEffect{
            view.layer.shadowColor = UIColor.gray.cgColor
            view.layer.shadowRadius = 3.0
            view.layer.shadowOpacity = 1.0
            view.layer.shadowOffset = CGSize(width: 0, height: 0)
        }
    }
    
    func setStringData_To_UserDefaults ( stringValue : String ,key :String ) {
        UserDefaults.standard.set(stringValue , forKey: key)
    }
    
    func setData_To_UserDefaults( intValue : Int ,key :String ) {
        UserDefaults.standard.set(intValue , forKey: key)
    }
    
    func  getIntData_From_UserDefaults(key : String ) -> Int {
        guard let intValue =  UserDefaults.standard.object(forKey: key) as? Int else {
            return 0
        }
        return intValue
    }
    
    func  getStringData_From_UserDefaults(key : String ) -> String {
        guard let intValue =  UserDefaults.standard.object(forKey: key) as? String else {
            return ""
        }
        return intValue
    }
    
    
    func get_currentTimeStamp() -> String {
        
        return "\(Date().timeIntervalSince1970)"
    }
    
    
    func getIPAddress()-> String {
        
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        if getifaddrs(&ifaddr) == 0 {
            
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next } // memory has been renamed to pointee in swift 3 so changed memory to pointee
                
                let interface = ptr?.pointee
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    
                    if let name: String = String(cString: (interface?.ifa_name)!), name == "en0" {  // String.fromCString() is deprecated in Swift 3. So use the following code inorder to get the exact IP Address.
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                    
                }
            }
            freeifaddrs(ifaddr)
        }
        
        return address ?? "10.10.10.167"
    }
    
    
    public static func getInstance()-> Common_Variables_and_Methods{
        if Obj_Commom_Variables_and_Methods == nil{
            Obj_Commom_Variables_and_Methods = Common_Variables_and_Methods()
        }
        return Obj_Commom_Variables_and_Methods
    }
    
    
    func errorAlert(title : String , message : String ,vc : UIViewController , preferredStyle : UIAlertController.Style  ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
        let cancelbtn = UIAlertAction(title: "OK", style: .default , handler: nil)
        alert.addAction(cancelbtn)
        vc.present(alert, animated: true, completion: nil)
    }
    
    func presentTost(title : String , message : String ,vc : UIViewController , delayTime : Double){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        vc.present(alert, animated: true) {
            DispatchQueue.main.asyncAfter(deadline: .now() + delayTime) { [weak vc] in
                guard vc?.presentedViewController == alert else { return }
                vc?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
   
    
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double , successBlock:@escaping (_ outPut : String  )->Void ) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = pdblLatitude
        center.longitude = pdblLongitude
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        var addressString : String = ""
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country)
                    print(pm.locality)
                    print(pm.subLocality)
                    print(pm.thoroughfare)
                    print(pm.postalCode)
                    print(pm.subThoroughfare)
                    
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + " "
                    }
                    
                    successBlock(addressString )
                    
                }
        })
        
    }
    
    
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "dd/MM/yyyy"
        
        if let date = inputFormatter.date(from: dateString) {
            
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            
            return outputFormatter.string(from: date)
        }
        
        return nil
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
        
    }
    
    
    func getCurrentDate() -> String{
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: date)
    }
    
    
    
    func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
    
    func convertedEventDate(stringDate : String , format : String) -> String{
        
        
        let string = stringDate
        
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let date = dateFormatter.date(from: string)!
        dateFormatter.dateFormat =  format // "yyyy-MM-dd"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        print("EXACT_DATE : \(dateString)")
        return dateString
        
    }
    
    func convertStringtoArray(str : String ,separatedBy : String ) -> [String]{
        
        return (str.components(separatedBy: separatedBy))
        
    }
    
    
    
    func getAlert(vc : UIViewController , title : String , msg : String, completion : @escaping (UIAlertAction)  -> Void)  {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "Ok", style: .cancel , handler: completion)
        alert.addAction(okButton)
        vc.present(alert, animated: true, completion: nil)
    }
    
    
    
    func getDecisionAlert(vc : UIViewController , title : String ,Ok_Title : String , msg : String, completion : @escaping (UIAlertAction)  -> Void)  {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let okButton = UIAlertAction(title: Ok_Title, style: .cancel , handler: completion)
        alert.addAction(okButton)
        let cancelbtn = UIAlertAction(title: "Cancel", style: .default , handler: nil)
        alert.addAction(cancelbtn)
        vc.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    func getAppName() -> String{
        let infoDictionary: NSDictionary = (Bundle.main.infoDictionary as NSDictionary?)!
        let appName: NSString = infoDictionary.object(forKey: "CFBundleName") as! NSString
        NSLog("Name \(appName)")
        return appName as String
    }
    
    
    
    
}
