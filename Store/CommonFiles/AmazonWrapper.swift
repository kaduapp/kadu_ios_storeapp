//
//  File.swift
//  AmazonWrapper
//
//  Created by Apple on 27/03/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit
import AWSS3

let AmazonAccessKey  = "AKIAQMFUVVY4XY4RIN4E"
let AmazonSecretKey  = "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"
let Bucket      = "kadubucket"
let AMAZON_URL  = "https://kadubucket.s3.amazonaws.com/"


protocol AmazonWrapperDelegate {
    /**
     *  Facebook login is success
     *1
     *  @param userInfo Userdict
     */
    
    func didImageUploadedSuccessfully(withDetails imageURL: String)
    /**
     *  Login failed with error
     *
     *  @param error error
     */
    
    func didImageFailtoUpload(_ error: Error?)
}

class AmazonWrapper: NSObject {
    
    static var share:AmazonWrapper?
    var delegate: AmazonWrapperDelegate?
    
    class func sharedInstance() -> AmazonWrapper {
        
        if (share == nil) {
            
            share = AmazonWrapper.self()
            
        }
        return share!
    }
    
    override init() {
        super.init()
        
    }
    
    func setConfigurationWithRegion(_ regionType: AWSRegionType, accessKey: String, secretKey: String) {
        
        let credentialsProvider = AWSStaticCredentialsProvider(accessKey: accessKey, secretKey: secretKey)
        let configuration = AWSServiceConfiguration(region: regionType, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
    }
    
    
    func uploadImageToAmazon(withImage image: UIImage, imgPath:String) {
        
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddhhmmssa"
        
        var paths: [AnyObject] = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [AnyObject]
        let documentsDirectory: String = paths[0] as! String
        
        let photoURLPath = NSURL(fileURLWithPath: documentsDirectory)
        let getImagePath  = photoURLPath.appendingPathComponent("\(formatter.string(from:Date())).jpg")
        
        
        if !FileManager.default.fileExists(atPath: getImagePath!.path) {
            do {
                try UIImageJPEGRepresentation(image, 1.0)?.write(to: getImagePath!)
                print("file saved")
            }catch {
                print("error saving file")
            }
        }
        else {
            print("file already exists")
            
        }
        AWSS3TransferUtility.default().uploadFile(getImagePath!,
                                                  bucket: Bucket,
                                                  key: imgPath,
                                                  contentType: "image/png", expression:nil) { (task, error) in
                                                    
                                                    if (error != nil) {
                                                        if (self.delegate != nil)  {
                                                            self.delegate?.didImageFailtoUpload(error)
                                                        }
                                                    }
                                                    else {
                                                        let uploadedImageURL = String(format:"%@%@/%@",AMAZON_URL,Bucket,imgPath)
                                                        
                                                        if (self.delegate != nil)  {
                                                            self.delegate?.didImageUploadedSuccessfully(withDetails: uploadedImageURL)
                                                        }
                                                    }
        }
    }
    
    func upload(withImage image: UIImage, imgPath:String, completion: @escaping(_ success: Bool, _ url: String) -> Void) {
        
        let formatter: DateFormatter = DateFormatter()
        formatter.dateFormat = "yyyyMMddhhmmssa"
        
        var paths: [AnyObject] = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true) as [AnyObject]
        let documentsDirectory: String = paths[0] as! String
        
        let photoURLPath = NSURL(fileURLWithPath: documentsDirectory)
        let getImagePath  = photoURLPath.appendingPathComponent("\(formatter.string(from:Date())).jpg")
        
        if !FileManager.default.fileExists(atPath: getImagePath!.path) {
            do {
                try UIImageJPEGRepresentation(image, 1.0)?.write(to: getImagePath!)
                print("file saved")
            }
            catch {
                print("error saving file")
            }
        }
        else {
            print("file already exists")
        }
        
        AWSS3TransferUtility.default().uploadFile(getImagePath!,
                                                  bucket: Bucket,
                                                  key: imgPath,
                                                  contentType: "image/png", expression:nil) { (task, error) in
                                                    
                                                    if (error != nil) {
                                                        completion(false, "")
                                                    }
                                                    else {
                                                        let uploadedImageURL = String(format:"%@%@/%@",AMAZON_URL,Bucket,imgPath)
                                                        completion(true, uploadedImageURL)
                                                    }
        }
    }
    
    
    func deleteImageFrom( keyName : String , onCompletionHandler:@escaping(Bool ) -> Void ) {
        let s3 = AWSS3.default()
        let deleteObjectRequest = AWSS3DeleteObjectRequest()
        deleteObjectRequest?.bucket = Bucket
        deleteObjectRequest?.key = keyName
        s3.deleteObject(deleteObjectRequest!).continueWith { (task:AWSTask) -> AnyObject? in
            if let error = task.error {
                print("Error occurred: \(error)")
                onCompletionHandler(false )
                return nil
            }
            if let response = task.result {
                print(response)
                onCompletionHandler(true )
                
            }
            print("Deleted successfully.")
            return nil
        }
    }
    
    
    
    
    
}
