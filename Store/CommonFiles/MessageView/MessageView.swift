//
//  MessageView.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 06/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit



class MessageView: UIView {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageLogo: UIImageView!
   static func loadViewFromNib() -> MessageView {
        return UINib(nibName: String(describing:self), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView as! MessageView
    }
}

extension UIView {
    
    func showErrorMessage(message:String, textColor:UIColor = UIColor.black) {
        let messageView = MessageView.loadViewFromNib()
        messageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        messageView.frame = self.bounds
        messageView.messageLabel.textColor = textColor
        
        if !message.isEmpty {
            messageView.messageLabel.text = message
        }
        
        if let view = self as? UITableView {
            view.backgroundView = messageView
        }
        
        if let view = self as? UICollectionView {
            view.backgroundView = messageView
        }
    }
    
    func hideErrorMessage(){
        if let view = self as? UITableView {
            if let backgroundView = view.backgroundView as? MessageView {
                backgroundView.removeFromSuperview()
                view.backgroundView = nil
            }
        }
        
        if let view = self as? UICollectionView {
            if let backgroundView = view.backgroundView as? MessageView {
                backgroundView.removeFromSuperview()
                view.backgroundView = nil
            }
        }
        
    }
}

