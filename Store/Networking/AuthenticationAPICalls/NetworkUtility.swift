//
//  NetworkUtility.swift
//  Ufy_Store
//
//  Created by Raghavendra Shedole on 01/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import Alamofire


class Connectivity {
    
    static let sharedInstance = Connectivity()
    let net = NetworkReachabilityManager()

    class func isConnectedToInternet() ->Bool {
        if NetworkReachabilityManager()!.isReachable {
            return true
        }else {
            Helper.showAlert(message: "Please connect to internet.", head: "Not connected to internet", type: 0)
            return false
        }
    }
    
    func startListening(completion: @escaping (Bool) -> ()) {
        net?.startListening()
        net?.listener = { status in
            switch status {
                
            case .reachable(.ethernetOrWiFi),.reachable(.wwan):
              //  MQTT.sharedInstance.createConnection()
                Toast.hideMessage()
                print("The network is reachable over the WiFi connection")
                completion(true)
                
//            case ):
//                Toast.hideMessage()
//                print("The network is reachable over the WWAN connection")
//
            case .notReachable:
                print("The network is not reachable")
                Toast.showNegativeMessage(message: "Not connected to internet.")
             //   MQTT.sharedInstance.disconnectMQTTConnection()
                completion(false)
                
            case .unknown:
             //   MQTT.sharedInstance.disconnectMQTTConnection()
                completion(false)
                
//            case  :
//                Toast.hideMessage()
//                print("It is unknown whether the network is reachable")
                
            }
        }
    }
    
    func  stopListening() {
        net?.stopListening()
    }
}


class Toast {
    class private func showAlert(backgroundColor:UIColor, textColor:UIColor, message:String)
    {
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let label = UILabel(frame: CGRect.zero)
        label.tag = 98765462342
        label.textAlignment = NSTextAlignment.center
        label.text = message
        label.font = UIFont(name: Roboto.Bold, size: 15)
        label.adjustsFontSizeToFitWidth = true
        
        label.backgroundColor =  backgroundColor //UIColor.whiteColor()
        label.textColor = textColor //TEXT COLOR
        
        label.sizeToFit()
        label.numberOfLines = 4
        label.layer.shadowColor = UIColor.gray.cgColor
        label.layer.shadowOffset = CGSize(width: 4, height: 3)
        label.layer.shadowOpacity = 0.3
        label.frame = CGRect(x: appDelegate.window!.frame.size.width, y: 0, width: appDelegate.window!.frame.size.width, height: 25)
        
        label.alpha = 1
        let window = appDelegate.window
        window?.windowLevel = UIWindowLevelStatusBar
        window?.addSubview(label)
        
        var basketTopFrame: CGRect = label.frame;
        basketTopFrame.origin.x = 0;
        
        UIView.animate(withDuration
            :2.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: UIViewAnimationOptions.curveEaseOut, animations: { () -> Void in
                label.frame = basketTopFrame
        }
        )
    }
    
    class func showPositiveMessage(message:String)
    {
        showAlert(backgroundColor: UIColor.green, textColor: UIColor.white, message: message)
    }
    class func showNegativeMessage(message:String)
    {
        if NetworkReachabilityManager()!.isReachable {
            return
        }
        showAlert(backgroundColor: UIColor.red, textColor: UIColor.white, message: message)
    }
    
    class func hideMessage() {
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        if let label = appDelegate.window?.viewWithTag(98765462342) {
            UIView.animate(withDuration:2.0, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
                label.alpha = 0
            },  completion: {
                (value: Bool) in
                label.removeFromSuperview()
            })
        }
    }
}
