//
//  APIClass.swift
//  Ufy_Store
//
//  Created by Rahulsharma on 09/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import CocoaLumberjack
import Alamofire


/// API Name
class APIEndTails {
    //    static let BaseUrl                                  = "https://api.instacart-clone.com/"
    //    static let Store                                    = "store/"
    //    static let Login                                    = BaseUrl + Store + "signIn"
//    https://apidispatcher.instacart-clone.com -> ufly link
//      https://apidispatcher.loopzdelivery.com -> loopzDelivery
    
    static let Order_BaseUrl                            = "https://apidispatcher.instacart-clone.com/"
    static let OrderList                                = Order_BaseUrl + "orders/"
    static let OrderDetails                             = Order_BaseUrl + "order/"
    static let ChangeOrderStatus                        = Order_BaseUrl + "order"
    static let DispatchOrder                            = Order_BaseUrl + "dispatchOrder"
    static let UserHistory                              = Order_BaseUrl + "userHistory/"
    static let OrderHistoryService                      = Order_BaseUrl + "ordersHistory/"
    static let CancellationReasonAPI                          = Order_BaseUrl + "cancellationReasons"
    
    
    static let LoginAuthAPI                             = Order_BaseUrl + "dispatcher/"
    static let LogoutService                            = LoginAuthAPI + "logout"
    static let Login                                    = LoginAuthAPI + "logIn"
    static let ProfileData                              = LoginAuthAPI + "profile"
    static let GetStoreDrivers                          = LoginAuthAPI + "drivers/"
    static let DispatchOrderToDriver                    = LoginAuthAPI + "dispatchOrder"
    static let OrderHistory                             = LoginAuthAPI + "orders/"
    static let ForgotPassword                           = LoginAuthAPI + "forgotPassword"
    static let OrderHistorySearch                       = LoginAuthAPI + "orders/"
    
    static let EditOrderAPI                             = "https://api.instacart-clone.com/dispatcher/order"
    

}


class APIRequestParams {
    
    //    API Header
    static let Language                                 = "language"
    static let Authorization                            = "authorization"
    static let LanguageDefault                          = "0"
    
    //  Login Request API
    static let Email                                    = "email"
    static let Password                                 = "password"
    static let DeviceID                                 = "deviceId"
    static let DeviceMake                               = "deviceMake"
    static let DeviceModal                              = "deviceModel"
    static let DeviceType                               = "deviceType"
    static let DeviceTime                               = "deviceTime"
    static let Latitude                                 = "latitude"
    static let Longitude                                = "longitude"
    static let PushToken                                = "pushToken"
    static let AppVersion                               = "appVersion"
    
    // Change Order Status
    
    static let StoreId                                  = "storeId"
    static let ManagerID                                = "managerId"
    static let Status                                   = "status"
    static let TimeStamp                                = "timestamp"
    static let OrderID                                  = "orderId"
    static let cancelReason                             = "reason"
    static let DueTime                                  = "dueDatetime"
    static let serviceType                              = "serviceType"
    static let Items                                    = "items"
    static let DeliveryCharge                           = "deliveryCharge"
    
    //DispatchOrderToDriver
    static let driverId                                 = "driverId"
    
}



/// Handling of api status code

class APIStatusCode {
    enum ErrorCode: Int {
        case success                = 200
        case created                = 201
        case deleted                = 202 //Accepted forgotPW
        case updated                = 203
        case NoUser                 = 204
        case badRequest             = 400
        case Unauthorized           = 401
        case Required               = 402
        case Forbidden              = 403
        case NotFound               = 404
        case MethodNotAllowed       = 405
        case NotAcceptable          = 406
        case ExpiredOtp             = 410
        case PreconditionFailed     = 412
        case RequestEntityTooLarge  = 413
        case TooManyAttemt          = 429
        case ExpiredToken           = 440
        case InvalidToken           = 498
        case InternalServerError    = 500
        case InternalServerError2   = 502
        
    }
    
    
    class func basicParsing(data:[String:Any],status: Int) {
        Helper.hidePI()
        let errNum = ErrorCode(rawValue: status)!
        switch errNum {
        case .success:
            Helper.showAlert(message: data[APIResponseParams.Message] as! String, head: StringConstants.Success, type: 0)
            break
        case .ExpiredToken,.InvalidToken,.NoUser:
            Logout.logout()
            break
            
        case .NotFound:
            Helper.showAlert(message: data[APIResponseParams.Message] as! String, head: StringConstants.Error, type: 0)
            break
            
        case .deleted:
             Helper.showAlert(message: data[APIResponseParams.Message] as! String, head: StringConstants.Error, type: 0)
            break
            
        default:
            var message = ""
            if let msg = data[APIResponseParams.Message] as? String {
                message = msg
            }
            Helper.showAlert(message: message, head: StringConstants.Error, type: 1)
            break
        }
    }
}



/// Class for getting api related data
class APIUtility {
    
    class func getHeaderForOrders() -> [String : String] {
        
        let keychainHelper = KeychainHelper.sharedInstance
        
        let header = [APIRequestParams.Language : APIRequestParams.LanguageDefault,
                      APIRequestParams.Authorization : keychainHelper.getAuthorizationKey() ]
        return header
    }
    
    class func getLogin() -> Bool {
        if UserDefaults.standard.bool(forKey: "_storeLoginStatus") {
            return true
        }else {
            return false
        }
    }
    
    class func setLogin(loginStatus:Bool) {
        UserDefaults.standard.set(loginStatus, forKey: "_storeLoginStatus")
    }
    
    class func removeLogin() {
        UserDefaults.standard.removeObject(forKey: "_storeLoginStatus")
    }
    class func getHeader() -> [String : String] {
        let header = [APIRequestParams.Language : APIRequestParams.LanguageDefault]
        return header
    }
}


class APIHandler {
    static let disposeBag = DisposeBag()
    
    static var networkAvailability:Bool {
        return Connectivity.isConnectedToInternet()
    }
    
    class func getRequest(header:[String:String], url:String) -> Observable<(Bool,Any)> {
        
        DDLogDebug("header : \(header)")
        DDLogDebug("url : \(url)")
        
        
        
        return Observable.create{ observer in
            
            if !networkAvailability {
                observer.onError(NSError(domain: "Network Not found", code: -1, userInfo: nil))
                observer.onCompleted()
                return Disposables.create()
            }
            Helper.showPI(string: StringConstants.ProgressIndicator)
            RxAlamofire.requestJSON(.get, url, parameters: nil, headers: header)
                .timeout(30, scheduler: MainScheduler.instance)
                .debug().subscribe(onNext: { (head, body) in
                    DDLogDebug("response : \(body)")
                    Helper.hidePI()
                    let errNum:APIStatusCode.ErrorCode = APIStatusCode.ErrorCode(rawValue: head.statusCode)!
                    if errNum == .success {
                        let bodyIn = body as! [String:Any]
                        let data = bodyIn[APIResponseParams.Data] as! [String:Any]
                        observer.onNext((true,data))
                        observer.onCompleted()
                    }else if errNum == .Required {
                        observer.onNext((false,""))
                        observer.onCompleted()
                    }else {
                        APIStatusCode.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    }
                    
                }, onError: { error in
                    if let error = error as? RxError {
                        print(error.debugDescription)
                        Helper.showAlert(message: error.debugDescription, head: "Network Error", type: 0)
                    }
                    Helper.hidePI()
                }).disposed(by: disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    class func postRequest(header:[String:String], url:String,params:[String:Any])-> Observable<(Bool,Any)> {
        DDLogDebug("header : \(header)")
        DDLogDebug("params : \(params)")
        return Observable.create{ observer in
            if !networkAvailability {
                observer.onError(NSError(domain: "Network Not found", code: -1, userInfo: nil))
                observer.onCompleted()
                return Disposables.create()
            }
            
            Helper.showPI(string: StringConstants.ProgressIndicator)
            RxAlamofire.requestJSON(.post, url, parameters: params, headers: header)
                .timeout(30, scheduler: MainScheduler.instance)
                .debug().subscribe(onNext: { (head,body) in
                    DDLogDebug("response : \(body)")
                    Helper.hidePI()
                    let errNum:APIStatusCode.ErrorCode? = APIStatusCode.ErrorCode(rawValue: head.statusCode)!
                    
                    if errNum == .success || errNum == .deleted  {
                        let bodyIn = body as! [String:Any]
                        var data:[String:Any]
                        if let response = bodyIn[APIResponseParams.Data] as? [String:Any] {
                            data = response
                            observer.onNext((true,data))
                        }
                        if let logoutResponse = bodyIn[APIResponseParams.LogoutData] as? String {
                            if logoutResponse.contains("Logged Out") {
                                observer.onNext((true, " Logged Out"))
                            }
                        }
                        observer.onCompleted()
                    }else if errNum == .Required {
                        observer.onNext((false,""))
                        observer.onCompleted()
                    }else {
                        APIStatusCode.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    }
                    
                }, onError: { error in
                    Helper.hidePI()
                   APIHandler.errorHandler(error: error)
                   
                    
//                    Helper.showAlert(message: "Server is unavaillable. Please try again!", head: "Network Error", type: 0)
                }).disposed(by: disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    class func patchRequest(header:[String:String], url:String,params:[String:Any])-> Observable<(Bool,Any)> {
        DDLogDebug("header : \(header)")
        DDLogDebug("params : \(params)")
        return Observable.create{ observer in
            if !networkAvailability {
                observer.onError(NSError(domain: "Network Not found", code: -1, userInfo: nil))
                observer.onCompleted()
                return Disposables.create()
            }
            
            Helper.showPI(string: StringConstants.ProgressIndicator)
            RxAlamofire.requestJSON(.patch, url, parameters: params, encoding: JSONEncoding.default, headers: header)
                .timeout(30, scheduler: MainScheduler.instance)
                .debug().subscribe(onNext: { (head,body) in
                    DDLogDebug("response : \(body)")
                    Helper.hidePI()
                    let errNum:APIStatusCode.ErrorCode? = APIStatusCode.ErrorCode(rawValue: head.statusCode)!
                    
                    if errNum == .success {
                        let bodyIn = body as! [String:Any]
                        
                        guard let data = bodyIn[APIResponseParams.Data] as? [String:Any] else{
                            observer.onNext((true,""))
                            return
                        }
                        observer.onNext((true,data))
                        observer.onCompleted()
                    }else if errNum == .Required {
                        observer.onNext((false,""))
                        observer.onCompleted()
                    }else {
                        APIStatusCode.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    }
                    
                }, onError: { error in
                    if let error = error as? RxError {
                        print(error.debugDescription)
                        Helper.showAlert(message: error.debugDescription, head: "Network Error", type: 0)
                    }
//                    Helper.showAlert(message: "Server is unavaillable. Please try again!", head: "Network Error", type: 0)
                    Helper.hidePI()
                }).disposed(by: disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
}

extension APIHandler {
    class func errorHandler(error:Any) {
        if let error = error as? RxError {
            print(error.debugDescription)
            Helper.showAlert(message: error.debugDescription, head: "Network Error", type: 0)
            return
        }
        if let error = error as? Error  {
            Helper.showAlert(message: error.localizedDescription, head: "Network Error", type: 0)
        }
    }
}


extension RxError {
    /// A textual representation of `self`, suitable for debugging.
    public var debugDescription: String {
        switch self {
        case .unknown:
            return "Unknown error occurred."
        case .disposed(let object):
            return "Object `\(object)` was already disposed."
        case .overflow:
            return "Arithmetic overflow occurred."
        case .argumentOutOfRange:
            return "Argument out of range."
        case .noElements:
            return "Sequence doesn't contain any elements."
        case .moreThanOneElement:
            return "Sequence contains more than one element."
        case .timeout:
            return "Request timeout."
        }
    }
}





