//
//  Colors.swift
//  DelivX Store
//
//  Created by Rahulsharma on 17/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit


class Colors {
    //redColor : #B9B9B9
    //ThemeColor : #5AA43E
    
    static let White                                        = "FFFFFF"//White Color
    static let Separator                                    = "F5F5F5" //Separators
    static let PrimaryText                                  = "222224"//Black
    static let SecoundPrimaryText                           = "6B6B6B"//Black
    static let SecounderyText                               = "747474"//Gray
    static let HeadColor                                    = "C4C4C4"
    static let ScreenBackground                             = "F5F5F5"
    static let AddButton                                    = "4695E6"
    static let DoneBtnNormal                                = "DFE2E7"//
    static let Red                                          = "E23E3E" // red for cart button
    static let BlueColor                                    = "D95656" //"4695E6" //payment ,orderDetail
    static let SectionHeader                                = "4A4A4A" //OrderDetail
    static let Unselector                                   = "B9B9B9" // orderDetail
    static let yellow                                       = "FFD180"
    static let green = "B9F6CA"
    static let black = "0000000"
    //StoreApp
    static let AppBaseColor                                 = "8D30F9"
     static let shadowColor                                 = "F4F4FF"
    static let preparingScreen                              = "FF9900"
    static let lightGray = "E7E7E7"
    static let blue = "078EF4"  //
    static let borderColor = "D3D6FF"
    static let tableViewBackGround = UIColor.colorWithHex(color: "EAEFF6") // BDBFC5 // F2F6F8 // EAEFF6
    
    
}


struct ColorConstants {
    
    //static let tableViewBackGround = UIColor.colorWithHex(color: "F2F6F8")
    static let Color_BarTint:UIColor = UIColor.colorWithHex(color: "8D30F9")
    static let Color_NavTitleColor:UIColor = UIColor.colorWithHex(color: "FFFFFF")
     static let Color_NavTitleColorGreen:UIColor = UIColor.colorWithHex(color: "1D0869")
    
    
    //HomeViewController
    static let Color_SerchBarPlaceholder: UIColor = UIColor.colorWithHex(color: "9A9A9A")
    static let Color_searchBarText: UIColor = UIColor.colorWithHex(color: "1A1B1C")
   /**/ static let Color_BarTintColor: UIColor = UIColor.colorWithHex(color: "9A9A9A")
    
    static let Color_whiteColor: UIColor = UIColor.colorWithHex(color: "FFFFFF")
    
    
    static let Color_ImageIndicator:UIColor = UIColor.colorWithHex(color: "000000")
    
    static let Color_PreparingLabel = UIColor.colorWithHex(color: "D3C520")
    static let Color_NewOrders: UIColor = UIColor.red
    static let Color_PreparingOrders: UIColor = UIColor.orange
    static let Color_ReadyOrders: UIColor = UIColor.green
    static let backgroundColor = UIColor.colorWithHex(color: "F4F4FF")
}

extension UIColor {
    class func colorWithHex(color:String) -> UIColor {
        var cString:String = color.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.sorted().count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

