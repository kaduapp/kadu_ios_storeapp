//
//  UIDatePicker.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 08/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class CustomDatePicker: UIDatePicker {
    
    @IBInspectable public var dateTextColor:UIColor = .black {
        didSet {
            self.setValue(dateTextColor, forKey: "textColor")
        }
    }
    
    
}
