//
//  UICollectionViewCell.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 13/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionViewCell {
    func addShadow(shadowColor:String) {
        self.layer.shadowColor = UIColor.colorWithHex(color: shadowColor).cgColor
        self.layer.shadowOffset = CGSize(width:4.0,height: 4.0)
        self.layer.shadowRadius = 4.0
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false;
        self.layer.shadowPath = UIBezierPath(roundedRect:self.bounds, cornerRadius:self.contentView.layer.cornerRadius).cgPath
    }
}
