//
//  Date.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 22/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation


//DateFormats
struct DateFormat {
    static let DateAndTimeFormatServer                  = "yyyy-MM-dd HH:mm:ss"
    static let TimeAndDateFormatServer                  = "HH:mm:ss dd-MM-yyyy"
    static let DateAndTimeFormatServerGET               = "yyyyMMddHHmmss"
    static let DateFormatToDisplay                      = "dd MMM yyyy"
    static let DateFormatServer                         = "yyyy-MM-dd"
    static let TimeFormatServer                         = "HH:mm"
    static let TimeFormatToDisplay                      = "hh:mm a"
    static let OrderDateFormate                         = "hh:mm a, dd MMM"
    static let DateOfBirthFormat                        = "MM/dd/yyyy"
    static let onlyTime                                 = "hh:mm a"
    static let onlyDate                                 = "dd MMM"
    static let fullDate                                 = "dd MMM YYYY"
}

enum RequiredDate {
    case Mins
    case HourMins
}

//convertGMTDateToLocalFromDate


extension Date {
    
    static func getDateFromString(value:String,format:String) -> String {
        
        let formatter = DateFormatter()
        //        formatter.locale = Locale.init(identifier: "en_IN")
        
      
        formatter.dateFormat = DateFormat.DateAndTimeFormatServer
        
        if formatter.date(from: value) == nil {
            return formatter.string(from: Date())
        }else {
            let someDate = formatter.date(from: value)
            let date = Helper.convertGMTDateToLocalFromDate(gmtDate: someDate!)
            formatter.dateFormat = format
            return formatter.string(from: date)
        }
        
        
        
        //        let formatter = DateFormatter()
////        formatter.locale = Locale.init(identifier: "en_IN")
//        formatter.dateFormat = DateFormat.DateAndTimeFormatServer
//
//        if formatter.date(from: value) == nil {
//            return formatter.string(from: Date())
//        }else {
//            let someDate = formatter.date(from: value)
//            formatter.dateFormat = format
//            return formatter.string(from: someDate!)
//        }
        
      
    }
    
    static func getCurrentData() -> String {
        
        let formatter = DateFormatter()
        formatter.dateFormat = DateFormat.DateAndTimeFormatServer
        return formatter.string(from: Date.init())
        
    }
    
    static func getTheDataFromTimeStamp(value:Int64,format:String) -> String {
        
        let formatter = DateFormatter()
//        formatter.dateFormat = DateFormat.DateAndTimeFormatServer
        let timestamp = Date(timeIntervalSince1970: TimeInterval(value))
        let date = Helper.convertGMTDateToLocalFromDate(gmtDate: timestamp)
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    
    static func getDueTime(value:String) -> String {
        
        
        let formatter = DateFormatter()
        //        formatter.locale = Locale.init(identifier: "en_IN")
        formatter.dateFormat = DateFormat.DateAndTimeFormatServer
        
        var dueTime:Date
        if formatter.date(from: value) == nil {
            dueTime  = Date()
        }else {
            let date = formatter.date(from: value)!
            dueTime = Helper.convertGMTDateToLocalFromDate(gmtDate: date)
        }
        
        return DateComponentsFormatter().difference(from:dueTime, to: Helper.convertGMTDateToLocalFromDate(gmtDate: Date()))!
        //            stringFromTimeInterval(interval: (dueTime.timeIntervalSinceNow)) as String
        
        
        
        //        let formatter = DateFormatter()
        //        //        formatter.locale = Locale.init(identifier: "en_IN")
        //        formatter.dateFormat = DateFormat.DateAndTimeFormatServer
        //
        //        var dueTime:Date
        //        if formatter.date(from: value) == nil {
        //            dueTime  = Date()
        //        }else {
        //            dueTime = formatter.date(from: value)!
        //        }
        //        return DateComponentsFormatter().difference(from:dueTime, to: Date())!
        //        //            stringFromTimeInterval(interval: (dueTime.timeIntervalSinceNow)) as String
    }
    
    
    
    static func dateDifference(dueDate:String, bookingDate:String) ->String {
        let toDate = getStringToDate(value: dueDate, format: DateFormat.DateAndTimeFormatServer)
        let fromDate = getStringToDate(value: bookingDate, format: DateFormat.DateAndTimeFormatServer)
        
        return DateComponentsFormatter().difference(from: fromDate, to: toDate)!
    }
    
    static func getDateString(dateFormat:String, requiredDateFormat:String, dateValue:String) -> String {
        
        let formatter = DateFormatter()
        //        formatter.locale = Locale.init(identifier: "en_IN")
        formatter.dateFormat = dateFormat
        
        if formatter.date(from: dateValue) == nil {
            return formatter.string(from: Helper.convertGMTDateToLocalFromDate(gmtDate: Date()))
        }else {
            let someDate = formatter.date(from: dateValue)
            
            formatter.dateFormat = requiredDateFormat
            return formatter.string(from: Helper.convertGMTDateToLocalFromDate(gmtDate: someDate!))
        }
        
        
        //        let formatter = DateFormatter()
        ////        formatter.locale = Locale.init(identifier: "en_IN")
        //        formatter.dateFormat = dateFormat
        //
        //        if formatter.date(from: dateValue) == nil {
        //            return formatter.string(from: Date())
        //        }else {
        //            let someDate = formatter.date(from: dateValue)
        //            formatter.dateFormat = requiredDateFormat
        //            return formatter.string(from: someDate!)
        //        }
    }
    
    static func stringFromTimeInterval(interval:TimeInterval) -> NSString {
        let time = NSInteger(interval)
        let minutes = time / 60
        return NSString(format: "%d",Int(minutes))
    }
    
    static func getDateString(value:Date,format:String) -> String {
        let formatter = DateFormatter()
//        formatter.locale = Locale.init(identifier: "en_IN")
        formatter.dateFormat = format
        let myString = formatter.string(from: value)
        return myString
    }
    
    
    
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    static func getCurrentDateAndTime() -> String{
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        
        formatter.dateFormat = DateFormat.DateAndTimeFormatServer
        let dateString = formatter.string(from: now)
        return dateString
    }
    

    static func getStringToDate(value:String,format:String) -> Date {
        let formatter = DateFormatter()
        //formatter.locale = Locale.init(identifier: "en_IN")
        formatter.locale = Locale.init(identifier: "en_US_POSIX")// for 24 hours
        formatter.dateFormat = format
        if formatter.date(from: value) == nil {
            return Helper.convertGMTDateToLocalFromDate(gmtDate: Date())
        }else {
            let date = formatter.date(from: value)!
            return Helper.convertGMTDateToLocalFromDate(gmtDate: date)
        }
    }
    
    static func convertStringToDate( value:String,format:String ) -> Date{
        let dateFormatter = DateFormatter()
        print(format)
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        dateFormatter.dateFormat = format //Your date format
        
        //according to date format your date string
        guard let date = dateFormatter.date(from: value) else {
            print("error........  date conversion failed")
            fatalError()
        }
        
        print(date)
        return date
    }
    
    static func getStringToDateFromTimeStamp(value:Int64,format:String) -> Date {
        
        let formatter = DateFormatter()
        formatter.dateFormat = DateFormat.DateAndTimeFormatServer
        let timestamp = Date(timeIntervalSince1970: TimeInterval(value))
        let date = Helper.convertGMTDateToLocalFromDate(gmtDate: timestamp)
        formatter.dateFormat = format
        let newDate = formatter.string(from: date)
        return formatter.date(from: newDate)!
    }
    
    static var getTimeStamp: Double {
        return Double(NSDate().timeIntervalSince1970 * 1000)
    }
    
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
}

extension DateComponentsFormatter {
    func difference(from fromDate: Date, to toDate: Date) -> String? {
        self.allowedUnits = [.day, .hour,.minute]
        //[.year,.month,.weekOfMonth,.day,.hour,.minute]
        self.maximumUnitCount = 0
        self.unitsStyle = .full
        return self.string(from: fromDate, to: toDate)?.replacingOccurrences(of: "-", with: "")
    }
}

extension Date {
    
   static func differenceDate(date: Date) -> Int {
        let minutes =  Calendar.current.dateComponents([.minute], from: Date(), to: date).minute ?? 0
        return minutes
    }
}


