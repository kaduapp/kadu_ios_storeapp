//
//  KeyBoardExtention.swift
//  UFly
//
//  Created by 3Embed on 06/11/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

extension UIViewController {
    func addObserver() {
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //Mark:- Keyboard ANimation
    @objc func keyboardWillShow(_ notification: NSNotification){
        // Do something here
        let info = notification.userInfo!
        let inputViewFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let screenSize: CGRect = UIScreen.main.bounds
        var frame = self.view.frame
        frame.size.height = screenSize.height - inputViewFrame.size.height
        self.view.frame = frame
    }
    @objc func keyboardWillHide(_ notification: NSNotification){
        // Do something here
        UIView.animate(withDuration: 0.2, animations: {() -> Void in
            let screenSize: CGRect = UIScreen.main.bounds
            var frame = self.view.frame
            frame.size.height = screenSize.height
            self.view.frame = frame
        })
    }
}
