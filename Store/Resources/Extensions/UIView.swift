//
//  UIView.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 01/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit
import CocoaLumberjack


extension UIView {
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize = CGSize(width:1.0,height:2.0), radius: CGFloat = 1, scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}



extension UIView {
   public func log(message:Any) {
        DDLogDebug("\(String(describing:self)) \(message)")
    }
}


@IBDesignable
class CustomView:UIView {
    
    @IBInspectable public var cornerRadius:CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
        }
    }
    
    @IBInspectable public var shadowColor:UIColor = UIColor.clear {
        didSet {
            self.dropShadow(color: shadowColor)
        }
    }
}
