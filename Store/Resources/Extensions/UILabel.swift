//
//  UILabel.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 05/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func setCornerRadius(radius:CGFloat) {
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
} 

class RobotoMediumLable:UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont(name: Roboto.Medium, size: self.font.pointSize)
    }
}

class RobotoRegularLable:UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont(name: Roboto.Regular, size: self.font.pointSize)
    }
}

class RobotoBoldLable:UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont(name: Roboto.Bold, size: self.font.pointSize)
    }
}

class RobotoCondensedRegularLable: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont(name: RobotoCondensed.Regular, size: self.font.pointSize)
    }
}

class RobotoCondensedBoldLable: UILabel {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont(name: RobotoCondensed.Bold, size: self.font.pointSize)
    }
}



@IBDesignable
class CustomLabel: RobotoCondensedBoldLable {
    
    @IBInspectable public var cornerRadius: CGFloat = 0.0{
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = true
        }
    }
    
    @IBInspectable public var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    
@IBInspectable public var borderColor: UIColor = .clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
}

