//
//  UIViewController.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 31/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit
import CocoaLumberjack

extension UIViewController {
    func setStatusColor()  {
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
//            statusBar.backgroundColor = UIColor.colorWithHex(color: "263238")
//        }
//        UIApplication.shared.statusBarStyle = .lightContent
    }
    
     /// Checking network availability
//     var isNewtrokAvailable:Bool  {
//        if Connectivity.isConnectedToInternet() {
//            return true
//        }else {
//            Helper.showAlert(message: "Please connect to internet.", head: "Not connected to internet", type: 0)
//            return false
//        }
//    }
    
    public func log(message:Any) {
        DDLogDebug("\(String(describing:self)) \(message)")
    }
    
    public func isIphone() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .phone ? true : false
    }
    
    
    func presentAnimation(vc:UIViewController) {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        transition.subtype = kCATransitionFromBottom
        self.navigationController!.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(vc, animated: false)
    }
}


