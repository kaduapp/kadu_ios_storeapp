//
//  DataConversion.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 22/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

protocol FloatValue {
    var floatValue:Float{get}
}

extension String:FloatValue {
    var floatValue: Float {
        let result =  self.replacingOccurrences( of:"[^0-9.]", with: "", options: .regularExpression)
        if let value = Float(result) {
            return value
        }
        return 0
    }
}
