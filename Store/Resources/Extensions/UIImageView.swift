//
//  UIImageView.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 31/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

@IBDesignable
class CustomImageView: UIImageView {
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setImage(url:String) {
        let indicator = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        indicator.frame = self.bounds
        indicator.color = ColorConstants.Color_ImageIndicator
        indicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.addSubview(indicator)
        indicator.startAnimating()
        let imageUrl = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        //        let trimmedString = url.trimmingCharacters(in: .whitespaces)
        
        self.kf.setImage(with: URL(string: imageUrl),
                         placeholder: UIImage.init(named: "placehoilder_user"),
                         options: [.transition(ImageTransition.fade(1))],
                         progressBlock: { receivedSize, totalSize in
        },
                         completionHandler: { image, error, cacheType, imageURL in
                          //  self?.image = image
                            indicator.stopAnimating()
                            indicator.removeFromSuperview()
        })
    }
    
    @IBInspectable public var cornerRadius:CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
            self.layer.masksToBounds = true
        }
    }
    
    @IBInspectable public var borderWidth:CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable public var borderColor:UIColor = .clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
}

extension UIImageView {
    
    func setCornerRadius(radius:CGFloat) {
        layer.cornerRadius = radius
        layer.masksToBounds = true
    }
}



