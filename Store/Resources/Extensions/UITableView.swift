//
//  UITableView.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 09/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit



//MARK: - Observers
extension UITableView {
    
    func addObserverForNotification(notificationName: String, actionBlock: @escaping (Notification) -> Void) {
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: notificationName), object: nil, queue: OperationQueue.main, using: actionBlock)
    }
    
    func removeObserver() {
       NotificationCenter.default.removeObserver(self)
    }
}

//MARK: - Keyboard observers
extension UITableView {
    
    typealias KeyboardHeightClosure = (CGFloat) -> ()
    
    func addKeyboardChangeFrameObserver(willShow willShowClosure: KeyboardHeightClosure?,
                                        willHide willHideClosure: KeyboardHeightClosure?) {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillChangeFrame,
                                               object: nil, queue: OperationQueue.main, using: { [weak self](notification) in
                                                                    if let userInfo = notification.userInfo,
                                                                        let frame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
                                                                        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double,
                                                                        let c = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? UInt,
                                                                        let kFrame = self?.convert(frame, from: nil),
                                                                        let kBounds = self?.bounds {
                                                                        
                                                                        let animationType = UIViewAnimationOptions(rawValue: c)
                                                                        let kHeight = kFrame.size.height
                                                                        UIView.animate(withDuration: duration, delay: 0, options: animationType, animations: {
                                                                            if kBounds.intersects(kFrame) { // keyboard will be shown
                                                                                willShowClosure?(kHeight)
                                                                            } else { // keyboard will be hidden
                                                                                willHideClosure?(kHeight)
                                                                            }
                                                                        }, completion: nil)
                                                                    } else {
                                                                        print("Invalid conditions for UIKeyboardWillChangeFrameNotification")
                                                                    }
        })
    }
}


//extension UITableView {
//
//    func registerKeyboardNotification() {
//
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
//    }
//
//    func removeKeyboardNotification() {
//        NotificationCenter.default.removeObserver(self)
//    }
//
//    // MARK: Keyboard Notifications
//
//    @objc func keyboardWillShow(notification: NSNotification) {
//        if let keyboardHeight = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
////            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardHeight, 0.0);
////            self.contentInset = contentInsets
//            self.setContentOffset(CGPoint.init(x: 0, y: keyboardHeight), animated: true)
//            
//        }
//    }
//
//    @objc func keyboardWillHide(notification: NSNotification) {
//        UIView.animate(withDuration: 0.2, animations: {
//            self.contentOffset = .zero
////            let contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
////            self.contentInset = contentInsets
////            self.scrollIndicatorInsets = contentInsets
//        })
//    }
//}

