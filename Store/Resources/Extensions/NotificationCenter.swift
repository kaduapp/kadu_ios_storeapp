//
//  NotificationCenter.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 20/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

extension Notification.Name {
      public static let kNewOrderFCMNotification: NSNotification.Name = NSNotification.Name(rawValue: "kNewOrderFCMNotification")
}
