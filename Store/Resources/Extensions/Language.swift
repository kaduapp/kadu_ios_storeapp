//
//  Language.swift
//  Ufy_Store
//
//  Created by Raghavendra Shedole on 02/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit


extension UILabel {
    open var OSText:String {
        get {
            return self.text ?? ""
        }
        set {
            self.text = NSLocalizedString(newValue, comment: newValue)
//            if Utility.forcedLangugeChange() {
//                if self.textAlignment == .left {
//                    self.textAlignment = .right
//                }
//            }
        }
    }
}
