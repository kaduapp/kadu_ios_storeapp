//
//  UIButton.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 13/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit


class RobotoBoldButton:UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel?.font = UIFont(name:Roboto.Bold, size:(self.titleLabel?.font.pointSize)!)
    }
}

class RobotoMediumButton:UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel?.font = UIFont(name:Roboto.Medium, size:(self.titleLabel?.font.pointSize)!)
    }

}

class RobotoRegularButton:UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel?.font = UIFont(name:Roboto.Regular, size:(self.titleLabel?.font.pointSize)!)
    }
}



class RobotoCondensedBoldButton:UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel?.font = UIFont(name:RobotoCondensed.Bold, size:(self.titleLabel?.font.pointSize)!)
    }
}

class RobotoCondensedRegularButton:UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel?.font = UIFont(name:RobotoCondensed.Regular, size:(self.titleLabel?.font.pointSize)!)
    }
    
}
