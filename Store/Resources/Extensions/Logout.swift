//
//  Logout.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 14/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit


class Logout {
    
    class func logout() {
        
        StoreUtility.removeUserData()
        StoreUtility.unsubscribeFCMTopic()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nav = storyboard.instantiateViewController(withIdentifier: "SplashNavController") as! UINavigationController
        
        MQTT.sharedInstance.disconnectMQTTConnection()
        APIUtility.setLogin(loginStatus: false)
        
     appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
        (appDelegate.window?.rootViewController as? UINavigationController)?.popToRootViewController(animated: true)
        appDelegate.window?.rootViewController = nav
        appDelegate.window?.makeKeyAndVisible()
    }
}
