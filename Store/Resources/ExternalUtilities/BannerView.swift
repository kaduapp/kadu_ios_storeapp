//
//  BannerView.swift
//  Ufy_Store
//
//  Created by Raghavendra Shedole on 02/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

private let NOTIFICATION_VIEW_HEIGHT: Int = 66

class Banner: UIView {
    
    var title = ""
    var message = ""
    var referenceView: UIView!
    var screenBounds = CGRect.zero
    var animator: UIDynamicAnimator!
    var gravity: UIGravityBehavior!
    var collision: UICollisionBehavior!
    var itemBehavior: UIDynamicItemBehavior!
    var collisionCount: Int = 0
    
    
//    init() {
//        super.init()
//    }
//    override init(frame:CGRect) {
//        super.init(frame:frame)
//    }
    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        fatalError("init(coder:) has not been implemented")
//    }
    
    /**
     call this method to show the banner
     
     @param title Title of the banner
     @param message Body of the banner
     @param referenceView view controller where you want to show the banner
     */
  open func initWithTitle(_ title: String, message: String, referenceView: UIView) {
        self.title = title
        self.message = message
        self.referenceView = referenceView
        screenBounds = UIScreen.main.bounds
        setup()
    }
    
   open func applyDynamics() {
        animator = UIDynamicAnimator(referenceView: referenceView!)
        gravity = UIGravityBehavior(items: [self])
        collision = UICollisionBehavior(items: [self])
        itemBehavior = UIDynamicItemBehavior(items: [self])
        itemBehavior?.elasticity = 0.5
        collision.addBoundary(withIdentifier: "Banner" as NSCopying, from: CGPoint(x: 0, y: bounds.size.height), to: CGPoint(x: referenceView.bounds.size.width, y: referenceView.bounds.size.height))
        animator.addBehavior(gravity)
//        animator.addBehavior(collision)
        animator.addBehavior(itemBehavior)
        perform(#selector(self.hideNotification), with: nil, afterDelay: 3.0)
    }

    @objc func hideNotification() {
        // remove the original gravity behavior
        animator.removeBehavior(gravity)
//        animator.remove(gravity)
        gravity = UIGravityBehavior(items: [self])
        gravity.gravityDirection = CGVector.init(dx: 0, dy: -1)
//        gravity.gravityDirection = CGVectorMake(0, -1)
        animator.addBehavior(gravity)
    }
    
    func setNotificationBackground() {
        backgroundColor = UIColor(red: 11 / 255, green: 11 / 255, blue: 11 / 255, alpha: 1.0)
    }
    
    func setup() {
        UIApplication.shared.delegate?.window??.windowLevel = UIWindowLevelAlert
        //    CGRect screenBounds = [[UIScreen mainScreen] bounds];
        frame = CGRect(x: 0, y: -1 * NOTIFICATION_VIEW_HEIGHT, width: Int(screenBounds.size.width), height: NOTIFICATION_VIEW_HEIGHT)
        setNotificationBackground()
        addBannerTitle()
        addBannerMessage()
    }
    
    //  Converted to Swift 4 by Swiftify v4.1.6600 - https://objectivec2swift.com/
    func addBannerTitle() {
        //     CGRect screenBounds = [[UIScreen mainScreen] bounds];
        // create the labels
        let titleLabel = UILabel(frame: CGRect(x: 64, y: 15, width: screenBounds.size.width - 60, height: 15))
        titleLabel.text = title
        titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 14)!
        titleLabel.textColor = UIColor.white
        addSubview(titleLabel)
    }
    
    func addBannerMessage() {
        //    CGRect screenBounds = [[UIScreen mainScreen] bounds];
        // create the labels
        let messageLabel = UILabel(frame: CGRect(x: 64, y: 25, width: screenBounds.size.width - 70, height: 40))
        messageLabel.text = message
        messageLabel.numberOfLines = 2
        messageLabel.lineBreakMode = .byWordWrapping
        messageLabel.font = UIFont(name: "HelveticaNeue", size: 14)!
        messageLabel.textColor = UIColor.white
        addSubview(messageLabel)
    }


  
  
}
