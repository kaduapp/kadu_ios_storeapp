//
//  APIResponseParams.swift
//  DelivX Store
//
//  Created by 3 Embed on 13/12/17.
//  Copyright © 2017 3 Embed. All rights reserved.
//

import UIKit

class APIResponseParams: NSObject {
    
    //Basic
    static let Message                                  = "message"
    static let Data                                     = "data"
    static let LogoutData                               = "message"
    
    //login
    static let ReferralCode                             = "referralCode"
    static let SessionToken                             = "token"
    static let SId                                      = "sid"
    static let Phone                                    = "mobile"
    static let Email                                    = "email"
    static let StoreName                                = "storeName"
    static let CountryCode                              = "countryCode"
    static let FCMTopic                                 = "fcmTopic"
    static let MQTTTopic                                = "mqttTopic"


}
