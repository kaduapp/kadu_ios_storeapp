//
//  Helper.swift
//  DelivX Store
//
//  Created by 3 Embed on 13/12/17.
//  Copyright © 2017 3 Embed. All rights reserved.
//

import UIKit
import AVFoundation
import LatLongToTimezone
import CoreLocation
import Kingfisher


class Helper {
    
    //Handling Progress Indicator
    class func showPI(string:String) {
        let progress: LoadingProgress = LoadingProgress.shared
        progress.showPI(message: string)
    }
    class func hidePI(){
        LoadingProgress.shared.hide()
    }
    
    //Handling Alert Messages
    class func showAlert(message:String, head:String, type:Int) {
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        if (appDelegate.window?.viewWithTag(98765462342)) != nil {
            return
        }
        
        let alert:CommonAlertView = CommonAlertView.shared
        alert.showAlert(message: message, head: head, type: type)
    }
    
    class func hideAlert() {
        CommonAlertView.shared.hideAlert()
    }
    
    class func setShadow(sender:UIView) {
        //Add Shadow
        sender.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(0))
        sender.layer.shadowColor = UIColor.lightGray.cgColor
        sender.layer.shadowOpacity = 1.0
    }
    
    
    //************ Add shadow to the view*****************//
    class func shadowView(sender: UIView, width:CGFloat, height:CGFloat , hideShadow:Bool) {
        
        sender.layer.cornerRadius = 4
        // Drop Shadow
        let squarePath = UIBezierPath(rect:CGRect.init(x: 0, y:0 , width: width, height: height)).cgPath
        
        sender.layer.shadowRadius = 4
        sender.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(2.0))
        sender.layer.shadowColor = Helper.getUIColor(color:Colors.AppBaseColor).cgColor//UIColor.lightGray.cgColor
        if hideShadow{
            sender.layer.shadowOpacity = 0.0
        }else{
            sender.layer.shadowOpacity = 0.1
        }
        
        sender.layer.masksToBounds = false
        sender.layer.shadowPath = squarePath;
    }
    
    class func addShadow(sender: UIView, width:CGFloat, height:CGFloat ) {
        
        // Drop Shadow
        let squarePath = UIBezierPath(rect:CGRect.init(x: 0, y:0 , width: width, height: height)).cgPath
        
        sender.layer.shadowOffset = CGSize(width: CGFloat(-10), height: CGFloat(0.5))
        sender.layer.shadowColor = Helper.getUIColor(color:Colors.AppBaseColor).cgColor
        sender.layer.shadowOpacity = 0.1
        sender.layer.masksToBounds = false
        sender.layer.shadowPath = squarePath;
    }
    
    ///Set ui element Border and corner
    class func setUiElementBorderWithCorner(element:UIView, radius:Float, borderWidth:Float , color:UIColor) {
        element.layer.cornerRadius = CGFloat(radius)
        element.layer.borderWidth = CGFloat(borderWidth)
        element.layer.borderColor = color.cgColor
        element.clipsToBounds = true
    }
    //Get UIColor from HexColorCode
    class func getUIColor(color:String) -> UIColor {
        var cString:String = color.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        if ((cString.sorted().count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    //Set Button
    class func setButton(button:UIButton,view:UIView,primaryColour:UIColor,seconderyColor:UIColor,shadow:Bool) {
        //Set Background
        button.setBackgroundImage(Helper.GetImageFrom(color: primaryColour), for: .normal)
        button.setBackgroundImage(Helper.GetImageFrom(color: seconderyColor), for: .highlighted)
        button.setBackgroundImage(Helper.GetImageFrom(color: seconderyColor), for: .selected)
        
        //Set Border and Corner
        button.layer.cornerRadius = CGFloat(UIConstants.ButtonCornerRadius)
        button.layer.borderWidth = CGFloat(UIConstants.ButtonBorderWidth)
        button.layer.borderColor = primaryColour.cgColor
        button.clipsToBounds = true
        
        
        if view.isKind(of: UIView.classForCoder()) {
            if shadow {
                self.setShadow(sender: view)
            }
            view.backgroundColor = UIColor .clear
        }
        
        //Set Title
        var pC = primaryColour
        var sC = seconderyColor
        if seconderyColor == UIColor.clear {
            sC = UIColor.white
        }
        if primaryColour == UIColor.clear {
            pC = UIColor.white
        }
        button.setTitleColor(sC, for: .normal)
        button.setTitleColor(pC, for: .highlighted)
        button.setTitleColor(pC, for: .selected)
    }
    //Set Button Title
    class func setButtonTitle(normal:String,highlighted:String,selected:String,button:UIButton) {
        button.setTitle(normal, for: .normal)
        button.setTitle(highlighted, for: .highlighted)
        button.setTitle(selected, for: .selected)
    }
    
    //Get Image From Color
    static func GetImageFrom(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
    
    //Multiple ColourText
    class func updateText(text: String,subText:String,_ sender: Any,color:UIColor) {
        
        let range = (text as NSString).range(of: subText)
        let attributedString = NSMutableAttributedString(string:text)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value:color , range: range)
        if let element = sender as? UIButton {
            element.setAttributedTitle(attributedString, for: .normal)
            let range2 = (text as NSString).range(of: text)
            let attributedString2 = NSMutableAttributedString(string:text)
            attributedString2.addAttribute(NSAttributedStringKey.foregroundColor, value:color , range: range2)
            element.setAttributedTitle(attributedString2, for: .highlighted)
            element.setAttributedTitle(attributedString2, for: .selected)
        }else if let element = sender as? UILabel {
            element.attributedText = attributedString
        }
        else if let element = sender as? UITextView {
            element.attributedText = attributedString
        }
    }
    
    class func removeNavigationSeparator(controller:UINavigationController) {
        controller.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        controller.navigationBar.shadowImage = UIImage()
    }
    
    //Transparent navigation
    class func transparentNavigation(controller:UINavigationController) {
        controller.navigationBar.backgroundColor = UIColor.clear
        controller.navigationBar.isTranslucent = true
        Helper.removeNavigationSeparator(controller: controller)
    }
    
    //NonTransparent navigation
    class func nonTransparentNavigation(controller:UINavigationController) {
        controller.navigationBar.backgroundColor = Helper.getUIColor(color: Colors.White)
        controller.navigationBar.isTranslucent = false
        controller.navigationBar.tintColor = Helper.getUIColor(color: Colors.PrimaryText)
        controller.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: Helper.getUIColor(color: Colors.PrimaryText)]
    }
   
    ///Adding Done Button On Keyboard
    class func addDoneButtonOnTextField(tf:UITextField, vc: UIView){
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem.init(title: StringConstants.Done(), style: .plain, target: vc, action: #selector(UIView.endEditing(_:)))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        tf.inputAccessoryView = keyboardToolbar
    }
    
  
    
    //To get the fonts
    class func getFonts(){
        for family: String in UIFont.familyNames
        {
            print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family)
            {
                print("== \(names)")
            }
        }
    }
    
    class func showHeader(title:String , cost: String, shadow:Bool)-> UIView{
//        let header:CellHeaderView = CellHeaderView().shared
//        header.headerTitle.text = title
//        header.itemCostTextfield.text = cost
//        Fonts.setPrimaryBlack(header.headerTitle)
//        header.headerTitle.textColor = Helper.getUIColor(color: Colors.PrimaryText)
//        Fonts.setPrimaryBlack(header.itemCostTextfield.text)
//        header.itemCostTextfield.textColor = Helper.getUIColor(color: Colors.PrimaryText)
//        Fonts.setPrimaryBlack(header.currencyLabel)
//        header.currencyLabel.textColor = Helper.getUIColor(color: Colors.PrimaryText)
//        header.commentLabel.isHidden = true
//        header.noteLabel.isHidden = true
        
//        if header.headerTitle.text?.sorted().count == 0{
//            header.topConstraint.constant = 0
//            header.bottomConstraint.constant = 0
//            header.isHidden = true
//        }
//        else {
//            header.topConstraint.constant = 10
//            header.bottomConstraint.constant = 10
//            header.isHidden = false
//        }
//        if shadow {
//            Helper.setShadow(sender: header.headerView)
//        }
//        return header
        return UIView()
    }
    
//    class func showFooter(title:String , shadow:Bool)-> UIView{
//        let footer:CellHeaderView = CellHeaderView().shared
//        footer.commentLabel.text = title
//        //Fonts.setPrimaryBlack(footer.commentLabel)
//        footer.commentLabel.textColor = Helper.getUIColor(color: Colors.PrimaryText)
//        footer.headerTitle.isHidden = true
//        footer.itemCostTextfield.isHidden = true
//        footer.currencyLabel.isHidden = true
//        if shadow {
//            Helper.setShadow(sender: footer.headerView)
//        }
//        
//        return footer
//    }
    
    
    //************ To get the IP address *****************//
    class func getIPAddresses() -> [String] {
        var addresses = [String]()
        
        // Get list of all interfaces on the local machine:
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return [] }
        guard let firstAddr = ifaddr else { return [] }
        
        // For each interface ...
        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let flags = Int32(ptr.pointee.ifa_flags)
            let addr = ptr.pointee.ifa_addr.pointee
            
            // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
            if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                        let address = String(cString: hostname)
                        addresses.append(address)
                    }
                }
            }
        }
        
        freeifaddrs(ifaddr)
        return addresses
    }

    class func nullKeyRemoval(data:[String:Any]) -> [String:Any] {
        var dict = data
        let keysToRemove = Array(dict.keys).filter { dict[$0] is NSNull }
        for key in keysToRemove {
            dict.removeValue(forKey: key)
        }
        let keysToUpdate = Array(dict.keys).filter { dict[$0] is [String:Any] }
        for key in keysToUpdate {
            dict.updateValue(nullKeyRemoval(data: dict[key] as! [String : Any]), forKey: key)
        }
        return dict
    }
    

    class func convertGMTDateToLocalFromDate(gmtDate:Date) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a"
        dateFormatter.timeZone = Helper.getTimeZoneFromPickUpLoaction()
        let newDateStr = dateFormatter.string(from: gmtDate)
        return dateFormatter.date(from: newDateStr)!
    }
    
    class func getTimeZoneFromPickUpLoaction() -> TimeZone? {
        let ud = UserDefaults.standard
        var lat = 24.774265
        var log = 46.738586
        if ud.object(forKey: UserDefaultConstants.CurrentLatitude) != nil{
             lat  = ud.object(forKey: UserDefaultConstants.CurrentLatitude) as! CLLocationDegrees
             log  = ud.object(forKey: UserDefaultConstants.CurrentLongitude)  as! CLLocationDegrees
        }
        let location =   CLLocationCoordinate2D.init(latitude: lat, longitude: log)
        return TimezoneMapper.latLngToTimezone(location)
    }
    
    
    /// Remove null from Objects
    ///
    /// - Returns: Object without Null
    class func removeNullKeys(data:[String:Any]) -> [String:Any] {
        var dict = data
        let keysToRemove = Array(dict.keys).filter { dict[$0] is NSNull }
        for key in keysToRemove {
            dict.removeValue(forKey: key)
        }
        var keysToUpdate = Array(dict.keys).filter { dict[$0] is [String:Any] }
        for key in keysToUpdate {
            dict.updateValue(nullKeyRemoval(data: dict[key] as! [String : Any]), forKey: key)
        }
        keysToUpdate = Array(dict.keys).filter { dict[$0] is [Any] }
        for key in keysToUpdate {
            let dataArray = dict[key] as! [Any]
            var temp:[Any] = []
            for item in dataArray {
                if item is NSNull {
                    
                }else{
                    if ((item as? [String:Any]) != nil){
                        temp.append(nullKeyRemoval(data: item as! [String:Any]))
                    }else{
                        temp.append(item)
                    }
                }
            }
            dict.updateValue(temp, forKey: key)
        }
        return dict
    }
    
    class  func getDayOfWeek(_ date:Date?) -> String? {
        
        let formatter  = DateFormatter()
        formatter.dateFormat = DateFormat.DateFormatServer
        
        guard let val = date else { return nil }
//        let myCalender = Calendar.init(identifier: .gregorian)
//
        if  Calendar.current.isDateInToday(val){
            return "Today"
        }
        
        if  Calendar.current.isDateInYesterday(val){
            return "Yesterday"
        }
        
       return Date.getDateString(value: val, format:"dd MMM, yyyy" )
        
    }
    
  class  func dialNumber(number : String ) {
    
        if let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    //Set Image From Url
    class func setImage(imageView:UIImageView,url:String,defaultImage:UIImage) {
        imageView.image = UIImage()
        let indicator = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        indicator.center = CGPoint(x: imageView.bounds.size.width/2, y: imageView.bounds.size.height/2)
        imageView.addSubview(indicator)
        indicator.startAnimating()
        let trimmedString = url.trimmingCharacters(in: .whitespaces)
        imageView.kf.setImage(with: URL(string: trimmedString),
                              placeholder: defaultImage,
                              options: [.transition(ImageTransition.fade(1))],
                              progressBlock: { receivedSize, totalSize in
        },
                              completionHandler: { image, error, cacheType, imageURL in
                                indicator.stopAnimating()
                                indicator.removeFromSuperview()
        })
    }
}
