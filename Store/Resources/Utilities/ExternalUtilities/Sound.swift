//
//  Sound.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 01/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import AVFoundation

class Sound {
    static let sharedInstance = Sound()
    private var orderSound: AVAudioPlayer?
    private let path = Bundle.main.path(forResource: "OrderSound.wav", ofType:nil)!
    private var soundCount = 0
    var inProgress = false
    
    func ringSound() {
        let url = URL(fileURLWithPath: path)
        
        if soundCount >= 5 {
            stopRing()
            soundCount = 0
            return
        }
        do {
            inProgress = true
            orderSound = try AVAudioPlayer(contentsOf: url)
            orderSound?.play()
            let when = DispatchTime.now() + 0.7 // change 20 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                // Your code with delay
                self.soundCount += 1
                self.ringSound()
            }
        } catch {
            inProgress = false
            soundCount = 5
            // couldn't load file :(
        }
    }
    
    func stopRing() {
        soundCount = 0
        inProgress = false
        self.orderSound?.stop()
    }
}
