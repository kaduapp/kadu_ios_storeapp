//
//  UIConstants.swift
//  DelivX Store
//
//  Created by 3 Embed on 13/12/17.
//  Copyright © 2017 3 Embed. All rights reserved.
//

import UIKit

class UIConstants: NSObject {

    static let ButtonBorderWidth                           = 1
    static let ButtonCornerRadius                          = 5
    static let time                                        = 5
    
    struct SegueIds {
        static let LaunchToLogin         = "launchToLogin"
        static let LaunchToHomeVC        = "goToHomeView"
        static let LoginToHomeVC         = "loginToHomeVC"
        static let HomeToHistoryVC       = "HomeToHistoryVC"
    }

    
    struct CellIds {
        
        static let OrderCollectionCell   = "OrderCollectionCell"
        static let MenuTableCell         = "MenuTableCell"
    }
    
    struct StoryboardIds {
        static let PopupViewController = "PopupViewController"
        static let MenuVC              = "MenuVC"
    }
}
