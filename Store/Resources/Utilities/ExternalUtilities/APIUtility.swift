//
//  APIClass.swift
//  DelivX Store
//
//  Created by Rahulsharma on 09/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire
import CocoaLumberjack
import Alamofire


// MARK: - API Names
class APIEndTails {
    
    static let VersionAppStore                          =  AppConfig.appUrl
    static let Order_BaseUrl                            =  AppConfig.baseUrl
    static let getProducts                              = Order_BaseUrl+"store/product/"
    static let patchProduct                             = Order_BaseUrl+"child/product/"
    static let getLanguage                              = AppConfig.languageUrl
    static let OrderDetails                             = Order_BaseUrl + "order/"
   
    static let DispatchOrder                            = Order_BaseUrl + "dispatchOrder"
    static let UserHistory                              = Order_BaseUrl + "userHistory/"
    static let OrderHistoryService                      = Order_BaseUrl + "ordersHistory/"
    static let CancellationReasonAPI                    = Order_BaseUrl + "cancellationReasons"
    static let LoginAuthAPI                             = Order_BaseUrl + "dispatcher/"
    
    static let SavelaundryImages                        = Order_BaseUrl + "laundry/saveimage"
    static let switchOnlineStatus                       = Order_BaseUrl + "store/status"
    
    static let storeWalletTranX                         = Order_BaseUrl + "store/walletTransaction/"
    static let GetStoreDrivers                          = LoginAuthAPI + "drivers/"
    static let DispatchOrderToDriver                    = LoginAuthAPI + "dispatchOrder"
    static let OrderHistory                             = LoginAuthAPI + "orders/"
    static let ForgotPassword                           = LoginAuthAPI + "forgotPassword"
    static let OrderHistorySearch                       = LoginAuthAPI + "orders/"
    static let EditOrderAPI                             = LoginAuthAPI + "order"
    static let cancelOrder                              = LoginAuthAPI + "order"
    static let getCustomerData                          = LoginAuthAPI + "search/"
    static let getCustDataWithID                        = LoginAuthAPI + "customer/"
    static let getProductDetails                        = LoginAuthAPI + "products/"
    
    static let getCategoriesAndSub                      = LoginAuthAPI + "categories/"
    
    static let getsubSubCat                             = LoginAuthAPI + "subSubCategories/"
    
    static let addCart                                  = LoginAuthAPI + "cartNew"
    
    static let cartDetails                              = LoginAuthAPI + "cart/"
    
    static let deleteCart                               = LoginAuthAPI + "cart"
    
    static let businessFare                             = LoginAuthAPI + "fare"
    
    static let createNewOrder                           = LoginAuthAPI + "orderNew"
    
    static let Franchise                                = Order_BaseUrl + "franchise/"
    static let OrderList                                = Order_BaseUrl + "orders/"
    static let Login                                    = Franchise + "logIn"
    static let ProfileData                              = Franchise + "profile"
    static let LogoutService                            = Franchise + "logout"
    static let ChangeOrderStatus                        = Franchise + "order/status"
    static let OnGoingOrders                            = Franchise + "ongoingOrders/"
    static let CustomerPastOrders                       = Franchise + "orders/"
    
    static let getAllStores                             = Franchise + "store/"
    
    static let getDrivers                               = Franchise + "driver/0/0/"

    static let accountConnect                           = Order_BaseUrl +  "admin/connectAccount"

    static let addExternalAccount                       = Order_BaseUrl + "admin/externalAccount"
    
}


// MARK: - API Params
class APIRequestParams {
    
    //    API Header
    static let Language                                 = "language"
    static let Authorization                            = "authorization"
    static let LanguageDefault                          = "en"
    
    //  Login Request API
    
    static let Password                                 = "password"
    static let DeviceID                                 = "deviceId"
    static let DeviceMake                               = "deviceMake"
    static let DeviceModal                              = "deviceModel"
    static let DeviceType                               = "deviceType"
    static let DeviceTime                               = "deviceTime"
    static let Latitude                                 = "latitude"
    static let Longitude                                = "longitude"
    static let PushToken                                = "pushToken"
    static let AppVersion                               = "appVersion"
    static let VerifyType                               = "verifyType"
    
    static let driverId                                 = "driverId"
    static let Email                                    = "email"
    static let firstName                                = "first_name"
    static let lastName                                 = "last_name"
    static let document                                 = "document"
    static let personal_id_number                       = "personal_id_number"
    static let city                                     = "city"
    static let date                                     = "date"
    static let country                                  = "country"
    static let line1                                    = "line1"
    static let postal_code                              = "postal_code"
    static let state                                    = "state"
    static let day                                      = "day"
    static let month                                    = "month"
    static let year                                     = "year"
    static let ip                                       = "ip"
    
    
    static let account_number                           = "account_number"
    static let routing_number                           = "routing_number"
    static let account_holder_name                      = "account_holder_name"
    static let currency                                 = "currency"
    static let userId                                   = "userId"
    
    
    // Change Order Status
    static let ProductId                                = "productId"
    static let StoreId                                  = "storeId"
    static let ManagerID                                = "managerId"
    static let Status                                   = "status"
    static let TimeStamp                                = "timestamp"
    static let OrderID                                  = "orderId"
    static let cancelReason                             = "reason"
    static let DueTime                                  = "dueDatetime"
    static let serviceType                              = "serviceType"
    static let Items                                    = "items"
    static let DeliveryCharge                           = "deliveryCharge"
    static let ipAddress                                = "ipAddress"
    
    //DispatchOrderToDriver
    
    static let Images                                   = "images"
}



// MARK: - API Status code
class APIStatusCode {
    enum ErrorCode: Int {
        case success                = 200
        case created                = 201
        case deleted                = 202 //Accepted forgotPW
        case updated                = 203
        case NoUser                 = 204
        case badRequest             = 400
        case Unauthorized           = 401
        case Required               = 402
        case Forbidden              = 403
        case NotFound               = 404
        case MethodNotAllowed       = 405
        case NotAcceptable          = 406
        case ExpiredOtp             = 410
        case PreconditionFailed     = 412
        case RequestEntityTooLarge  = 413
        case TooManyAttemt          = 429
        case ExpiredToken           = 440
        case InvalidToken           = 498
        case InternalServerError    = 500
        case InternalServerError2   = 502
        case storeBanned   = 415
        
    }
    
    
    // MARK:-
   
    
    class func basicParsing(data:[String:Any],status: Int) {
        Helper.hidePI()
        let errNum = ErrorCode(rawValue: status)!
        switch errNum {
        case .success:
            Helper.showAlert(message: data[APIResponseParams.Message] as! String, head: StringConstants.Success(), type: 0)
            break
        case .ExpiredToken,.InvalidToken,.NoUser:
            Logout.logout()
            break
            
        case .NotFound:
            Helper.showAlert(message: data[APIResponseParams.Message] as! String, head: StringConstants.Error(), type: 0)
            break
            
        case .deleted:
            Helper.showAlert(message: data[APIResponseParams.Message] as! String, head: StringConstants.Error(), type: 0)
            break
            
        default:
            var message = ""
            if let msg = data[APIResponseParams.Message] as? String {
                message = msg
            }
            Helper.showAlert(message: message, head: StringConstants.Error(), type: 1)
            break
        }
    }
}


//MARK:- Api related data like header etc
class APIUtility {
    
    class func getHeaderForOrders() -> HTTPHeaders {
        
        let header:HTTPHeaders = [APIRequestParams.Language : APIRequestParams.LanguageDefault,
                      APIRequestParams.Authorization : StoreUtility.getStore().authToken ]
        return header
    }
    
    class func getLogin() -> Bool {
        if UserDefaults.standard.bool(forKey: "_storeLoginStatus") {
            return true
        }else {
            return false
        }
    }
    
    class func getIsLoginFirstTime() -> Bool{
        
        if UserDefaults.standard.bool(forKey: "isLoginFirstTime") == true {
            return true
        }else {
            return false
        }
    }
    
    class func setIsLoginFirstTime(loginStatus : Bool ){
            UserDefaults.standard.set(loginStatus, forKey: "isLoginFirstTime")
    }
    
    class func setLogin(loginStatus:Bool) {
        UserDefaults.standard.set(loginStatus, forKey: "_storeLoginStatus")
    }
    
    class func removeLogin() {
        UserDefaults.standard.removeObject(forKey: "_storeLoginStatus")
    }
    class func getHeader() -> HTTPHeaders {
        let header:HTTPHeaders = [APIRequestParams.Language : APIRequestParams.LanguageDefault]
        return header
    }
}

//MARK:- API Calls(.get,.patch,.post)
class APIHandler {
    static let disposeBag = DisposeBag()
    
    static var networkAvailability:Bool {
        return Connectivity.isConnectedToInternet()
    }
    
    class func getRequestData(header:HTTPHeaders, url:String) -> Observable<(Bool,Any)> {
        
        DDLogDebug("header : \(header)")
        DDLogDebug("url : \(url)")
        
        
        
        return Observable.create{ observer in
            
            if !networkAvailability {
                observer.onError(NSError(domain: "Network Not found", code: -1, userInfo: nil))
                observer.onCompleted()
                return Disposables.create()
            }
          //  Helper.showPI(string: StringConstants.ProgressIndicator)
            RxAlamofire.requestJSON(.get, url, parameters: nil, headers: header)
                .timeout(30, scheduler: MainScheduler.instance)
                .debug().subscribe(onNext: { (head, body) in
                    DDLogDebug("response : \(body)")
                    Helper.hidePI()
                    let errNum:APIStatusCode.ErrorCode = APIStatusCode.ErrorCode(rawValue: head.statusCode)!
                    if errNum == .success {
                        let bodyIn = body as! [String:Any]
                        //let data = bodyIn[APIResponseParams.Data] as! [String:Any]
                        observer.onNext((true,bodyIn))
                        observer.onCompleted()
                    }else if errNum == .Required {
                        observer.onNext((false,""))
                        observer.onCompleted()
                    }else {
                        APIStatusCode.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    }
                    
                }, onError: { error in
                    if let error = error as? RxError {
                        print(error.debugDescription)
                        Helper.showAlert(message: error.debugDescription, head: "Network Error", type: 0)
                    }
                    Helper.hidePI()
                }).disposed(by: disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    class func getDataFromAPI( header:HTTPHeaders , params : [String : Any], url:String , onCompletionHandler:@escaping(Bool , Any) -> Void ){
        
        AF.request(url, method:.get , headers: header)
            .responseJSON { response in
                switch response.result {
                case .success:
                    //      print(response.result.value)
                    onCompletionHandler(true , response.value)
                case .failure(let error):
                    print(error)
                   onCompletionHandler(false ,error.localizedDescription)
                }
        }
    }
    
    
    
    
    class func getRequest(header:HTTPHeaders, url:String) -> Observable<(Bool,Any)> {
        
        return Observable.create{ observer in
            
            if !networkAvailability {
                observer.onError(NSError(domain: "Network Not found", code: -1, userInfo: nil))
                observer.onCompleted()
                return Disposables.create()
            }
            
            RxAlamofire.requestJSON(.get, url, parameters: nil, headers: header)
                .timeout(30, scheduler: MainScheduler.instance)
                .debug().subscribe(onNext: { (head, body) in
                    DDLogDebug("response : \(body)")
                    let errNum:APIStatusCode.ErrorCode = APIStatusCode.ErrorCode(rawValue: head.statusCode)!
                       var data = [String:Any]()
                    if errNum == .success {
                        let bodyIn = body as! [String:Any]
                        
                        if let responseData = bodyIn[APIResponseParams.Data] as? [String:Any]{
                            data = responseData
                        }else{
                            data = bodyIn
                        }
        
                        observer.onNext((true,data))
                        observer.onCompleted()
                    }else if errNum == .Required {
                        observer.onNext((false,""))
                        observer.onCompleted()
                    }else if errNum == .NotFound{
                        observer.onNext((true,data))
                        observer.onCompleted()
                    }
                    else {
                        Helper.hidePI()
                        APIStatusCode.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    }
                    
                }, onError: { error in
                    if let error = error as? RxError {
                        print(error.debugDescription)
                        Helper.showAlert(message: error.debugDescription, head: "Network Error", type: 0)
                    }
                    Helper.hidePI()
                }).disposed(by: disposeBag)
            
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    class func deleteItem(header:HTTPHeaders, url:String)-> Observable<(Bool,Any)> {
        DDLogDebug("header : \(header)")
        return Observable.create{ observer in
            if !networkAvailability {
                observer.onError(NSError(domain: "Network Not found", code: -1, userInfo: nil))
                observer.onCompleted()
                return Disposables.create()
            }
                 Helper.showPI(string: StringConstants.ProgressIndicator())
            
            RxAlamofire.requestJSON(.delete, url, parameters: nil, headers: header)
                .timeout(30, scheduler: MainScheduler.instance)
                .debug().subscribe(onNext: { (head,body) in
                    DDLogDebug("response : \(body)")
                    Helper.hidePI()
                    let errNum:APIStatusCode.ErrorCode? = APIStatusCode.ErrorCode(rawValue: head.statusCode)!
                    
                    if errNum == .success || errNum == .deleted  || errNum == .created {
                        let bodyIn = body as! [String:Any]
                        var data:[String:Any]
                        
                        if let response = bodyIn[APIResponseParams.Data] as? [String:Any] {
                            data = response
                            observer.onNext((true,data))
                        }
                        
                        if let logoutResponse = bodyIn[APIResponseParams.LogoutData] as? String {
                            if logoutResponse.contains("Logged Out") {
                                observer.onNext((true, " Logged Out"))
                            }
                        }
                        
                        if errNum == .deleted{
                            observer.onNext((true,""))
                        }
                        
                        observer.onCompleted()
                    }else if errNum == .Required {
                        observer.onNext((false,""))
                        observer.onCompleted()
                    }else {
                        APIStatusCode.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    }
                    
                }, onError: { error in
                    Helper.hidePI()
                    APIHandler.errorHandler(error: error)
                    
                }).disposed(by: disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    class func postCartRequest(header:HTTPHeaders, url:String,params:[String:Any])-> Observable<(Bool,Any)> {
        DDLogDebug("header : \(header)")
        DDLogDebug("params : \(params)")
        return Observable.create{ observer in
            if !networkAvailability {
                observer.onError(NSError(domain: "Network Not found", code: -1, userInfo: nil))
                observer.onCompleted()
                return Disposables.create()
            }
            
            RxAlamofire.requestJSON(.post, url, parameters: params, headers: header)
                .timeout(30, scheduler: MainScheduler.instance)
                .debug().subscribe(onNext: { (head,body) in
                    DDLogDebug("response : \(body)")
                    Helper.hidePI()
                    let errNum:APIStatusCode.ErrorCode? = APIStatusCode.ErrorCode(rawValue: head.statusCode)!
                    
                    if errNum == .success || errNum == .deleted  || errNum == .created {
                        let bodyIn = body as! [String:Any]
                        var data:[String:Any]
                        
                        if let response = bodyIn[APIResponseParams.Data] as? [String:Any] {
                            data = response
                            observer.onNext((true,data))
                        }else{
                            if (bodyIn[APIResponseParams.Data] as? [[String:Any]]) != nil {
                                observer.onNext((true,bodyIn))
                            }else{
                               observer.onNext((true,bodyIn))
                            }
                        }
                        
                        
                        
                        if let logoutResponse = bodyIn[APIResponseParams.LogoutData] as? String {
                            if logoutResponse.contains("Logged Out") {
                                observer.onNext((true, " Logged Out"))
                            }
                        }
                        
                        observer.onCompleted()
                    }else if errNum == .Required {
                        observer.onNext((false,""))
                        observer.onCompleted()
                    }else {
                        APIStatusCode.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    }
                    
                }, onError: { error in
                    Helper.hidePI()
                    APIHandler.errorHandler(error: error)
  
                }).disposed(by: disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    class func postRequest(header:HTTPHeaders, url:String,params:[String:Any])-> Observable<(Bool,Any)> {
        DDLogDebug("header : \(header)")
        DDLogDebug("params : \(params)")
        return Observable.create{ observer in
            if !networkAvailability {
                observer.onError(NSError(domain: "Network Not found", code: -1, userInfo: nil))
                observer.onCompleted()
                return Disposables.create()
            }
            
            RxAlamofire.requestJSON(.post, url, parameters: params,encoding:JSONEncoding.default, headers: header)
                .timeout(30, scheduler: MainScheduler.instance)
                .debug().subscribe(onNext: { (head,body) in
                    DDLogDebug("response : \(body)")
                    Helper.hidePI()
                    let errNum:APIStatusCode.ErrorCode? = APIStatusCode.ErrorCode(rawValue: head.statusCode)!
                    if errNum == .success || errNum == .deleted  {
                        let bodyIn = body as! [String:Any]
                        var data:[String:Any]
                        
                        if let response = bodyIn[APIResponseParams.Data] as? [String:Any] {
                            data = response
                            observer.onNext((true,data))
                        }else{
                            observer.onNext((true, bodyIn))
                        }
                        
                        if let logoutResponse = bodyIn[APIResponseParams.LogoutData] as? String {
                            if logoutResponse.contains("Logged Out") {
                                observer.onNext((true, " Logged Out"))
                            }
                        }
                        
                        observer.onCompleted()
                    }else if errNum == .Required {
                        observer.onNext((false,""))
                        observer.onCompleted()
                    }else {
                        APIStatusCode.basicParsing(data:body as! [String : Any],status:head.statusCode)
                        observer.onNext((false,""))
                    }
                    
                }, onError: { error in
                    Helper.hidePI()
                   APIHandler.errorHandler(error: error)
                  observer.onNext((false,""))
//                    Helper.showAlert(message: "Server is unavaillable. Please try again!", head: "Network Error", type: 0)
                }).disposed(by: disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    class func putRequest(header:HTTPHeaders, url:String,params:[String:Any])-> Observable<(Bool,Any)> {
        DDLogDebug("header : \(header)")
        DDLogDebug("params : \(params)")
        return Observable.create{ observer in
            if !networkAvailability {
                observer.onError(NSError(domain: "Network Not found", code: -1, userInfo: nil))
                observer.onCompleted()
                return Disposables.create()
            }
            
            Helper.showPI(string: StringConstants.ProgressIndicator())
            RxAlamofire.requestJSON(.put, url, parameters: params, encoding: JSONEncoding.default, headers: header)
                .timeout(30, scheduler: MainScheduler.instance)
                .debug().subscribe(onNext: { (head,body) in
                    DDLogDebug("response : \(body)")
                    Helper.hidePI()
                    let errNum:APIStatusCode.ErrorCode? = APIStatusCode.ErrorCode(rawValue: head.statusCode)!
                    
                    if errNum == .success {
                        let bodyIn = body as! [String:Any]
                        
                        guard let data = bodyIn[APIResponseParams.Data] as? [String:Any] else{
                            observer.onNext((true,""))
                            return
                        }
                        observer.onNext((true,data))
                        observer.onCompleted()
                    }else if errNum == .Required {
                        observer.onNext((false,""))
                        observer.onCompleted()
                    }else {
                        APIStatusCode.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    }
                    
                }, onError: { error in
                    if let error = error as? RxError {
                        print(error.debugDescription)
                        Helper.showAlert(message: error.debugDescription, head: "Network Error", type: 0)
                    }
                    //                    Helper.showAlert(message: "Server is unavaillable. Please try again!", head: "Network Error", type: 0)
                    Helper.hidePI()
                }).disposed(by: disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
    
    
    
    class func patchRequest(header:HTTPHeaders, url:String,params:[String:Any])-> Observable<(Bool,Any)> {
        DDLogDebug("header : \(header)")
        DDLogDebug("params : \(params)")
        return Observable.create{ observer in
            if !networkAvailability {
                observer.onError(NSError(domain: "Network Not found", code: -1, userInfo: nil))
                observer.onCompleted()
                return Disposables.create()
            }
            
            Helper.showPI(string: StringConstants.ProgressIndicator())
            RxAlamofire.requestJSON(.patch, url, parameters: params, encoding: JSONEncoding.default, headers: header)
                .timeout(30, scheduler: MainScheduler.instance)
                .debug().subscribe(onNext: { (head,body) in
                    DDLogDebug("response : \(body)")
                    Helper.hidePI()
                    let errNum:APIStatusCode.ErrorCode? = APIStatusCode.ErrorCode(rawValue: head.statusCode)!
                    
                    if errNum == .success {
                        let bodyIn = body as! [String:Any]
                        
                        guard let data = bodyIn[APIResponseParams.Data] as? [String:Any] else{
                            observer.onNext((true,""))
                            return
                        }
                        observer.onNext((true,data))
                        observer.onCompleted()
                    }else if errNum == .Required {
                        observer.onNext((false,""))
                        observer.onCompleted()
                    }else if errNum == .updated {
                        observer.onNext((true,data))
                        observer.onCompleted()
                    }else{
                        APIStatusCode.basicParsing(data:body as! [String : Any],status:head.statusCode)
                    }
                    
                }, onError: { error in
                    if let error = error as? RxError {
                        print(error.debugDescription)
                        Helper.showAlert(message: error.debugDescription, head: "Network Error", type: 0)
                    }
//                    Helper.showAlert(message: "Server is unavaillable. Please try again!", head: "Network Error", type: 0)
                    Helper.hidePI()
                }).disposed(by: disposeBag)
            return Disposables.create()
            }.share(replay: 1)
    }
}

// MARK: - Erro handler
extension APIHandler {
    class func errorHandler(error:Any) {
        if let error = error as? RxError {
            print(error.debugDescription)
            Helper.showAlert(message: error.debugDescription, head: "Network Error", type: 0)
            return
        }
        if let error = error as? Error  {
            Helper.showAlert(message: error.localizedDescription, head: "Network Error", type: 0)
        }
    }
}


// MARK: - RX Switc error codes
extension RxError {
    /// A textual representation of `self`, suitable for debugging.
    public var debugDescription: String {
        switch self {
        case .unknown:
            return "Unknown error occurred."
        case .disposed(let object):
            return "Object `\(object)` was already disposed."
        case .overflow:
            return "Arithmetic overflow occurred."
        case .argumentOutOfRange:
            return "Argument out of range."
        case .noElements:
            return "Sequence doesn't contain any elements."
        case .moreThanOneElement:
            return "Sequence contains more than one element."
        case .timeout:
            return "Request timeout."
        }
    }
}





