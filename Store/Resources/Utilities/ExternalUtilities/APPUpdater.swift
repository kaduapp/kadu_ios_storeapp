//
//  APPUpdater.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 02/04/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

class APPUpdater {
    
    static let shared = APPUpdater()
    
    func isUpdateAvailable() throws -> (Bool,String) {
        guard let info = Bundle.main.infoDictionary,
            let currentVersion = info["CFBundleShortVersionString"] as? String,
            let identifier = info["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                throw VersionError.invalidBundleInfo
        }
        let data = try Data(contentsOf: url)
        guard let json = try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String: Any] else {
            throw VersionError.invalidResponse
        }
        if let result = (json["results"] as? [Any])?.first as? [String: Any], let version = result["version"] as? String {
            print("version in app store", version,currentVersion);
            
            return (version != currentVersion,version)
        }
        throw VersionError.invalidResponse
    }
    
    
    func popupUpdateDialogue(versionInfo:String){
        
        let alertMessage = "A new version of \(Bundle.main.infoDictionary!["CFBundleName"] ?? "") is available,Please update to version " + versionInfo;
        let alert = UIAlertController(title: "New Version Available", message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okBtn = UIAlertAction(title: "Update", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            if let url = URL(string: "itms-apps://itunes.apple.com/us/app/magento2-mobikul-mobile-app/id1166583793"),
                UIApplication.shared.canOpenURL(url){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        })
        let noBtn = UIAlertAction(title:"Skip this Version" , style: .destructive, handler: {(_ action: UIAlertAction) -> Void in
        })
        alert.addAction(okBtn)
        alert.addAction(noBtn)
        
        // show alert
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    enum VersionError: Error {
        case invalidResponse, invalidBundleInfo
    }
    
    class func callFromVC() {
        DispatchQueue.global().async {
            do {
                let (update,version) = try APPUpdater.shared.isUpdateAvailable()
                
                print("update",update)
                DispatchQueue.main.async {
                    if update{
                        APPUpdater.shared.popupUpdateDialogue(versionInfo:version);
                    }
                }
            } catch {
                print(error)
            }
        }
    }
}
