//
//  UserDefaultConstants.swift
//  DelivX Store
//
//  Created by 3 Embed on 21/12/17.
//  Copyright © 2017 3 Embed. All rights reserved.
//

import UIKit

class UserDefaultConstants: NSObject {
    static let StoreData                                    = "StoreData"
    static let PushToken                                    = "FCMPushToken"
    static let ProfileData                                  = "ProfileData"
    static let IsLoggedIn                                   = "IsLoggedIn"
    static let SessionToken                                 = "SessionToken"
    static let ReferralCode                                 = "referralCode"
    static let Currency                                     = "Currency"
    static let CurrentLatitude                              = "currentLatitude"
    static let CurrentLongitude                             = "currentLongitude"
    static let ZoneId                                       = "zoneId"
    static let OrderType                                    = "orderType"
    static let IsEditable                                   = "isEditable"
    static let fcmStoreTopic                                   = "fcmStoreTopic"
    static let fcmCityTopic                                   = "fcmCityTopic"
   
    
    
}
