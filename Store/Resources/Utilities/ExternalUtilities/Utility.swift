//
//  Utility.swift
//  DelivX Store
//
//  Created by 3 Embed on 13/12/17.
//  Copyright © 2017 3 Embed. All rights reserved.
//

import UIKit

class Utility: NSObject {

    ///Device Details
    static var DeviceId: String {
        
        if let ID: String = UIDevice.current.identifierForVendor?.uuidString {
            return ID
        }
        else {
            return "iPhone_Simulator_ID4"
        }
    }
    
    static var DeviceType: Int {
        return 1//"iOS"
    }
    
    static var DeviceMake: String {
        return "Apple"
    }
    
    static var AppName: String {
        return (Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String)!
    }
    
    static var AppVersion: String {
        return (Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String)!
    }
    
    static var DeviceName: String {
        return UIDevice.current.systemName as String
    }
    
    static var DeviceVersion: String {
        return UIDevice.current.systemVersion as String
    }
    
    static var DeviceModel: String {
        return UIDevice.current.model as String
    }
    
    static var DeviceTime: String {
        let myString = Date.getDateString(value: Date(),format: DateFormat.DateAndTimeFormatServer)
        return myString
    }
    
    static var DeviceTimeWithoutBlankSpace: String {
        let myString = Date.getDateString(value: Date(),format: DateFormat.DateAndTimeFormatServerGET)
        return myString
    }
    
    //Get User Type
    static var UserType: Int {
        return 1//"iOS"
    }
    
    
    
    static var emailID : String {
        return UserDefaults.standard.string(forKey: APIRequestParams.Email)!
    }
    
    // Push Token
    static var PushToken: String {
        if let token: String = UserDefaults.standard.value(forKey: UserDefaultConstants.PushToken) as! String? {
            return token
        }
        return "default_" + UserDefaultConstants.PushToken
    }
    
    // Zone ID
    static var ZoneId: String {
        if let id: String = UserDefaults.standard.value(forKey: UserDefaultConstants.ZoneId) as! String? {
            return id
        }
        return "0"
    }
    
    
    class func savePushToken(token : String) {
        UserDefaults.standard.set(token, forKey: UserDefaultConstants.PushToken)
        UserDefaults.standard.synchronize()
    }
    
    class func saveZoneId(id : String) {
        UserDefaults.standard.set(id, forKey: UserDefaultConstants.ZoneId)
        UserDefaults.standard.synchronize()
    }
    
    ///Set Login
    class func saveLogin(data:Bool) {
        UserDefaults.standard.set(data, forKey: UserDefaultConstants.IsLoggedIn)
        UserDefaults.standard.synchronize()
    }

    
    class func saveSession(token:String){
        UserDefaults.standard.set(token, forKey: UserDefaultConstants.SessionToken)
        UserDefaults.standard.synchronize()
    }
    
    
    class func setStoreData(data: Any) {
        
        if let data = data as? [String: Any] {
            //isAutoDispatch
            //isStoreDriver
            //isForcedAccept
            
            
            if let storeName = data["userType"] as? NSNumber {  //0= centra 1- franchise 2- store
                UserDefaults.standard.set(storeName, forKey: "user_Type")
            }

            if let managerImage = data["managerImage"] as? String {
                UserDefaults.standard.set(managerImage, forKey: "StoreName_ManagerImage")
            }
            
            if let managerName = data["name"] as? String {
                UserDefaults.standard.set(managerName, forKey: "StoreName_Managername")
            }
            
            if let isAutoDispatch = data["isAutoDispatch"] as? Int{
                UserDefaults.standard.set(isAutoDispatch, forKey: "isAutoDispatch")
            }
            
            if let isStoreDriver = data["isStoreDriver"] as? Int{
                UserDefaults.standard.set(isStoreDriver, forKey: "isStoreDriver")
            }
            
            if let isForcedAccept = data["isForcedAccept"] as? Int{
                UserDefaults.standard.set(isForcedAccept, forKey: "isForcedAccept")
            }
            
            if let driverType = data["driverType"] as? Int {
                //1 = freelancing 2= store drivers availlable
                
                let showDriver = driverType == 2 ? true : false
                
                
                UserDefaults.standard.set(driverType, forKey: "isStoreDriver")
                UserDefaults.standard.set(showDriver, forKey: "StoreDriver_type")
            }
            
            if let storeType = data["storeType"] as? Int {
                UserDefaults.standard.set(storeType, forKey: "Store_Type")
            }
            
//            if let managerKeyId = data["mid"] as? String {
//                UserDefaults.standard.set(managerKeyId, forKey: "StoreDriver_type")
//            }
            
            
            if let forcedAccept = data["forcedAccept"] as? Int {
                //2 = disabled and 1 =  enable
                let showDriver = forcedAccept == 1 ? true : false
                UserDefaults.standard.set(showDriver, forKey: "StoreForced_Accept")
            }
            
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            if let city = data["cityId"] as? String{
//                  UserDefaults.standard.set(city, forKey: "city_ID")
//                appDelegate.subscribePushTopic(topic: "storeManager_" + city)
//
//            }
            
//            if let serviceZones = data["serviceZones"] as? [String] {
//                    UserDefaults.standard.set(serviceZones, forKey: "service_Zones")
//                for zone in serviceZones{
//                    if data["userType"] as! NSNumber == 2 {
//                        self.saveZoneId(id : zone)
//
//                    }
//                    appDelegate.subscribePushTopic(topic: "storeManager_" + zone)
//                }
//            }
            
            let userType = UserDefaults.standard.integer(forKey: "user_Type")
            switch userType{
            case 0:
                UserDefaults.standard.set("managerImage", forKey: "StoreName_ManagerImage")
                if let userTypeMsg = data["userTypeMsg"] as? String {
                    if let cityName = data["cityName"] as? String {
                        UserDefaults.standard.set(userTypeMsg + ": " + cityName, forKey: "StoreName_Data")
                        UserDefaults.standard.set(userTypeMsg, forKey: "StoreName_Managername")
                        
                    }
                }
                break
            case 1:
                UserDefaults.standard.set("managerImage", forKey: "StoreName_ManagerImage")
                
                if let franchiseName = data["franchiseName"] as? String {
                    UserDefaults.standard.set("Franchise :" + franchiseName, forKey: "StoreName_Data")
                    UserDefaults.standard.set(franchiseName, forKey: "StoreName_Managername")
                }
                break
            case 2:
                
                if let storeName = data["storeName"] as? String {
//                    UserDefaults.standard.set("Store :" + storeName, forKey: "StoreName_Data")
                    UserDefaults.standard.set(storeName, forKey: "StoreName_Data")
                }
                break
            default:
                break
            }
            
            UserDefaults.standard.synchronize()
        }
        
        APIUtility.setLogin(loginStatus: true)

    }
    
    class func isForcedAccept() -> Bool {
        return UserDefaults.standard.bool(forKey: "StoreForced_Accept")
    }
    
    class func getStoreDriverType() -> Bool {
         return UserDefaults.standard.bool(forKey: "StoreDriver_type")
    }
    
    class func getStoreName() -> String {
        return UserDefaults.standard.string(forKey: "StoreName_Data") ?? ""
    }
    class func getCurrencySymbol() -> String {
       return UserDefaults.standard.string(forKey: "CurrencySymbol") ?? ""
    }
    
    class func getManagerImage() -> String? {
        
        if let storeName = UserDefaults.standard.string(forKey: "StoreName_ManagerImage") {
            return storeName
        }
         return nil
    }
    
    class func getManagerName() -> String {
        
        if let name = UserDefaults.standard.string(forKey: "StoreName_Managername") {
            return name
        }
        return ""
    }
    
    class func getStoreType() -> Int{
        
//        if UserDefaults.standard.integer(forKey: "Store_Type") != 0 {
            return UserDefaults.standard.integer(forKey: "Store_Type")
//        }
//        return 0
    }
    
    class func getIsAutoDispatch() -> Int{
         return UserDefaults.standard.integer(forKey: "isAutoDispatch")
    }
   
    class func getIsStoreDriver() -> Int{
        return UserDefaults.standard.integer(forKey: "isStoreDriver")
    }
    
    class func getISForcedAccept() -> Int{
        return UserDefaults.standard.integer(forKey: "isForcedAccept")
    }
    
    
    class func clearStoreData() {
        UserDefaults.standard.removeObject(forKey: "StoreName_Data")
        UserDefaults.standard.removeObject(forKey: "StoreName_Data")
    }
    
    
    class func setOrderIdFromFCM(orderId:Int) {
        let orderID = "\(orderId)"
        UserDefaults.standard.set(orderID, forKey: "OrderID_FCM")
    }
    
    class func removeOrderIDFromFCM() {
        UserDefaults.standard.removeObject(forKey: "OrderID_FCM")
    }
    
    class func getOrderIDFromFCM() -> Int {
        if let orderID = UserDefaults.standard.string(forKey: "OrderID_FCM") {
            return Int(orderID)!
        }
        return 0
    }
    
    static var getLanguageName: String {
        if let langName: String = UserDefaults.standard.value(forKey: "lan_name") as! String? {
            return langName
        }
        return "Spanish, Castilian"
    }

    // Get getTheLanguageCode
    static var langCode: String {
        if let lang = UserDefaults.standard.value(forKey: "langCode") as? String {
             return lang
        }
        return "es"
    }
    
    
    static func getTabCounts() -> Int{
        
        if  Utility.getISForcedAccept() == 1 && Utility.getIsAutoDispatch() == 1 {
            return 3
        }  else if  Utility.getISForcedAccept() == 0 && Utility.getIsAutoDispatch() == 1 {
            return 4
        }else if Utility.getISForcedAccept() == 0 && Utility.getIsAutoDispatch() == 0  {
            return 5
        }
        return 0
    }
    
    
    class func connectMQTT() {
        if  MQTT.sharedInstance.isConnected == false {
            MQTT.sharedInstance.createConnection()
        }
    }
    
    class func disconnectMQTT() {
        if StoreUtility.isLogin() && MQTT.sharedInstance.isConnected == false {
            MQTT.sharedInstance.disconnectMQTTConnection()
        }
    }
}





