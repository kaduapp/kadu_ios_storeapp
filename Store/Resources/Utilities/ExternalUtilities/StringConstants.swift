import UIKit


class StringConstants: NSObject {
    
    
    //Alerts
    class func Error() -> String {
        return OSLocalizedString("Error")
    }
    class func message() -> String {
        return OSLocalizedString("Message")
    }
    
    class func Done() -> String {
        return OSLocalizedString("Done")
    }
    class func Success() -> String {
        return OSLocalizedString("Success")
    }
    
    class func enterEmail() -> String {
        return OSLocalizedString("Please enter an email address.")
    }
    class func invalidMail() -> String {
        return OSLocalizedString("Invalid email address, please try again.")
    }
    class func ProgressIndicator() -> String {
        return OSLocalizedString("Loading...")
    }
    class func connectInternet() -> String {
        return OSLocalizedString("Please connect to internet.")
    }
    class func noInternet() -> String {
        return OSLocalizedString("Not connected to internet")
    }
    class func checkEmail() -> String {
        return OSLocalizedString("Please check your email id and enter a valid email id")
    }
    class func enterPass() -> String {
        return OSLocalizedString("Please check your password.")
    }
    class func updated() -> String {
        return OSLocalizedString("Updated Successfully")
    }
    
    class func noDrivers() -> String {
        return OSLocalizedString("No driver available!")
    }

    // Authentication
    
    class func email() -> String {
        return OSLocalizedString("Email")
    }
    
    class func password() -> String {
        return OSLocalizedString("Password")
    }
    
    class func SignIn() -> String {
        return OSLocalizedString("Sign In")
    }
    
    class func forgotPassword() -> String {
        return OSLocalizedString("Forgot Password?")
    }
    
    class func forgotInstruction() -> String{
        return OSLocalizedString("Enter your email below to receive your password reset instructions")
    }
    
    class func submit() -> String{
        return OSLocalizedString("Submit")
    }
    
    
    // Home
    
    class func new() -> String{
        return OSLocalizedString("New")
    }
    class func accepted() -> String{
        return OSLocalizedString("Accepted")
    }
    class func orderPickup() -> String{
        return OSLocalizedString("Order Pickup")
    }
    class func inDispatch() -> String{
        return OSLocalizedString("In Dispatch")
    }
    class func pickupReady() -> String{
        return OSLocalizedString("Pickup Ready")
    }
    class func washing() -> String{
        return OSLocalizedString("Washing")
    }
    class func assign() -> String{
        return OSLocalizedString("Assign")
    }
    class func searchPreOrders() -> String{
        return OSLocalizedString("Search Pre Orders")
    }
    
    class func noCurrentOrders() -> String{
        return OSLocalizedString("Currently no order availlable!")
    }

    
    //MenuVC
    
    class func home() -> String{
        return OSLocalizedString("Home")
    }
    class func orderHistory() -> String{
        return OSLocalizedString("Order History")
    }
    class func needHelp() -> String{
        return OSLocalizedString("Need Help?")
    }
    class func wallet() -> String{
        return OSLocalizedString("Wallet")
    }
    class func bankDetails() -> String{
        return OSLocalizedString("Bank Details")
    }
    class func selectLanguage() -> String{
        return OSLocalizedString("Select Language")
    }
    class func inventory() -> String{
        return OSLocalizedString("Inventory")
    }
    
    
    // Order
    
    class func items() -> String{
        return OSLocalizedString("Items")
    }
    class func price() -> String{
        return OSLocalizedString("Price")
    }
    
    class func ASAP() -> String{
        return OSLocalizedString("ASAP")
    }
    class func later() -> String{
        return OSLocalizedString("Later")
    }
    class func PickUp() -> String{
        return OSLocalizedString("PICK UP")
    }
    class func Delivery() -> String{
        return OSLocalizedString("DELIVERY")
    }
    class func waitingFroDriver() -> String{
        return OSLocalizedString("Waiting for the driver")
    }
    class func now() -> String{
        return OSLocalizedString("Now")
    }
    
    class func manualAssign() -> String{
        return OSLocalizedString("Manual Assign")
    }
    class func accept() -> String{
        return OSLocalizedString("Accept")
    }
    class func reject() -> String{
        return OSLocalizedString("Deny")
    }
    class func autoDispatch() -> String{
        return OSLocalizedString("Auto Dispatch")
    }
    
    class func pickupCompleted() -> String{
        return OSLocalizedString("Pickup Completed")
    }
    
    


    // Payment
    
    class func cash() -> String{
        return OSLocalizedString("Cash")
    }
    class func card() -> String{
        return OSLocalizedString("Card")
    }
    class func cashNwallet() -> String{
        return OSLocalizedString("Cash + Wallet")
    }
    class func cardNwallet() -> String{
        return OSLocalizedString("Cash + Wallet")
    }
  

    //Amount
    class func Total() -> String{
        return OSLocalizedString("Total")
    }
    class func subTotal() -> String{
        return OSLocalizedString("SubTotal")
    }
    class func discount() -> String{
        return OSLocalizedString("Discount")
    }
 
    class func taxTotal() -> String{
        return OSLocalizedString("Tax Total")
    }


    // Laundry
    
    class func cleaningCharge() -> String{
        return OSLocalizedString("Cleaning Fee")
    }
    
   // Stripe
    class func addStripeAC() -> String{
        return OSLocalizedString("Add Stripe Account")
    }
    
    // History
    
    class func searchBy() -> String{
        return OSLocalizedString("Search by name or order id")
    }
   
    
    // Profile
    
    class func profile() -> String{
        return OSLocalizedString("Profile")
    }
    
    class func name() -> String{
        return OSLocalizedString("Name")
    }

    class func phoneNumber() -> String{
        return OSLocalizedString("Phone Number")
    }

    class func storeName() -> String{
        return OSLocalizedString("Store Name")
    }
    class func cityPartner() -> String{
        return OSLocalizedString("City Partner")
    }
    class func franchiseName() -> String{
        return OSLocalizedString("Franchise Name")
    }
 

    class func logout() -> String{
        return OSLocalizedString("Logout")
    }
  
    class func PAYMENTTYPE () -> String{
        return OSLocalizedString("PAYMENT TYPE")
    }
    class func ORDERTYPE () -> String{
        return OSLocalizedString("ORDER TYPE")
    }
    class func DUEIN () -> String{
        return OSLocalizedString("DUE IN")
    }
    class func DELIVERTO ()->String{
        return OSLocalizedString("DELIVER TO")
    }
    class func EXTRANOTES () ->String{
        return OSLocalizedString("EXTRA NOTES")
    }
    class func DriverDetails() -> String{
        return OSLocalizedString("DRIVER DETAILS")
    }
    class func STEP1CONNECTTOYOURSTRIPEACCOUNT()->String{
        return OSLocalizedString("STEP 1 : CONNECT TO YOUR STRIPE ACCOUNT.")
    }
    class func Connectstripeaccount()->String{
        return OSLocalizedString("Connect stripe account")
    }
    class func Linkbankaccount() ->String{
        return OSLocalizedString("Link bank account")
    }
    class func LINKBANKACCOUNT()->String{
        return OSLocalizedString("LINK BANK ACCOUNT")
    }
    class func RecentTransactions() ->String{
        return OSLocalizedString("RecentTransactions")
    }
    class func ALL()->String{
        return OSLocalizedString("ALL")
    }
    class func DEBIT() ->String{
        return OSLocalizedString("DEBIT")
    }
    class func CREDIT() ->String{
        return OSLocalizedString("CREDIT")
    }
    class func CURRENTCREDIT() -> String{
        return OSLocalizedString("CURRENT CREDIT")
    }
    class func CustomerName() ->String{
        return OSLocalizedString("Customer Name")
    }
    class func OrderType() -> String{
        return OSLocalizedString("Order Type")
    }
    class func OrderDetails() -> String{
        return OSLocalizedString("Order Details")
    }
    class func Status() -> String{
        return OSLocalizedString("Status")
    }
    class func DeliveredBy()-> String{
        return OSLocalizedString("Delivered by")
    }
    class func StoreEarning() -> String{
        return OSLocalizedString("Store Earning")
    }
    
    
}

public func OSLocalizedString(_ key: String) -> String {
    return OneSkyOTAPlugin.localizedString(forKey: key, value: "", table: nil)
}

