//
//  CommonAlertView.swift
//  UFly
//
//  Created by 3Embed on 18/09/17.
//  Copyright © 2017 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CommonAlertView: UIView {

    @IBOutlet weak var contentLabel: UILabel!
    private static var obj: CommonAlertView? = nil
//    @IBOutlet weak var alertTypeImage: UIImageView!
    @IBOutlet weak var headLabel: UILabel!
    
    @IBOutlet weak var alertBackGroundView: UIView!
    
    
    static var shared: CommonAlertView {
        
        if obj == nil {
            obj = UINib(nibName: "CommonAlertView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CommonAlertView
            obj?.frame = UIScreen.main.bounds
        }
        return obj!
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.hideAlert()
    }

    private func setup() {

        let window:UIWindow = UIApplication.shared.delegate!.window!!
        window.windowLevel = UIWindowLevelAlert
        
        window.addSubview(self)
        alertBackGroundView.layer.cornerRadius = 4
        alertBackGroundView.layer.masksToBounds = true
        
        self.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
        self.backgroundColor = UIColor.colorWithHex(color: "6f6f6f").withAlphaComponent(0.0)
        Helper.setShadow(sender: alertBackGroundView)
        UIView.animate(withDuration: 0.3, delay: 0.01, options: .beginFromCurrentState, animations: {() -> Void in
            self.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
        }, completion: {(_ finished: Bool) -> Void in
            self.backgroundColor = UIColor.colorWithHex(color: "6f6f6f").withAlphaComponent(0.5)
        })
        
//        let when = DispatchTime.now() + 30
//        
//        DispatchQueue.main.asyncAfter(deadline: when){
//            self.transform = CGAffineTransform.identity.scaledBy(x: 1, y: 1)
//            UIView.animate(withDuration: 0.01, delay: 0.01, options: .beginFromCurrentState, animations: {() -> Void in
//                self.transform = CGAffineTransform.identity.scaledBy(x: 0.0, y: 0.0)
//            }, completion: {(_ finished: Bool) -> Void in
//                self.hideAlert()
//            })
//        }
        
    }
    
    func showAlert(message: String, head:String, type:Int) {
        setup()
        headLabel.text = head
        contentLabel.text = message
//        if type == 1 {
//           alertTypeImage.image = #imageLiteral(resourceName: "ErrorIcon")
//        }else {
//            alertTypeImage.image = #imageLiteral(resourceName: "SuccessIcon")
//        }
        
    }
    
    func hideAlert() {
        if (CommonAlertView.obj != nil) {
            self.backgroundColor = UIColor.colorWithHex(color: "6f6f6f").withAlphaComponent(0.0)
            UIView.animate(withDuration: 0.3, animations: {
                self.transform = CGAffineTransform(scaleX:0.1, y:0.1)
            }, completion: { _ in
                 self.removeFromSuperview()
            })
        }
    }
}
