//
//  Fonts.swift
//  DelivX Store
//
//  Created by 3 Embed on 13/12/17.
//  Copyright © 2017 3 Embed. All rights reserved.
//

import UIKit


struct DinPro {
    
    static let Regular          = "DINPro-Regular"
    static let Medium           = "DINPro-Medium"
    static let Light            = "DINPro-Light"
    static let Bold             = "DINPro-Bold"
    static let Black            = "DINPro-Black"
}

public struct Roboto {
    static let Regular          = "Roboto-Regular"
    static let Medium           = "Roboto-Medium"
    static let Bold             = "Roboto-Bold"
    static let Italic           = "Roboto-Italic"
}

public struct RobotoCondensed {
    static let Regular          = "RobotoCondensed-Regular"
    static let Bold             = "RobotoCondensed-Bold"
}


struct FontConstants {
    
    static let Font_NavigationTitle:UIFont = UIFont(name: Roboto.Bold, size: 17)!
    
    //Home view controller
    static let Font_SearchBar = DinPro.Medium
    static let Font_OrderTitleLabel = DinPro.Bold
    

}




class Fonts: NSObject {
    
    
    
    class func setPrimaryBold(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: DinPro.Bold, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: DinPro.Bold, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: DinPro.Bold, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    class func setPrimaryLight(_ sender: Any){
        
        if let element = sender as? UILabel {
            element.font = UIFont(name: DinPro.Light, size: element.font.pointSize - 10)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: DinPro.Light, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: DinPro.Light, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    class func setPrimaryMedium(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: DinPro.Medium, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: DinPro.Medium, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: DinPro.Medium, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    class func setPrimaryRegular(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: DinPro.Regular, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: DinPro.Regular, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: DinPro.Regular, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    class func setPrimaryBlack(_ sender: Any){
        if let element = sender as? UILabel {
            element.font = UIFont(name: DinPro.Black, size: element.font.pointSize)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: DinPro.Black, size: (element.font?.pointSize)!)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: DinPro.Black, size: (element.titleLabel?.font.pointSize)!)
        }
    }
    
    class func setPrimaryRegular(_ sender: Any, size:CGFloat)
    {
        if let element = sender as? UILabel {
            element.font = UIFont(name: DinPro.Regular, size: size)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: DinPro.Regular, size: size)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: DinPro.Regular, size: size)
        }
    }

    class func setPrimaryBold(_ sender: Any, size:CGFloat)
    {
        if let element = sender as? UILabel {
            element.font = UIFont(name: DinPro.Bold, size: size)
        }else if let element = sender as? UITextField {
            element.font = UIFont(name: DinPro.Bold, size: size)
        }else if let element = sender as? UIButton {
            element.titleLabel?.font = UIFont(name: DinPro.Bold, size: size)
        }
    }

}

