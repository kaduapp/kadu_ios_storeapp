//
//  LauncVC.swift
//  DelivX Store
//
//  Created by 3 Embed on 13/12/17.
//  Copyright © 2017 3 Embed. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import FirebaseMessaging

class LaunchVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let fcmStoreTopic     = UserDefaults.standard.object(forKey: UserDefaultConstants.fcmStoreTopic) as? String , fcmStoreTopic.count > 0 {
          Messaging.messaging().unsubscribe(fromTopic:fcmStoreTopic ) { (Error) in
                if Error == nil{
                    UserDefaults.standard.removeObject(forKey: UserDefaultConstants.fcmStoreTopic)
                 self.navigateNext()
                }
            }
        }
        else if let fcmCityTopic     = UserDefaults.standard.object(forKey: UserDefaultConstants.fcmCityTopic) as? String,fcmCityTopic.count > 0{
        
        Messaging.messaging().unsubscribe(fromTopic:fcmCityTopic ) { (Error) in
                if Error == nil{
                    UserDefaults.standard.removeObject(forKey: UserDefaultConstants.fcmCityTopic)
                    self.navigateNext()
                }
            }
        }
        else{
            self.navigateNext()
        }
    }
    
    
    func navigateNext(){
        if StoreUtility.isLogin() {
            navigationBarColor()
            addMenu()
        }else {
            
            performSegue(withIdentifier: UIConstants.SegueIds.LaunchToLogin, sender: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
