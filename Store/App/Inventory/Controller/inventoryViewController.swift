//
//  inventoryViewController.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 17/12/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxAlamofire
import Alamofire
import CocoaLumberjack

class inventoryViewController : UIViewController{
    private var searchStr = ""
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var productSearchBar: UISearchBar!
    var productArray = [Inventry]()
    var searchActive : Bool = false
    var filtered = [Inventry]()
    var selectedProduct : Inventry?
    var selectedIndex = -1
    override func viewDidLoad() {
        super.viewDidLoad()
        
        productSearchBar.delegate = self
        self.registerCell()
        self.addObserver()
        self.setNavigationBarItem()
        addShadowToBar()
        self.navigationItem.title = LeftMenuData.Data[LeftMenu.inventoryVC.rawValue].MenuText
        self.getProducts()
        self.registerCell()
    }
    
    
    private func registerCell(){
        collectionView.register(UINib.init(nibName: "InvertoryCVCell", bundle: nil), forCellWithReuseIdentifier: "InvertoryCVCell")
    }
    
    func getProducts(){
        Helper.showPI(string: "")
        let header = APIUtility.getHeader()
        let strUrl =  APIEndTails.getProducts + "\(StoreUtility.getStore().storeId)"
        RxAlamofire.requestJSON(.get,
                                strUrl,
                                parameters: nil,
                                encoding: JSONEncoding.default,
                                headers: header)
            .subscribe(onNext: { (r,json) in
                if  json is [String:Any] {
                    Helper.hidePI()
                    let _:Int = r.statusCode
                    if let dict = json as? [String:Any] {
                        if((dict["data"]) != nil){
                            let data = (dict["data"]) as! [String:Any]
                            self.productArray = Inventry.getInventoryArray(data: data["products"] as! [[String:Any]])
                            
                            if(self.searchActive) {
                                self.filtered = self.productArray.filter {"\($0.name)".contains(self.searchStr)}
                            }
                            
                            self.collectionView.reloadData()
                            
                        }
                    }
                }
            }, onError: { (error) in
                Helper.hidePI()
                DDLogError("API Response \(strUrl))\nError:\(error.localizedDescription)")
            }, onCompleted: {
                Helper.hidePI()
                DDLogInfo("API Response Completed")
            }, onDisposed:{
                DDLogInfo("API Response Disposed")
            }).disposed(by: disposeBag)
    }
    
    func patchProductStatus(productId:String, status:Int) {
        let header = APIUtility.getHeader()
        let urlString = APIEndTails.patchProduct + "\(productId)/\( status)"
        let paramsDict:[String:Any] = ["" :""]
        APIHandler.patchRequest(header: header, url: urlString, params: paramsDict).subscribe(onNext: {[weak self]  success, data in
            if var product = self?.selectedProduct, success == true {
                if product.status == 1 || product.status == 6{
                   product.status = 5
                }
                else{
                    product.status = 6
                }
                if((self?.searchActive)!){
                    self?.filtered[(self?.selectedIndex)!] = product
                }
                else{
                    self?.productArray[(self?.selectedIndex)!] = product
                }
                
                self?.collectionView.reloadData()
            }
            }, onError: { Error in
                print(Error)
        }).disposed(by: disposeBag)
    }
    
}


extension inventoryViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(searchActive) {
            return filtered.count
        } else {
            return productArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "InvertoryCVCell", for: indexPath) as! InvertoryCVCell
        
        var product = Inventry.init(data: [:])
        if(searchActive && filtered.count != 0) {
           product = filtered[indexPath.row]
            
            cell.itemName.text = product.name
            switch product.status {
            case 6,1:
                cell.selectedButton.setImage( #imageLiteral(resourceName: "selector") , for: .normal )
            default:
                cell.selectedButton.setImage( #imageLiteral(resourceName: "unselector"),  for: .normal )
            }

            Helper.setImage(imageView: cell.imageView, url: product.image, defaultImage:#imageLiteral(resourceName: "default_placeholder"))
            
            
        } else {
           product = productArray[indexPath.row]
            cell.itemName.text = product.name
            switch product.status  {
            case 6,1:
                cell.selectedButton.setImage( #imageLiteral(resourceName: "selector") , for: .normal )
            default:
                cell.selectedButton.setImage( #imageLiteral(resourceName: "unselector"),  for: .normal )
            }
            Helper.setImage(imageView: cell.imageView, url: product.image, defaultImage: UIImage.init())
        }
        return  cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if  self.isIphone(){
            return CGSize(width: (self.view.frame.width / 2) - 20
                , height: (self.view.frame.width / 2) + 10)
        }else{
            return CGSize(width: 120
                , height: 140)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var product = Inventry.init(data: [:])
        
        if(searchActive){
            if filtered.count > 0{
          product = filtered[indexPath.row]
            }
        }else{
            if productArray.count > 0{
           product  = productArray[indexPath.row]
            }
        }
        
        selectedProduct = product
        selectedIndex = indexPath.row
        if product.status == 6 || product.status  == 1 {
            patchProductStatus(productId: product.productId, status: 5)
        }else{
            patchProductStatus(productId: product.productId, status: 6)
        }
        
    }
    
}




extension inventoryViewController : UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchStr = ""
        searchActive = false;
        filtered.removeAll()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchStr = ""
        searchActive = false;
        filtered.removeAll()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        searchStr = ""
        searchActive = false;
        filtered.removeAll()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        if searchText == ""{
         searchStr = ""
         searchActive = false
         filtered.removeAll()
        }
        else{
        searchStr = searchText
        searchActive = true
        filtered = productArray.filter {"\($0.name)".contains(searchText)}
        }
        
        self.collectionView.reloadData()
    }
    
}



struct Inventry {
    
    
    var productId = ""
    var image = ""
    var name = ""
    var status : Int = 0

    
    
    init(data:[String:Any]) {
        
        
        if let id = data["childProductId"] as? String {
            productId = id
        }
      
        if let mobileImages = data["mobileImage"] as? [[String:Any]], mobileImages.count > 0,   let url = mobileImages[0]["mobile"] as? String {
            image = url
        }
        if let itemName = data["productName"] as? String {
            name = itemName
        }
        if let statusValue = data["status"] as? Int {
            status = statusValue
        }
      
    }
    
    static  func getInventoryArray (data: [[String:Any]]) -> [Inventry]{
        var allProducts = [Inventry]()
        for dict in data{
            
            let product = Inventry.init(data: dict)
            allProducts.append(product)
        }
        return allProducts
    }
}
