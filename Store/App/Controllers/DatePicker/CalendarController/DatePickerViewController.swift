//
//  DatePickerViewController.swift
//  Ufy_Store
//
//  Created by Raghavendra Shedole on 07/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift

class DatePickerViewController: UIViewController,CalendarViewDataSource, CalendarViewDelegate {
    
    
    let calenderPublishSubject = PublishSubject<Any>()
    
    @IBOutlet weak var calendarView: CalendarView!
//    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCanlendar()
        
        // Do any additional setup after loading the view.
    }
    
    func setCanlendar() {
        CalendarView.Style.cellShape                = .bevel(8.0)
        CalendarView.Style.cellColorDefault         = UIColor.clear
        CalendarView.Style.cellColorToday           = UIColor(red:1.00, green:0.84, blue:0.64, alpha:1.00)
        CalendarView.Style.cellSelectedBorderColor  = UIColor(red:1.00, green:0.63, blue:0.24, alpha:1.00)
        CalendarView.Style.cellEventColor           = UIColor(red:1.00, green:0.63, blue:0.24, alpha:1.00)
        CalendarView.Style.headerTextColor          = UIColor.white
        CalendarView.Style.cellTextColorDefault     = UIColor.white
        CalendarView.Style.cellTextColorToday       = UIColor(red:0.31, green:0.44, blue:0.47, alpha:1.00)
        
        
        calendarView.dataSource = self
        calendarView.delegate = self
        
        calendarView.direction = .horizontal
        calendarView.multipleSelectionEnable = false
        calendarView.marksWeekends = true
        
        calendarView.backgroundColor = UIColor(red:0.31, green:0.44, blue:0.47, alpha:1.00)
    }

    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        EventsLoader.load(from: self.startDate(), to: self.endDate()) { // (events:[CalendarEvent]?) in
            if let events = $0 {
                self.calendarView.events = events
            } else {
                // notify for access not access not granted
            }
        }
        
        
//        var tomorrowComponents = DateComponents()
//        tomorrowComponents.day = 1
        
//        let today = Date()
        
//        let tomorrow = self.calendarView.calendar.date(byAdding: tomorrowComponents, to: today)!
//        self.calendarView.selectDate(today)
//        self.calendarView.setDisplayDate(today)
//        self.datePicker.setDate(today, animated: false)
        
    }
    
    // MARK : KDCalendarDataSource
    
    func startDate() -> Date {
        
        var dateComponents = DateComponents()
        dateComponents.month = -11
        
        let today = Date()
        
        let threeMonthsAgo = self.calendarView.calendar.date(byAdding: dateComponents, to: today)!
        
        return threeMonthsAgo
    }
    
    func endDate() -> Date {
        
        var dateComponents = DateComponents()
        
        dateComponents.month = 11;
        dateComponents.day = -10
        let today = Date()
        
        let twoYearsFromNow = self.calendarView.calendar.date(byAdding: dateComponents, to: today)!
        
        return twoYearsFromNow
        
    }
    
    
    // MARK : KDCalendarDelegate
    
    func calendar(_ calendar: CalendarView, didSelectDate date : Date, withEvents events: [CalendarEvent]) {
        
        print("Did Select: \(date) with \(events.count) events")
        
        self.calenderPublishSubject.onNext(date)
        dismiss(animated: true, completion: nil)
//        for event in events {
//            print("\t\"\(event.title)\" - Starting at:\(event.startDate)")
//        }
        
    }
    
    @IBAction func dismissViewController(sender:UIButton) {
        self.calenderPublishSubject.onNext("")
        dismiss(animated: true, completion: nil)
    }
    
    func calendar(_ calendar: CalendarView, didScrollToMonth date : Date) {

//        self.datePicker.setDate(date, animated: true)
    }
    
    // MARK : Events
    
    @IBAction func onValueChange(_ picker : UIDatePicker) {
        self.calendarView.setDisplayDate(picker.date, animated: true)
    }
    
    @IBAction func goToPreviousMonth(_ sender: Any) {
        self.calendarView.goToPreviousMonth()
    }
    @IBAction func goToNextMonth(_ sender: Any) {
        self.calendarView.goToNextMonth()
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}

