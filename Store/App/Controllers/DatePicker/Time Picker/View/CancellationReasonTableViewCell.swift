//
//  CancellationReasonTableViewCell.swift
//  Ufy_Store
//
//  Created by Raghavendra Shedole on 08/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CancellationReasonTableViewCell: UITableViewCell {

    @IBOutlet weak var cancellationReasonLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static var identifier:String? {
        return String(describing:self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
