//
//  TimeIntervalCollectionCell.swift
//  Ufy_Store
//
//  Created by Raghavendra Shedole on 09/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class TimeIntervalCollectionCell: UITableViewCell {

    @IBOutlet weak var timeIntervalCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    static var identifier:String? {
        return String(describing:self)
    }

}
