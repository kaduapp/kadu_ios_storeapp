//
//  TimePickerViewController.swift
//  Ufy_Store
//
//  Created by Raghavendra Shedole on 08/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift

protocol TimePickerViewControllerDelegate {
    func delayOrder(cancellationResion:String, dueTime:String)
}

class TimePickerViewController: UIViewController {

  
    
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var dueTime: UILabel!
    @IBOutlet weak var currentDelay: UILabel!
    @IBOutlet weak var headerLabel: RobotoBoldLable!
    
    var delegate:TimePickerViewControllerDelegate?
    var numberOfTimeIntervals = 0
    let orderDelaySubject = PublishSubject<Any>()
    var reasonsTableVC:ReasonsTableViewController!
    var timePickerMV:TimePickerModalView!
    var headerText = ""
    
    var orderSummary:Any? {
        didSet {
            timePickerMV = TimePickerModalView.init(orderSummary: orderSummary ?? "")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.headerLabel.text = headerText
        
        reasonsTableVC = self.childViewControllers[0] as! ReasonsTableViewController
        reasonsTableVC.numberOfTimeIntervals = numberOfTimeIntervals
        reasonsTableVC.arrayOfReasons = ["1","2","3","4","5","1","2","3","4","5",]
        appearance()
    }
    
    
    func appearance() {
        self.dueTime.text = timePickerMV.dueTime
        self.currentDelay.text = timePickerMV.currentDelay
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func doneButtonAction(_ sender: UIButton) {
        
        if reasonsTableVC.delayTime.count == 0 &&  numberOfTimeIntervals != 0 {
            Helper.showAlert(message: "Please select delay time", head: "Message", type: 0)
            return
        }
        
        if let delegateObj = delegate {
            delegateObj.delayOrder(cancellationResion: self.reasonsTableVC.cancellationReasonTextView.text, dueTime: self.reasonsTableVC.delayTime)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}







