//
//  ReasonsTableViewController.swift
//  Ufy_Store
//
//  Created by Raghavendra Shedole on 09/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class ReasonsTableViewController: UITableViewController {

    @IBOutlet weak var cancellationReasonTextView: UITextView!
    
    var timeCollectionCell:TimeIntervalCollectionCell? = nil
    
    var numberOfTimeIntervals = 4
    var delayTime:String = ""
    var reasonString = ""
    var selectedIndex = -1
    var arrayOfReasons = [Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = 50
        self.tableView.estimatedRowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.addKeyboardChangeFrameObserver(willShow: { [weak self](height) in
            //Update constraints here
            self?.tableView.contentOffset.y += height
            self?.view.setNeedsUpdateConstraints()
            }, willHide: { [weak self](height) in
                //Reset constraints here
                self?.tableView.contentOffset = .zero
                self?.view.setNeedsUpdateConstraints()
        })

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tableView.removeObserver()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfReasons.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CancellationReasonTableViewCell.identifier!) as! CancellationReasonTableViewCell
        cell.cancellationReasonLabel.text = "Cancellatin Reason : \(indexPath.row)"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! CancellationReasonTableViewCell
        self.cancellationReasonTextView.text = cell.cancellationReasonLabel.text
        self.cancellationReasonTextView.resignFirstResponder()
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        
        if numberOfTimeIntervals > 0 {
            if timeCollectionCell == nil {
                timeCollectionCell = tableView.dequeueReusableCell(withIdentifier: TimeIntervalCollectionCell.identifier!) as? TimeIntervalCollectionCell
                timeCollectionCell?.timeIntervalCollectionView.delegate = self
                timeCollectionCell?.timeIntervalCollectionView.dataSource = self
            }
            
            return timeCollectionCell
        }else {
            return nil
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if numberOfTimeIntervals > 0 {
            return 80
        }else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
}


extension ReasonsTableViewController:UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfTimeIntervals
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let timeIntervalCell = collectionView.dequeueReusableCell(withReuseIdentifier: TimeIntervalCell.identifier, for: indexPath) as! TimeIntervalCell
        timeIntervalCell.timeIntervalLabel.text = "\((indexPath.row + 1) * 15) min"
        return timeIntervalCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width:Int = Int(collectionView.frame.width)
        return  CGSize(width: (width - 5 * (numberOfTimeIntervals + 1))/numberOfTimeIntervals, height: 40)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedIndex >= 0 {
            let cell = collectionView.cellForItem(at: IndexPath.init(row: selectedIndex, section: 0)) as! TimeIntervalCell
            cell.deSelectCell()
            selectedIndex = -1
        }
        
        let cell = collectionView.cellForItem(at: indexPath) as! TimeIntervalCell
        cell.selectedCell()
        
        delayTime =  Date.getDateString(value: Date().adding(minutes: 15 * (indexPath.row + 1)), format: DateFormat.DateAndTimeFormatServer)
        selectedIndex = indexPath.row
    }
}

extension ReasonsTableViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(15, 5, 15, 5)
    }
}



