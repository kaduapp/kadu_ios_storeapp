//
//  TimePickerViewModal.swift
//  Ufy_Store
//
//  Created by Raghavendra Shedole on 09/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

class TimePickerModalView:NSObject {
    var currentDelay = ""
    var dueTime = ""
    
    init(orderSummary:Any) {
    if let orderSummary = orderSummary as? HomeOrderModel {
        dueTime = Date.getDateFromString(value: orderSummary.orderSummary.bookingDateTime, format: DateFormat.TimeFormatToDisplay)
        currentDelay = Date.getDueTime(value: orderSummary.orderSummary.bookingDateTime)
        }
    }
}
