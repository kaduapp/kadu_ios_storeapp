//
//  DriverTVCell.swift
//  Ufy_Store
//
//  Created by 3Embed on 30/05/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

typealias DriverTVCellConfig = TableCellConfigurator<DriverTVCell , String>

class DriverTVCell: UITableViewCell , ConfigurableCell{
   
    @IBOutlet weak var driverName : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    func configure(data: String) {
        driverName.text = data
    }
    
    
}
