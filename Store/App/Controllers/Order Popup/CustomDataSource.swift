//
//  CustomDataSource.swift
//  Ufy_Store
//
//  Created by Raghavendra Shedole on 29/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit


class CustomDataSource: NSObject, UITableViewDataSource {
    
    var protocolItem:PopUpOrderDataProtocol? = nil
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return protocolItem?.numberOfSections ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return protocolItem?.numberOfRows ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
            if protocolItem?.popupTableCellType == .PopupOrderSummaryCell {
                let cell = tableView.dequeueReusableCell(withIdentifier: PopUpOrderSummaryCell.identifier) as! PopUpOrderSummaryCell
                cell.item = protocolItem!
                return cell
            }
                
            else  if protocolItem?.popupTableCellType == .ItemListPortionCall {
                let cell = tableView.dequeueReusableCell(withIdentifier: ItemListTablePortionCell.identifier) as! ItemListTablePortionCell
                cell.item = protocolItem as? PopupOrderItemListClass
                return cell
            }
                
            else if popUpdata?.popupTableCellType == .OrderAmountSummaryCell {
                let cell = tableView.dequeueReusableCell(withIdentifier: OrderAmountSummaryCell.identifier)as! OrderAmountSummaryCell
                cell.item = popUpdata as? PopupOrderAmountClass
                return cell
            }
            //
            //        else if tableView == self.itemsListTableView {
            //            if  indexPath.row == 0 {
            //                let cell = tableView.dequeueReusableCell(withIdentifier: ItemListTablePortionCell.identifier) as! ItemListTablePortionCell
            //                return cell
            //            }else {
            //                let cell = tableView.dequeueReusableCell(withIdentifier: ItemListTableAddonCell.identifier) as! ItemListTableAddonCell
            //                return cell
            //            }
            //        }
            //

        
    }
}
