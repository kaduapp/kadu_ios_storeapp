//
//  StoreDriverViewModel.swift
//  DelivX Store
//
//  Created by Vengababu Maparthi on 09/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxSwift

class StoreDriverViewModel {
    
    var disposeBag = DisposeBag()
    let storeDriverData = PublishSubject<(Any)>()
    
    var storeDriverDetails = StoreDriverModel()
    
    func getStoreData(completionHandler:@escaping([StoreDataModel])->()) {
        
        let header = APIUtility.getHeaderForOrders()
        let getOrdersUrl = "\(APIEndTails.getAllStores)" + StoreUtility.getStore().storeId
        
        APIHandler.getRequestData(header:header, url:getOrdersUrl).subscribe(onNext: {[weak self]  success, data in
            if success == true {
                completionHandler((self?.storeDriverDetails.getTheStoreData(storeData:data as! [String : Any]))!)
                self?.storeDriverData.onNext(true)
            }else {
                self?.storeDriverData.onNext(false)
            }
            }, onError: { Error in
                print(Error)
        }).disposed(by: disposeBag)
    }
    
    
    func getDriversData(storeID:String, completionHandler:@escaping([Driver])->()) {
        var storesID = ""
        if StoreUtility.getStore().storeId == "0"{
            storesID = storeID
        }else{
            storesID = StoreUtility.getStore().storeId
        }
        
        let header = APIUtility.getHeaderForOrders()
        let getOrdersUrl = "\(APIEndTails.getDrivers)" + StoreUtility.getStore().storeId + "/" + storesID + "/0/0"
        
        APIHandler.getRequest(header:header, url:getOrdersUrl).subscribe(onNext: {[weak self]  success, data in
          
            if success == true {
                
                completionHandler((self?.storeDriverDetails.getTheDriversData(driverData:data as! [String : Any]))!)
                self?.storeDriverData.onNext(true)
            }else {
                self?.storeDriverData.onNext(false)
            }
            }, onError: { Error in
                print(Error)
        }).disposed(by: disposeBag)
    }
    
}

