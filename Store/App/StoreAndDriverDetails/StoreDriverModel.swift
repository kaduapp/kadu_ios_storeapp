//
//  StoreDriverModel.swift
//  DelivX Store
//
//  Created by Vengababu Maparthi on 09/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

struct StoreDataModel:Equatable {
    
    
    var storeName = ""
    var storeID = ""

    init(data:[String:Any]) {
        
        if let storeNam = data["storeName"] as? String {
            storeName = storeNam
        }
        
        if let stoID = data["id"] as? String {
            storeID = stoID
        }
    }

}

struct DriverData:Equatable {
    
    var driverName = ""
    var driverImage = ""
    var driverPhone = ""
    
    init(data:[String:Any]) {
        
        if let driverNam = data["name"] as? String {
            driverName = driverNam
        }
        
        if let driverImg = data["image"] as? String {
            driverImage = driverImg
        }
        
        if let driverPhon = data["phone"] as? String {
            driverPhone = driverPhon
        }
    }
}

class StoreDriverModel {
    
    var driverDetails = [Driver]()
    var storeDetails = [StoreDataModel]()
    
    func getTheStoreData(storeData:[String:Any]) -> [StoreDataModel] {
        storeDetails = [StoreDataModel]()
        if let response = storeData["data"] as? [[String:Any]]  {
            self.storeDetails = response.map{
                StoreDataModel.init(data: $0)
            }
        }
        return storeDetails
    }
    
    func getTheDriversData(driverData:[String:Any]) -> [Driver] {
        driverDetails = [Driver]()
        if let response = driverData["availableDriver"] as? [[String:Any]]  {
            self.driverDetails = response.map{
                Driver.init(driver: $0)
            }
        }
        return driverDetails
    }

}

