//
//  DriverTableViewCell.swift
//  DelivX Store
//
//  Created by Vengababu Maparthi on 09/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class DriverTableViewCell: UITableViewCell {

    @IBOutlet weak var driverNumber: UILabel!
    @IBOutlet weak var driveName: UILabel!
    @IBOutlet weak var driverImage: CustomImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    static var identifier: String {
        return String(describing:self)
    }
    
    func updateTheDriverDetails(data:Driver) {
        driveName.text = data.driverName
        driverNumber.text = data.driverPhoneNumber
        self.driverImage.setImage(url: data.driverImage)
    }
}
