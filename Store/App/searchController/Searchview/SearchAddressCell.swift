//
//  SearchAddressCell.swift
//  DayRunner
//
//  Created by Rahul Sharma on 15/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit

class SearchAddressCell: UITableViewCell {


    @IBOutlet weak var addressLine1: UILabel!

    @IBOutlet weak var addressLine2: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
