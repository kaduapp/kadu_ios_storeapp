//
//  SearchViewModel.swift
//  DelivX Store
//
//  Created by Vengababu Maparthi on 24/08/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation



import Foundation
import RxSwift
import SearchTextField

class SearchViewModel:NSObject {
    
    let disposeBag = DisposeBag()
    
    let addressData = PublishSubject<(Bool, [Any])>()
    
    let placeIDData = PublishSubject<(Float, Float)>()
    
    func loadAddress(addressText:String) {
        
        let header = APIUtility.getHeaderForOrders()
        
        let latitude = 13.34553
        let longitude = 77.235432
        let language = "en"
        let key = "AIzaSyBlvz3SXKry5AS7A_BHGixOqijuGjlBkhM" //Key
        
        let getOrdersUrl = String(format: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&location=%f,%f&radius=500&amplanguage=%@&key=%@" ,addressText,latitude,longitude,language,key)
        
        APIHandler.getRequest(header:header, url:getOrdersUrl).subscribe(onNext: {[weak self]  success, data in
            if success == true {
                self?.passAddressData(addData: data as! [String:Any])
            }else {
                self?.addressData.onNext((false,[]))
            }
            }, onError: { Error in
                print(Error)
                self.addressData.onNext((false,[]))
        }).disposed(by: disposeBag)
    }
    
    func loadAddressPlaceID(placedID:String) {
        
        let header = APIUtility.getHeaderForOrders()
 
        let key = "AIzaSyCEseJ5-b9bWvyaWMU-VuUqoXdzqHEOYFw"
        
        let getOrdersUrl = String(format: "https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=%@",placedID,key)
        
        APIHandler.getRequest(header:header, url:getOrdersUrl).subscribe(onNext: {[weak self]  success, data in
            if success == true {
                self?.passPlaceIDData(placeData: data as! [String:Any])
                
            }else {
                self?.placeIDData.onNext((13.234,77.2345))
            }
            }, onError: { Error in
                print(Error)
                self.placeIDData.onNext((13.234,77.2345))
        }).disposed(by: disposeBag)
    }
    
    func passAddressData(addData:[String:Any]) {
        if let array = addData["predictions"] as? [Any] {
            self.addressData.onNext((true,array))
        }
    }
    
    func passPlaceIDData(placeData:[String:Any]) {
        
        if let dictionary = placeData["result"] as? [String:Any]{
            
            let locationDict: [String: Any] = dictionary["geometry"] as! [String : Any]
            let obj = locationDict["location"] as! [String:Any]
            if let lat = obj["lat"] as? NSNumber{
                if let log = obj["lng"] as? NSNumber{
                    self.placeIDData.onNext((Float(truncating: lat), Float(truncating: log)))
                }
            }
        }
    }
}
