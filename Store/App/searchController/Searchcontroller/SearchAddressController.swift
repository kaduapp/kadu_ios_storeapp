//
//  SearchAddressController.swift
//  DayRunner
//
//  Created by Rahul Sharma on 15/04/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import GooglePlaces
import RxSwift
import RxCocoa




protocol SearchAddressControllerDelegate {
    func didSelectAddress(lat: Float, long: Float, address1 : String, address2: String)
}

struct Address {
    
    var line1 = ""
    var line2 = ""
    var latitude = 0.0
    var longitude = 0.0
    var placeID = ""
    var referenceID = ""
    var type = ""
    var postalCode = ""
}


class SearchAddressController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var searchBar: UISearchBar!

    @IBOutlet weak var addressLine2TV: UITextView!
    
    var searchActive : Bool = false
    
    var arrayOfLocation: [Any] = []
    var arrayOfRecent: [Any] = []
    let disposeBag = DisposeBag()
    
    var delegate: SearchAddressControllerDelegate? = nil
    
    //    let address = SearchAddressModel()
    var indexSelected: Int = 0
    var extraAddress = ""
    
    var dropLatitude: Float = 0.00
    var dropLongitude: Float = 0.00
    var dropAddress = ""
    var searchModel = SearchViewModel()
    var selectedIndex = -1
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        tableView.estimatedRowHeight = 10
        tableView.rowHeight = UITableViewAutomaticDimension
        updateUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.searchModel.addressData.subscribe(onNext: { data in
            if data.0{
                self.arrayOfLocation = data.1
            }
             self.tableView.reloadData()
        }).disposed(by: self.disposeBag)
        
        self.searchModel.placeIDData.subscribe(onNext: { data in
            var addAddres = self.modify(addressDict: self.arrayOfLocation[self.selectedIndex] as! [String : AnyObject])
            addAddres.latitude = Double(data.0)
            addAddres.longitude = Double(data.1)
            
            self.delegate?.didSelectAddress(lat: Float(addAddres.latitude),
                                            long: Float(addAddres.longitude),
                                            address1: addAddres.line1 + addAddres.line2,
                                            address2: self.extraAddress)
            
            self.dismiss(animated: true, completion: nil)
        }).disposed(by: self.disposeBag)
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// Update UI
    fileprivate func updateUI() {
        
        navigationItem.title = "Pickup Location"
    }
    
    
    //MARK: - Google Auto Complete
    
    /// Call Google Auto Complete API
    ///
    /// - Parameter searchText: Words to be Searched
    func search(searchText: String) {
        
        // Remove Blank space from word
        let text = searchText.replacingOccurrences(of: " ", with: "")
        if text.count == 0 {
            self.arrayOfLocation = []
            self.tableView.reloadData()
        }
  
        searchModel.loadAddress(addressText: text)
        
    }
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func setLocationOnMap(_ sender: Any) {
        //        self.performSegue(withIdentifier: "selectPinSegue", sender:self)
    }
    
    /// Modify the Details from Google
    ///
    /// - Parameter array: Array of Google Search
    /// - Returns: Array Of Address
    
    func modify(addressDict: [String: AnyObject]) -> Address {
        
        // Take an temp Dict
        var addTitle: String = ""
        var addAddress: String = ""
        let addLatitude: Float = 0.0
        let addLongitude: Float = 0.0
        let addPlaceID: String = addressDict["place_id"] as! String
        let addReferenceID: String = addressDict["reference"] as! String
        
        
        
        let dict: [String: AnyObject] = addressDict["structured_formatting"] as! [String : AnyObject]
        
        // Get Address Line 1 and Matching offset and Length
        if let name = dict["main_text"] as! String? {
            
            addTitle = name
        }
        
      //  self.getAddressDetails(with: addPlaceID)
        
        
        // Get Address Line 2
        if let add = dict["secondary_text"] as! String? {
            addAddress = add
        }
        
        return Address(line1: addTitle,
                       line2: addAddress,
                       latitude: Double(addLatitude),
                       longitude: Double(addLongitude),
                       placeID: addPlaceID,
                       referenceID: addReferenceID,
                       type: "123435",
                       postalCode:"123456")
    }
    
    
    
    /// Invoke Google API to get Address Details
    ///
    /// - Parameters:
    ///   - placeID: Place Id of Selected address
    ///   - completion: Result with Lat and Long
    func getAddressDetails(with placeID: String){
        self.searchModel.loadAddressPlaceID(placedID:placeID)
    }
    
    
    @IBAction func backAction(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
}

extension SearchAddressController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(searchActive)
        {
            if arrayOfLocation.count != 0
            {
                return arrayOfLocation.count
            }
            else
            {
                return 0
            }
            
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell: SearchAddressCell = (tableView.dequeueReusableCell(withIdentifier: "SearchAddressCell") as! SearchAddressCell?)!
        if(searchActive)
        {
            if arrayOfLocation.count != 0
            {
                
                let addAddres = modify(addressDict: arrayOfLocation[indexPath.row] as! [String : AnyObject])
                cell.addressLine1.text = addAddres.line1
                cell.addressLine2.text = addAddres.line2
                return cell
            }
        }
        else
        {
            cell.addressLine1.text = ""
            cell.addressLine2.text = ""
            return cell
        }
        
        return cell
    }
}


extension SearchAddressController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        selectedIndex = indexPath.row
        let addAddres = self.modify(addressDict: self.arrayOfLocation[indexPath.row] as! [String : AnyObject])
        getAddressDetails(with: addAddres.placeID)
    }
}

extension SearchAddressController: UISearchBarDelegate
{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        }
        func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
            
            searchBar.text = ""
            searchBar.showsCancelButton = true
            searchActive = true;
        }
        func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
            
            searchActive = false;
            searchBar.showsCancelButton = false
        }
        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            
            searchBar.text = ""
            searchActive = false;
            searchBar.endEditing(true)
        }
        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       
            search(searchText: searchBar.text!)
            searchBar.showsCancelButton = true
        }
}

extension SearchAddressController:UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "eg: House name, street name, neighborhood, landmark."{
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "eg: House name, street name, neighborhood, landmark."
            textView.textColor = UIColor.lightGray
        }else{
            if textView.text  != "eg: House name, street name, neighborhood, landmark."{
               extraAddress = textView.text
            }
        }
    }
    
    ///MARK: - This func Count the Total characters for AboutMeController Text.
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
}

