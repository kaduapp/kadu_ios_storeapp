//
//  OrderHistoryModalView.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 05/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxSwift
import CocoaLumberjack


enum OrderParseType {
    case AllOrders
    case OrdersByDates
    case OrdersBySearch
}

class OrderHistoryModalView {

    var historyOrders = [ManageOrder]()
    var historyOrdersSections = [[ManageOrder]]()
    var showOrderByDates = false
    var showSearchedOrders = false
    var limit  = 20
    
    private var orderParseType = OrderParseType.AllOrders
    private let disposeBag = DisposeBag()
    let orderHistor_response = PublishSubject<(Bool,[[ManageOrder]])>()
    
    
    // MARK:- date wise , all orders , search wise
    func makeDateSections(_ ordersArray: [ManageOrder]) -> [[ManageOrder]] {
        
        var sectionsArray = [[ManageOrder]]()
        let datesArray = ordersArray.compactMap { $0.orderSummary.date } // return array of date
        
        let result = datesArray.unique()
        
        for date in result {
            let dateKey = date
            let filterArray = ordersArray.filter { $0.orderSummary.date == dateKey }
            sectionsArray.append(filterArray)
        }
        
        return sectionsArray
    }
    
    public func getOrderHistory(_ pagingIndex: Int, from:String = "0",  to:String = "0" , searchText:String = "") {
        
        let header = APIUtility.getHeaderForOrders()
        let storeID = StoreUtility.getStore().storeId
        let cityID = StoreUtility.getStore().cityId
        var getOrderHistory = ""
        
        if searchText == ""{
            getOrderHistory = "\(APIEndTails.OrderHistoryService)\(cityID)/\(storeID)/\(pagingIndex)/\(from)/\(to)/\(0)"
        }else{
            getOrderHistory = "\(APIEndTails.OrderHistoryService)\(cityID)/\(storeID)/\(pagingIndex)/\(from)/\(to)/\(searchText)"
        }
       
        APIHandler.getRequest(header:header, url:getOrderHistory).subscribe(onNext: { success, data in
            if success == true {
                self.historyOrdersSections.removeAll()
                if pagingIndex == 0{
                    self.historyOrders.removeAll()
                }
                if let ordersDist = data as? [String:Any], let pastOrders = ordersDist["pastOrders"] as? [[String:Any]] {
                    let  orders = pastOrders.map {
                         ManageOrder.init(data: $0)
                    }
                self.historyOrders.append(contentsOf: orders)
                self.historyOrdersSections = self.makeDateSections(self.historyOrders)
                }
        self.orderHistor_response.onNext((true,self.historyOrdersSections))
            }else {
              Helper.hidePI()
            }
            }, onError: { Error in
              Helper.hidePI()
        }).disposed(by: disposeBag)
    }
    
    
    public func getCustomerOrderHistory(_ pagingIndex: Int, customerId:String) {
        
        let header = APIUtility.getHeaderForOrders()
        
        let getOrderHistory = "\(APIEndTails.CustomerPastOrders)\(customerId)/\(pagingIndex)"
        
        APIHandler.getRequest(header:header, url:getOrderHistory).subscribe(onNext: { success, data in
            if success == true {
                self.historyOrdersSections.removeAll()
                if pagingIndex == 0{
                    self.historyOrders.removeAll()
                }
                if let ordersDist = data as? [String:Any], let pastOrders = ordersDist["orders"] as? [[String:Any]] {
                    let  orders = pastOrders.map {
                        ManageOrder.init(data: $0)
                    }
                    self.historyOrders.append(contentsOf: orders)
                    self.historyOrdersSections = self.makeDateSections(self.historyOrders)
                }
            self.orderHistor_response.onNext((true,self.historyOrdersSections))
            }else {
                Helper.hidePI()
            }
        }, onError: { Error in
            Helper.hidePI()
        }).disposed(by: disposeBag)
    }
    
}

