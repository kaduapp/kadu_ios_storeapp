//
// HistoryViewController.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 05/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift

class HistoryViewController: UIViewController {
    
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var storeEarningTitle: UILabel!
    @IBOutlet weak var storeEarningValue: UILabel!
    @IBOutlet weak var orderSummaryTableView: OrderHistoryTableView!
     @IBOutlet weak var ipadHistoryItemsTV: iPadHistoryItemsTableView!
     @IBOutlet weak var ipadHistoryTaxesTV: iPadHistoryTaxesTableView!
    
    @IBOutlet weak var searchOrder: UISearchBar!
    let disposeBag = DisposeBag()
    private var isDateSearchEnable = false
    var orderSections : [[ManageOrder]] = []
        let searchText = Variable<String?>(nil)

    /// Section of searched Order Id
    var searchOrderSections : [[ManageOrder]] = []
    var selectedOrder = ManageOrder(data: [:])
    var selectedId = "0"
    var searchString = "0"
    var endDate:Date!
    var startDate:Date!
    var fromStore = false
    var pagingIndex = 0
    var historyVM = OrderHistoryModalView()
    
    /// For customer history this will be true
    var isCustomer = false
    var customerId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchOrder.placeholder =  StringConstants.searchBy()
        addShadowToBar()
         self.navigationItem.title = LeftMenuData.Data[LeftMenu.historyVC.rawValue].MenuText
        if isCustomer{
        self.navigationItem.rightBarButtonItem = nil
        self.setNavigationBackButton()
        self.searchOrder.isUserInteractionEnabled = true
        }
        else{
        self.setNavigationBarItem()
        searchOrdersSubscribe()
        }
        Helper.showPI(string: "")
        setSubscriberForHistoryResponse()
        getOrderHistory()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    @IBAction func pressedFilterButton(_ sender: UIButton) {

        let storyBorad1 = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil)
        let selector = storyBorad1.instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
        // 2. You can then set delegate, and any customization options
        selector.optionSelectionType = WWCalendarTimeSelectorSelection.range
        selector.optionButtonShowCancel = true
        selector.optionTintColor = UIColor(named: "ThemeColor")!
        selector.optionButtonFontColorDone = .black
        selector.optionMultipleSelectionGrouping = .pill
        selector.optionTopPanelTitle = "Select Date"
        selector.delegate = self
        
        // 3. Then you simply present it from your view controller when necessary!
        self.present(selector, animated: true, completion: nil)

    }

    
    
    /// Method for search an order from order list
    func searchOrdersSubscribe() {
   searchOrder.rx.text.asDriver()
      .drive(searchText)
      .disposed(by: disposeBag)

  searchText.asObservable().subscribe(onNext: { [weak self] (text) in
      if let welf = self, welf.searchOrder.text != text {
          welf.searchOrder.text = text
      }
  })
      .disposed(by: disposeBag)

//        searchOrder
//            .rx.text
//            .orEmpty
//            .debounce(1, scheduler: MainScheduler.instance) // Wait 1 for changes.
//            .distinctUntilChanged() // If they didn't occur, check if the new value is the same as old.
//            .subscribe(onNext: { [weak self] query in // Here we subscribe to every new value
        searchText.value = searchOrder.text
        searchText.asObservable().subscribe(onNext: { [weak self] query in // Here we subscribe to every new value

                print("new order search text :\(query)")
            if query?.count ?? 0 > 0{
                self?.searchString = query ?? ""
                }else{
                    self?.searchString = "0"
                }
                self?.pagingIndex = 0
                if !(self?.searchOrder.isUserInteractionEnabled)! {
                    self?.searchOrder.isUserInteractionEnabled = true
                }else {
               
                    if self?.endDate == nil ||  self?.startDate == nil {
                        self?.historyVM.getOrderHistory(self?.pagingIndex ?? 0, from: "0", to: "0", searchText: self?.searchString ?? "0")
                       
                    }else{
                        
                         self?.historyVM.getOrderHistory(self?.pagingIndex ?? 0,from: Date.getDateString(value: self?.startDate ?? Date(), format: DateFormat.DateFormatServer), to: Date.getDateString(value: self?.endDate  ?? Date() , format: DateFormat.DateFormatServer) , searchText: self?.searchString ?? "0")
                        
                    }
                }
            })
            .disposed(by: disposeBag)
    }
    

   
    
    /// call api and get history orders pass the index for pagination
    func getOrderHistory() {
        if isCustomer{
            historyVM.getCustomerOrderHistory(pagingIndex, customerId: customerId)
        }else{
        historyVM.getOrderHistory(pagingIndex,searchText: searchString)
        }
    }
    
    func setSubscriberForHistoryResponse(){
    self.historyVM.orderHistor_response.subscribe(onNext:{ success in
            if success.0 {
                self.orderSections = success.1
                self.orderSummaryTableView.reloadData()
                if !self.isIphone(){
                  
                    if self.orderSections.count > 0,self.orderSections[0].count > 0{
                      self.selectedOrder = self.orderSections[0][0]
                      
                        DispatchQueue.main.async {
                            self.ipadHistoryItemsTV.reloadData()
                        }
                        DispatchQueue.main.async {
                            self.ipadHistoryTaxesTV.reloadData()
                        }
                    }
                    self.storeEarningValue.text = "\(self.selectedOrder.orderSummary.currencySymbol) \(self.selectedOrder.orderSummary.storeEarningValue)"
                }
            Helper.hidePI()
            }
        }).disposed(by: disposeBag)
    }
}


extension HistoryViewController : WWCalendarTimeSelectorProtocol{
   
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")

    }
    
    func WWCalendarTimeSelectorDone(_ selector: Bool ){
        pagingIndex = 0
        startDate = nil
        endDate = nil
        isDateSearchEnable = false
        getOrderHistory()
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
        startDate = dates.first
        endDate = dates.last
        isDateSearchEnable = true
        
        if (startDate != nil) && (endDate != nil) {
            if endDate.compare(startDate) == .orderedAscending {
            }else{
                
              pagingIndex = 0
                if startDate == endDate {
                    
                 
                    self.historyVM.getOrderHistory(pagingIndex, from: Date.getDateString(value: startDate, format: DateFormat.DateFormatServer),  to: Date.getDateString(value: endDate, format: DateFormat.DateFormatServer))
                    

                }else{
                    
                    self.historyVM.getOrderHistory(pagingIndex, from: Date.getDateString(value: startDate, format: DateFormat.DateFormatServer),  to: Date.getDateString(value: endDate, format: DateFormat.DateFormatServer))
                    
                }
            }
        }
    }
}
