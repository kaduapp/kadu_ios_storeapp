//
//  OrderHistoryViewController.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 15/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit


class OrderHistoryTableView: UITableView {
    override func awakeFromNib() {
        super.awakeFromNib()

        register(HistorySectionHeader.nib, forCellReuseIdentifier: HistorySectionHeader.identifier)
        
        self.register(UINib.init(nibName: "PastOrderDetailsTVCell", bundle: nil) , forCellReuseIdentifier: "PastOrderDetailsTVCell")
    
    }
}

class iPadHistoryItemsTableView: UITableView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.register(ItemListTablePortionCell.nib, forCellReuseIdentifier: ItemListTablePortionCell.identifier)
        self.register(ItemListTableHeaderCell.nib, forCellReuseIdentifier: ItemListTableHeaderCell.identifier)
    }
}

class iPadHistoryTaxesTableView: UITableView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.register(OrderAmountSummaryCell.nib, forCellReuseIdentifier: OrderAmountSummaryCell.identifier)
    }
}

extension HistoryViewController : UITableViewDataSource, UITableViewDelegate{
    // There is one TableView for iPhone with tag 0
    // There are two three table view for iPad with tag 0,1,2
    // In case of customer, search will be local,
    // Case is handled for search order with searchOrderSections
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView.tag {
        case 0:
            if isCustomer && searchOrderSections.count > 0{
             return searchOrderSections.count
            }
            return orderSections.count
        default:
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView.tag {
        case 0:
            if isCustomer && searchOrderSections.count > 0{
              return searchOrderSections[section].count
            }
            return orderSections[section].count
        case 1:
           return selectedOrder.itemsList.count
        case 2:
            let order = ManageOrderAmount.init(homeOrderModal: selectedOrder)
            return order.excTaxes.count + order.amountArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "PastOrderDetailsTVCell") as! PastOrderDetailsTVCell
            var order = OrderSummary.init(data: [:])
            if isCustomer && searchOrderSections.count > 0{
              order = searchOrderSections[indexPath.section][indexPath.row].orderSummary
            }
            else{
              order = orderSections[indexPath.section][indexPath.row].orderSummary
            }
            cell.setDataToCell(item:order )
            
                if !self.isIphone(), selectedOrder.orderSummary.orderId == order.orderId {
                 cell.makeSelected()
                    }else {
                 cell.makeDeselected()
                }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: ItemListTablePortionCell.identifier) as! ItemListTablePortionCell
            
            cell.quantity.text = "\(indexPath.row + 1)"
             cell.item = self.selectedOrder.itemsList[indexPath.row]
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: OrderAmountSummaryCell.identifier)as! OrderAmountSummaryCell
              let amountObj = ManageOrderAmount.init(homeOrderModal: selectedOrder)
             cell.setOrderAmount(amount: amountObj, index: indexPath.row)
            
            return cell
        default:
            return UITableViewCell.init()
        }
     

        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        switch tableView.tag {
        case 0:
            let headerViewCell = tableView.dequeueReusableCell(withIdentifier: HistorySectionHeader.identifier)as! HistorySectionHeader
            var date = Date()
            if isCustomer && searchOrderSections.count > 0{
                date = searchOrderSections[section][0].orderSummary.date
            }
            else{
             date = orderSections[section][0].orderSummary.date
            }
            headerViewCell.headerText.text =  Helper.getDayOfWeek(date)
            return headerViewCell
            
        case 1:
            let headerViewCell = tableView.dequeueReusableCell(withIdentifier: ItemListTableHeaderCell.identifier)as! ItemListTableHeaderCell
            return headerViewCell
        default:
            return nil
         
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch tableView.tag {
        case 0,1:
            return 40
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case 0:
            if isIphone(){
         let orderDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsViewController") as! OrderDetailsViewController
            if isCustomer && searchOrderSections.count > 0{
             orderDetailsVC.orderDetails = self.searchOrderSections[indexPath.section][indexPath.row]
            }
            else{
            orderDetailsVC.orderDetails = self.orderSections[indexPath.section][indexPath.row]
            }
        self.navigationController?.pushViewController(orderDetailsVC, animated: true)
            }
            else if selectedOrder.orderSummary.orderId != orderSections[indexPath.section][indexPath.row].orderSummary.orderId  {
                
             selectedOrder = orderSections[indexPath.section][indexPath.row]
             if isCustomer && searchOrderSections.count > 0{
                selectedOrder = orderSections[indexPath.section][indexPath.row]
              }
             self.orderSummaryTableView.reloadData()
                DispatchQueue.main.async {
                    self.ipadHistoryItemsTV.reloadData()
                }
                DispatchQueue.main.async {
                    self.ipadHistoryTaxesTV.reloadData()
                }
            self.storeEarningValue.text = "\(self.selectedOrder.orderSummary.currencySymbol) \(self.selectedOrder.orderSummary.storeEarningValue)"
            }
        default:
            print("No Action")
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if searchOrderSections.count == 0 && indexPath.section == orderSections.count - 1 && historyVM.historyOrders.count > (historyVM.limit * pagingIndex) + (historyVM.limit - 1) {
            pagingIndex = pagingIndex + 1
            self.getOrderHistory()
         }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Hide keyboard on scrolling
       self.view.endEditing(true)
    }
}


// MARK: - Search bar observers
extension HistoryViewController:UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if isCustomer{
         searchOrderSections.removeAll()
         let orders = historyVM.historyOrders
         let orderId = searchText.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator:"")
        if orderId.count > 0{
            let IdFiltered = orders.filter {"\($0.orderSummary.orderId)".contains(searchText)}
             self.searchOrderSections = self.historyVM.makeDateSections(IdFiltered)
         }
        self.orderSummaryTableView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }

}

