//
//  OrderDetailsViewController.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 15/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class OrderDetailsViewController: UIViewController {
 
    @IBOutlet weak var navTitle: UILabel!
    var orderDetails = ManageOrder.init(data: [:])
    @IBOutlet weak var orderDetailsTableView: OrderSummaryTV!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    self.navTitle.text =  "\(orderDetails.orderSummary.storeTypeMsg) Order Id \n \(orderDetails.orderSummary.orderId)"
     self.navigationController?.navigationBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}


class OrderSummaryTV: UITableView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.register(OrderHeaderFooterTVCell.nib, forCellReuseIdentifier: OrderHeaderFooterTVCell.identifier)
        self.register(ItemListTablePortionCell.nib, forCellReuseIdentifier: ItemListTablePortionCell.identifier)
        self.register(OrderAmountSummaryCell.nib, forCellReuseIdentifier: OrderAmountSummaryCell.identifier)
          self.register(ItemListTableHeaderCell.nib, forCellReuseIdentifier: ItemListTableHeaderCell.identifier)
        
    }
}

extension OrderDetailsViewController : UITableViewDataSource, UITableViewDelegate{
    // There is one TableView for iPhone with tag 0
    // There are two three table view for iPad with tag 0,1,2
    func numberOfSections(in tableView: UITableView) -> Int {
    
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        switch section {
        case 0,3: // Case for Header Footer
            return 3
        case 1:
            return self.orderDetails.itemsList.count
        case 2:
            let order = ManageOrderAmount.init(homeOrderModal: orderDetails)
            return   order.excTaxes.count + order.amountArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.section{
        case 0,3:
            let cell = tableView.dequeueReusableCell(withIdentifier: OrderHeaderFooterTVCell.identifier) as! OrderHeaderFooterTVCell
            cell.setHeaderFooterViews(index: indexPath,order:orderDetails)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: ItemListTablePortionCell.identifier) as! ItemListTablePortionCell
            
            cell.quantity.text = "\(indexPath.row + 1)"
            cell.item = self.orderDetails.itemsList[indexPath.row]
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: OrderAmountSummaryCell.identifier)as! OrderAmountSummaryCell
            let amountObj = ManageOrderAmount.init(homeOrderModal: orderDetails)
            cell.setOrderAmount(amount: amountObj, index: indexPath.row)
            
            return cell
        default:
            return UITableViewCell.init()
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 1:
            let headerViewCell = tableView.dequeueReusableCell(withIdentifier: ItemListTableHeaderCell.identifier)as! ItemListTableHeaderCell
            headerViewCell.setHeader(orderDetails.itemsList.count)
            return headerViewCell
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        switch section {
        case 1:
            return 40
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        // Hide unhide delivered by field
        if indexPath.section == 3 && indexPath.row == 1{
            if orderDetails.orderSummary.serviceType == .Pickup || orderDetails.orderSummary.driverDetails == nil {
              return 0
            }
        }
        return  UITableViewAutomaticDimension
    }
}

