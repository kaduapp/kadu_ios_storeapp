//
//  PastOrderDetailsTVCell.swift
//  DelivX Store
//
//  Created by 3Embed on 10/05/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class PastOrderDetailsTVCell: UITableViewCell {

    @IBOutlet weak var lbl_OrderId: UILabel!
    @IBOutlet weak var lbl_Price: UILabel!
    
    @IBOutlet weak var lbl_StoreNameTemp: UILabel!
    @IBOutlet weak var lbl_StoreName: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var lbl_DateTime: UILabel!
    
    @IBOutlet weak var lbl_OrderStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.shadowView.layer.cornerRadius = 4
        self.shadowView.layer.shadowColor = UIColor.gray.cgColor
        self.shadowView.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.shadowView.layer.shadowRadius = 2
        self.shadowView.layer.shadowOpacity = 1.0
        lbl_OrderStatus.layer.masksToBounds = true
        lbl_OrderStatus.layer.cornerRadius = 5.0
    }
    
    func setDataToCell(item : OrderSummary ){
            lbl_OrderId.text = "OID : \(item.orderId)"
            lbl_Price.text = "\(item.currencySymbol) \(item.totalAmount)"
            lbl_StoreName.text = item.customerName
            lbl_DateTime.text = item.bookingDateTime
            lbl_OrderStatus.text = item.orderStatusMsg
            lbl_StoreNameTemp.text =  item.storeName
            orderStatus(status: item.orderStatus)
    }
    
    
    
     private func orderStatus(status:OrderStatus.RawValue)  {
        switch status {
        case OrderStatus.DriverJobFinished.rawValue,OrderStatus.OrderCompleted.rawValue:
            self.lbl_OrderStatus.backgroundColor = UIColor.colorWithHex(color: "289B24")
            
        default:
            self.lbl_OrderStatus.backgroundColor = UIColor.red
        }
    }

    
    
    func makeSelected() {
        self.shadowView.backgroundColor = UIColor.colorWithHex(color: "263238")
        changeTextColor(color: UIColor.white)
    }
    
    func makeDeselected() {
        self.shadowView.backgroundColor = UIColor.colorWithHex(color: "FFFFFF")
        changeTextColor(color: UIColor.colorWithHex(color: "1A1B1C"))
    }
    
    
    func changeTextColor(color : UIColor){
        
        self.lbl_OrderId.textColor = color
        self.lbl_Price.textColor = color
        self.lbl_StoreNameTemp.textColor = color
        self.lbl_StoreName.textColor = color
        self.lbl_DateTime.textColor = color
        
    }
  
    
    
}

    

