//
//  OrderHeaderFooterTVCell.swift
//  Ufy_Store
//
//  Created by Rahul Sharma on 27/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class OrderHeaderFooterTVCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName:self.identifier, bundle:nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setHeaderFooterViews(index : IndexPath, order : ManageOrder){
        
        if index.section == 0{
            switch index.row{
            case 0:
                topView.isHidden = true
                bottomView.isHidden = false
                title.text = StringConstants.CustomerName()
                value.text = order.customerData.customerName
            case 1:
                topView.isHidden = true
                bottomView.isHidden = false
                title.text = StringConstants.OrderType()
                value.text = order.orderSummary.storeTypeMsg
            case 2:
                topView.isHidden = true
                bottomView.isHidden = true
                title.text = StringConstants.OrderDetails()
                value.text = ""
            default:
                topView.isHidden = true
                bottomView.isHidden = true
                title.text = ""
                value.text = ""
            }
        }
        else{
            switch index.row{
            case 0:
                topView.isHidden = false
                bottomView.isHidden = false
                title.text = StringConstants.Status()
                    value.text = order.orderSummary.orderStatusMsg
            case 1:
                topView.isHidden = true
                bottomView.isHidden = false
                title.text = StringConstants.DeliveredBy()
                value.text = ""
                if order.orderSummary.driverDetails != nil {
                value.text = order.orderSummary.driverDetails.driverName
                }
            case 2:
                topView.isHidden = true
                bottomView.isHidden = true
                title.text = StringConstants.StoreEarning()
                    value.text = "\(order.orderSummary.currencySymbol) \(order.orderSummary.storeEarningValue)"
            default:
                title.text = ""
                value.text = ""
            }
        }
    }
}
