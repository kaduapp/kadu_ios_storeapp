//
//  HistorySectionHeader.swift
//  DelivX Store
//
//  Created by 3Embed on 12/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit


class HistorySectionHeader: UITableViewCell  {

    @IBOutlet weak var headerText : UILabel!
    
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName:HistorySectionHeader.identifier, bundle:nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
