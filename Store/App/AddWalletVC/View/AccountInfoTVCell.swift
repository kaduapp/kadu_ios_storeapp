//
//  AccountInfoTVCell.swift
//  DelivX Store
//
//  Created by 3Embed on 27/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

typealias accountInfoTVCellConfig = TableCellConfigurator<AccountInfoTVCell , (String , String , Bool)>


class AccountInfoTVCell: UITableViewCell , ConfigurableCell {

    
    @IBOutlet weak var saperatorLeftAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var saperatorRightAnchor: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_Header: UILabel!
    
    @IBOutlet weak var lbl_Info: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       self.selectionStyle = .none
    }

    func configure(data:(String , String , Bool) ) {
    
        if data.2{
            saperatorLeftAnchor.constant = 10.0
            saperatorRightAnchor.constant = 10.0
        }
        
        lbl_Header.text = data.0
        lbl_Header.textAlignment = (data.0 == "" ? .center : .left )
        lbl_Info.textAlignment = (data.0 == "" ? .center : .left)
        lbl_Info.text = data.1
        
    }
    
    
   
    
}
