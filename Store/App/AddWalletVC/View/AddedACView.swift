//
//  AddedACView.swift
//  DelivX Store
//
//  Created by 3Embed on 27/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

protocol AddedACViewDelegate : class {
    func pressedHiddenButton()
}

class AddedACView: UIView {

    weak var delegate : AddedACViewDelegate?
    @IBOutlet weak var hiddenButton: UIButton!
    
    @IBOutlet weak var lbl_AccountHolder: UILabel!
    @IBOutlet weak var lbl_AccountNo: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet var containerView: UIView!
    static var identifier:String {
        return String(describing: self)
    }
    

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        UINib(nibName:"AddedACView", bundle:nil).instantiate(withOwner: self, options: nil)
        containerView.frame = self.bounds
        addSubview(containerView)
        
        shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        shadowView.layer.shadowOpacity = 2.0
        shadowView.layer.shadowColor = UIColor.lightGray.cgColor
        shadowView.layer.shadowRadius = 3.0
        
    }
    
    
    func setDataToView( accountNo : String  , accountHolder : String){
        lbl_AccountNo.text = accountNo
        lbl_AccountHolder.text = accountHolder
    }
    
    @IBAction func pressedHiddenButton(_ sender: UIButton) {
        delegate?.pressedHiddenButton()
    }
    
    
    
}
