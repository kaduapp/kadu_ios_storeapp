//
//  AddACView.swift
//  DelivX Store
//
//  Created by 3Embed on 25/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit


protocol AddACViewDelegate : class {
    func pressedButton(sender : UIButton )
}

class AddACView: UIView {

    @IBOutlet weak var lbl_Msg2: UILabel!
    
    @IBOutlet weak var lbl_Varified: UILabel!
    @IBOutlet weak var btn_Verified: UIButton!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var lbl_Msg: UILabel!
    @IBOutlet weak var btn: UIButton!
    let themeColor = Helper.getUIColor(color:  Colors.AppBaseColor)

    weak var delegete : AddACViewDelegate?
    static var identifier:String {
        return String(describing: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        UINib(nibName:"AddACView", bundle:nil).instantiate(withOwner: self, options: nil)
        containerView.frame = self.bounds
        addSubview(containerView)
        
        shadowView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        shadowView.layer.shadowOpacity = 2.0
        shadowView.layer.shadowColor = UIColor.lightGray.cgColor
        shadowView.layer.shadowRadius = 3.0
        
    }
    
    @IBAction func pressedButton(_ sender: UIButton) {
        delegete?.pressedButton( sender : sender)
    }
    
    func isVarified( text : String ,  hidden : Bool){
        
        lbl_Varified.text = text.capitalized
        lbl_Varified.isHidden = hidden
        btn_Verified.isHidden = hidden
    }
    
    func setButtonText( title : String  , color : UIColor ){
        
        if title == ""{
            btn.isHidden = true
            lbl_Msg2.isHidden = false
        }else{
            lbl_Msg2.isHidden = true
            btn.setTitle(title, for: .normal)
            btn.setTitleColor(color, for: .normal)
            btn.layer.cornerRadius = 3.0
            btn.layer.borderColor = (color).cgColor
            btn.layer.borderWidth = 1.0
        }
        btn.isUserInteractionEnabled = true
        if color == .gray{
            btn.isUserInteractionEnabled = false
        }
    }
    
}
