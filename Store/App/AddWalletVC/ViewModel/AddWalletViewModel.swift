//
//  AddWalletViewModel.swift
//  DelivX Store
//
//  Created by 3Embed on 26/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
class AddWalletViewModel {
    let disposeBag = DisposeBag()
    private var verficationStatus =  false
    
    private var accountModelArray = [AccountModel]()
    private var verifiedText = ""
    func getRegisterStripeAccount( onCompletionHandler:@escaping(Bool  , [AccountModel]) -> Void ){
      
        let header = APIUtility.getHeaderForOrders()
        let url = APIEndTails.accountConnect + "/" + StoreUtility.getStore().storeId
        APIHandler.getRequest(header: header , url: url).subscribe(onNext: {   success, data in
            if success {
                print(data)
                self.convertDataToObjects(data: data)
                onCompletionHandler(true , self.accountModelArray)
            }else {
                onCompletionHandler(false ,self.accountModelArray )
            }
            }, onError: { Error in
                    print(Error)
            }).disposed(by: disposeBag)
    }

    
    
    private func convertDataToObjects(data : Any){
        
        if let dict = data as? [String : Any]{
            if let external_accounts = dict["external_accounts"] as? [String : Any]{
                print(external_accounts)
                if let data = external_accounts["data"] as? [[String : Any]]{
                    print(data)
                    
                    for item in data{
                        accountModelArray.append(AccountModel.init(accountHolder: item["account_holder_name"] as? String ?? "",
                            accountNumber:"xxxxxxxx\( item["last4"] as? String ?? "xxxx" )",
                            routingNo: "\( item["routing_number"] as? String ?? "0" )",
                            bankName: item["bank_name"] as? String ?? "" ,
                            currency: (item["currency"] as? String ?? "").uppercased() ,
                            country: item["country"] as? String ?? "" ))
                    }
                }
            }
            
            if let legal_entity = dict["legal_entity"] as? [String : Any]{
                
                if let verification = legal_entity["verification"] as? [String : Any]{
                    print(verification)
                    if (verification["status"] as? String ?? "") ==  "verified" || (verification["status"] as? String ?? "") ==  "pending" {
                         verficationStatus = true
                    }else{
                        verficationStatus = false
                    }
                    verifiedText = (verification["status"] as? String ?? "")
                }
            }
        }
        print(accountModelArray)
    }
    
    func getVerificationStatus() -> Bool{
        return verficationStatus
    }
    func getVerifiedText() -> String {
        return verifiedText
    }
    
}
    
    



