//
//  AddWalletVC.swift
//  DelivX Store
//
//  Created by 3Embed on 25/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class AddWalletVC: UIViewController {
    
    let placeHolder = ["Account Holder" ,
                       "Account No.",
                       "Rounting No." ,
                       "Bank Name",
                       "Currency",
                       "Country"]
    
    let topSpace  : CGFloat = 100.0
    
    
    private var timer : Timer!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var tableViewHolderHeight: NSLayoutConstraint!
    
    private var configrats = [CellConfigurator]()
    @IBOutlet weak var tableViewHolderTopAnchor: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    
    
    @IBOutlet weak var addedACHolder: UIView!
    @IBOutlet weak var addedACView: AddedACView!
    @IBOutlet weak var addACView: AddACView!
    let addWalletViewModel = AddWalletViewModel()
    @IBOutlet weak var linkedACView: AddACView!
 
    private var accountDetailsArray = [AccountModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.isHidden = true
        addedACHolder.isHidden = true
        addedACView.isHidden = true
        self.registerCel()
        
        if isIphone(){
            tableViewHolderHeight.isActive = true
            tableViewHolderTopAnchor.isActive = false
            tableViewHolderTopAnchor.constant = UIScreen.main.bounds.height
        }else{
            tableViewHolderHeight.isActive = false
            tableViewHolderTopAnchor.isActive = true
            tableViewHolderHeight.constant = 0.0
            
        }
        self.setNavigationBarItem()
        tableView.delegate = self
        tableView.dataSource = self
        self.addACView.delegete = self
        self.linkedACView.delegete = self
        self.addedACView.delegate = self
        self.navigationItem.title = StringConstants.bankDetails()
        
        addACView.btn.tag = 0
        addACView.setButtonText(title: StringConstants.Connectstripeaccount(), color: addACView.themeColor)
        addACView.isVarified(text: "", hidden: true)
        addACView.lbl_Msg.text = StringConstants.STEP1CONNECTTOYOURSTRIPEACCOUNT()
        
        linkedACView.btn.tag = 1
        linkedACView.isVarified(text: "", hidden: true)
        linkedACView.setButtonText(title: StringConstants.Linkbankaccount(), color: .gray)
        linkedACView.lbl_Msg.text = StringConstants.LINKBANKACCOUNT()
        
        self.getDataFromAPI()
    }
    
    private func getDataFromAPI(){
        addWalletViewModel.getRegisterStripeAccount { (isSuccess , data) in
            if self.timer != nil{
                self.timer.invalidate()
            }
            
            if isSuccess{
                self.accountDetailsArray = data
                print(data)
                self.updateUI()
            }else{
    
            }
        }
    }
   
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
     //   self.getDataFromAPI()
    }
    
    @IBAction func tapOnView(_ sender: UIButton) {
        if isIphone(){
            UIView.animate(withDuration: 1.0, animations: {
                self.tableViewHolderTopAnchor.constant = UIScreen.main.bounds.height
                self.containerView.isHidden = true
                self.view.layoutIfNeeded()
            })
        }else{
            UIView.animate(withDuration: 1.0, animations: {
                self.tableViewHolderHeight.constant = UIScreen.main.bounds.height
                self.containerView.isHidden = true
                self.view.layoutIfNeeded()
            })
        }
    }
    
    
    private func registerCel(){
       tableView.register(UINib.init(nibName: "AccountInfoTVCell", bundle: nil), forCellReuseIdentifier: "AccountInfoTVCell")
    }
    private func updateUI(){
        Helper.hidePI()
        if addWalletViewModel.getVerificationStatus() {
           
            linkedACView.setButtonText(title: "Link bank account", color:  addWalletViewModel.getVerifiedText() == "pending" ?  .gray :  addACView.themeColor)
            
            
            addACView.lbl_Msg2.text = "Stripe Account No : XXXXXXXX"
            if accountDetailsArray.count > 0{
               addACView.lbl_Msg2.text = "Stripe Account No : \(accountDetailsArray[0].getAccountDetails().accountNumber)"
            }
            addACView.setButtonText(title: "" , color: .gray)
            addACView.isVarified(text: addWalletViewModel.getVerifiedText(), hidden: false)
             addedACHolder.isHidden = true
            
            
        }
        
        
        if accountDetailsArray.count > 0{
            addedACView.isHidden = false
            addedACHolder.isHidden = false
            linkedACView.isHidden = true
            addedACView.setDataToView(accountNo: accountDetailsArray[0].getAccountDetails().accountNumber, accountHolder: accountDetailsArray[0].getAccountDetails().accountHolder)
        }
    }
    
    /// adding data to tableview cell
    private func createConfigurator(){
        configrats.removeAll()
        configrats.append( accountInfoTVCellConfig(item: ("", "Account Details", false))   )
        for (i,header) in placeHolder.enumerated(){
             configrats.append( accountInfoTVCellConfig(item: (header, accountDetailsArray[0].getAccountDetailsArray()[i], true))   )
        }
        tableView.reloadData()
        showBottomView()
    }
    
    private func showBottomView(){
        if isIphone(){
            UIView.animate(withDuration: 0.5, animations: {
                self.tableViewHolderTopAnchor.constant = (self.topSpace)
                self.containerView.isHidden = false
                self.view.layoutIfNeeded()
            })
        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.tableViewHolderHeight.constant = 400
                self.containerView.isHidden = false
                self.view.layoutIfNeeded()
            })
        }
        
    }
}


extension AddWalletVC : AddedACViewDelegate{
    func pressedHiddenButton() {
        self.createConfigurator()
    }
}

/// AddACViewDelegate method, used to open register view and add account view
extension AddWalletVC : AddACViewDelegate{
    func pressedButton(sender: UIButton) {
        if sender.tag == 0{
            
            guard  let  popupVC = self.storyboard?.instantiateViewController(withIdentifier: String(describing: AddStripeACVC.self) ) as? AddStripeACVC else {
                return
            }
            
            popupVC.delegate = self
            popupVC.pageType = popupVC.NEW_ACCOUNT_REG
            if (isIphone()) {
                self.navigationController?.pushViewController(popupVC, animated: true)
            }else {
                self.present(popupVC, animated: false, completion: nil)
            }
            
        }else if sender.tag == 1{
            
            guard  let  popupVC = self.storyboard?.instantiateViewController(withIdentifier: String(describing: AddStripeACVC.self) ) as? AddStripeACVC else {
                return
            }
            popupVC.delegate = self
            popupVC.pageType = popupVC.NEW_ACCOUNT_ADD
            if (isIphone()) {
                self.navigationController?.pushViewController(popupVC, animated: true)
            }else {
                self.present(popupVC, animated: false, completion: nil)
            }
        }
    }
}



// tableview delegate and datasource methods
extension AddWalletVC : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if configrats.count > 0{
            return configrats.count
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.configrats[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        /// set the height according to cell Configuration
        if self.configrats[indexPath.row] is accountInfoTVCellConfig {
            return UITableViewAutomaticDimension
        }
        return 0
    }
}


extension AddWalletVC : AddStripeACVCDelegate {
    
    func registerSucessFully() {
        Helper.showPI(string: "")
        timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: { timer in
            self.getDataFromAPI()
        })
    }
}




