//
//  AccountModel.swift
//  DelivX Store
//
//  Created by 3Embed on 27/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class AccountModel{
    /// account_holder_name
    private var accountHolder : String
    /// last4
    private var accountNumber : String
    /// routing_number
    private var routingNo : String
    /// bank_name
    private var bankName: String
    
    /// currency
    private var currency : String
    /// country
    private var country : String
    
    
    init(accountHolder : String , accountNumber : String , routingNo : String  ,  bankName: String , currency : String , country : String) {
        
        self.accountHolder  =   accountHolder
        self.accountNumber  =  accountNumber
        self.routingNo  = routingNo
        self.bankName  = bankName
        self.currency = currency
        self.country = country
        
    }
    
    func getAccountDetails() -> (accountHolder : String , accountNumber : String , routingNo : String  ,  bankName: String , currency : String , country : String) {
        return (accountHolder : self.accountHolder , accountNumber : self.accountNumber , routingNo : self.routingNo  ,  bankName: self.bankName , currency : self.currency , country : self.country)
    }
    
    
    func getAccountDetailsArray() -> [String]{
        return  [self.accountHolder ,  self.accountNumber ,  self.routingNo  ,  self.bankName , self.currency , self.country]
    }
    
    
}
