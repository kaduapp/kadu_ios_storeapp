//
//  Driver.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 19/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation


struct Driver:Comparable {
    static func < (lhs: Driver, rhs: Driver) -> Bool {
        return lhs.bookingCount > rhs.bookingCount
    }
    
    static func ==(lhs: Driver, rhs: Driver) -> Bool {
        return lhs.driverId == rhs.driverId
    }
    
    var driverId = ""
    var bookingCount = 0
    var driverName = ""
    var driverDistanceFromStore = 0
    var driverImage = ""
    var driverPhoneNumber = ""
    var storeId = ""
    
    init(driver:[String:Any]) {
        
        if let id = driver["_id"] as? String {
            driverId = id
        }
        if let count = driver["bookingCount"] as? Int {
            bookingCount = count
        }
        
        if let name = driver["name"] as? String {
            driverName = name
        }
        
        if let storeID = driver["storeId"] as? String {
            storeId = storeID
        }
        
        if let image = driver["image"] as? String {
            driverImage = image
        }
        
        if let phone = driver["phone"] as? String {
            driverPhoneNumber = phone
        }
    }
}
