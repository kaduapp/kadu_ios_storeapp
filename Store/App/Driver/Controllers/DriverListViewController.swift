//
//  DriverListViewController.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 31/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift


class DriverListTableView:UITableView {
    
    override func awakeFromNib() {
        self.register(PopUpOrderStoreDriverCell.nib, forCellReuseIdentifier: PopUpOrderStoreDriverCell.identifier)
    }
}

class DriverListViewController: UIViewController {
   
    
    @IBOutlet weak var driverListTableView: DriverListTableView!
    
    var arrayOfDrivers = [Driver]()
    var driverVM = DriverViewModel()
    var orderId:Int!
    let disposeBag = DisposeBag()
    var storeID = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setSubscriberForDriversResponse()
        driverVM.getAllStoreDrivers(storeID: storeID)
        driverListTableView.rowHeight = 80
        subScribeMQTT()

    }
    
    
    
    func showErrorMessage() {
        if arrayOfDrivers.count == 0 {
            self.driverListTableView.showErrorMessage(message: StringConstants.noDrivers())
        }else {
            self.driverListTableView.hideErrorMessage()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setSubscriberForDriversResponse(){
        driverVM.drivers_Response.subscribe(onNext: {success in
         self.arrayOfDrivers = self.driverVM.arrayOfDrivers
         self.driverListTableView.reloadData()
         self.showErrorMessage()
        }, onError: {error in
            print(error)
        }).disposed(by: disposeBag)
    }
    @IBAction func backButtonAction(_ sender: Any) {
        if self.isIphone() {
            self.navigationController?.popViewController(animated: true)
        }else {
            self.dismiss(animated: true, completion: nil)
        }
    }
        
    /// Api for assign order to a store driver manually.
    /// - Parameter index: Showing index of driver
    func assignOrderToDriver(atIndex index:Int) {
        
        Helper.showPI(string: "")
        let driverId = arrayOfDrivers[index].driverId
        let header = APIUtility.getHeaderForOrders()
        
        let params: [String : Any]  = [APIRequestParams.OrderID : orderId,
                                       APIRequestParams.driverId : driverId,
                                       APIRequestParams.TimeStamp : Date.getTimeStamp]
        
        APIHandler.postRequest(header: header, url: APIEndTails.DispatchOrderToDriver, params: params).subscribe(onNext: {success, data in
             Helper.hidePI()
            if success {
         self.navigationController?.popToRootViewController(animated: false)
            }else {
            }
        }, onError: { Error in
            Helper.hidePI()
            print(Error.localizedDescription)
        }).disposed(by: disposeBag)
    }
    
    @objc func subScribeMQTT() {
        _ = MQTT.sharedInstance.publishSubject.subscribe(onNext: {[weak self] response, messageType in
            if messageType == .MQTTDriverOnlineStatus {
                self?.driverData(data: response)
            }
            }).disposed(by: disposeBag)
    }
    
    /// Online and offline status of driver
    ///
    /// - Parameter data: MQTT message
    func driverData(data:Any) {
        if let data = data as? [String:Any], let driverStatus = data["status"] as? Int {
            
            let driver = Driver.init(driver: data)
            if driver.storeId == storeID{
                if driverStatus == 3  {
                    if !self.arrayOfDrivers.contains(driver) {
                        self.arrayOfDrivers.insert(driver, at: 0)
                    }
                }else {
                    self.arrayOfDrivers = self.arrayOfDrivers.filter{$0 != driver}
                }
            }
            self.arrayOfDrivers.sort()
            self.showErrorMessage()
            self.driverListTableView.reloadData()
        }
    }
}

extension DriverListViewController:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfDrivers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PopUpOrderStoreDriverCell.identifier) as! PopUpOrderStoreDriverCell
        cell.item = arrayOfDrivers[indexPath.row]
        cell.assignButton.tag = 123456 + indexPath.row
        cell.assignButton.addTarget(self, action: #selector(assignOrder(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func assignOrder(sender:UIButton) {
        let index = sender.tag % 123456
        self.assignOrderToDriver(atIndex: index)
    }
}

