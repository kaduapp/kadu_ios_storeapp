//
//  DriverViewModel.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 19/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxSwift

/// Store Drivers modal
class DriverViewModel : NSObject {
    var arrayOfDrivers = [Driver]()
     let drivers_Response = PublishSubject<(Bool)>()
    func handleDriversData(data:Any) {
        if let data = data as? [String:Any], let drivers = data["availableDriver"] as? [[String:Any]] {
            arrayOfDrivers = drivers.map {
                Driver.init(driver: $0)
            }
        }
    }
    
    /// Api call for getting store drivers
    func getAllStoreDrivers(storeID:String ) {
        
        Helper.showPI(string: "")
        let header = APIUtility.getHeaderForOrders()
        let getUserHistoryOrdersUrl = "\(APIEndTails.GetStoreDrivers)\(storeID)/\(0)"
        APIHandler.getRequest(header: header, url: getUserHistoryOrdersUrl).subscribe(onNext: {[weak self] success, data in
            Helper.hidePI()
            if success {
                self?.handleDriversData(data: data)
               self?.drivers_Response.onNext(true)
            }else {
            //self?.orderStatus_Response.onNext((false,OrderStatus.none))
            }
            }, onError: { Error in
                Helper.hidePI()
                print(Error)
        }).disposed(by: disposeBag)
    }
}
