//
//  PopUpOrderStoreDriverCell.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 07/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit


class PopUpOrderStoreDriverCell: UITableViewCell {
    
    
    @IBOutlet weak var pic: CustomImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var bookingCount: UILabel!
    @IBOutlet weak var assignButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName:PopUpOrderStoreDriverCell.identifier, bundle:nil)
    }
    
    var item: Driver? {
        didSet {
            name.text = item?.driverName
            phone.text = item?.driverPhoneNumber
            bookingCount.text = "\(item?.bookingCount ?? 0)"
            pic.setImage(url: (item?.driverImage)!)
        }
    }
    deinit {
        print("PopUpOrderStoreDriverCell deinit")
    }
}
