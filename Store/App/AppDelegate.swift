
//
//  AppDelegate.swift
//  DelivX Store
//
//  Created by 3 Embed on 13/12/17.
//  Copyright © 2017 3 Embed. All rights reserved.
//

import UIKit
import FirebaseMessaging
import FirebaseCore
import UserNotifications
import Fabric
import Crashlytics
import AWSS3
import Messages
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Crashlytics.self])
        
        self.setupNotification(application: application)
        Messaging.messaging().delegate = self
    
        let amazonWrapper:AmazonWrapper = AmazonWrapper.sharedInstance()
        amazonWrapper.setConfigurationWithRegion(AWSRegionType.EUCentral1, accessKey: AmazonAccessKey, secretKey: AmazonSecretKey)
        
        OneSkyOTAPlugin.provideAPIKey(OneSkyKeys.APIKey, apiSecret: nil, projectID: OneSkyKeys.ProjectID)
        OneSkyOTAPlugin.setLanguage(Utility.langCode)
        OneSkyOTAPlugin.checkForUpdate()
        return true
    }
    
    func subscribePushTopic(topic:String) {
        Messaging.messaging().subscribe(toTopic: topic)
    }
    
    func unSubscribePushTopic(topic:String) {
        Messaging.messaging().unsubscribe(fromTopic: topic)
    }
    

    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    func applicationDidEnterBackground(_ application: UIApplication) {
         Connectivity.sharedInstance.startListening(completion: {_ in })

    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
         Utility.connectMQTT()
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        Utility.disconnectMQTT()
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        Connectivity.sharedInstance.startListening(completion: {_ in })
    
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests()
        center.removeAllDeliveredNotifications()
        OneSkyOTAPlugin.checkForUpdate()
        Utility.connectMQTT()
 
    }
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    func applicationWillTerminate(_ application: UIApplication) {
        
        Connectivity.sharedInstance.stopListening()
    }
}



// MARK: - FCM delegate methods
extension AppDelegate: MessagingDelegate, UNUserNotificationCenterDelegate {
    
    
    //MARK: - Setup Notification
    func setupNotification(application: UIApplication) {
        //FCM Push
        FirebaseApp.configure()
        // iOS 10 support
        if #available(iOS 10,iOSApplicationExtension 10.0, *) {
            
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        else{
        let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound]
        let setting = UIUserNotificationSettings(types: type, categories: nil)
        UIApplication.shared.registerUserNotificationSettings(setting)
        application.registerForRemoteNotifications()
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(tokenRefreshNotification(notification:)),
                                              name: NSNotification.Name.InstanceIDTokenRefresh,
                                              object: nil)
    }
    
    // [START refresh_token]
   @objc func tokenRefreshNotification(notification: Notification) {
        print("Connecting to FCM Token.")
       if let refreshedToken = Messaging.messaging().fcmToken {
            print("InstanceID token: \(refreshedToken)")
          //  Utility.savePushToken(token: refreshedToken)
        }
        connectToFcm()
    }

    // [START connect_to_fcm]
    
    
   func connectToFcm() {
        print("Connecting to FCM.")
    
   Messaging.messaging().shouldEstablishDirectChannel = true
     StoreUtility.subscribeFCMTopic()
//        Messaging.messaging().connect { (error) in
//           if error != nil {
//               print("Unable to connect with FCM. \(String(describing: error))")
//           } else {
//               StoreUtility.subscribeFCMTopic()
//               print("Connected to FCM.")
//               // if Utility.getAddress().ZoneId.length > 0 || Utility.getAddress().CityId.length > 0 {
//                //    Utility.subscribeFCMTopicAll()
//               // }
//           }
//        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
        print(fcmToken)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        notificationAction(dictionary: userInfo)
        
    }
    
    func notificationAction(dictionary:Any) {
        
        if let dict = dictionary as? [String:Any], let action = dict["action"] as? String {
            switch Int(action)! {
                case 11:
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)

                    // instantiate the view controller we want to show from storyboard
                    // root view controller is tab bar controller
                    // the selected tab is a navigation controller
                    // then we push the new view controller to it
                    if  let conversationVC = storyboard.instantiateViewController(withIdentifier: "ManageOrderVC") as? ManageOrderVC,
                        let tabBarController = self.window?.rootViewController as? UITabBarController,
                        let navController = tabBarController.selectedViewController as? UINavigationController {

                            // we can modify variable of the new view controller using notification data
                            // (eg: title of notification)
                            //conversationVC.senderDisplayName = response.notification.request.content.title
                            // you can access custom data of the push notification by using userInfo property
                            // response.notification.request.content.userInfo
                            navController.pushViewController(conversationVC, animated: true)
                    }
                    
                    // tell the app that we have finished processing the user’s action / response
                    //completionHandler()
           
         case 12:
                Logout.logout()
            default:
                break
            }
        }
        
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        Messaging.messaging().apnsToken = deviceToken
        
        var token = NSString(format: "%@", deviceToken as CVarArg)
        token = token.replacingOccurrences(of: "<", with: "") as NSString
        token = token.replacingOccurrences(of: ">", with: "") as NSString
        token = token.replacingOccurrences(of: " ", with: "") as NSString
        
        print("\n\nPush Token: \(token)\n\n")
    }
    
    // Push notification received
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        // Print notification payload data
        
        print("Push notification received: \(data)\n\n\n")
       
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void){
        
        completionHandler([.sound, .alert, .badge])
        
    }
    
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)\n\n\n")
    }
    
    private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
        // Request Authorization
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
                if let error = error {
                    print("Request Authorization Failed (\(error), \(error.localizedDescription))")
                }
                completionHandler(success)
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Push message received: \(remoteMessage.appData)\n\n\n")
        
    }
    
}







