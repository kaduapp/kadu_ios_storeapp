//
//  ProfileTableViewController.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 27/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift


struct Profile {
    var name = ""
    var email = ""
    var phone = ""
    var storeName = ""
    var imageUrl = ""
    var countryCode = ""
    var franchiseName = ""
    var cityName = ""
    
    init(response:Any) {
        if let response = response as? [String:Any] {
            if let emailID = response["email"] as? String {
                self.email = emailID
            }
            
            if let cityNam = response["cityName"] as? String {
                self.cityName = cityNam
            }
            
            if let managerName = response["name"] as? String {
                self.name = managerName.capitalized
            }
            
            if let store = response["storeName"] as? String {
                storeName = store.capitalized
            }
            
            if let phoneNumber = response["phone"] as? String {
                self.phone = phoneNumber
            }
            
            if let countryCode = response["countryCode"] as? String {
                self.countryCode = countryCode
            }
            
            if let franchiseName = response["franchiseName"] as? String {
                self.franchiseName = franchiseName
            }
            
            if let profileImg = response["image"] as? String {
                self.imageUrl = profileImg
            }
        }
    }
}


class ProfileTableViewController: UITableViewController {
   
    let header = APIUtility.getHeaderForOrders()
    @IBOutlet weak var storeTitle: RobotoRegularLable!
    
    @IBOutlet weak var tf_OnlineStatus: UITextField!
    @IBOutlet weak var managerName: UITextField!
    @IBOutlet weak var emailId: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var storeName: UITextField!
    @IBOutlet weak var manageImageView: CustomImageView!
    @IBOutlet weak var switchButton: UISwitch!
    @IBOutlet weak var lbl_name: RobotoRegularLable!
    @IBOutlet weak var lbl_Phone: RobotoRegularLable!
    @IBOutlet weak var lbl_Email: RobotoRegularLable!
    @IBOutlet weak var logoutButton: UIButton!
    let disposeBag = DisposeBag()
    var profile:Profile!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_name.text = StringConstants.name()
        lbl_Phone.text = StringConstants.phoneNumber()
        lbl_Email.text = StringConstants.email()
        
        logoutButton.setTitle(StringConstants.logout(), for:UIControlState.normal)
        self.setNavigationBarItem()
        addShadowToBar()
        self.tableView.alpha = 0.01
        getProfileDetils()
      
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.barTintColor = ColorConstants.Color_NavTitleColorGreen
    
        self.navigationController?.navigationBar.tintColor = ColorConstants.Color_whiteColor
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : ColorConstants.Color_whiteColor,NSAttributedStringKey.font : FontConstants.Font_NavigationTitle]
        self.setNavigationBarItem()
    }

  
    @IBAction func pressedSwitchButton(_ sender: UISwitch) {
        
//        let status = KeychainHelper.sharedInstance.getSwitchOnOFF() == 1 ? 0 : 1
//        
//        forUpDateOnlineStatus(status: status) { (success) in
//            print("print ok")
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
   /// update profile data
    private func updateProfile() {
        self.managerName.text = profile.name
        self.emailId.text = profile.email
        self.phoneNumber.text = profile.countryCode + profile.phone
   
        
        
        let userType = UserDefaults.standard.integer(forKey: "user_Type")
        switch userType{
        case 0:
            self.storeName.text = profile.cityName
            self.storeTitle.text = StringConstants.cityPartner()
            break
        case 1:
            self.storeName.text = profile.franchiseName
            self.storeTitle.text = StringConstants.franchiseName()
            break
        case 2:
            self.storeName.text = profile.storeName
            self.storeTitle.text = StringConstants.storeName()
            break
        default:
            break
        }
        self.manageImageView.setImage(url: profile.imageUrl)
        
        self.tableView.alpha = 1.0
    }
    
    @IBAction func logoutButtonAction(_ sender: UIButton) {
        logout()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.isIphone() {
            return 80
        }
        return 100
    }
}

// MARK: - API CALL
extension ProfileTableViewController {
    /// Logout Api Call
    
    func logout() {
        let header  = APIUtility.getHeaderForOrders()
        
        APIHandler.postRequest(header: header, url: APIEndTails.LogoutService, params: [:]).subscribe(onNext: { success,response in
            
            if success {
                Logout.logout()
                
            }else {
            }
        }, onError: { Error in
        }).disposed(by: disposeBag)
    }
    
    
    /// Get profile data
    func getProfileDetils() {
        self.navigationController?.view.backgroundColor = UIColor.white
        Helper.showPI(string: "")
        let header  = APIUtility.getHeaderForOrders()
        APIHandler.getRequest(header: header, url: APIEndTails.ProfileData).subscribe(onNext: { success,response  in
            if success {
                Helper.hidePI()
                self.profile = Profile(response: response)
                self.updateProfile()
                 self.navigationController?.view.backgroundColor = UIColor.black
            }
        }, onError: { error in
            Helper.hidePI()
            self.log(message: error.localizedDescription)
        }).disposed(by: disposeBag)
    }
    
    
    /// online offline status change
    func forUpDateOnlineStatus(status : Int , onCompletionHandler:@escaping(Bool ) -> Void  ){
        let params = [ APIRequestParams.Status : status , APIRequestParams.StoreId : StoreUtility.getStore().storeId] as [String : Any]
        APIHandler.patchRequest(header: header, url: APIEndTails.switchOnlineStatus, params: params ).subscribe(onNext: {success, data in
            if success {
                print(data)
                onCompletionHandler(true)
                
            }else {
                onCompletionHandler(false)
            }
        }, onError: { Error in
            print(Error.localizedDescription)
        }).disposed(by: disposeBag)
        
    }
    
}





