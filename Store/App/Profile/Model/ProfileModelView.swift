//
//  ProfileModelView.swift
//  DelivX Store
//
//  Created by 3Embed on 23/07/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
import  RxSwift
class ProfileModelView {
    let disposeBag = DisposeBag()
    let header = APIUtility.getHeaderForOrders()
    func forUpDateOnlineStatus(status : Int , onCompletionHandler:@escaping(Bool ) -> Void  ){
        let params = [ APIRequestParams.Status : status , APIRequestParams.StoreId : StoreUtility.getStore().storeId] as [String : Any]
        APIHandler.patchRequest(header: header, url: APIEndTails.switchOnlineStatus, params: params ).subscribe(onNext: {success, data in
            if success {
                print(data)
                onCompletionHandler(true)
                
            }else {
                onCompletionHandler(false)
            }
        }, onError: { Error in
            print(Error.localizedDescription)
        }).disposed(by: disposeBag)
        
    }
}
