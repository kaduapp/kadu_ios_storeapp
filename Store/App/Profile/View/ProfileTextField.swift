//
//  ProfileTextField.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 27/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

class ProfileTextFields: UITextField {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.font = UIFont(name: Roboto.Medium, size: (self.font?.pointSize)!)
    }
}

