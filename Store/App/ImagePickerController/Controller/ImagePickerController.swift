//
//  ImagePickerController.swift
//  DelivX Store
//
//  Created by 3Embed on 24/04/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
/// This class helps you to pick images from gallery or camera


/// struct for amazon constant 
struct AMAZONUPLOAD {
    static let APPNAME    = "Store/"
    static let ORDERIMAGE = APPNAME + "orderImages/"
    
}

protocol  ImagePickerControllerDelegate : class {
        func getUpdatedOrderFromAPI(index : Int , images : [String])
}

class ImagePickerController: UIViewController {

    @IBOutlet weak var navBar: UINavigationBar!
    
    private let userDefaults = UserDefaults.standard
    
    var images : [String] = []
    weak var delegate : ImagePickerControllerDelegate?
    
    private var obj_imageCollection : [ImageCollection] = []
    
    let imagePickerViewModel = ImagePickerViewModel()
   
    private var imagePicker = UIImagePickerController()
    private var common_Variables_and_Methods : Common_Variables_and_Methods!
    private var imageCollcetion : [UIImage] =  []
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    var orderId : Int!
    
    var productID : String!
    
    var indexID : Int!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isIphone(){
            navBar.isHidden = true
        }
        
        obj_imageCollection.removeAll()
        print(self.orderId , productID)
        
        self.common_Variables_and_Methods = Common_Variables_and_Methods.getInstance()

        if userDefaults.object(forKey: APIRequestParams.ProductId) as? String  == productID{
           images = userDefaults.object(forKey: APIRequestParams.Images) as! [String]
        }
//
        self.setupView()
        self.registerCell()
    }
    
    @IBAction func pressedBackButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    private func setupView(){
        for url in images{
            obj_imageCollection.append(ImageCollection(image: UIImage(), url: url) )
        }
        self.navigationItem.title = "Order Details"
        imageCollectionView.dataSource = self
        imageCollectionView.delegate = self
    }
    
    private func registerCell(){
        imageCollectionView.register(UINib.init(nibName: "IamgeViewCVCell", bundle: nil), forCellWithReuseIdentifier: "IamgeViewCVCell")
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
      //  self.navigationController?.navigationBar.isHidden = false
        
    }
    private func openImagePickerOptionsView(){
        
        let actionSheet = UIAlertController(title: "", message: "Select image source", preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.openCamera()
        }
        
        let gallery = UIAlertAction(title: "Gallery", style: .default) { (action) in
            self.openGallery()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }

        actionSheet.addAction(camera)
        actionSheet.addAction(gallery)
        actionSheet.addAction(cancel)
        
        if !self.isIphone() {
            self.addActionSheetForiPad(actionSheet: actionSheet)
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    private func openGallery(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    private func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }else {
            common_Variables_and_Methods.getAlert(vc: self, title: "", msg: "Camera is not available ") { (action) in
                
            }
        }
    }
    
    
    
    func uploadImageToAmazon(){
        self.images.removeAll()
        var imagesCount:Int = 0
        var temp = [UploadImage]()
        
        for imagesData in obj_imageCollection {
            
            var strUrl = String()
            let distanceTime = Date().timeIntervalSince1970
            strUrl = "image" + String(distanceTime) + ".jpg"
                
                
//                AMAZONUPLOAD.ORDERIMAGE + String(distanceTime) + String(imagesCount) + ".jpg"
            strUrl = strUrl.replacingOccurrences(of: " ", with: "_")
            strUrl = strUrl.lowercased()
            
            // Resize Image
            if obj_imageCollection[imagesCount].getdata().1 == ""{
                
                let resizedImage = imagesData.getdata().0.resizeImage(size: CGSize(width: 100, height: 100))
            
                temp.append(UploadImage(image: resizedImage, path: strUrl))
            
                let finalUrl = AMAZON_URL + Bucket + "laundry/" + String(orderId) + "/" + strUrl
          //  shipmentImg.append(finalUrl)
            // rahul
//               self.images.append(finalUrl)
                
            imagesData.setURL(url: finalUrl)
                obj_imageCollection[imagesCount].setURL(url: finalUrl)
            }
         
            images.append(imagesData.getdata().1)
            
//            print("Image Url: \(finalUrl)")
           
            imagesCount =  imagesCount + 1
        }
        
        // Upload Images in Background Using Singletone Class
        let upMoadel = UploadImageModel.shared
        upMoadel.uploadImages = temp
        upMoadel.start()
        
//        liveBooking()
    }
    
    @IBAction func pressedSaveButton(_ sender: UIButton) {
        uploadImageToAmazon()
       
        
        let params = [APIRequestParams.OrderID : orderId! , APIRequestParams.ProductId : productID! , APIRequestParams.Images :  images] as [String : Any]
        print(params)
        
        imagePickerViewModel.uploadImageForItem(params: params) { (result) in
            if result{
                self.userDefaults.set(self.images, forKey: APIRequestParams.Images)
                self.userDefaults.set(self.productID, forKey: APIRequestParams.ProductId)
//        NotificationCenter.default.post(name: .getUpdatedOrder, object: nil)
//                self.delegate?.getUpdatedOrderFromAPI(index : self.indexID , images : self.images)
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}


extension ImagePickerController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if obj_imageCollection.count == 0{
            return 1
        }else{
            return obj_imageCollection.count + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell =  imageCollectionView.dequeueReusableCell(withReuseIdentifier: "IamgeViewCVCell", for: indexPath) as! IamgeViewCVCell
        
        cell.delegate = self

        if obj_imageCollection.count - 1 >= indexPath.row{
            
            if obj_imageCollection[indexPath.row].getdata().1 == ""{
                cell.setDataToCell(value: true , image:
                    obj_imageCollection[indexPath.row].getdata().0, indexNo: indexPath.row)
            }else{
                  cell.setImageFromURL(imageURL: images[indexPath.row], value: true, indexNo: indexPath.row)
            }
        }else{
            cell.setDataToCell(value: false , image: #imageLiteral(resourceName: "add_photo")
                , indexNo: 0)
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if  self.isIphone(){
            return CGSize(width: self.view.frame.width / 3
                , height: self.view.frame.width / 3)
        }else{
            return CGSize(width: 120
                , height: 120)
        }
    }
}


extension ImagePickerController : ImageViewCVCellDelegate{
    
    func pressedRemoveButton(indexNo: Int) {
        print("remove buton pressd \(indexNo)")
        print(obj_imageCollection[indexNo].getdata().1)
       
        let imageName  =  obj_imageCollection[indexNo].getdata().1
        
        if let index = (imageName.range(of: "delivx/")?.upperBound)
        {
            //prints "value"
            let keyName = String(imageName.suffix(from: index))
           //  let upMoadel = 
            
            
            //prints "element="
//            let beforeEqualsToContainingSymbol = String(smth.prefix(upTo: index))
        }
        
        
      
        
        
        self.obj_imageCollection.remove(at: indexNo)
       
       // self.imageCollcetion.remove(at: indexNo)
        self.imageCollectionView.reloadData()
    }
    
    func pressedAddImageButton() {

        self.openImagePickerOptionsView()
        print("add buton pressd")
        
    }
    
}


extension ImagePickerController : UINavigationControllerDelegate, UIImagePickerControllerDelegate {


    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        obj_imageCollection.append(ImageCollection(image: chosenImage, url: ""))
        
//        self.imageCollcetion.append(chosenImage)
        self.imageCollectionView.reloadData()
        self.dismiss(animated:true, completion: nil)
    }

}
