//
//  IamgeViewCVCell.swift
//  DelivX Store
//
//  Created by 3Embed on 24/04/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//
import Foundation
import UIKit
import Kingfisher

protocol ImageViewCVCellDelegate : class {
    func pressedAddImageButton()
    func pressedRemoveButton( indexNo : Int )
//    func getImageFromCell(image : UIImage)
}


class IamgeViewCVCell: UICollectionViewCell {
    
    var delegate : ImageViewCVCellDelegate?
    @IBOutlet weak var hiddenButton: UIButton!
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.closeButton.isHidden = true
        self.closeButton.layer.cornerRadius = self.closeButton.frame.height / 2
    }

    
    func setImageFromURL(imageURL : String , value : Bool , indexNo : Int){
        
        hiddenButton.isHidden = value
        closeButton.isHidden = !value
        closeButton.tag = indexNo
        let indicator = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        indicator.frame = self.bounds
        indicator.color = ColorConstants.Color_ImageIndicator
        indicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.addSubview(indicator)
        indicator.startAnimating()
        
        
        imageView.kf.setImage(with: URL(string: imageURL),
                    placeholder: UIImage.init(),
                    options: [.transition(ImageTransition.fade(1))],
                    progressBlock: { receivedSize, totalSize in
        },
                    completionHandler: { image, error, cacheType, imageURL in
                        //  self?.image = image
                        
//                        self.delegate?.getImageFromCell(image: image!)
                        indicator.stopAnimating()
                        indicator.removeFromSuperview()
                        
        })
        
    }
    
    
    
    
    func setDataToCell( value : Bool , image : UIImage? , indexNo : Int){
        
        hiddenButton.isHidden = value
        closeButton.isHidden = !value
        imageView.image = (value ? image : #imageLiteral(resourceName: "add_photo"))
        closeButton.tag = indexNo
    }
    
    
    @IBAction func pressedAddimageButton(_ sender: UIButton) {
        delegate?.pressedAddImageButton()
    }
    
    @IBAction func pressedRmovedButton(_ sender: UIButton) {
        delegate?.pressedRemoveButton(indexNo: closeButton.tag)
    }
    

    
}






