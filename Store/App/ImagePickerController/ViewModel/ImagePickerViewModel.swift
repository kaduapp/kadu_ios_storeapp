//
//  ImagePickerViewModel.swift
//  DelivX Store
//
//  Created by 3Embed on 30/04/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxSwift


class ImagePickerViewModel {
    
  var disposeBag = DisposeBag()
    /// upload the images on server with params
    /// - Parameter : params :[ String : Any]
    /// - onCompletionHandler(Boo) 
    func uploadImageForItem( params: [String : Any] , onCompletionHandler:@escaping(Bool) -> Void ){
        
        let header = APIUtility.getHeaderForOrders()
        print(header)
        
        APIHandler.patchRequest(header: header, url: APIEndTails.SavelaundryImages, params: params).subscribe(onNext: { success, data in
            if success == true {
            
                onCompletionHandler(true)
            }else {
              onCompletionHandler(false)
            }
            
        }, onError: { Error in
            print(Error)
        }).disposed(by: disposeBag)

    }
    
    
}
/// this model contains the image and stringUrl
/// getdata() -> this method returns (UIImage , String)
class ImageCollection {
    private var image : UIImage!
    private var url : String!
    
    init(image : UIImage , url : String) {
        self.image = image
        self.url = url
    }
    
    func setImage( image : UIImage){
        self.image = image
    }
    func setURL( url : String){
        self.url = url
    }
    
    func getdata() -> (UIImage, String){
        return (image , url)
    }
}
