//
//  TransactionTVCell.swift
//  DelivX Store
//
//  Created by 3Embed on 22/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

typealias  transactionTVCellConfig = TableCellConfigurator<TransactionTVCell , TransXDetails>

class TransactionTVCell: UITableViewCell , ConfigurableCell{

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var img_ArrawImage: UIImageView!
    @IBOutlet weak var lbl_TransX_No: UILabel!
    
    @IBOutlet weak var lbl_TransX_Date: UILabel!
    
    @IBOutlet weak var lbl_TriggerType: UILabel!
    @IBOutlet weak var lbl_TransX_Amount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cellView.layer.cornerRadius = 5.0
        self.selectionStyle = .none
    }

    
    /// ConfigurableCell pritocol func to set data in cell
    func configure(data: TransXDetails ) {
        lbl_TransX_No.text = data.getTranXDetails().tranX_ID
        lbl_TransX_Date.text = data.getTranXDetails().tranX_Date
        lbl_TransX_Amount.text = data.getTranXDetails().tranX_Amount
        lbl_TriggerType.text = data.getTranXDetails().trigger
        img_ArrawImage.image = (data.getTranXDetails().isDebited ? #imageLiteral(resourceName: "arrowRed") : #imageLiteral(resourceName: "arrowGreen"))
    }
}

