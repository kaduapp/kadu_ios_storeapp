//
//  WalletCollectionViewCell.swift
//  DelivX Store
//
//  Created by 3Embed on 22/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

 typealias walletCollectionViewCellConfig = CollectionCellConfigurator<WalletCollectionViewCell , [TransXDetails] >

protocol WalletCollectionViewCellDelegate : class {
    func callApi()
}

class WalletCollectionViewCell: UICollectionViewCell , ConfigurableCell{

    fileprivate var configurator : [CellConfigurator] = []
    weak var delegate : WalletCollectionViewCellDelegate?
    @IBOutlet weak var tableView: UITableView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.registerCell()
        tableView.rowHeight = 50
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }

    
    private func registerCell(){
        self.tableView.register(   UINib.init(nibName: "TransactionTVCell", bundle: nil), forCellReuseIdentifier: "TransactionTVCell")
    }
    
    func configure(data: [TransXDetails]) {
        self.configurator.removeAll(keepingCapacity: false )
        for item in data{
            self.configurator.append(transactionTVCellConfig(item: item))
        }
        tableView.reloadData()
    }
}


// setting data to each tableview in WalletCollectionViewCell
extension WalletCollectionViewCell : UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if configurator.count > 0{
            return configurator.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = self.configurator[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
    
        cell.selectionStyle = .none
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.configurator[indexPath.row] is transactionTVCellConfig {
            return UITableViewAutomaticDimension
        }
        return 0 
    }
    
  
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (configurator.count - 1) == indexPath.row{
            delegate?.callApi()
        }
    }
    
}
