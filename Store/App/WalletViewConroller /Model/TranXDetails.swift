//
//  TranXDetails.swift
//  DelivX Store
//
//  Created by 3Embed on 24/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//
import UIKit

enum TranXType : String {
    case DEBIT
    case CREDIT
}

class TransXDetails{
    
    private var tranX_ID : String
    private var tranX_Date: String
    private var tranX_Amount: String
    private var isDebited : Bool
    private var trigger : String
    
    init(tranX_ID : String , tranX_Date: String , tranX_Amount: String , tranxType : String , trigger : String) {
       
        self.tranX_ID = tranX_ID
        self.tranX_Amount = tranX_Amount
        self.isDebited = (tranxType == TranXType.DEBIT.rawValue ? true : false )
        self.trigger = trigger
        self.tranX_Date = tranX_Date
        
    }
    
    
    func getTranXDetails() -> (tranX_ID : String , tranX_Date: String , tranX_Amount: String , isDebited : Bool , trigger : String ){
        return (tranX_ID : self.tranX_ID , tranX_Date: self.tranX_Date , tranX_Amount: self.tranX_Amount , isDebited : self.isDebited , trigger : self.trigger)
    }

}
