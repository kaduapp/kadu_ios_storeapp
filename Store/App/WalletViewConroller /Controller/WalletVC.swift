//
//  WalletVC.swift
//  DelivX Store
//
//  Created by 3Embed on 20/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class WalletVC: UIViewController {

    let walletModelView  = WalletModelView()
    
    @IBOutlet weak var lbl_CreditAmount: UILabel!
    @IBOutlet weak var CURRENTCREDIT: UILabel!
    
    fileprivate var configrator : [CellConfigurator] = []
    private var tranXDetails_Collection : [[TransXDetails]] = [[]]
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btn_ALL: UIButton!
    @IBOutlet weak var btn_DEBIT: UIButton!
    @IBOutlet weak var btn_CREDIT: UIButton!
    @IBOutlet weak var TopView: UIView!
    
    private var indexID = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.registerCell()
        self.setNavigationBarItem()
        collectionView.delegate = self
        collectionView.dataSource = self
        tranXDetails_Collection.removeAll()
        self.title = StringConstants.RecentTransactions()
        CURRENTCREDIT.text = StringConstants.CURRENTCREDIT()
        self.callWalletDataAPI()
        
        
    }
    
    private func callWalletDataAPI(){
        self.walletModelView.getTranXDetails(indexID: indexID) { (isSuccess, tranxDetailsResponse) in
            if isSuccess{
                print(tranxDetailsResponse)
                self.tranXDetails_Collection = tranxDetailsResponse
                self.configrationData()
                self.lbl_CreditAmount.text = self.walletModelView.getwalletAmont()
            }else{
                
            }
        }
    }
    
    
    private func setupView(){
        self.TopView.layer.shadowRadius = 2.0
        self.TopView.layer.shadowColor = UIColor.gray.cgColor
        self.TopView.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.TopView.layer.shadowOpacity = 1
        
        self.navigationItem.title = StringConstants.RecentTransactions()
        setTitleColorForButton(sender: btn_ALL)
        btn_ALL.addTarget(self, action: #selector(pressedAllButton(sender:) ), for: .touchUpInside)
        btn_DEBIT.addTarget(self, action: #selector(pressedDebitButton(sender:) ), for: .touchUpInside)
        btn_CREDIT.addTarget(self, action: #selector(pressedCreditButton(sender:) ), for: .touchUpInside)
        btn_ALL.setTitle(StringConstants.ALL(), for: .normal)
        btn_DEBIT.setTitle(StringConstants.DEBIT(), for: .normal)
        btn_CREDIT.setTitle(StringConstants.CREDIT(), for:.normal)

    }
    
    // set button text color according to collectionview cell
    private func setTitleColorForButton(sender : UIButton){
        btn_ALL.setTitleColor( UIColor.lightGray, for: .normal)
        btn_DEBIT.setTitleColor( UIColor.lightGray, for: .normal)
        btn_CREDIT.setTitleColor( UIColor.lightGray, for: .normal)
        sender.setTitleColor(Helper.getUIColor(color:  Colors.AppBaseColor), for: .normal)
    }
    
    
    @objc func pressedAllButton(sender : UIButton){
       
        setTitleColorForButton(sender: btn_ALL)
        if tranXDetails_Collection.count > 0{
            self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0),
                                             at: .left,
                                             animated: true)
        }

    }
    
    @objc func pressedDebitButton(sender : UIButton){
       
        setTitleColorForButton(sender: btn_DEBIT)
        if tranXDetails_Collection.count > 0{
            self.collectionView.scrollToItem(at: IndexPath(row: 1, section: 0),
                                         at: .left,
                                         animated: true)
        }
    }
    
    @objc func pressedCreditButton(sender : UIButton){
        
        setTitleColorForButton(sender: btn_CREDIT)
        if tranXDetails_Collection.count > 0{
            self.collectionView.scrollToItem(at: IndexPath(row: 2, section: 0),
                                         at: .left,
                                         animated: true)
        }
    }
    
    
    
    private func registerCell(){
        self.collectionView.register(UINib.init(nibName: "WalletCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "WalletCollectionViewCell")
        
    }
    
    
    // add data for configrator array
    private func configrationData(){
        self.configrator.removeAll(keepingCapacity: false )
        if tranXDetails_Collection.count > 0{
            for collection in tranXDetails_Collection{
                 self.configrator.append(walletCollectionViewCellConfig(item: collection))
            }
            self.collectionView.reloadData()
        }
    }

}

// collection view delegate and datasource
extension WalletVC : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if configrator.count > 0{
            return configrator.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.configrator[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: type(of: item).reuseId, for: indexPath)
        
         ( cell as! WalletCollectionViewCell).delegate = self
        item.configure(cell: cell)
        return cell

    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isIphone(){
            return   CGSize(width: collectionView.frame.width  , height: collectionView.frame.height )
        }
        return  CGSize(width: (collectionView.frame.width / 3 ) , height: collectionView.frame.height)
    }
    
   
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        print("====================" , indexPath.row)
        
        if indexPath.row == 0{
            self.setTitleColorForButton(sender: btn_ALL)
        } else if indexPath.row == 1{
            self.setTitleColorForButton(sender: btn_DEBIT)
        }else if  indexPath.row == 2{
            self.setTitleColorForButton(sender: btn_CREDIT)
        }
    
    }
    
    
}

extension WalletVC : WalletCollectionViewCellDelegate{
    func callApi() {
        indexID += 1
        callWalletDataAPI()
    }
}


