//
//  WalletModelView.swift
//  DelivX Store
//
//  Created by 3Embed on 24/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift

class WalletModelView{
    
    private var currencySymbol  = "$"
    private var walletHardLimit : Float = 0.0
    private var walletSoftLimit : Float = 0.0
    private var walletBalance = "N/A"
    
    private var all_TranX : [TransXDetails] = []
    private var debit_TranX : [TransXDetails] = []
    private var credit_TranX : [TransXDetails] = []
    
    
    var tranX_Details_Array : [[TransXDetails]] =  [[]]
    var disposeBag = DisposeBag()
    
    func getTranXDetails(indexID : Int , onCompletionHandler:@escaping(Bool , [[TransXDetails]] ) -> Void ){
        let header = APIUtility.getHeaderForOrders()
        let url = APIEndTails.storeWalletTranX + "\(indexID)"
        
        APIHandler.getRequest(header: header , url: url).subscribe(onNext: { success, data in
            if success {
                
                if indexID == 0{
                    self.convertDataToArray(data: data , isEmpty: true)
                    onCompletionHandler(true , (self.tranX_Details_Array))
                }else{
                    self.convertDataToArray(data: data , isEmpty:  false )
                    onCompletionHandler(true , (self.tranX_Details_Array))
                }
                
            }else {
                self.convertDataToArray(data: data, isEmpty: false )
                onCompletionHandler(false , (self.tranX_Details_Array))
                print(data)
            }
            }, onError: { Error in
                print(Error)
        }).disposed(by: disposeBag)
        
    }
    
    
    /// convert api response into transXDetails collection
    /// - Parameters: data (Any type)
    private func convertDataToArray( data : Any , isEmpty : Bool ){
        
        if isEmpty{
            all_TranX.removeAll()
            debit_TranX.removeAll()
            credit_TranX.removeAll()
        }
        
        
        if let apiResponse = data as? [String : Any]{
            
            self.currencySymbol = apiResponse["currencySymbol"] as! String
        
            if let walletAmont = apiResponse["walletBalance"] as? NSNumber  {
                 self.walletBalance =  String(format:"%@ %0.2f",currencySymbol,Float(truncating: walletAmont) )
            }else{
                 self.walletBalance =  String(format:"%@ %0.2f",currencySymbol,Float(0.00) )
            }
            
            // all tranX
            if let allTranx = apiResponse["creditDebitTransctions"] as? [NSDictionary]{
                for tranx in allTranx{
                    all_TranX.append(convertDictTo_TranxDetailsObject(data: tranx))
                }
            }
            // debit tranX
            if let debitTranx = apiResponse["debitTransctions"] as? [NSDictionary]{
                for tranx in debitTranx{
                    debit_TranX.append(convertDictTo_TranxDetailsObject(data: tranx))
                }
            }
            // credit tranX
            if let creditTranx = apiResponse["creditTransctions"] as? [NSDictionary]{
                for tranx in creditTranx{
                    credit_TranX.append(convertDictTo_TranxDetailsObject(data: tranx))
                }
            }
            tranX_Details_Array.removeAll(keepingCapacity: false )
            self.tranX_Details_Array.append(all_TranX)
            self.tranX_Details_Array.append(debit_TranX)
            self.tranX_Details_Array.append(credit_TranX)
        }
        
    }
    
    /// convert NSDictionary into tranxDetails onbject and restun TransXDetails object
    /// - Parameters: data (NSDictionary type object )
    
    private func convertDictTo_TranxDetailsObject( data : NSDictionary ) -> TransXDetails{
        var amount = ""
        var date = ""
        if let amt = data.value(forKey: "amount") as? NSNumber{
           amount =  String(format:"%@ %0.2f",currencySymbol,Float(truncating: amt) )
        }else{
             amount =  String(format:"%@ %0.2f",currencySymbol,Float(0.00) )
        }
        
        if let timeStamp = data.value(forKey: "txnDate") as? NSNumber{
            date =  Date.getDateString(value: Date.getStringToDateFromTimeStamp(value: Int64(truncating: timeStamp), format: DateFormat.DateAndTimeFormatServer) , format: DateFormat.fullDate)
        }
        

        let tempData = TransXDetails(tranX_ID: data.value(forKey: "txnId") as? String ??  "",
                                     tranX_Date: date,
                                     tranX_Amount: amount ,
                                     tranxType: data.value(forKey: "txnType") as? String ?? ""  ,
                                     trigger: data.value(forKey: "trigger") as? String ?? "" )
        return tempData
        
    }
    
    func getwalletAmont() -> String{
        return  self.walletBalance
    }
    
}

