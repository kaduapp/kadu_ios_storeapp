//
//  AboutViewController.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 07/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var headerTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBarItem()
        addShadowToBar() 

    }

    @IBAction func dismissViewController(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
