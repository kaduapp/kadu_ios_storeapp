//
//  TimePickerViewModal.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 09/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation

class TimePickerModalView {
    var currentDelay = ""
    var dueTime = ""
    var arrayOfReasons = [String]()
    
    init(orderSummary:Any) {
    if let reasonModal = orderSummary as? ReasonModal {
        dueTime = Date.getDateFromString(value: reasonModal.bookingDate, format: DateFormat.TimeFormatToDisplay)
        currentDelay = Date.getDueTime(value: reasonModal.bookingDate)
        arrayOfReasons = reasonModal.arrayOfReasons
        ReasonsTableViewController.cancelReasonArray = reasonModal.arrayOfReasons
        
        }
    }
    
    func getReasonAtIndex(index:Int) -> String {
        if arrayOfReasons.indices.contains(index) {
            return arrayOfReasons[index]
        }
        return arrayOfReasons[0]
    }

    deinit {
        print("TimePickerModalView deinit")
    }
}
