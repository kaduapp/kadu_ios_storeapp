//
//  TimeIntervalCell.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 08/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit

class TimeIntervalCell: UICollectionViewCell {
    @IBOutlet weak var timeIntervalLabel: CustomLabel!
    
    static var identifier:String {
        return String(describing:self)
    }
    
    
    func selectedCell() {
        timeIntervalLabel.backgroundColor = .red
        timeIntervalLabel.textColor = .white
    }
    
    func deSelectCell() {
        timeIntervalLabel.backgroundColor = .white
        timeIntervalLabel.textColor = UIColor.colorWithHex(color: "6F7678")
    }
    
    deinit {
        print("TimeIntervalCell deinit")
    }
}
