//
//  TimePickerViewController.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 08/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift

protocol TimePickerViewControllerDelegate {
    func delayOrder(cancellationResion:String, dueTime:String)
}

class TimePickerViewController: UIViewController {

    @IBOutlet weak var customview: CustomView!
    @IBOutlet weak var verticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var dueTime: UILabel!
    @IBOutlet weak var currentDelay: UILabel!
    @IBOutlet weak var headerLabel: RobotoBoldLable!
    @IBOutlet weak var reasonTextView: UITextView!

    @IBOutlet weak var dueTimeLabelConstraints: NSLayoutConstraint!
    
    
    @IBOutlet weak var currentDelayLabelConstraints: NSLayoutConstraint!
    
    @IBOutlet weak var dueDelayTimeViewConstraints: NSLayoutConstraint!
    
    var delegate:TimePickerViewControllerDelegate?
    var numberOfTimeIntervals = 0
    let orderDelaySubject = PublishSubject<Any>()
    var reasonsTableVC:ReasonsTableViewController!
    var timePickerMV:TimePickerModalView!
    var headerText = ""
    var reasonSelected = false
    var reason = ""
    var disposeBag = DisposeBag()
    static var comeFromCancelReason = Bool()

    var orderSummary:ReasonModal? {
        didSet {
            timePickerMV = TimePickerModalView.init(orderSummary: orderSummary ?? "")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if TimePickerViewController.comeFromCancelReason {
            self.dueTimeLabelConstraints.constant = 0
            self.currentDelayLabelConstraints.constant = 0
            self.dueDelayTimeViewConstraints.constant = 0
            TimePickerViewController.comeFromCancelReason = false
        }
        // Do any additional setup after loading the view.
    //addObserver()
        reasonTextView.delegate = self
        self.headerLabel.text = headerText
    //   self.reasonTextView.text = self.timePickerMV.getReasonAtIndex(index: 0)
        showReasons()
    }

    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    @IBAction func HideKeyboard(_ sender: Any) {
       hideTheView()
    }


    //Mark:- Keyboard ANimation
    override func keyboardWillShow(_ notification: NSNotification){
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

        self.textFieldResponded(keyHeigt: keyboardFrame.height)
    }


    override func keyboardWillHide(_ notification: NSNotification){
         hideTheView()
    }


    func hideTheView()  {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.4,
                       animations: { () -> Void in

                        self.verticalConstraint.constant = 0

                        self.view.layoutIfNeeded()
        })
    }

    func showReasons() {
        if let reasonsTableVC = self.childViewControllers[0] as? ReasonsTableViewController {
            reasonsTableVC.selectedReason.subscribe(onNext: { (reason) in
                self.reasonSelected = reason.count > 0 ? true : false
                self.reason = reason
            }, onError: { (error) in
                print(error.localizedDescription)
            }, onCompleted: {
                print("Complete")
            }, onDisposed: {
                print("Disposed")
            }).disposed(by: disposeBag)
//            reasonsTableVC.reason = { [weak self] reason in
//            }
            appearance()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    func appearance() {
        self.dueTime.text = timePickerMV.dueTime
        self.currentDelay.text = timePickerMV.currentDelay
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func doneButtonAction(_ sender: UIButton) {
        if let reasonsTableVC = self.childViewControllers[0] as? ReasonsTableViewController {
            if reasonsTableVC.delayTime.count == 0 &&  numberOfTimeIntervals != 0 {
               // Helper.showAlert(message: StringConstants.selectReason(), head: StringConstants.message() , type: 0)
                return
            }

            if !reasonSelected {
               // Helper.showAlert(message: StringConstants.selectReason(), head: StringConstants.message() , type: 0)
                return
            }

            if let delegateObj = delegate {
                
                if let reasonText = self.reasonTextView.text {
                    self.reason = self.reason + ", " + reasonText
                }
                delegateObj.delayOrder(cancellationResion: self.reason, dueTime: reasonsTableVC.delayTime)
            }
            dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func cancelButtonAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }

    /// Reason View Controller Delegate
    ///
    /// - Parameter reason: Selected reason
    func selectedReason(reason: String) {
        reasonTextView.text = reason
    }

    deinit {
        print("TimePickerViewController deinit")
    }

    func textFieldResponded(keyHeigt:CGFloat) {

        UIView.animate(withDuration: 0.5,
                       animations: {

                        let height =   UIScreen.main.bounds.size.height


                        UIView.animate(withDuration: 0.4,
                                       animations: { () -> Void in
                                        if (self.isIphone()) {

                                            self.verticalConstraint.constant = -((keyHeigt) - ((height - self.customview.frame.size.height)/2))
                                        }else{
                                            self.verticalConstraint.constant = -((keyHeigt) - ((height - self.customview.frame.size.height)/2))
                                        }
                                        self.view.layoutIfNeeded()
                        })
        })

    }
}



extension TimePickerViewController:UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {

//        if textView.text == "Enter your reason".localized{
//            textView.text  = ""
//        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"{
            hideTheView()
        }
        return true;
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty{
          //  textView.text = "Enter your reason".localized
        }
    }
}




