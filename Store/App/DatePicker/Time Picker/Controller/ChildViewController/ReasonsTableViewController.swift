//
//  ReasonsTableViewController.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 09/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
//protocol ReasonsTableViewControllerDelegate {
//    func selectedReason(reason:String)
//}

class ReasonsTableViewController: UITableViewController {
    
    
    //  var delegate:ReasonsTableViewControllerDelegate?
    
    //    var reason:(String) ->()
    var reason = { (reason: String) in }
    
    var timeCollectionCell:TimeIntervalCollectionCell? = nil
    
    var numberOfTimeIntervals = 4
    var delayTime:String = ""
    var reasonString = ""
    var selectedIndex = -1
    var selectedReasonIndex = -1
    var arrayOfReasons = [String]()
    var timePickerMV:TimePickerModalView!
    let reasonModel = ReasonModal()
    static var cancelReasonArray = [String]()
    var timePicker = TimePickerViewController()
    static var comeFromCancelation = Bool()

    var selectedReason = PublishSubject<String>()
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if ReasonsTableViewController.comeFromCancelation {
            ReasonsTableViewController.comeFromCancelation = false
            numberOfTimeIntervals = 0
        } else {
            numberOfTimeIntervals = 4
        }
        
        self.tableView.rowHeight = 50
        self.tableView.estimatedRowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        disposeBag = DisposeBag()
    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        self.tableView.addKeyboardChangeFrameObserver(willShow: { [weak self](height) in
//            //Update constraints here
//            self?.tableView.contentOffset.y += height
//            self?.view.setNeedsUpdateConstraints()
//            }, willHide: { [weak self](height) in
//                //Reset constraints here
//                self?.tableView.contentOffset = .zero
//                self?.view.setNeedsUpdateConstraints()
//        })
//        
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        self.tableView.removeObserver()
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        print("ReasonsTableViewController deinit")
    }
}

// MARK: - Table View Methods
extension ReasonsTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//       return timePickerMV.arrayOfReasons.count
        return ReasonsTableViewController.cancelReasonArray.count
//        return reasonModel.arrayOfReasons.count
//        return arrayOfReasons.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CancellationReasonTableViewCell.identifier!) as! CancellationReasonTableViewCell
        cell.cancellationReasonLabel.text = ReasonsTableViewController.cancelReasonArray[indexPath.row]
        cell.ticMarkImage.tintColor = Helper.getUIColor(color: Colors.AppBaseColor)
        if selectedReasonIndex == indexPath.row {
            cell.ticMarkImage.isHidden = false
            
        }else{
            cell.ticMarkImage.isHidden = true
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if selectedReasonIndex >= 0 {
            let selectedCell = tableView.cellForRow(at: IndexPath(row: selectedReasonIndex, section: 0)) as! CancellationReasonTableViewCell
            timePicker.reasonSelected = true
            selectedCell.ticMarkImage.isHidden = true
        }
        
        if let cell = tableView.cellForRow(at: indexPath) as? CancellationReasonTableViewCell {
            cell.ticMarkImage.isHidden = false
            if let reason = cell.cancellationReasonLabel.text {
                self.selectedReason.onNext(reason)
            }
            selectedReasonIndex = indexPath.row
        }

    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        if numberOfTimeIntervals > 0 {
            if timeCollectionCell == nil {
                timeCollectionCell = tableView.dequeueReusableCell(withIdentifier: TimeIntervalCollectionCell.identifier!) as? TimeIntervalCollectionCell
                timeCollectionCell?.timeIntervalCollectionView.delegate = self
                timeCollectionCell?.timeIntervalCollectionView.dataSource = self
            }
            
            return timeCollectionCell
        }else {
            return nil
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if numberOfTimeIntervals > 0 {
            return 80
        }else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
}


extension ReasonsTableViewController:UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfTimeIntervals
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let timeIntervalCell = collectionView.dequeueReusableCell(withReuseIdentifier: TimeIntervalCell.identifier, for: indexPath) as! TimeIntervalCell
        timeIntervalCell.timeIntervalLabel.text = "\((indexPath.row + 1) * 15) min"
        return timeIntervalCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width:Int = Int(collectionView.frame.width)
        return  CGSize(width: (width - 5 * (numberOfTimeIntervals + 1))/numberOfTimeIntervals, height: 40)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedIndex >= 0 {
            let cell = collectionView.cellForItem(at: IndexPath.init(row: selectedIndex, section: 0)) as! TimeIntervalCell
            cell.deSelectCell()
            selectedIndex = -1
        }
        
        let cell = collectionView.cellForItem(at: indexPath) as! TimeIntervalCell
        cell.selectedCell()
        
        delayTime =  Date.getDateString(value: Date().adding(minutes: 15 * (indexPath.row + 1)), format: DateFormat.DateAndTimeFormatServer)
        selectedIndex = indexPath.row
    }
}

extension ReasonsTableViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(15, 5, 15, 5)
    }
}



