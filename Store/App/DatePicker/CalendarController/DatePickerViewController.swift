//
//  DatePickerViewController.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 07/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
//import RxSwift

protocol DatePickerViewControllerDelegate {
    func datePicked(date:Any)
}

class DatePickerViewController: UIViewController,CalendarViewDataSource, CalendarViewDelegate {
    
    
//    let calenderPublishSubject = PublishSubject<Any>()
//    let disposeBag = DisposeBag()
    
    var delegate:DatePickerViewControllerDelegate?
    
    @IBOutlet weak var calendarView: CalendarView!
//    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCanlendar()
        
        // Do any additional setup after loading the view.
    }
    
    func setCanlendar() {
        CalendarView.Style.cellShape                = .bevel(8.0)
        CalendarView.Style.cellColorDefault         = UIColor.clear
        CalendarView.Style.cellColorToday           = UIColor(red:1.00, green:0.84, blue:0.64, alpha:1.00)
        CalendarView.Style.cellSelectedBorderColor  = UIColor(red:1.00, green:0.63, blue:0.24, alpha:1.00)
        CalendarView.Style.cellEventColor           = UIColor(red:1.00, green:0.63, blue:0.24, alpha:1.00)
        CalendarView.Style.headerTextColor          = UIColor.white
        CalendarView.Style.cellTextColorDefault     = UIColor.white
        CalendarView.Style.cellTextColorToday       = UIColor(red:0.31, green:0.44, blue:0.47, alpha:1.00)

        
        calendarView.dataSource = self
        calendarView.delegate = self
        calendarView.direction = .vertical
        calendarView.multipleSelectionEnable = false
//        calendarView.marksWeekends = true
       // calendarView.goToPreviousMonth()
        calendarView.backgroundColor = UIColor(red:0.31, green:0.44, blue:0.47, alpha:1.00)
        
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        EventsLoader.load(from: self.startDate(), to: self.endDate()) { // (events:[CalendarEvent]?) in
            if let events = $0 {
                self.calendarView.events = events
            } else {
                // notify for access not access not granted
            }
        }
        
        let today = Date()
        self.calendarView.setDisplayDate(today)
        self.calendarView.goToMonthWithOffet(0,animated: false)
    }

    
    // MARK : KDCalendarDataSource
    
    func startDate() -> Date {
        var dateComponents = DateComponents()
        dateComponents.month = -1
         dateComponents.timeZone = Helper.getTimeZoneFromPickUpLoaction()
        let today = Date()
        let threeMonthsAgo = self.calendarView.calendar.date(byAdding: dateComponents, to: today)!
        
        return Calendar.current.date(byAdding: .month, value: -10, to: Date())!
    }
    
    func endDate() -> Date {
        var dateComponents = DateComponents()
        dateComponents.month = 0;
        dateComponents.day = -10
        dateComponents.timeZone = Helper.getTimeZoneFromPickUpLoaction()
        let today = Date()
        let twoYearsFromNow = self.calendarView.calendar.date(byAdding: dateComponents, to: today)!
        return Calendar.current.date(byAdding: .month, value: 10, to: Date())!
    }
    
    
    // MARK : KDCalendarDelegate
    
    func calendar(_ calendar: CalendarView, didSelectDate date : Date, withEvents events: [CalendarEvent]) {
        
        print("Did Select: \(date) with \(events.count) events")
        
        if let delegate = self.delegate {
            delegate.datePicked(date: date)
        }
        
  //    self.calenderPublishSubject.onNext(date)
        dismiss(animated: true, completion: nil)
//        for event in events {
//            print("\t\"\(event.title)\" - Starting at:\(event.startDate)")
//        }
        
    }
    
    @IBAction func dismissViewController(sender:UIButton) {
        
        if let delegate = self.delegate {
            delegate.datePicked(date: "")
        }
//        self.calenderPublishSubject.onNext("")
        dismiss(animated: true, completion: nil)
    }
    
    func calendar(_ calendar: CalendarView, didScrollToMonth date : Date) {

//        self.datePicker.setDate(date, animated: true)
    }
    
//    // MARK : Events
//    
//    @IBAction func onValueChange(_ picker : UIDatePicker) {
//        self.calendarView.setDisplayDate(picker.date, animated: true)
//    }
//    
//    @IBAction func goToPreviousMonth(_ sender: Any) {
//        self.calendarView.goToPreviousMonth()
//    }
//    @IBAction func goToNextMonth(_ sender: Any) {
//        self.calendarView.goToNextMonth()
//        
//    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}

