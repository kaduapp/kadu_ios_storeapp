//
//  AddImageTVCell.swift
//  DelivX Store
//
//  Created by 3Embed on 25/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

typealias  addImageTVCellConfig = TableCellConfigurator< AddImageTVCell , UIImage >


protocol AddImageTVCellDelegate : class {
    func addImageButtonPressed()
}
class AddImageTVCell: UITableViewCell , ConfigurableCell{

    weak var delegate : AddImageTVCellDelegate?
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var img_ID: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    func configure(data: UIImage) {
        img_ID.image = data
    }
        
    @IBAction func pressedAddImageButton(_ sender: UIButton) {
        delegate?.addImageButtonPressed()
    }
    
}
