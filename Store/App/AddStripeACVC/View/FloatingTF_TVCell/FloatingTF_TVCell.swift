//
//  FloatingTF_TVCell.swift
//  DelivX Store
//
//  Created by 3Embed on 25/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

typealias floatingTF_TVCellConfig = TableCellConfigurator<FloatingTF_TVCell , (String , String , Bool )>

class FloatingTF_TVCell: UITableViewCell  , ConfigurableCell{


    @IBOutlet weak var leftTF: UITextField!
    
    @IBOutlet weak var rightTF: UITextField!
    
    @IBOutlet weak var rightStackView: UIStackView!
    var textData = ""
    var textData2 = ""
    var isHiddenRightTF = false
    override func awakeFromNib() {
        super.awakeFromNib()
        if textData != "" {
            leftTF.text = textData
        }
        
        if textData2 != "" {
            rightTF.text = textData2
        }
    }


    func configure(data: ( String , String ,Bool)) {
        
        leftTF.placeholder = data.0
        isHiddenRightTF = data.2
        if data.2 {
            rightStackView.isHidden = true
        }else{
            rightTF.placeholder = data.1
            rightStackView.isHidden = false
        }
    }
}



extension FloatingTF_TVCell : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
}





    
    

