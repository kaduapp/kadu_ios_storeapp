//
//  AddStripeACVC.swift
//  DelivX Store
//
//  Created by 3Embed on 25/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

protocol AddStripeACVCDelegate : class  {
    func registerSucessFully()
}

class AddStripeACVC: UIViewController {
    
    
    private var date = ""
    private var paramsArray = [String]()
    let NEW_ACCOUNT_REG = 0
    let NEW_ACCOUNT_ADD = 1
    private var imageName = ""
    private var isAddParams = false
    let storeURL = "store/BankProof/"
    private var common_Variables_and_Methods : Common_Variables_and_Methods!
    private var configrator : [CellConfigurator] = []
    private var imagePicker = UIImagePickerController()
    private var selectedImage = UIImage()
    
    let addStripeACViewModel = AddStripeACViewModel()
    weak var delegate : AddStripeACVCDelegate?
    var pageType = 0
    
    /// place holder array , to use display and no. of textField in viewController
    let placeHolderArray = ["Date of Birth(mm/dd/yyyy)",
                            "SSN" , "Address" , "City" , "State",
                            "Zip Code" ]
    let placeHolderArray2 = ["Name" , "Account Number" , "Rounting Number"]
    let placeHolderArray3 = ["Name" , "IBN Number"]
    
    @IBOutlet weak var lbl_Title: RobotoBoldLable!
    @IBOutlet weak var popupView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        common_Variables_and_Methods = Common_Variables_and_Methods.getInstance()
        addShadowToBar()
        addObserver()
        tableView.delegate = self
        tableView.dataSource = self
        self.lbl_Title.text = StringConstants.addStripeAC()
        self.registerCell()
        self.presentAnimation()
        
        if pageType == NEW_ACCOUNT_REG {
            self.configration()
        }else if pageType == NEW_ACCOUNT_ADD {
           self.configration1()
        }
    }
    
    
    private func registerCell(){
        tableView.register(UINib.init(nibName: "FloatingTF_TVCell", bundle: nil), forCellReuseIdentifier: "FloatingTF_TVCell")
        tableView.register(UINib.init(nibName: "AddImageTVCell", bundle: nil), forCellReuseIdentifier: "AddImageTVCell")
    }
    
    
    
    
    // for adding new account
    private func configration1(){
        configrator.removeAll()
        let store = StoreUtility.getStore()
        if store.defaultBankAccount == "Account Number"{
            for item in placeHolderArray2{
                configrator.append(floatingTF_TVCellConfig(item: (item, "", true)))
            }
        }else{
            for item in placeHolderArray3{
                configrator.append(floatingTF_TVCellConfig(item: (item, "", true)))
            }
        }
        tableView.reloadData()
    }
    
    
    
    // for registring new stripe accunt
    private func configration(){
        configrator.removeAll()
        configrator.append(addImageTVCellConfig(item: #imageLiteral(resourceName: "camera")))
        configrator.append(floatingTF_TVCellConfig(item: ("First Name", "Last Name", false )))
        for item in placeHolderArray{
             configrator.append(floatingTF_TVCellConfig(item: (item, "", true)))
        }
        tableView.reloadData()
    }
    
    
    
    /// Open an alertviewController for selecting options(Camera , Gallery) for adding image
    private func openImagePickerOptionsView(){
        
        let actionSheet = UIAlertController(title: "", message: "Select image source", preferredStyle: .actionSheet)
        
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.openCamera()
        }
        
        let gallery = UIAlertAction(title: "Gallery", style: .default) { (action) in
            self.openGallery()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        
        actionSheet.addAction(camera)
        actionSheet.addAction(gallery)
        actionSheet.addAction(cancel)
        
        if !self.isIphone() {
            self.addActionSheetForiPad(actionSheet: actionSheet)
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    /// Open Gallery Option
    private func openGallery(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    /// Open Camera 
    private func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            print("Button capture")
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }else {
            common_Variables_and_Methods.getAlert(vc: self, title: "", msg: "Camera is not available ") { (action) in
            }
        }
    }
    
    /// fetching data from textfields
    private func registerNewAccount(){
        
        Helper.showPI(string: "")
        var array = [String]()
        for (i ,item) in configrator.enumerated(){
            
            let indexPathID = IndexPath(row: i, section: 0)
            let cell = tableView.cellForRow(at: indexPathID)
        
            if item is floatingTF_TVCellConfig{
                let cellView = (cell as! FloatingTF_TVCell)
                if !cellView.isHiddenRightTF {
                    if checkTextFieldEmpty(textFld: cellView.leftTF) {
                        common_Variables_and_Methods.getAlert(vc: self, title: "", msg: "Enter First Name") {
                            (action) in
                        }
                        isAddParams = false
                    }else{
                        array.append( cellView.leftTF.text! )
                        isAddParams = true
                    }
                    
                    if checkTextFieldEmpty(textFld:cellView.rightTF){
                        common_Variables_and_Methods.getAlert(vc: self, title: "", msg: "Enter Last Name") {
                            (action) in
                        }
                        isAddParams = false
                    }else{
                        array.append( cellView.rightTF.text ?? "" )
                        isAddParams = true
                    }
                }else{
                    if checkTextFieldEmpty(textFld: cellView.leftTF){
                        if i-2 == 0{
                            common_Variables_and_Methods.getAlert(vc: self, title: "", msg: "Select a Date ") {
                                (action) in
                            }
                        }else{
                            common_Variables_and_Methods.getAlert(vc: self, title: "", msg: "Enter \(placeHolderArray[i-2])") {
                                (action) in
                            }
                        }
                        isAddParams = false
                    }else{
                        array.append( cellView.leftTF.text! )
                        isAddParams = true
                    }
                }
            }
        }
        if isAddParams{
            self.addStripeACViewModel.registerForStripe(params: self.createParams(array: array)) { (isSuccess) in
                if isSuccess {
                   Helper.hidePI()
                    self.delegate?.registerSucessFully()
                    self.closeVC()
                }
            }
        }
    }
   
    
    /// fetching data from textfields
    private func addNewAccount(){
        var array = [String]()
        
        for (i ,item) in configrator.enumerated(){
            let indexPathID = IndexPath(row: i, section: 0)
            let cell = tableView.cellForRow(at: indexPathID)
            
            if item is floatingTF_TVCellConfig{
                let cellView = (cell as! FloatingTF_TVCell)
                if cellView.isHiddenRightTF {
                    
                    if StoreUtility.getStore().defaultBankAccount == "Account Number"{
                        if checkTextFieldEmpty(textFld: cellView.leftTF){
                            common_Variables_and_Methods.getAlert(vc: self, title: "", msg: "Enter \(cellView.leftTF.placeholder!)") {
                                (action) in
                            }
                            isAddParams = false
                        }else{
                            array.append( cellView.leftTF.text! )
                            isAddParams = true
                        }
                    }else{
                        
                        if checkTextFieldEmpty(textFld: cellView.leftTF){
                            common_Variables_and_Methods.getAlert(vc: self, title: "", msg: "Enter \(cellView.leftTF.placeholder!)") {
                                (action) in
                            }
                            isAddParams = false
                        }else{
                            array.append( cellView.leftTF.text! )
                            isAddParams = true
                        }
                    }
                }
            }
        }
        if isAddParams{
            Helper.showPI(string: "")
            self.addStripeACViewModel.addExternalAccount(params: self.createParamsForExternalAC(array: array)) { (isSuccess) in
                if isSuccess {
                    Helper.hidePI()
                    self.delegate?.registerSucessFully()
                    self.closeVC()
                }
            }
        }
    }
    
    @IBAction func pressedSaveButton(_ sender: UIButton ) {
        self.view.endEditing(true)
        
        if pageType == NEW_ACCOUNT_REG {
            uplaodImageToAmazon()
        }else{
            addNewAccount()
        }
    }
    
    /// upload image to amazon bucket
    private func uplaodImageToAmazon(){
        
        if imageName == ""{
            common_Variables_and_Methods.getAlert(vc: self, title: "", msg: "Select an image") { (action) in
            }
        }else{
            let upMoadel = UploadImageModel.shared
            let imageData = [UploadImage(image: selectedImage, path: imageName)]
            upMoadel.uploadImages = imageData
            
            upMoadel.delegate = self
            Helper.showPI(string: "")
            upMoadel.start()
        }
    }
    
    
    /// method for checking textfield is not empty
    private func checkTextFieldEmpty( textFld : UITextField) -> Bool{
        
        if (textFld.text?.isEmpty)! && textFld.text == ""{
            return true
        }else{
            return false
        }
    }
    
    /// creating params from providing array (adding new bank account )
    private func createParamsForExternalAC( array : [String]) -> [String : Any]{
    
        var routingNumber = ""
        let store = StoreUtility.getStore()
        if array.count == 3 {
            routingNumber = array[2]
        }
        
            let params :  [String : Any] = [
                APIRequestParams.account_holder_name : array[0],
                APIRequestParams.account_number : array[1],
                APIRequestParams.routing_number : routingNumber,
                APIRequestParams.currency : store.currency,
                APIRequestParams.userId : store.storeId,
                APIRequestParams.country : store.countryCode ,
                APIRequestParams.Email : Utility.emailID
            ]
            print(params)
            return params
       
    }
    
    /// creating params from providing array (adding stripe account )
    func createParams( array : [String]) -> [String : Any]{
    
        let params :  [String : Any] = [
            APIRequestParams.firstName : array[0],
            APIRequestParams.lastName : array[1],
            APIRequestParams.date : array[2],
            APIRequestParams.month : String(array[2].split(separator: "/")[0]),
            APIRequestParams.day : String(array[2].split(separator: "/")[1]),
            APIRequestParams.year : String(array[2].split(separator: "/")[2]),
            APIRequestParams.personal_id_number : array[3],
            APIRequestParams.line1 : array[4],
            APIRequestParams.city : array[5],
            APIRequestParams.state : array[6],
            APIRequestParams.postal_code : array[7],
            APIRequestParams.userId : StoreUtility.getStore().storeId,
            APIRequestParams.country : StoreUtility.getStore().countryCode ,
            APIRequestParams.document : imageName ,  // amazone link
            APIRequestParams.ip : self.common_Variables_and_Methods.getIPAddress(),
            APIRequestParams.Email : Utility.emailID
        ]
        
        print(params)
        return params
    }
    
    // close or dismiss view
    @IBAction func pressedCloseButton(_ sender: UIButton) {
        closeVC()
    }
    
    private func closeVC(){
        if self.isIphone() {
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
        }else {
            dismissViewController()
        }
    }
    
    private func presentAnimation() {
        self.navigationController?.navigationBar.isHidden = true
        self.paintSafeAreaBottomInset(withColor: UIColor.white)
        self.paintSafeAreaTopInset()
    }
    
    func dismissViewController() {
        UIView.animate(withDuration: 0.5, animations: {
            self.popupView.layer.opacity = 0.01
            self.popupView.layer.transform = CATransform3DMakeScale(0.1,0.1,4)
            if UIDevice.current.userInterfaceIdiom == .phone {
                self.hideBottomTopSafeArea()
            }
        }) { success in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// tableview delegate and dataSource
extension AddStripeACVC : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if configrator.count > 0{
            return configrator.count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.configrator[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
        
        if self.configrator[indexPath.row] is floatingTF_TVCellConfig {
            (cell as! FloatingTF_TVCell).leftTF.delegate = self
            (cell as! FloatingTF_TVCell).rightTF.delegate = self
        }
        if self.configrator[indexPath.row] is addImageTVCellConfig {
            (cell as! AddImageTVCell).delegate = self
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func openCalander(){
        let storyBorad1 = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil)
        let selector = storyBorad1.instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
        // 2. You can then set delegate, and any customization options
        selector.optionSelectionType = .single
        selector.optionRangeOfEnabledDates = .past
        selector.optionButtonShowCancel = true
        selector.optionTintColor = UIColor(named: "ThemeColor")!
        selector.optionButtonFontColorDone = .black
        selector.optionMultipleSelectionGrouping = .pill
        selector.optionTopPanelTitle = "Select Date"
        selector.delegate = self
        
        // 3. Then you simply present it from your view controller when necessary!
        self.present(selector, animated: true, completion: nil)
        
        self.view.layoutSubviews()
    }
    
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.configrator[indexPath.row] is addImageTVCellConfig {
            return 100
        }else if self.configrator[indexPath.row] is floatingTF_TVCellConfig{
            return 60
        }else {
            return 0
        }
    }
}


// textField Delegate methods
extension  AddStripeACVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if let plcholder = textField.placeholder{
            if plcholder == "Date of Birth(mm/dd/yyyy)"{
                self.view.endEditing(true)
                openCalander()
                return false
            }
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.placeholder == "SSN"  ||  textField.placeholder == "Zip Code" || textField.placeholder == "Account Number" || textField.placeholder == "Routing Number" {
            textField.keyboardType = UIKeyboardType.numberPad
            if range.location == 0{
            }
            
            let ACCEPTABLE_NUMBERS = "1234567890"
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_NUMBERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        }
        return true
        
    }
    
    
    func addDoneButtonOnKeyboard(textField:UITextField)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        done.tintColor = UIColor.black
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        textField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        self.view.endEditing(true)
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.placeholder == "SSN"  ||  textField.placeholder == "Zip Code" || textField.placeholder == "Account Number" || textField.placeholder == "Routing Number" {
            textField.keyboardType = UIKeyboardType.numberPad
        }
    
        if textField.placeholder == "Date of Birth(mm/dd/yyyy)"{
            self.view.endEditing(true)
            openCalander()
        }else{
            addDoneButtonOnKeyboard(textField: textField)
        }
    }
}

// AddImageTVCellDelegate method
extension AddStripeACVC : AddImageTVCellDelegate {
    func addImageButtonPressed() {
        self.openImagePickerOptionsView()
    }
}


extension AddStripeACVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func creatingFileName (){
    
        let distanceTime = String(format: "%.0f", Date.getTimeStamp)

        imageName = "image" + distanceTime + ".png"
        let finalUrl = storeURL + imageName
        imageName = finalUrl
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        selectedImage = chosenImage
        
        self.configrator.remove(at: 0)
        self.creatingFileName()
        self.configrator.insert(addImageTVCellConfig(item: chosenImage) , at:0)
        self.tableView.reloadData()
        picker.dismiss(animated:true, completion: nil)
    }
}


extension AddStripeACVC : WWCalendarTimeSelectorProtocol{
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        
        let displayDate = Date.getDateString(value: date, format: DateFormat.DateOfBirthFormat)
        print(displayDate)
        self.date = displayDate
        let indexPathID = IndexPath(row: 2, section: 0)
        let cell = tableView.cellForRow(at: indexPathID)
        let cellView = (cell as! FloatingTF_TVCell)
        cellView.leftTF.text = displayDate
    }
}

extension AddStripeACVC : UploadImageModelDelegate {
    
    func callBackWithURL(isSuccess: Bool, url: String) {
        DispatchQueue.main.async {
            self.imageName = AMAZON_URL + self.imageName
            self.registerNewAccount()
        }
    }
}


