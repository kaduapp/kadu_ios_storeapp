//
//  AddStripeACViewModel.swift
//  DelivX Store
//
//  Created by 3Embed on 26/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
class AddStripeACViewModel {
    let disposeBag = DisposeBag()
    
    func registerForStripe(params : [String : Any] , onCompletionHandler:@escaping(Bool ) -> Void ){
        let header = APIUtility.getHeaderForOrders()
        
        APIHandler.postRequest(header: header, url: APIEndTails.accountConnect, params: params).subscribe(onNext: {success, data in
            if success {
                print(data)
                onCompletionHandler(success)
                
                print("Successfully dispatched")
            }else {
                 onCompletionHandler(success)
            }
        }, onError: { Error in
            print(Error.localizedDescription)
        }).disposed(by: disposeBag)
        
    }
    
    func addExternalAccount(params : [String : Any] , onCompletionHandler:@escaping(Bool ) -> Void ){
        let header = APIUtility.getHeaderForOrders()
        
        APIHandler.postRequest(header: header, url: APIEndTails.addExternalAccount, params: params).subscribe(onNext: {success, data in
            if success {
                print(data)
                 onCompletionHandler(success)
                print("Successfully dispatched")
            }else {
                onCompletionHandler(success)
            }
        }, onError: { Error in
            print(Error.localizedDescription)
        }).disposed(by: disposeBag)        
    }
    

}


