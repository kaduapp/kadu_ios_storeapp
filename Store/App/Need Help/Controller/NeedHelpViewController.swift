//
//  NeedHelpViewController.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 07/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxAlamofire
import RxSwift
import WebKit

class NeedHelpViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    let disposeBag = DisposeBag()
    
    let licence_url = "https://cdn.livechatinc.com/app/mobile/urls.json"
    let licence_Key = "4711811"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addShadowToBar() 
        self.setNavigationBarItem()
        webView.navigationDelegate = self
        self.navigationItem.title = LeftMenuData.Data[LeftMenu.needHelpVC.rawValue].MenuText
        webViewConfiguration()
    }
    
    func webViewConfiguration() {
        webView.layer.cornerRadius = 5;
        webView.layer.masksToBounds = true

        if self.isIphone() {
            self.paintSafeAreaBottomInset(withColor: UIColor.white)
        }
        
        Helper.showPI(string: "Loading...")
        getLiveChatAPIRequest(completionHandler: { response in
            guard let url = response as? URLRequest else {
                return
            }
            Helper.showPI(string: "Loading...")
            //self.webView.loadRequest(url)
             self.webView.load(url)
        })
    }
    
    
    @IBAction func dismissViewController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension NeedHelpViewController {
   
    
    func getLiveChatAPIRequest(completionHandler: @escaping (Any) -> ()) {
        
        RxAlamofire.requestJSON(.get, licence_url, parameters: [:], headers: [:]).debug().subscribe(onNext: { (head, body) in
            let errNum:APIStatusCode.ErrorCode? = APIStatusCode.ErrorCode(rawValue: head.statusCode)!
            if errNum == .success {
                let dataIn = body as! [String : Any]
                if dataIn.isEmpty == false {
                    
                    if (dataIn["error"] == nil) {
                        guard let url = dataIn["chat_url"] as? String else {
                              completionHandler(false)
                              return
                          }
                          guard let url1 = url as? String else {
                               completionHandler(false)
                              return
                          }
                             let url2 = self.prepareURL(url1)
                          let urlString = url2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                          let url3 = URL(string: urlString ?? "")
                          
                          let request = URLRequest(url: url3!)
                          completionHandler(request)
                      } else {
                          completionHandler(false)
                      }
                }else {
                   Helper.hidePI()
                }
            }else{
                Helper.hidePI()
            }
        }, onError: { (Error) in
            Helper.hidePI()
        }).disposed(by: disposeBag)
        }
    
    /// Prepare URL
    ///
    /// - Parameter url: URL after Requesting
    /// - Returns: URL to be loaded on WebPage
    func prepareURL(_ url: String) -> String {
        
        var string: String = "https://\(url)"
        string = string.replacingOccurrences(of: "{%license%}", with: licence_Key)
        string = string.replacingOccurrences(of: "{%group%}", with: Utility.AppName)
        return string
    }

}

extension NeedHelpViewController:WKNavigationDelegate {
    
     func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)  {
        Helper.hidePI()
    }
    
     func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        Helper.hidePI()
    }
    
   func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        Helper.showPI(string: "Loading...")
    }
}

