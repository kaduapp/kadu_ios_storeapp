//
//  LanguageTableCell .swift
//  DelivX Store
//
//  Created by Rahul Sharma on 08/10/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//


import UIKit

class LanguageTableCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var seperator: UIView!
    @IBOutlet weak var tickMark: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
