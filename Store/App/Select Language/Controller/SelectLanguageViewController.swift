//
//  SelectLanguageViewController.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 08/10/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import RxAlamofire
import Alamofire
import CocoaLumberjack
let disposeBag = DisposeBag()
protocol SeletedLanguageDelegate {
    /// When Seletced Language
    ///
    /// - Parameter good: Object of Language
}

class SelectLanguageViewController:UIViewController {
   @IBOutlet weak var tableView: UITableView!
    var delegate: SeletedLanguageDelegate?
    
    fileprivate var timer = Timer()
    fileprivate var arrayOfLanguage = [[String:Any]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        addShadowToBar() 
        self.setNavigationBarItem()
        self.navigationItem.title = LeftMenuData.Data[LeftMenu.selectLanguageVC.rawValue].MenuText
    self.sendRequestForLanguageType()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    @IBAction func backButton(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func dismissViewController(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    func sendRequestForLanguageType(){
        Helper.showPI(string: "")
        RxAlamofire.requestJSON(.get,
                                APIEndTails.getLanguage,
                                parameters: nil,
                                headers: nil)
            .subscribe(onNext: { (r,json) in
                if  let dict  = json as? [String:Any] {
                    Helper.hidePI()
                    let statuscode:Int = r.statusCode
                    DDLogVerbose("Status Code = \(statuscode) and response = \(dict)")
                    if let dict = json as? [String:Any] {
                        DDLogVerbose("dict response = \(dict)")
                        if((dict["data"]) != nil){
                            let langDetails = dict["data"]
                                self.arrayOfLanguage = []
                                self.arrayOfLanguage = langDetails as! [[String : Any]]
                            
                        self.tableView.reloadData()
                        }
                    }
                }
            }, onError: { (error) in
            
                Helper.hidePI()
            }, onCompleted: {
                DDLogInfo("API Response Completed")
            }, onDisposed:{
                DDLogInfo("API Response Disposed")
            }).disposed(by: disposeBag)
    }
}

extension SelectLanguageViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfLanguage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "languageCell") as! LanguageTableCell
        
        let model = arrayOfLanguage[indexPath.row]
        if Utility.langCode == model["langCode"] as! String || Utility.langCode.contains(model["langCode"] as! String){
            cell.tickMark.isHighlighted = true
        }
        cell.descriptionLabel?.text = (model["lan_name"] as! String)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension SelectLanguageViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell =  tableView.cellForRow(at: indexPath) as! LanguageTableCell
        cell.tickMark.isHighlighted = true
        let ud = UserDefaults.standard
        ud.set(String(describing:self.arrayOfLanguage[indexPath.row]["langCode"]!), forKey: "langCode")
        ud.set(String(describing:self.arrayOfLanguage[indexPath.row]["lan_name"]!), forKey: "lan_name")
        OneSkyOTAPlugin.setLanguage(Utility.langCode)
        ud.synchronize()
        
        if LeftViewController.obj != nil {
       LeftViewController.sharedInstance().tableView.reloadData()
        }

        tableView.reloadData()
        
        
        DispatchQueue.main.async(execute: {
            self.addMenu()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let splash = storyboard.instantiateInitialViewController()
            let window = UIApplication.shared.keyWindow
                window?.rootViewController = splash
        })
        
    }
}

