//
//  ForgotPasswordViewController.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 21/03/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift

class ForgotPasswordViewController: UIViewController,UITextFieldDelegate {
    //submitButtonBackgroundView
    @IBOutlet weak var submitButtonBackgroundView: UIView!
    @IBOutlet weak var backgroundScrollView: UIScrollView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var emialTextField: UITextField!
    @IBOutlet weak var instructionText: UILabel!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        instructionText.text = StringConstants.forgotInstruction()
        submitButton.setTitle(StringConstants.submit(), for: UIControlState.normal)
        emialTextField.placeholder = StringConstants.email()
        submitButtonSubscription()
    }
    
    func submitButtonSubscription() {
        
        submitButton.rx.tap.subscribe(onNext:{[weak self] in
            self?.view.endEditing(true)
            if (self?.emialTextField.text?.isEmpty)!{
                Helper.showAlert(message: StringConstants.enterEmail(), head: StringConstants.message(), type: 0)
            }else{
                
                if (self?.emailValidation(email: (self?.emialTextField.text!)!))! {
                    self?.forgotPasswordAPI()
                }
            }
        }).disposed(by: disposeBag)
    }
    
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        self.view.endEditing(true)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+0.5, execute: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addKeyboardObserver()
        
           self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard)))
    }
    
    @objc func dismissKeyBoard() {
        self.view.endEditing(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeKeyboardObserver()
    }
    
    func addKeyboardObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.addKeyboardObserver(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeKeyboardObserver(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func removeKeyboardObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //Mark:- Keyboard ANimation
    @objc func addKeyboardObserver(_ notification: NSNotification){
        // Do something here
        //Need to calculate keyboard exact size due to Apple suggestions
        let inputViewFrame: CGRect? = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect
        let frame = self.view.convert(self.submitButtonBackgroundView.frame, to: nil)
        let scrollOffSet = (inputViewFrame?.height)! - (UIScreen.main.bounds.height - frame.origin.y - 60)
        
        if scrollOffSet <= 0 {
            return
        }

        backgroundScrollView.setContentOffset(CGPoint(x: 0, y: scrollOffSet), animated: true)
    }
    
    @objc func removeKeyboardObserver(_ notification: NSNotification){
        // Do something here
        backgroundScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func emailValidation(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if emailTest.evaluate(with: email) {
            return true
        }else {
            Helper.showAlert(message: StringConstants.invalidMail() , head: StringConstants.message(), type: 0)
            return false
        }
    }
    
    func forgotPasswordAPI() {
        
        let header = APIUtility.getHeader()
        let params:[String:Any] = ["emailOrMobile":emialTextField.text ?? "",
                                   APIRequestParams.VerifyType:"2"]
        APIHandler.postRequest(header: header, url: APIEndTails.ForgotPassword, params: params).subscribe(onNext: {[weak self] success,data in
            if success {
                if let data = data as? [String:Any]{
                    Helper.showAlert(message: data[APIResponseParams.Message] as! String, head: StringConstants.Error() , type: 0)
                }
                
                self?.backButtonAction(UIButton())
            }
        }, onError: { Error in
            print("\(Error)")
        }).disposed(by: disposeBag)
    }
    
    
    deinit {
        print("Forgot Password Deinited")
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
}
