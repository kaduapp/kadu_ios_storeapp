//
//  LoginVM.swift
//  DelivX Store
//
//  Created by 3 Embed on 21/12/17.
//  Copyright © 2017 3 Embed. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class LoginVM {
    
    let disposeBag = DisposeBag()
    let login_response = PublishSubject<Bool>()
    func gotoLogin(email:String , password: String) {
                
        let params : [String : Any] = [
            APIRequestParams.Email          :       email,
            APIRequestParams.Password       :       password,
            APIRequestParams.DeviceID       :       Utility.DeviceId,
            APIRequestParams.DeviceMake     :      Utility.DeviceMake,
            APIRequestParams.DeviceModal    :       Utility.DeviceModel,
            APIRequestParams.DeviceType     :       Utility.DeviceType,
            APIRequestParams.DeviceTime     :       Date.getTimeStamp,
            APIRequestParams.AppVersion     :       Utility.AppVersion,
            "deviceOsVersion" : Utility.DeviceVersion
        ]
        
        let header = APIUtility.getHeader()
        APIHandler.postRequest(header: header, url: APIEndTails.Login, params: params).subscribe(onNext: { (success, response) in
            if success == true {
                
                if let dict = response as? [String:Any]{
                let storeData = Helper.removeNullKeys(data: dict)
                UserDefaults.standard.set(storeData, forKey: UserDefaultConstants.StoreData)
                UserDefaults.standard.synchronize()
                }
                Utility.setStoreData(data: response)
                StoreUtility.subscribeFCMTopic()
                Utility.connectMQTT()
                self.login_response.onNext(true)
            }else{
                self.login_response.onNext(false)
            }
            
        }, onError: { Error in
            print(Error)
            self.login_response.onNext(false)
            Helper.hidePI()
        }).disposed(by: disposeBag)
    }


    func emailValidation(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
        
    }
    
    
    
    
    
    
    
}


