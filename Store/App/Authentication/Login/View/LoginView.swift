//
//  LoginView.swift
//  DelivX Store
//
//  Created by 3 Embed on 18/12/17.
//  Copyright © 2017 3 Embed. All rights reserved.
//

import UIKit

class LoginView: UIView {
    
    @IBOutlet weak var logo: UIImageView!
//    @IBOutlet weak var userNameView: UIView!
//    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var pwSeperator: UIView!
    @IBOutlet weak var userNSeperator: UIView!
    @IBOutlet weak var loginBtnView: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var forgotPassword: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
    }
    
    /// INITIAL VIEW SETUP
    func initialSetup(){
     
        Fonts.setPrimaryRegular(userName)
        Fonts.setPrimaryRegular(password)
        Fonts.setPrimaryBold(loginBtn)
    
        userName.textColor = Helper.getUIColor(color:Colors.PrimaryText)
        password.textColor = Helper.getUIColor(color: Colors.PrimaryText)
        //Contraseña
        
       if Utility.langCode == "es"{
          password.placeholder = "Contraseña"
       }else{
         password.placeholder = StringConstants.password()
        }
        
        if Utility.langCode == "es"{
           userName.placeholder = StringConstants.email()
        }else{
          userName.placeholder = StringConstants.email()
         }
        
        pwSeperator.backgroundColor = Helper.getUIColor(color: Colors.Separator)
        userNSeperator.backgroundColor = Helper.getUIColor(color: Colors.Separator)
        
        Helper.setButton(button:loginBtn , view: loginBtnView, primaryColour: Helper.getUIColor(color: Colors.AppBaseColor), seconderyColor: Helper.getUIColor(color: Colors.White), shadow: true)
        
        if Utility.langCode == "es"{
           forgotPassword.setTitle("Se te olvidó tu contraseña?", for: UIControlState.normal)
        }else{
          forgotPassword.setTitle(StringConstants.forgotPassword(), for: UIControlState.normal)
         }
        
        if Utility.langCode == "es"{
            Helper.setButtonTitle(normal:"Registrate", highlighted:"Registrate", selected:"Registrate", button: loginBtn)
            }else{
            Helper.setButtonTitle(normal: StringConstants.SignIn(), highlighted: StringConstants.SignIn(), selected: StringConstants.SignIn(), button: loginBtn)
          }
    }
    
}
