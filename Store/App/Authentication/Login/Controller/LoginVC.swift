//
//  LoginVC.swift
//  DelivX Store
//
//  Created by 3 Embed on 18/12/17.
//  Copyright © 2017 3 Embed. All rights reserved.
//

import UIKit
import RxSwift
import CoreLocation


class LoginVC: UIViewController {
    
    @IBOutlet weak var LoginScrollView: UIScrollView!
    @IBOutlet weak var loginView: LoginView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    //    @IBOutlet weak var switchBtn: UISwitch!
    //    @IBOutlet weak var firstLanguage: UILabel!
    //    @IBOutlet weak var secondLanguage: UILabel!
    private var a = 0
    let loginVM = LoginVM()
    let disposeBag = DisposeBag()
    var  locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        StoreUtility.unsubscribeFCMTopic()
        viewSetup()
        
        didLoggedIn()
      //  loginView.userName.becomeFirstResponder()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addKeyboardObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeKeyboardObserver()
    }
    
    func addKeyboardObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.addKeyboardObserver(_:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeKeyboardObserver(_:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func removeKeyboardObserver() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //Mark:- Keyboard ANimation
    @objc func addKeyboardObserver(_ notification: NSNotification){
        // Do something here
        //Need to calculate keyboard exact size due to Apple suggestions
        let inputViewFrame: CGRect? = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect
        let frame = self.view.convert(self.loginView.loginBtnView.frame, to: nil)
        let scrollOffSet = (inputViewFrame?.height)! - (UIScreen.main.bounds.height - frame.origin.y - 60)
        
        if scrollOffSet <= 0 {
            return
        }
        LoginScrollView.setContentOffset(CGPoint(x: 0, y: scrollOffSet), animated: true)
    }
    
    @objc func removeKeyboardObserver(_ notification: NSNotification){
        // Do something here
        self.view.endEditing(true)
        LoginScrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginAction(_ sender: Any) {
        self.view.endEditing(true)
        if !loginVM.emailValidation(email: loginView.userName.text!) || loginView.userName.text?.count == 0 {
            Helper.showAlert(message: StringConstants.checkEmail(), head: StringConstants.message(), type: 0)
            return
        }else if loginView.password.text?.count == 0 {
            Helper.showAlert(message: StringConstants.enterPass(), head: StringConstants.message(), type: 0)
        }else {
            
            UserDefaults.standard.set(loginView.userName.text!, forKey: APIRequestParams.Email)
            
           self.loginView.loginBtn.isHidden = true
           self.activityIndicator.startAnimating()
            loginVM.gotoLogin(email:loginView.userName.text! , password: loginView.password.text! )
        }
    }
    
    
    @IBAction func forgotPasswordAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: String(describing:ForgotPasswordViewController.self), sender: nil)
    }
    
    
    @objc func dismissKeyBoard() {
      self.view.endEditing(true)
    }
    
    func didLoggedIn(){
        
        loginVM.login_response.subscribe(onNext: { success in
            self.loginView.loginBtn.isHidden = false
            self.activityIndicator.stopAnimating()
            if success {
                self.navigationBarColor()
                self.addMenu()
            }
        }, onError: { error in
            print(error)
        }).disposed(by: disposeBag)
    }
}

extension LoginVC {
    
    ///Initial UI setup
    func viewSetup(){
        loginView.userName.delegate = self
        loginView.password.delegate = self
       Helper.transparentNavigation(controller: self.navigationController!)
     self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard)))

    }
}

extension LoginVC: UITextFieldDelegate {
    
    //MARK: UITextFieldDelegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == loginView.userName {
            loginView.password.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        
        return true
    }
}

extension LoginVC:CLLocationManagerDelegate{
   
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let coordLatLon = locations.last?.coordinate
        let ud = UserDefaults.standard
        ud.set(coordLatLon?.latitude, forKey: UserDefaultConstants.CurrentLatitude)
        ud.set(coordLatLon?.longitude, forKey: UserDefaultConstants.CurrentLongitude)
        ud.synchronize()
    }
}

