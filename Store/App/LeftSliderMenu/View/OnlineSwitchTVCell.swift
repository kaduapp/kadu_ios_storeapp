//
//  OnlineSwitchTVCell.swift
//  DelivX Store
//
//  Created by 3Embed on 22/07/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

protocol  OnlineSwitchTVCellDelegate : class {
   func pressedSwitchButton(value : Int)
}

class OnlineSwitchTVCell: UITableViewCell {
    weak var delegate : OnlineSwitchTVCellDelegate?
    
    private var on_Off_Val = 0
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var switchButton: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setDataToCell( value  : Int ){
        lbl_Title.text = value == 1 ? "Online" : "Offline"
        on_Off_Val = value == 1 ? 0 : 1
        switchButton.isOn = value == 1 ? true : false
    }
    
    
    @IBAction func pressedSwitchButton(_ sender: UISwitch) {
        delegate?.pressedSwitchButton(value: on_Off_Val)
        
    }
    
}
