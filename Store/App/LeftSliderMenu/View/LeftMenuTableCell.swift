//
//  LeftMenuTableCell.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 12/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

class LeftMenuTableCell: UITableViewCell {
    
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var menuLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    static var identifier:String? {
        return String(describing:self)
    }
    
    
    func setupCellData(data: MenuModel){
        menuImage.image = data.Image
        menuLabel.text = data.MenuText
    }
    
}
