//
//  LeftViewController.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 12/3/14.
//

import UIKit
import RxSwift

enum LeftMenu: Int {
    case mainHomeVC = 0
    case historyVC
    case needHelpVC
    
    case wallet
    case addWallet
    case selectLanguageVC
    case inventoryVC
    case profileVC
    case none
}



protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class LeftViewController : UIViewController, LeftMenuProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var managerProfilePic: CustomImageView!
    @IBOutlet weak var managerName: UILabel!
    @IBOutlet weak var version: UILabel!
    let leftMenuResponse = PublishSubject<Any>()
    
    let disposeBag = DisposeBag()
    var currentPage = 0
    static var obj:LeftViewController?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    class func sharedInstance() -> LeftViewController {
        
        return obj!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        version.text = AppConfig.version
        registerCell()
        LeftViewController.obj = self
        self.tableView.rowHeight = 50
        self.tableView.estimatedRowHeight = UITableViewAutomaticDimension
        
        changeViewController(.mainHomeVC)
        
    }
    
    func registerCell(){
        tableView.register(UINib.init(nibName: "OnlineSwitchTVCell", bundle: nil), forCellReuseIdentifier: "OnlineSwitchTVCell")
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.setManagerData()
        NotificationCenter.default.addObserver(self, selector: Selector(("showOrder")), name: Notification.Name.kNewOrderFCMNotification, object: nil)
    }
    
    /// When clicked on order
    func showOrder() {
        changeViewController(LeftMenu.mainHomeVC)
    }
    
    func setManagerData() {
        self.managerName.text = Utility.getManagerName()
        guard let url =  Utility.getManagerImage() else {
            return
        }
        self.managerProfilePic.setImage(url:url)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    @IBAction func profileButtonAction(_ sender: UIButton) {
        changeViewController(.profileVC)
    }

    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        self.view.layoutIfNeeded()
    }
    
    func segueIndentifier(index:LeftMenu) -> String {
        switch index {
            
        case .mainHomeVC:
             return "HomeSlidesViewController"
            
        case .historyVC:
            return "HistoryViewControllerNav"
        case .needHelpVC:
            return "NeedHelpViewController"
            
        case .wallet:
            return "WalletVC"
        case .addWallet:
            return "AddWalletVC"
        case .selectLanguageVC:
            return "SelectLanguageViewController"
        case .inventoryVC:
            return "inventoryViewController"
        case .profileVC:
            return "ProfileTableViewController"
        case .none:
            return ""
        }
    }
    
    func changeViewController(_ menu: LeftMenu) {
        
        if menu == .none {
            return
        }else if menu.rawValue == currentPage {
            self.slideMenuController()?.closeLeft()
        }else {
            currentPage = menu.rawValue
            let identifier = self.segueIndentifier(index: menu)
            
            if identifier.count == 0 {
                self.slideMenuController()?.closeLeft()
                return
            }
            let vc = self.storyboard?.instantiateViewController(withIdentifier: identifier)
            self.slideMenuController()?.changeMainViewController(vc!, close: true)
            
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}


extension LeftViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LeftMenuData.Data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: LeftMenuTableCell.identifier!, for: indexPath) as! LeftMenuTableCell
            cell.setupCellData(data: LeftMenuData.Data[indexPath.row])
                return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
//        if LeftMenuData.Data.count  > indexPath.row{
            self.changeViewController(LeftMenu(rawValue:indexPath.row)!)
//        }
    }
}


extension LeftViewController : OnlineSwitchTVCellDelegate{
    func pressedSwitchButton(value: Int) {
        
        let leftMenuViewModel = LeftMenuViewModel()
        leftMenuViewModel.forUpDateOnlineStatus(status: value) { (success) in
            
            if success{
                print("it's success")
            }else{
                print("it's failed")
            }
        }
    }
}


class ExSlideMenuController : SlideMenuController {
    
    override func isTagetViewController() -> Bool {
        
                if let vc = UIApplication.topViewController() {
        
                     if vc is HomeSlidesViewController ||
                        vc is HistoryViewController ||
                        vc is NeedHelpViewController ||
                        vc is AboutViewController || vc is SelectLanguageViewController || vc is inventoryViewController || vc is WalletVC || vc is AddWalletVC
                    {
                        return true
                    }
                }
        return false
    }
    
    override func track(_ trackAction: TrackAction) {
        switch trackAction {
        case .leftTapOpen:
//            let mainVc = HomeSlidesViewController()
//            if mainVc.storeDriverDetails != nil{
//                mainVc.storeDriverDetails.didMoveToWindow()
//            }
            print("TrackAction: left tap open.")
        case .leftTapClose:
            print("TrackAction: left tap close.")
        case .leftFlickOpen:
            print("TrackAction: left flick open.")
        case .leftFlickClose:
            print("TrackAction: left flick close.")
        case .rightTapOpen:
            print("TrackAction: right tap open.")
        case .rightTapClose:
            print("TrackAction: right tap close.")
        case .rightFlickOpen:
            print("TrackAction: right flick open.")
        case .rightFlickClose:
            print("TrackAction: right flick close.")
        }
    }
}
