//
//  UIViewControllerExtension.swift
//  SlideMenuControllerSwift
//
//  Created by Yuji Hato on 1/19/15.
//  Copyright (c) 2015 Yuji Hato. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// add shadow on navigation bar
    func addShadowToBar() {
        
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.navigationController?.navigationBar.layer.shadowOpacity = 1.0
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2
        
    }
    
    /// add navigation bar on viewcontoller
    public func setNavigationBarItem() {
        
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        self.slideMenuController()?.addLeftGestures()
        self.navigationController?.navigationBar.isTranslucent = false
        
        self.slideMenuController()?.changeLeftViewWidth(270.0)
        
        let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
            menuButton.setImage(#imageLiteral(resourceName: "menu_icon"), for: .normal)
            menuButton.setImage(#imageLiteral(resourceName: "menu_icon"), for: .highlighted)
        menuButton.addTarget(self, action: #selector(menuButtonAction(sender:)), for: .touchUpInside)
        
        let barButtonItem = UIBarButtonItem(customView: menuButton)
        self.navigationItem.setLeftBarButton(barButtonItem, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    /// add navigation bar on viewcontoller
    public func setNavigationBackButton() {
 
        let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        backButton.setImage(#imageLiteral(resourceName: "close_icon"), for: .normal)
        backButton.setImage(#imageLiteral(resourceName: "close_icon"), for: .highlighted)
        backButton.addTarget(self, action: #selector(backButtonAction(sender:)), for: .touchUpInside)
        
        let barButtonItem = UIBarButtonItem(customView: backButton)
        self.navigationItem.setLeftBarButton(barButtonItem, animated: true)
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    @objc private func menuButtonAction(sender: UIButton) {
        if Utility.langCode == "ar" {
            slideMenuController()?.toggleRight()
        }else {
            slideMenuController()?.toggleLeft()
        }
        
    }
    @objc private func backButtonAction(sender: UIButton) {
  self.navigationController?.popViewController(animated:true)
        
    }
    /// set navigation bar theme
    func navigationBarColor() {
       
        UINavigationBar.appearance().barTintColor = Helper.getUIColor(color:  Colors.AppBaseColor)
       // UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor :  ColorConstants.Color_NavTitleColor,NSAttributedStringKey.font : FontConstants.Font_NavigationTitle]
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white  ]
        UINavigationBar.appearance().barStyle = .blackTranslucent
        
    }
    /// remove navigation bar from viewcontoller
    func removeNavigationBarItem() {
        
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItem = nil
        self.slideMenuController()?.removeLeftGestures()
        self.slideMenuController()?.removeRightGestures()
        
    }
    
    /// That mathed return tab count of MainHomeViewContoller
    func getTabCounts() -> Int{
        
        if  Utility.getISForcedAccept() == 1 && Utility.getIsAutoDispatch() == 1 {
            return 3
        }  else if  Utility.getISForcedAccept() == 0 && Utility.getIsAutoDispatch() == 1 {
            return 4
        }else if Utility.getISForcedAccept() == 0 && Utility.getIsAutoDispatch() == 0  {
            return 5
        }
        return 0
    }
    
    
    /// Add Menu
    func addMenu() {
        // create viewController code...
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var mainViewController:UIViewController!
      mainViewController  = storyboard.instantiateViewController(withIdentifier: String(describing: HomeSlidesViewController.self))
        
        let leftViewController  = storyboard.instantiateViewController(withIdentifier: "MenuVC") as! LeftViewController
        
        var slideMenuController:ExSlideMenuController!
        
        if Utility.langCode == "ar" {
        
         UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            slideMenuController = ExSlideMenuController(mainViewController:mainViewController, rightMenuViewController: leftViewController)
        }
        else {
         
            UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
          
            slideMenuController = ExSlideMenuController(mainViewController:mainViewController,
                                                        leftMenuViewController: leftViewController)
        }
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController as? SlideMenuControllerDelegate
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        appDelegate?.window?.rootViewController = self
        appDelegate?.window?.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        appDelegate?.window?.rootViewController = slideMenuController
        appDelegate?.window?.makeKeyAndVisible()
    }
}


extension UIViewController {
    
    private static let insetBackgroundViewTag = 98721 //Cool number
    private static let insetTopbackgroundViewTag = 98722
    
    func paintSafeAreaBottomInset(withColor color:UIColor = Helper.getUIColor(color:  Colors.AppBaseColor)) {
        guard #available(iOS 11.0, *) else {
            return
        }
        if let insetView = view.viewWithTag(UIViewController.insetBackgroundViewTag) {
            insetView.backgroundColor = color
            return
        }
        
        let insetView = UIView(frame: .zero)
        insetView.tag = UIViewController.insetBackgroundViewTag
        insetView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(insetView)
        view.bringSubview(toFront: insetView)
        
        
        insetView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        insetView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        insetView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        insetView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        insetView.backgroundColor = color
    }
    
    func paintSafeAreaTopInset(withColor color:UIColor = Helper.getUIColor(color:  Colors.AppBaseColor)) {
        guard #available(iOS 11.0, *) else {
            return
        }
        if let insetView = view.viewWithTag(UIViewController.insetTopbackgroundViewTag) {
            insetView.backgroundColor = color
            return
        }
        
        let insetView = UIView(frame: .zero)
        insetView.tag = UIViewController.insetTopbackgroundViewTag
        insetView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(insetView)
        view.bringSubview(toFront: insetView)
        
        insetView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        insetView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        insetView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        insetView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        
        insetView.backgroundColor = color
    }
    
    
    func hideBottomTopSafeArea() {
        if let insetView = view.viewWithTag(UIViewController.insetTopbackgroundViewTag) {
            insetView.layer.transform = CATransform3DMakeScale(0.01,0.01,1)
            insetView.layer.opacity = 0.01
        }
        
        if let insetView = view.viewWithTag(UIViewController.insetBackgroundViewTag) {
            insetView.layer.transform = CATransform3DMakeScale(0.01,0.01,1)
            insetView.layer.opacity = 0.01
        }
    }
}

