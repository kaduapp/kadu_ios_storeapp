//
//  LeftMenuModal.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 12/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation


//
//  MenuModel.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 03/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
struct MenuModel{
    var Image = UIImage()
    var MenuText = ""
}
/// left menu class

class LeftMenuData {
    static var Data: [MenuModel] {
        
        var home = MenuModel()
        home.Image = #imageLiteral(resourceName: "menu_icon")
        home.MenuText = StringConstants.home()
        
        var history = MenuModel()
        history.Image = #imageLiteral(resourceName: "order_history")
        history.MenuText = StringConstants.orderHistory()

        
        var help = MenuModel()
        help.Image = #imageLiteral(resourceName: "help")
        help.MenuText = StringConstants.needHelp()
        
        var wallet = MenuModel()
        wallet.Image = #imageLiteral(resourceName: "wallet-outline")
        wallet.MenuText = StringConstants.wallet()

        var addWallet = MenuModel()
        addWallet.Image = #imageLiteral(resourceName: "bank-outline")
        addWallet.MenuText = StringConstants.bankDetails()
        
        var selectLang = MenuModel()
        selectLang.Image = #imageLiteral(resourceName: "translate (1)")
        selectLang.MenuText = StringConstants.selectLanguage()

         // if user type is equal to 0 ( laundry )  then inverntory not added in left menu 
        if UserDefaults.standard.integer(forKey: "user_Type") == 0{
            return [home, history, help, wallet , addWallet, selectLang]
        }
        // if store type is equal to 5 ( laundry )  then inverntory not added in left menu
        if Utility.getStoreType() == 5{
             return [home, history, help,wallet , addWallet, selectLang]
        }
        
        var inventory = MenuModel()
        inventory.Image = #imageLiteral(resourceName: "order_history")
        inventory.MenuText = StringConstants.inventory()
        return [home, history, help,wallet , addWallet,  selectLang, inventory]
    }
    
}



class LeftMenuViewModel{
    
    let disposeBag = DisposeBag()
    let header = APIUtility.getHeaderForOrders()
    func forUpDateOnlineStatus(status : Int , onCompletionHandler:@escaping(Bool ) -> Void  ){
        let params = [ APIRequestParams.Status : status , APIRequestParams.StoreId : StoreUtility.getStore().storeId] as [String : Any]
        APIHandler.patchRequest(header: header, url: APIEndTails.switchOnlineStatus, params: params ).subscribe(onNext: {success, data in
            if success {
                print(data)
                onCompletionHandler(true)
               
            }else {
               onCompletionHandler(false)
            }
        }, onError: { Error in
            print(Error.localizedDescription)
        }).disposed(by: disposeBag)
        
    }
}
