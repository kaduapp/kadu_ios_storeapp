//
//  ItemListTablePortionCell.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 20/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit


protocol  ItemListTablePortionCellDelegate : class  {
    func pressedAddButton(sender : UIButton , images : [String])
    func tapOnItem( itemPrice : String , itemQty : String , itemName: String , sender : UIButton  )
}


class ItemListTablePortionCell: UITableViewCell {

    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName:self.identifier, bundle:nil)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        addImageButon.isHidden = true
//        self.addImageButon.addTarget(self, action: #selector(pressedAddButton(sender:)), for: .touchUpInside)
    }
    

    
    
    @IBOutlet weak var lbl_UnitName: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var portionAmount: UILabel!
    @IBOutlet weak var portionName: UILabel!
    @IBOutlet weak var tableView: AddOnTableView!
    var images : [String] = []
    var addOnArrayVar:[AddOn]?
    
    var price = ""
    var qty = ""
    var itemName = ""

    weak var delegate : ItemListTablePortionCellDelegate?
    
    var item = Item.init(data: [:]) {
        didSet {
          
            portionName.text = item.itemName
    
            lbl_UnitName.text = item.unitName
            portionName.attributedText = NSAttributedString(string: item.itemName , attributes:
                [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue , ] )
            portionName.textColor = #colorLiteral(red: 0.1028192525, green: 0.1185307794, blue: 0.77653941, alpha: 1)
            portionAmount.text =  "\(item.currencySymbol) \(String(format:"%0.2f",Float(item.itemQnty) * item.unitPrice))"
            
            itemName = item.itemName
            price =  "\(item.currencySymbol) \(String(format:"%0.2f",Float(item.itemQnty) * item.unitPrice))"
            qty = "\(item.itemQnty)"
            quantity.text =  "\(item.itemQnty) X \(item.currencySymbol) \(String(format:"%0.2f", item.unitPrice))"
            self.addOnArrayVar = item.addOnsArray
            self.images = item.images
            self.tableView.reloadData()
        }
    }
    
    
    @objc func pressedAddButton(sender : UIButton){
        delegate?.pressedAddButton(sender: sender, images: images)
    }

    
}

class AddOnTableView: UITableView {
    override func awakeFromNib() {
        super.awakeFromNib()
        register(ItemListTableAddonCell.nib, forCellReuseIdentifier: ItemListTableAddonCell.identifier)
        
    }
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        let newContentSize = CGSize.init(width:self.contentSize.width , height: self.contentSize.height )
        return newContentSize
    }
    
    override var contentSize: CGSize {
        didSet{
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
    }
}

extension ItemListTablePortionCell:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addOnArrayVar!.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: ItemListTableAddonCell.identifier) as! ItemListTableAddonCell?)!

        cell.addOnName.text = addOnArrayVar![indexPath.row].name
        cell.addOnPrice.text = Utility.getCurrencySymbol() + " " + addOnArrayVar![indexPath.row].price!
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension
    }
    
}




