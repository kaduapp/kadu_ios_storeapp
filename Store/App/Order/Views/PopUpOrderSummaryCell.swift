//
//  PopUpOrderSummaryCell.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 20/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class PopUpOrderSummaryCell: UITableViewCell {
    var timeEnd : Date?
    var dateString = ""
    var driverPhonenumber = ""
    
    @IBOutlet weak var extraNoteView: UIView!
    @IBOutlet weak var lbl_ExtraNote: RobotoMediumLable!
    @IBOutlet weak var extraNoteTitle: UILabel!
    
    @IBOutlet weak var callButtonWidthConstraints: NSLayoutConstraint!
    @IBOutlet weak var btn_CallButton: UIButton!
    @IBOutlet weak var lbl_DriverName: UILabel!
    @IBOutlet weak var img_DriverImage: UIImageView!
    @IBOutlet weak var waitingImage: UIImageView!
    @IBOutlet weak var containerStackView: UIStackView!
    @IBOutlet weak var customerDetailsView: CustomerDetails!
    @IBOutlet weak var bookingTimeLabel: UILabel!
    @IBOutlet weak var bookingTypeLabel: UILabel!
    @IBOutlet weak var nowLaterLabel: UILabel!
    @IBOutlet weak var deliveryAddressLabel: UILabel!
    @IBOutlet weak var deliverToLabel: UILabel!
    @IBOutlet weak var bottomLineView: UIView!
    @IBOutlet weak var bottomLine: UIView!
    @IBOutlet weak var spacingView: UIView!
    @IBOutlet weak var ordersCountLabel: UILabel!
    @IBOutlet weak var ordersLabel: UILabel!
    @IBOutlet weak var revenueAmount: UILabel!
    @IBOutlet weak var revenueLabel: UILabel!
    @IBOutlet weak var topLine: UIView!
    @IBOutlet weak var driverView: UIView!
    @IBOutlet weak var deliveryAddressView: UIView!
    @IBOutlet weak var driverWaitingView: UIView!
    @IBOutlet weak var oderType:UILabel!
    @IBOutlet weak var DriverDetails:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btn_CallButton.layer.cornerRadius = btn_CallButton.frame.height / 2
        self.img_DriverImage.layer.cornerRadius =  self.img_DriverImage.frame.width/2
        self.img_DriverImage.layer.masksToBounds = true
        
        ordersLabel.text = StringConstants.PAYMENTTYPE()
        oderType.text = StringConstants.ORDERTYPE()
        revenueLabel.text = StringConstants.DUEIN()
        deliverToLabel.text = StringConstants.DELIVERTO()
        //DRIVER DETAILS
        DriverDetails.text = StringConstants.DriverDetails()
        
        
        
        
    }
    
   static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName:PopUpOrderSummaryCell.identifier, bundle:nil)
    }
    
    
    var item : ManageOrder? {
        didSet {
            guard let data = item else {
                return
            }
            
            
            if data.orderSummary.extraNote.count > 0{
                extraNoteTitle.text = StringConstants.EXTRANOTES()
                lbl_ExtraNote.text = data.orderSummary.extraNote
            }
            else{
                extraNoteTitle.text = ""
                lbl_ExtraNote.text = ""
            }

            self.customerDetailsView.lbl_CustomerName.text = data.orderSummary.customerName
            self.customerDetailsView.btn_Call.layer.cornerRadius = customerDetailsView.btn_Call.frame.height / 2
            self.customerDetailsView.customerPhonenumber = data.customerData.customerPhone
            
            // driver
            if data.orderSummary.driverDetails != nil,data.orderSummary.driverDetails.driverName.count > 0{
                self.lbl_DriverName.text = data.orderSummary.driverDetails.driverName
                self.driverPhonenumber = data.orderSummary.driverDetails.phoneNumber
                Helper.setImage(imageView: img_DriverImage, url: data.orderSummary.driverDetails.drivePic, defaultImage:#imageLiteral(resourceName: "placehoilder_user"))
                self.driverView.isHidden = false
                driverWaitingView.isHidden = true
            }
            else if data.orderSummary.orderStatus == 40{
                waitingImage.image = UIImage.gifImageWithName("pulsator1")
                self.driverView.isHidden = false
                driverWaitingView.isHidden = false
            }
            else{
                 self.driverView.isHidden = true
            }
            
            if data.orderSummary.serviceType == .Pickup{
                self.deliveryAddressView.isHidden = true
                self.driverView.isHidden = true
            }
            else{
                self.deliveryAddressView.isHidden = false
                self.deliveryAddressLabel.text = data.orderSummary.deliveryAddress
            }
            
            self.bookingTimeLabel.text = data.orderSummary.serviceType == .Delivery ? StringConstants.Delivery() : StringConstants.PickUp()
            self.bookingTypeLabel.text = self.getPaymentType(data)
            
            if (data.orderSummary.bookingType == .Now ? StringConstants.now() : StringConstants.later()) == StringConstants.now() {
                  self.nowLaterLabel.text = StringConstants.ASAP()
            }else{
                dateString = data.orderSummary.dueDateTime
                self.timeEnd = Date.getStringToDateFromTimeStamp(value: data.orderSummary.dueTimeStamp, format: DateFormat.DateAndTimeFormatServer)
                self.dsiplayTimer()
            }
        
            self.nowLaterLabel.textColor = .black

        }
        
    }
    
    func showGif(){
        self.lbl_DriverName.text = "DRIVER NOT ASSIGNED"
        self.callButtonWidthConstraints.constant = 0.0
        self.img_DriverImage.image = UIImage.gifImageWithName("pulsator1")
    
    }

    weak var timer : Timer?
    func dsiplayTimer(){
        self.setTimeLeft()
        
         timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.setTimeLeft), userInfo: nil, repeats: true)
    }
    
    @objc func setTimeLeft( ) {
        let timeNow =  Date.getStringToDate(value: Date.getCurrentDateAndTime() , format: DateFormat.DateAndTimeFormatServer )
        
        
        if timeEnd?.compare(timeNow) == ComparisonResult.orderedDescending {
            
            let interval = timeEnd?.timeIntervalSince(timeNow)
            
            let days =  (interval! / (60*60*24)).rounded(.down)
            
            let daysRemainder = interval?.truncatingRemainder(dividingBy: 60*60*24)
            
            let hours = (daysRemainder! / (60 * 60)).rounded(.down)
            
            let hoursRemainder = daysRemainder?.truncatingRemainder(dividingBy: 60 * 60).rounded(.down)
            
            let minites  = (hoursRemainder! / 60).rounded(.down)
            
            let formatter = NumberFormatter()
            formatter.minimumIntegerDigits = 2
            
            
             let timeLeft = "\( ( formatter.string(from: NSNumber(value:days)) == "00" ? "" : formatter.string(from: NSNumber(value:days))  )  ?? "" )\(formatter.string(from: NSNumber(value:days)) == "00" ? "" : "D:" )\(formatter.string(from: NSNumber(value:hours)) ?? "")H:\(formatter.string(from: NSNumber(value:minites)) ?? "" )M"
            
            nowLaterLabel.text = timeLeft
            nowLaterLabel.textColor = .black
        } else {
            let date = dateString
            let dateFormatter = DateFormatter()
           
            dateFormatter.dateFormat = DateFormat.DateAndTimeFormatServer
            let s = dateFormatter.date(from: date)
            
            let components = Calendar.current.dateComponents([.day, .hour, .minute], from: (s!).toLocalTime(), to:timeNow)
                
            guard   let day = components.day , let hoursPortion =  components.hour, let minutesPortion = components.minute
                else {
                    preconditionFailure("Should not happen")
            }
            
            var timeOverLay = "\(hoursPortion)H:\(minutesPortion)M"
            
            if timeOverLay.contains("-"){
                timeOverLay =  timeOverLay.replacingOccurrences(of: "-", with: "")
            }
            nowLaterLabel.text = timeOverLay
            nowLaterLabel.textColor = .red
        }
    }
    
    
    
    deinit {
        timer?.invalidate()
    }
    
    func getPaymentType(_ orderData : ManageOrder) -> String{
        var paymentType = ""
        if orderData.orderSummary.paymentType == .Cash && orderData.orderSummary.payByWallet == 1 {
            paymentType =  StringConstants.cashNwallet()
        } else if orderData.orderSummary.paymentType == .Card && orderData.orderSummary.payByWallet == 1 {
            paymentType =  StringConstants.cardNwallet()
        }else if orderData.orderSummary.paymentType == .Card && orderData.orderSummary.payByWallet == 0 {
            paymentType =  StringConstants.card()
        }else if orderData.orderSummary.paymentType == .Cash && orderData.orderSummary.payByWallet == 0 {
            paymentType =  StringConstants.cash()
        }else if orderData.orderSummary.paymentType != .Card && orderData.orderSummary.paymentType != .Cash && orderData.orderSummary.payByWallet == 1 {
            paymentType =  StringConstants.wallet()
        }
        return paymentType
    }
    
    
    @IBAction func callDriverButtonAction(sender: UIButton){

        Helper.dialNumber(number: driverPhonenumber)
    }
}

