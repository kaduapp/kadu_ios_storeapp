//
//  OrderAmountSummaryCell.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 20/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

class OrderAmountSummaryCell: UITableViewCell {
    static var identifier:String {
        return String(describing: self)
    }
    @IBOutlet weak var leftAnchorOfTitle: NSLayoutConstraint!
    
    static var nib: UINib {
        return UINib(nibName:self.identifier, bundle:nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var topContraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    var item:POrderAmount? {
        didSet {
            lineView.isHidden = true
            
            titleLabel.adjustsFontSizeToFitWidth = true
            titleLabel.lineBreakMode = .byWordWrapping
            
            titleLabel.font  =  UIFont(name: DinPro.Regular, size: 16)
            amountLabel.font  =  UIFont(name: DinPro.Regular, size: 16)
            
            titleLabel.textColor = UIColor.black
            amountLabel.textColor = UIColor.black
            
            titleLabel.text = item?.itemTitle
            amountLabel.text = item?.itemAmount
        }
    }
    
    
    var  totalAmt : String?{
        didSet{
            amountLabel.text = totalAmt
        }
    }
    
    
    var tax:ExclusiveTax? {
        didSet {
            if tax?.taxName == StringConstants.Total(){
                lineView.isHidden = false
                titleLabel.font  =  UIFont(name: DinPro.Bold, size: 16)
                amountLabel.font  =  UIFont(name: DinPro.Bold, size: 16)
                titleLabel.text = tax?.taxName
            }else{
                lineView.isHidden = true
                //leftAnchorOfTitle.constant = 20.0
                titleLabel.font  =  UIFont(name: Roboto.Italic, size: 13)
                amountLabel.font  =  UIFont(name: Roboto.Italic, size: 13)
                amountLabel.text = tax?.price
                titleLabel.text = tax?.taxName
                titleLabel.textColor = UIColor.lightGray
                amountLabel.textColor = UIColor.lightGray
            }
        }
    }
    
    func setOrderAmount(amount :ManageOrderAmount,index: Int){
        if amount.amountArray.count - 1 >= index{
            
            if Utility.getStoreType() == 5{
                if amount.amountArray[index].itemTitle.contains("Cleaning Fee"){
                    self.item = amount.amountArray[index]
                }
            }else if UserDefaults.standard.integer(forKey: "user_Type") == 0{
                if UserDefaults.standard.integer(forKey: "storeType") == 5 {
                    if amount.amountArray[index].itemTitle.contains("Cleaning Fee"){
                        item = amount.amountArray[index]
                    }
                }else{
                    
                    if amount.amountArray[index].itemTitle == StringConstants.subTotal(){
                        item = amount.amountArray[index]
                
                    }
                    if amount.amountArray[index].itemTitle == StringConstants.discount(){
                        item = amount.amountArray[index]
                    }
                    if amount.amountArray[index].itemTitle == StringConstants.taxTotal(){
                        item = amount.amountArray[index]
                    }
                }
            }else{
                if amount.amountArray[index].itemTitle == StringConstants.subTotal(){
                    item = amount.amountArray[index]
                }
                if amount.amountArray[index].itemTitle == StringConstants.discount(){
                    item = amount.amountArray[index]
                  
                }
                if amount.amountArray[index].itemTitle == StringConstants.taxTotal(){
                    item = amount.amountArray[index]
                }
            }
        }else{
            let itemIndex = index - amount.amountArray.count
            if amount.excTaxes.count - 1 >= itemIndex {
                if amount.excTaxes[itemIndex].taxName == StringConstants.Total(){
                    var total : Float = 0.0
                    
                    for amt in amount.amountArray{
                        
                        if Utility.getStoreType() == 5{
                            if amt.itemTitle.contains("Cleaning Fee"){
                                total += amt.tempAmt
                            }
                        }else if UserDefaults.standard.integer(forKey: "user_Type") == 0{
                            if UserDefaults.standard.integer(forKey: "storeType") == 5 {
                                if amt.itemTitle.contains("Cleaning Fee"){
                                    total += amt.tempAmt
                                }
                            }else{
                                
                                if amt.itemTitle == StringConstants.subTotal(){
                                    total += amt.tempAmt
                                }
                                
                                if amt.itemTitle == StringConstants.discount(){
                                    total -= amt.tempAmt
                                }
                                
                                if amt.itemTitle == StringConstants.taxTotal(){
                                    total += amt.tempAmt
                                }
                            }
                        }else{
                            if amt.itemTitle == StringConstants.subTotal(){
                                total += amt.tempAmt
                            }
                            
                            if amt.itemTitle == StringConstants.discount(){
                                total -= amt.tempAmt
                            }
                            
                            if amt.itemTitle == StringConstants.taxTotal(){
                                total += amt.tempAmt
                            }
                        }
                    }
                    tax = amount.excTaxes[itemIndex]
                totalAmt = String(format:"%@ %0.2f",amount.amountArray[0].currSym,total)
                    
                }else{
                    tax = amount.excTaxes[itemIndex]
                }
            }
        }
        
    }
}



typealias OrderAmountSummaryCellConfig = TableCellConfigurator< OrderAmountSummaryCell , (String , String , CGFloat , UIFont, Bool , UIColor) >

extension OrderAmountSummaryCell : ConfigurableCell {
    
    func configure(data: (String , String , CGFloat  , UIFont , Bool , UIColor)) {
        
        titleLabel.text = data.0
        amountLabel.text = data.1
        leftAnchorOfTitle.constant = data.2
        (titleLabel.font , amountLabel.font ) = (data.3 , data.3)
        lineView.isHidden = data.4
        (titleLabel.textColor , amountLabel.textColor ) = (data.5 , data.5)
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.textAlignment = .left
    }
    
}
