//
//  ItemListTableHeaderCell.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 20/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit


protocol  ItemListTableHeaderCellDelegate : class {
    func pressedEditButton()
}
class ItemListTableHeaderCell: UITableViewCell {
    @IBOutlet weak var leftLabel: UILabel!
    @IBOutlet weak var rightLabel: UILabel!
  
    weak var delegate : ItemListTableHeaderCellDelegate?
    
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName:ItemListTableHeaderCell.identifier, bundle:nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        leftLabel.text = StringConstants.items()
        rightLabel.text = StringConstants.price()
    }
    
    
    func setHeader(_ count: Int){
        leftLabel.text = "\(StringConstants.items()) (\(count))"
    }
}
