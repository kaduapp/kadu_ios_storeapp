//
//  ItemListTableAddonCell.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 20/01/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit


class ItemListTableAddonCell: UITableViewCell {
    
    @IBOutlet weak var addOnName: UILabel!
    @IBOutlet weak var addOnPrice: UILabel!
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: self.identifier, bundle:nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
   
    
    
}
