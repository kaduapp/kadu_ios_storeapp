//
//  CustomerDetails.swift
//  DelivX Store
//
//  Created by 3Embed on 04/05/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class CustomerDetails: UIView {
    
    @IBOutlet weak var lbl_CustomerName: UILabel!
    
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var btn_Call: UIButton!
    @IBOutlet weak var pastOrdersButton: UIButton!
    @IBOutlet var containerView: UIView!
    
    var customerPhonenumber = ""
    static var identifier:String {
        return String(describing: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       
        UINib(nibName:"CustomerDetails", bundle:nil).instantiate(withOwner: self, options: nil)
        containerView.frame = self.bounds
       
        
        addSubview(containerView)
        
    }
    
    @IBAction func pressedCallButton(_ sender: UIButton) {
     Helper.dialNumber(number: customerPhonenumber)
    }
}


