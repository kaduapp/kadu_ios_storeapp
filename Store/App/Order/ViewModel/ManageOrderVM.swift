//
//  ManageOrderVM.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 15/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class ManageOrderVM: NSObject {

    
  let order_Response = PublishSubject<(Bool,ManageOrder)>()
  let orderStatus_Response = PublishSubject<(Bool,OrderStatus)>()
    
    /// Get Particular order details
    ///
    /// - Parameter orderID: order id of the order
    func callOrderDetails(orderID:Int) {
        Helper.showPI(string: "")
        let header = APIUtility.getHeaderForOrders()
        let orderDetails = "\(APIEndTails.OrderDetails)\(orderID)"
        APIHandler.getRequest(header:header, url:orderDetails).subscribe(onNext: {[weak self]  success, data in
            if success == true {
                
                if let dict = data as? [String:Any]{
                
               let sortedData =  ManageOrder.init(data: dict)
             self?.order_Response.onNext((true,sortedData))
                }
            }else {
                
                
            }
            }, onError: { Error in
                print(Error)
        }).disposed(by: disposeBag)
    }
    
    /// Change Order status api
    ///
    /// - Parameters:
    ///   - orderStatus: Order status
    ///   - cancellationReason: if order cancelled by store it will be a string or else empty string
    func changeStatusOfOrder(_ orderId : Int,orderStatus: OrderStatus, cancellationReason:String,dueDatetime:String) {
        if orderStatus == .none{
            return
        }
        
        let header = APIUtility.getHeaderForOrders()
        var params : [String : Any]
        var APIName : String
        
        if (orderStatus == .Dispatch) {
            params = [ APIRequestParams.TimeStamp     :      "\(Date.getTimeStamp)",
                APIRequestParams.OrderID       :  orderId]
            APIName = APIEndTails.DispatchOrder
            APIHandler.patchRequest(header: header, url: APIName, params: params).subscribe(onNext: { success, data in
                if success == true {
                    self.orderStatus_Response.onNext((true,OrderStatus.none))
                }else {
                    self.orderStatus_Response.onNext((false,OrderStatus.none))
                }
                
            }, onError: { Error in
                print(Error)
            }).disposed(by: disposeBag)
            
        } else if (orderStatus == .ManagerCancelled) {
            var ipAddress = Helper.getIPAddresses()
            
            params  = [
                APIRequestParams.OrderID       : orderId,
                APIRequestParams.ipAddress    :ipAddress[0],
                "latitude"                       :0.00,
                "longitude"                      :0.00]
            
            if(cancellationReason.count > 0) {
                params[APIRequestParams.cancelReason] = cancellationReason
            }
            if (dueDatetime.count > 0) {
                params[APIRequestParams.DueTime] = dueDatetime
            }
            
            APIName = APIEndTails.cancelOrder
            
            APIHandler.putRequest(header: header, url: APIName, params: params).subscribe(onNext: { success, data in
                if success == true {
                    self.orderStatus_Response.onNext((true,OrderStatus.none))
                }else {
                    self.orderStatus_Response.onNext((false,OrderStatus.none))
                }
                
            }, onError: { Error in
                print(Error)
            }).disposed(by: disposeBag)
            
        }
        else {
            params  = [
                APIRequestParams.Status        :       orderStatus.rawValue,
                APIRequestParams.TimeStamp     :       Date.getTimeStamp,
                APIRequestParams.OrderID       : orderId]
            
            
            if(cancellationReason.count > 0) {
                params[APIRequestParams.cancelReason] = cancellationReason
            }
            if (dueDatetime.count > 0) {
                params[APIRequestParams.DueTime] = dueDatetime
            }
            APIName = APIEndTails.ChangeOrderStatus
            APIHandler.patchRequest(header: header, url: APIName, params: params).subscribe(onNext: { success, data in
                if success == true {
                    self.orderStatus_Response.onNext((true,OrderStatus.none))
                }else {
                    self.orderStatus_Response.onNext((false,OrderStatus.none))
                }
                
            }, onError: { Error in
                print(Error)
            }).disposed(by: disposeBag)
            
        }
        
    }
    
    
    /// rahul edit amount and items number
    func editItem(orderId: Int, itemList:[[String:Any]] ,status :Bool,quantityStatus:Bool) {
        let header = APIUtility.getHeaderForOrders()
        
        let params: [String : Any]  = [
            APIRequestParams.Items : itemList,
            APIRequestParams.OrderID : orderId,
            APIRequestParams.DeliveryCharge: 0.0
        ]
        if status{
        APIHandler.putRequest(header: header, url: APIEndTails.EditOrderAPI, params: params).subscribe(onNext: {[weak self] success, data in
            if success {
                if let dict = data as? [String:Any]{
                 let sortedData =  ManageOrder.init(data: dict)
                 self?.order_Response.onNext((true,sortedData))
                }
                if quantityStatus{
                                   Helper.showAlert(message: "you can't updated the quantity more than the customer order", head: StringConstants.message(), type: 0)
                                               }else {
                                               Helper.showAlert(message: StringConstants.updated(), head: StringConstants.message(), type: 0)
                                           }
            }
            }, onError: { Error in
                Helper.showAlert(message: Error.localizedDescription, head: "", type: 0)
        }).disposed(by: disposeBag)
        }else{
        APIHandler.patchRequest(header: header, url: APIEndTails.EditOrderAPI, params: params).subscribe(onNext: {[weak self] success, data in
            if success {
                if let dict = data as? [String:Any]{
                 let sortedData =  ManageOrder.init(data: dict)
                 self?.order_Response.onNext((true,sortedData))
                }
                 if quantityStatus{
                                   Helper.showAlert(message: "you can't updated the quantity more than the customer order", head: StringConstants.message(), type: 0)
                                               }else {
                                               Helper.showAlert(message: StringConstants.updated(), head: StringConstants.message(), type: 0)
                                           }
                
            }
            }, onError: { Error in
                Helper.showAlert(message: Error.localizedDescription, head: "", type: 0)
        }).disposed(by: disposeBag)
        }
    }
}

class ManageOrder:Equatable {
    static func ==(lhs: ManageOrder, rhs: ManageOrder) -> Bool {
        return lhs.orderSummary.orderId == rhs.orderSummary.orderId
    }
    var customerData:CustomerData!
    lazy var itemsList = [Item]()
    lazy var currentItemList = [[String:Any]]()
    var orderSummary: OrderSummary!
    
    init(data: [String:Any], currentOrder:Bool = false) {
        
        if let itemsArray = data["Items"] as? [[String:Any]] {
                itemsList = itemsArray.map {
                    Item.init(data: $0)
                }
                currentItemList = itemsArray
        }
        orderSummary = OrderSummary.init(data: data)
        customerData = CustomerData.init(data: data)
    }
    
   
}


class ManageOrderAmount : NSObject{
    
    var amountArray = [POrderAmount]()
    var excTaxes = [ExclusiveTax]()
    init(homeOrderModal:ManageOrder) {
        if Utility.getStoreType() != 5 {
            if UserDefaults.standard.integer(forKey: "storeType") == 5{
                amountArray.append(POrderAmount.init(title: StringConstants.cleaningCharge() + " (\(homeOrderModal.orderSummary.weight)kg)", amount: homeOrderModal.orderSummary.subTotalAmount, currency: homeOrderModal.orderSummary.currencySymbol))
                
            }else{
                
                amountArray.append(POrderAmount.init(title:StringConstants.subTotal() , amount: homeOrderModal.orderSummary.subTotalAmount, currency: homeOrderModal.orderSummary.currencySymbol))
                
                amountArray.append(POrderAmount.init(title: StringConstants.discount(), amount: homeOrderModal.orderSummary.discount, currency:     homeOrderModal.orderSummary.currencySymbol))
            }
        }
        
        if Utility.getStoreType() == 5 {
            amountArray.append(POrderAmount.init(title: StringConstants.cleaningCharge() + " (\(homeOrderModal.orderSummary.weight)kg)", amount: homeOrderModal.orderSummary.subTotalAmount, currency: homeOrderModal.orderSummary.currencySymbol))
            
        }
        
        if homeOrderModal.orderSummary.taxTotalAmt != 0.0 {
            amountArray.append(POrderAmount.init(title: StringConstants.taxTotal(), amount: homeOrderModal.orderSummary.taxTotalAmt, currency: homeOrderModal.orderSummary.currencySymbol))
            
            for tax in homeOrderModal.orderSummary.taxData {
                
                if let taxtName = tax["taxtName"] as? String{
                    if let taxPrice = tax["price"] as? NSNumber{
                        excTaxes.append(ExclusiveTax.init(taxNam: "  " + taxtName + "(\(tax["taxValue"] ?? Int())%)", taxPrice: Float(truncating: taxPrice), currency: homeOrderModal.orderSummary.currencySymbol))
                    }
                }
            }
        }
        excTaxes.append(ExclusiveTax.init(taxNam: StringConstants.Total(), taxPrice: homeOrderModal.orderSummary.totalAmount, currency: homeOrderModal.orderSummary.currencySymbol))
        
    }
}


struct  ExclusiveTax {
    var price = "";
    var taxName = "";
    
    init(taxNam:String, taxPrice:Float, currency:String) {
        taxName = taxNam
        price = String(format:"%@ %0.2f",currency,taxPrice)
    }
}

struct POrderAmount {
    
    var itemTitle = ""
    var itemAmount = ""
    var tempAmt : Float = 0.00
    var currSym = ""
    init(title:String, amount:Float, currency:String) {
        itemTitle = title
        itemAmount = String(format:"%@ %0.2f",currency,amount)
        tempAmt = amount
        currSym = currency
    }
}


public enum OrderStatus:Int {
    //Manager Related statuses
    case NewOrder = 1
    case ManagerCancelled = 2
    case ManagerRejected = 3
    case PreparingOrder = 4 // Waiting for Driver
    case OrderReady = 5
    case OrderPicked = 6
    case OrderCompleted = 7
    case Dispatch = 40
    case InProgress = 25
    //Driver related statuses
    case DriverAccepted = 8
    case DriverRejecte = 9
    case DriverToStore = 10
    case DriverAtStore = 11
    case DriverToCustomer = 12
    case DriverAtDeliveryLocation = 13
    case DriverDeliverOrder = 14
    case DriverJobFinished = 15
    case CancelledByCustomer = 16
    case OrderEditedByStore = 17
    case DelayOrder = 18
    case OrderExpired = 20
    case OrderUnassigned = 21
    case ManualDispatch = 80
    case none = 404
    case Empty = 502
}
