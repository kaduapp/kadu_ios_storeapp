//
//  EditOrderViewController.swift
//  DelivX Store
//
//  Created by 3Embed on 10/06/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

protocol EditOrderViewControllerDelegate : class  {
    func PressedUpdateOrder( index : Int , itemPrice : String , itemQty : String, status:Bool )
}


class EditOrderViewController: UIViewController {

    @IBOutlet weak var alertViewLeft: NSLayoutConstraint!
    
    @IBOutlet weak var alertViewRight: NSLayoutConstraint!
    
    
    @IBOutlet weak var alertViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var centerConstraints: NSLayoutConstraint!
    @IBOutlet weak var lbl_ItemName: UILabel!
    
    weak var delegate : EditOrderViewControllerDelegate?
    @IBOutlet weak var buttonContainerView: UIView!
    @IBOutlet weak var topView: UIView!
     @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var tf_Price: UITextField!
    @IBOutlet weak var tf_Qty: UITextField!
    @IBOutlet weak var closeButton: UIButton!
    var itemName = ""
    var price = ""
    var qty = ""
    var indexId = 0
    override func viewDidLoad() {
        super.viewDidLoad()
    
        tf_Qty.becomeFirstResponder()
        addObserver()
        tf_Price.text = price
        tf_Qty.text = qty
        
        tf_Price.delegate = self
        tf_Qty.delegate = self
        
        lbl_ItemName.text = itemName
        
        buttonContainerView.layer.cornerRadius = buttonContainerView.frame.height / 2
        closeButton.layer.cornerRadius = closeButton.frame.height / 2
        
        if isIphone(){
            
            alertViewWidth.isActive = false
            alertViewLeft.isActive = true
            alertViewRight.isActive = true
        }else{
            alertViewLeft.isActive = false
            alertViewRight.isActive = false
            alertViewWidth.isActive = true
        }

    }
    
    @IBAction func pressedCloseButton(_ sender: UIButton) {
        self.view.endEditing(true)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func pressedUpdateButton(_ sender: UIButton) {
        
        if tf_Qty.text != "" &&  qty != tf_Qty.text {
            if let quantity = tf_Qty.text{

                if Int(quantity) ?? 0 > Int(qty) ?? 0{
                               delegate?.PressedUpdateOrder(index: indexId, itemPrice: tf_Price.text ?? "" , itemQty: qty ,status: true)
                  } else {

                                    delegate?.PressedUpdateOrder(index:indexId, itemPrice: tf_Price.text ?? "" , itemQty: tf_Qty.text ?? "",status: false)
            }

            }
        }
        self.view.endEditing(true)
        self.dismiss(animated: false, completion: nil)
    }
    
    func addDoneButtonOnKeyboard(textField:UITextField)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        done.tintColor = UIColor.black
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        textField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.view.endEditing(true)
    }
    
}
//  self.popupModalView?.changeQuntity(changedQuantity: textField.text!, isUnitQuantity: isUnitQuantity, atIndex: index)

extension EditOrderViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag < 1000 {
            if range.location == 0{
            }
            
            let ACCEPTABLE_NUMBERS = "1234567890"
            let cs = NSCharacterSet(charactersIn: ACCEPTABLE_NUMBERS).inverted
            let filtered = string.components(separatedBy: cs).joined(separator: "")
            return (string == filtered)
        }
        return true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

        addDoneButtonOnKeyboard(textField: textField)
    }
    
}

