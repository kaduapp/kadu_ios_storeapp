//
//  ManageOrderVC.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 15/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import JaneSliderControl

class ManageOrderVC: UIViewController {

    @IBOutlet weak var popView: UIView!
    @IBOutlet weak var sliderView: UIView!
    @IBOutlet weak var acceptRejectView: UIView!
    @IBOutlet weak var leftSlider: SliderControl!
    @IBOutlet weak var sliderViewHeight : NSLayoutConstraint!
    @IBOutlet weak var orderStatusTitle: UILabel!
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var itemsTableView: OrderItemTableView!
    @IBOutlet weak var ipadItemsTV: iPadItemsTableView!
    @IBOutlet weak var ipadTaxesTV: iPadTaxesTableView!
    @IBOutlet weak var acceptRejectViewHeight : NSLayoutConstraint!
    @IBOutlet weak var manualAssignViewHeight : NSLayoutConstraint!
    @IBOutlet weak var itemsTVHeightAgainstBottom : NSLayoutConstraint!
      @IBOutlet weak var manualAssignButton: UIButton!
     @IBOutlet weak var acceptButton: UIButton!
     @IBOutlet weak var rejectButton: UIButton!
    
    var order = HomeOrder.init(data: [:])
    var disposeBag = DisposeBag()
     var manageVM = ManageOrderVM()
     var orderDetails =  ManageOrder.init(data: [:])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        self.presentAnimation()
        self.setSubscriberForOrderDetails()
        manageVM.callOrderDetails(orderID: order.orderID)
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !self.isIphone() {
            
            
            UIView.animate(withDuration: 0.3, animations: {
                self.popView.layer.opacity = 1.0
                self.popView.layer.transform = CATransform3DMakeScale(1,1,1)
            })
        }
    }

    /// Presenting animation by checking device as we are maintaining two different animations for iphone and ipad
    private func presentAnimation() {
        if self.isIphone() {
            self.navigationController?.navigationBar.isHidden = true
            self.paintSafeAreaBottomInset(withColor: UIColor.white)
            self.paintSafeAreaTopInset()
        }else {
            self.popView.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
            self.popView.layer.opacity = 0.01
        }
    }

  func setSubscriberForOrderDetails(){
    self.manageVM.order_Response.subscribe(onNext: {success in
        if success.0{
        self.orderDetails = success.1
            self.orderStatusTitle.text  = self.orderDetails.orderSummary.orderStatusMsg
              self.itemsTableView.reloadData()
                if !self.isIphone(){
                    DispatchQueue.main.async {
                            self.ipadItemsTV.reloadData()
                    }
                    DispatchQueue.main.async {
                         self.ipadTaxesTV.reloadData()
                    }
                }
        Helper.hidePI()
            
        }
    }, onError: {error in
        print(error)
    }).disposed(by: disposeBag)
    
    self.manageVM.orderStatus_Response.subscribe(onNext: { success, status in

        if self.isIphone() {
        self.navigationController?.popViewController(animated: true)
        }else {
        self.dismissViewController()
        }
        
    }, onError: { Error in
        print(Error)
    }).disposed(by: disposeBag)
    
    _ = MQTT.sharedInstance.publishSubject.subscribe(onNext: {[weak self] response, messageType in
        if  messageType == .MQTTNewOrder
        {
            let homeOrder = HomeOrder.init(data: response)
            self?.orderStatusUpdated(updatedOrder:homeOrder )
        }
        else if messageType == .MQTTDriverOnlineStatus {
            //  self?.driverData(data: response)
        }
        }).disposed(by: disposeBag)
 }
    
    
    
    /// Handle the position of order for real time update
    ///
    /// - Parameter order: Home order object
    func orderStatusUpdated(updatedOrder: HomeOrder){
        if updatedOrder.orderID == self.order.orderID && !updatedOrder.statusMessage.contains("has been updated by one of the manager"){
            if self.isIphone() {
                self.navigationController?.popViewController(animated: true)
            }else {
                self.dismissViewController()
            }
        }
    }

    
    @IBAction func changeOrderStatusAction(_ sender: UIButton) {
    
        if sender.tag == 1{
       self.manageVM.changeStatusOfOrder(order.orderID, orderStatus: OrderStatus.PreparingOrder, cancellationReason: "",  dueDatetime: "")
        }
        else{
    self.manageVM.changeStatusOfOrder(order.orderID, orderStatus: OrderStatus.ManagerRejected, cancellationReason: "",  dueDatetime: "")
        }
    }
    
    /// close button action. We are present this view controller in ipad and pushing in iphone, so while closing we are checking
    /// device type and according to that we removing this view controller.along this we are allocating dispose bag to unsubscribe
    /// all the rxswift subscription
    @IBAction func closeBtnTapped(_ sender: Any) {
        if self.isIphone() {
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
        }else {
            dismissViewController()
        }
        disposeBag = DisposeBag()
    }
    
    @IBAction func manualAssignButtonAction(){
        
        self.performSegue(withIdentifier: "GoToDriversList", sender: self)

    }
    
    /// Calling in ipad.We are scalling the view for dismissing
    func dismissViewController() {
        UIView.animate(withDuration: 0.5, animations: {
            if UIDevice.current.userInterfaceIdiom == .phone {
                self.hideBottomTopSafeArea()
            }
        }) { success in
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == String(describing:"GoToDriversList") {
            if let driversVC = segue.destination as? DriverListViewController {
             driversVC.storeID = orderDetails.orderSummary.storeID
            driversVC.orderId = orderDetails.orderSummary.orderId
            }
        }
    }

}


// Handle Bottom Slider Methods
extension ManageOrderVC {
    func resetSlider()
    {
        self.leftSlider.reset()
    }
    
    @IBAction func slideCanceled(_ sender: SliderControl) {
        //        print("Cancelled")
    }
    @IBAction func sliderChanged(_ sender: SliderControl) {
        //        print("Changed")
    }
    
    ///MARK:- Slider text data
    @IBAction func sliderFinished(_ sender: SliderControl) {
        
            switch orderDetails.orderSummary.orderStatus {
            case 4:
                if StoreUtility.getStore().storeType == 5{
                   self.manageVM.changeStatusOfOrder(order.orderID, orderStatus: OrderStatus.InProgress, cancellationReason: "",  dueDatetime: "")
                }
                else if order.serviceType == 2 {
                self.manageVM.changeStatusOfOrder(order.orderID, orderStatus: OrderStatus.OrderReady, cancellationReason: "",  dueDatetime: "")
                }
                self.manageVM.changeStatusOfOrder(order.orderID, orderStatus: OrderStatus.Dispatch, cancellationReason: "",  dueDatetime: "")
            case 5:
             self.manageVM.changeStatusOfOrder(order.orderID, orderStatus: OrderStatus.OrderCompleted, cancellationReason: "",  dueDatetime: "")
            case 40:
                if StoreUtility.getStore().driverType == 2{
            self.manageVM.changeStatusOfOrder(order.orderID, orderStatus: OrderStatus.Dispatch, cancellationReason: "",  dueDatetime: "")
                }
            default:
               print ("No Action")
        }
        self.resetSlider()
    }
    
}

extension ManageOrderVC : EditOrderViewControllerDelegate{
 
    func PressedUpdateOrder(index: Int, itemPrice: String, itemQty: String,status:Bool) {
        self.orderDetails.currentItemList[index]["quantity"] = Int(itemQty)!
        self.manageVM.editItem(orderId:order.orderID , itemList:self.orderDetails.currentItemList, status: orderDetails.itemsList.count == 1 && Int(itemQty) ?? 0 == 0 ? true:false , quantityStatus:status )
    }
}
