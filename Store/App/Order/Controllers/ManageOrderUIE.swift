//
//  ManageOrderUIE.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 15/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

extension ManageOrderVC {
    
    func setUI(){
        
     manualAssignButton.setTitle(StringConstants.manualAssign(), for: UIControlState.normal)
    acceptButton.setTitle(StringConstants.accept(), for: UIControlState.normal)
    rejectButton.setTitle(StringConstants.reject(), for: UIControlState.normal)
     self.navTitle.text =  "\(order.storeTypeMsg) Order Id \n \(order.orderID)"
        
        
        switch order.orderStatus {
        case 1:
            self.showAcceptRejectView()
        case 4:
            // Pickup Order
            if order.serviceType == 2{
               self.showSliderViewOnly()
            }
             // Laundry Order
            else if StoreUtility.getStore().storeType == 5{
                self.showSliderViewOnly()
            }
            
            else if StoreUtility.getStore().driverType == 2 {
               if StoreUtility.getStore().isAutoDispatch{
                 self.hideAllBottomViews()
                }
                else{
                 self.showSliderAndManualAssignView()
                }
            }
            else{
                self.hideAllBottomViews()
            }
            
        case 5:
             self.showSliderViewOnly()
            
        case 40:
            // Order preparing && waiting for driver
            if  order.indispatch{
                self.hideAllBottomViews()
            }
            else if StoreUtility.getStore().driverType == 2{
                if StoreUtility.getStore().isAutoDispatch{
                    self.hideAllBottomViews()
                }
                else{
                    self.showSliderAndManualAssignView()
                }
            }
            else{
                self.hideAllBottomViews()
            }
            
        default:
            self.hideAllBottomViews()
        }
    }
    
    func setSliderTitle(){
        switch order.orderStatus  {
        case 4:
            if StoreUtility.getStore().storeType == 5{
                self.leftSlider.sliderText = StringConstants.washing()
            }
            else if order.serviceType == 2 {
                self.leftSlider.sliderText = StringConstants.pickupReady()
            }
            else if StoreUtility.getStore().driverType != 1{
                self.leftSlider.sliderText = StringConstants.autoDispatch()
            }
        case 5:
            if order.serviceType == 2{
                self.leftSlider.sliderText = StringConstants.pickupCompleted()
            }
        case 40:
            // Order preparing && waiting for driver
             if StoreUtility.getStore().driverType == 2{
                self.leftSlider.sliderText = StringConstants.autoDispatch()
                }
        default:
            self.leftSlider.sliderText = ""
        }
    }
    
    
    func showSliderViewOnly(){
        self.setSliderTitle()
        self.acceptRejectViewHeight.constant = 20
        self.sliderViewHeight.constant = 60
        self.manualAssignViewHeight.constant = 10
        self.itemsTVHeightAgainstBottom.constant = 70
        self.acceptRejectView.isHidden = true
        self.view.bringSubview(toFront: self.sliderView)
    }
    
    func showSliderAndManualAssignView(){
        self.setSliderTitle()
        self.acceptRejectViewHeight.constant = 20
        self.sliderViewHeight.constant = 60
        self.manualAssignViewHeight.constant = 60
        self.itemsTVHeightAgainstBottom.constant = 130
        self.acceptRejectView.isHidden = true
        self.view.bringSubview(toFront: self.sliderView)
        
    }
    
    func showAcceptRejectView(){
        self.acceptRejectViewHeight.constant = 60
        self.sliderViewHeight.constant = 10
        self.manualAssignViewHeight.constant = 10
        self.itemsTVHeightAgainstBottom.constant = 80
        self.sliderView.isHidden = true
        self.view.bringSubview(toFront: self.acceptRejectView)
    }
    
    func hideAllBottomViews(){
        self.acceptRejectViewHeight.constant = 20
        self.sliderViewHeight.constant = 10
        self.manualAssignViewHeight.constant = 10
        self.itemsTVHeightAgainstBottom.constant = 20
        self.acceptRejectView.isHidden = true
        self.sliderView.isHidden = true
    }
    
}
