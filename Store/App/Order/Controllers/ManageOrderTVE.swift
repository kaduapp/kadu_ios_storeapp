//
//  ManageOrderTVE.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 15/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit


class OrderItemTableView: UITableView {
    override func awakeFromNib() {
        super.awakeFromNib()
     self.register(ItemListTablePortionCell.nib, forCellReuseIdentifier: ItemListTablePortionCell.identifier)
    self.register(PopUpOrderSummaryCell.nib, forCellReuseIdentifier: PopUpOrderSummaryCell.identifier)
    self.register(OrderAmountSummaryCell.nib, forCellReuseIdentifier: OrderAmountSummaryCell.identifier)
    self.register(ItemListTableHeaderCell.nib, forCellReuseIdentifier: ItemListTableHeaderCell.identifier)
    }
}

class iPadItemsTableView: UITableView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.register(ItemListTablePortionCell.nib, forCellReuseIdentifier: ItemListTablePortionCell.identifier)
        self.register(ItemListTableHeaderCell.nib, forCellReuseIdentifier: ItemListTableHeaderCell.identifier)
    }
}

class iPadTaxesTableView: UITableView {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.register(OrderAmountSummaryCell.nib, forCellReuseIdentifier: OrderAmountSummaryCell.identifier)
    }
}

extension ManageOrderVC : UITableViewDataSource, UITableViewDelegate{
    // There is one TableView for iPhone with tag 0
    // There are two three table view for iPad with tag 0,1,2
    func numberOfSections(in tableView: UITableView) -> Int {
        if !isIphone(){
         return 1
        }
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var switchCase = section
        if !isIphone(){
         switchCase = tableView.tag
        }
        
        switch switchCase {
        case 0:
            return 1
        case 1:
            return self.orderDetails.itemsList.count
        case 2:
            let order = ManageOrderAmount.init(homeOrderModal: orderDetails)
            return   order.excTaxes.count + order.amountArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var switchCase = indexPath.section
        if !isIphone(){
            switchCase = tableView.tag
        }
        switch switchCase{
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: PopUpOrderSummaryCell.identifier) as! PopUpOrderSummaryCell
            cell.item = self.orderDetails
        cell.customerDetailsView.pastOrdersButton.addTarget(self, action:#selector(showOrderHistoryForCustomer) , for: UIControlEvents.touchUpInside)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: ItemListTablePortionCell.identifier) as! ItemListTablePortionCell
            
            cell.quantity.text = "\(indexPath.row + 1)"
            cell.item = self.orderDetails.itemsList[indexPath.row]
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: OrderAmountSummaryCell.identifier)as! OrderAmountSummaryCell
            let amountObj = ManageOrderAmount.init(homeOrderModal: orderDetails)
            cell.setOrderAmount(amount: amountObj, index: indexPath.row)
            
            return cell
        default:
            return UITableViewCell.init()
        }

    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var switchCase = section
        if !isIphone(){
            switchCase = tableView.tag
        }
        switch switchCase {
        case 1:
            let headerViewCell = tableView.dequeueReusableCell(withIdentifier: ItemListTableHeaderCell.identifier)as! ItemListTableHeaderCell
            headerViewCell.setHeader(orderDetails.itemsList.count)
            return headerViewCell
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var switchCase = section
        if !isIphone(){
            switchCase = tableView.tag
        }
        switch switchCase {
        case 1:
             return 40
        default:
            return 0
        }
     
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var switchCase = indexPath.section
        if !isIphone(){
            switchCase = tableView.tag
        }
        switch switchCase {
        case 1:
            if order.orderStatus != 12 && order.orderStatus != 13 && order.orderStatus != 14 {
            let item = self.orderDetails.itemsList[indexPath.row]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditOrderViewController") as! EditOrderViewController
                
            vc.qty = String(item.itemQnty)
            vc.price = "\(item.currencySymbol) \(item.unitPrice)"
            vc.itemName = item.itemName
            vc.indexId = indexPath.row
            vc.delegate = self
            self.present(vc, animated: true , completion: nil)
            }
        default:
            print("No Action")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension
    }
    
    
    @objc func showOrderHistoryForCustomer(){
        let customerHistoryVC = self.storyboard?.instantiateViewController(withIdentifier: "HistoryViewController") as! HistoryViewController
        customerHistoryVC.customerId = orderDetails.customerData.customerId
        customerHistoryVC.isCustomer = true
    self.navigationController?.pushViewController(customerHistoryVC, animated: true)
    }
}
