//
//  Order.swift
//  Ufy_Store
//
//  Created by Rahul Sharma on 28/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

class CustomerData {
    var customerName:String = ""
    var customerImage:String = ""
    var deviceType: DeviceType!
    var customerId:String = ""
    var customerPhone:String = ""
    
    
    
    init(data: [String:Any]) {
        if let customerDetails = data["customerDetails"] as? [String:Any] {
            customerName = customerDetails["name"] as! String
            customerId = customerDetails["customerId"] as! String
            deviceType = .AppleDevice // venga added
            if let val = customerDetails["deviceType"] as? Int{
                deviceType = val == 2 ? .AndroidDevice  : .AppleDevice
            }
            
            if let countryCode = customerDetails["countryCode"] as? String, let mobile = customerDetails["mobile"] as? String{
                customerPhone = countryCode + mobile
            }
            
            
            
            if let profilePic = customerDetails["profilePic"] as? String {
                customerImage = profilePic
            }
        }
    }
    
    deinit {
        print("CustomerData deinit")
    }
}

/// order details
class OrderSummary {
    
    var orderId:Int = 0
    var bookingDateTime:String = ""
    var date = Date()
    var dueDateTime:String = ""
    var bookingType:BookingType? = .Now
    var serviceType:ServiceType? = .Delivery
    var inDispatch:Bool = false
    var deliveryAddress:String = ""
    var currencySymbol:String = ""
    var subTotalAmount:Float = 0.0
    var deliveryAmount:Float = 0.0
    var taxTotalAmt:Float = 0.0
    var serviceAmount:Float = 0.0
    var discount:Float = 0.0
    var customerSavings:Float = 0.0
    var totalAmount:Float = 0.0
    var orderStatus:Int = 0
    var orderStatusMsg = ""
    var paymentType:PaymentType = .None
    var payByWallet:Int = 0
    var driverDetails:DriverDetails!
    var customerOrders:String = "0"
    var customRevenue:String = "0"
    var storeID:String = "0"
    var customerName:String = ""
    var storeName:String = ""
    var storeAddress:String = ""
    var taxData:[[String:Any]] = [[:]]
    var weight:Int = 0
    var storeTypeMsg = ""
    var storeType = 0
    var storeEarningValue: Float = 0.0
    var dueTimeStamp:Int64 = 0
    var extraNote = ""
    init(data: [String:Any]) {
        
        if let extraNote = data["extraNote"] as? String{
            self.extraNote = extraNote
        }
        if let dueStamp = data["dueDatetimeTimeStamp"] as? NSNumber{
            dueTimeStamp = Int64(truncating: dueStamp)
        }
        
        if let customerData = data["customerDetails"] as? [String:Any]{
            if let customerOrdersCount = customerData["customerOrderCount"] as? NSNumber{
                customerOrders = String(describing: customerOrdersCount)
            }
            
            if let customerRevenue = customerData["customerRevenue"] as? NSNumber{
                
                customRevenue = String(format: "%.2f",Float(truncating: customerRevenue))
            }
            
            if let custName = customerData["name"] as? String{
                
                customerName = custName
            }
        }
        
        if let stName = data["storeName"] as? String{
            storeName = stName
        }
        
        if let stAddress = data["storeAddress"] as? String{
            storeAddress = stAddress
        }
        
        if let taxObjects = data["exclusiveTaxes"] as? [[String:Any]]{
            taxData = taxObjects
        }
        
        if let currentOrderId = data["orderId"] as? Int {
            orderId = currentOrderId
        }
        
        if let payment = data["paymentType"] as? Int {
            paymentType = PaymentType(rawValue: payment)!
        }
        
        if let paByWallet = data["payByWallet"] as? Int {
            payByWallet = paByWallet
        }
        
        if let bookingType = data["bookingType"] as? NSNumber{
            if bookingType == 2{
                if let bookingDate = data["dueDatetime"] as? String {
                    bookingDateTime = bookingDate
                    let dateString = Date.getDateFromString(value: bookingDate, format:DateFormat.DateFormatServer )
                    date  = Date.getStringToDate(value: dateString, format: DateFormat.DateFormatServer)
                }
                
                
            }else{
                if let bookingDate = data["bookingDate"] as? String {
                    bookingDateTime = bookingDate
                    let dateString = Date.getDateFromString(value: bookingDate, format:DateFormat.DateFormatServer )
                    date  = Date.getStringToDate(value: dateString, format: DateFormat.DateFormatServer)
                }
            }
        }
        
        
        if let storeId = data["storeId"] as? String{
            storeID = storeId
        }
        if let wight = data["weight"] as? Int{
            weight = wight
        }
        
        if let status = data["status"] as? Int {
            orderStatus = status
        }
        
        if let dueDate = data["dueDatetime"] as? String {
            dueDateTime = dueDate
        }
        
        if let statusMsg = data["statusMsg"] as? String {
            orderStatusMsg = statusMsg;
        }
        
        if let service = data["serviceType"] as? Int {
            serviceType = service == 1 ? .Delivery : .Pickup
        }
        
        if let bookinType = data["bookingType"] as? Int {
            bookingType = bookinType == 1 ? .Now : .Later
        }
        
        if let dispatch = data["inDispatch"] as? Int{
            inDispatch = dispatch == 0 ? false : true
        }
        
        if let storeTypeMsg = data["storeTypeMsg"] as? String{
            self.storeTypeMsg = storeTypeMsg
        }
        
        if let storeType = data["storeType"] as? Int{
            
            UserDefaults.standard.set(storeType, forKey: "myStoreType")
            
            self.storeType = storeType
        }
        
        if let driverType = data["driverType"] as? Int {
            //1 = freelancing 2= store drivers availlable
            let showDriver = driverType == 2 ? true : false
            UserDefaults.standard.set(showDriver, forKey: "StoreDriver_type")
            UserDefaults.standard.synchronize()
        }
        
        
        if let account = data["accouting"] as? [String:Any] {
            if let storeEarningValue  = account["storeEarningValue"] as? NSNumber {
                self.storeEarningValue =  Float(truncating: storeEarningValue)
            }
        }
        
        
        // address
        if let dropData = data["drop"] as? [String:Any] {
              if let add0 = dropData["flatNumber"] as? String {
                      deliveryAddress =  add0
                       if let addo1 = dropData["landmark"] as? String {
                           deliveryAddress =  deliveryAddress + "," + addo1
    
            if let add1 = dropData["addressLine1"] as? String{
                deliveryAddress = deliveryAddress + " " + add1
                if let placeName = dropData["placeName"] as? String{
                    deliveryAddress = deliveryAddress + " " + placeName
                    if let landMark = dropData["landmark"] as? String{
                        deliveryAddress = deliveryAddress + " " + landMark
                        if let city = dropData["city"] as? String{
                            deliveryAddress = deliveryAddress + " " + city
                            if let postal = dropData["postalCode"] as? String{
                                deliveryAddress = deliveryAddress + " " + postal
                            }
                        }
                    }
                }
            }
        }
            }
        }
        
        
        if let taxTotal =  data["excTax"] as? NSNumber {
            taxTotalAmt = Float(truncating: taxTotal)
        }
        
        if let subTotalCharge =  data["subTotalAmount"] as? NSNumber {
            subTotalAmount = Float(truncating: subTotalCharge)
        }
        
        if let deliveryCharge =  data["deliveryCharge"] as? NSNumber {
            deliveryAmount = Float(truncating: deliveryCharge)
        }
        if let discountCharge = data["discount"] as? NSNumber {
            discount = Float(truncating: discountCharge)
        }
        if let customerSav = data["cartDiscount"] as? NSNumber {
            customerSavings = Float(truncating: customerSav)
        }
        if let serviceCharge =  data["serviceCharge"] as? Float {
            serviceAmount = serviceCharge
        }
        if let totalCharge =  data["totalAmount"] as? NSNumber {
            totalAmount =  Float(truncating: totalCharge)
        }
        
        if let currency = data["currencySymbol"] as? String {
            UserDefaults.standard.set(currency, forKey: "CurrencySymbol")
            UserDefaults.standard.synchronize()
            currencySymbol = (currency as NSString) as String
        }
        
        if let driver = data["driverDetails"] as? [String:Any]{
            driverDetails = DriverDetails.init(driver: driver)
        }
    }
    
    deinit {
        print("OrderSummary Deinit")
    }
}

class DriverDetails {
    var driverName = ""
    var drivePic = ""
    var phoneNumber = ""
    
    init(driver:[String:Any]) {
        if let name = driver["fName"] as? String {
            driverName = name
        }
        
        if let pic = driver["image"] as? String {
            drivePic = pic
        }
        
        if let phone = driver["mobile"] as? String {
            phoneNumber = phone
        }
    }
}


struct ReasonModal {
    var bookingDate = ""
    var arrayOfReasons = [String]()
}




/// Store Drivers modal
class OnlineDriversModal {
    var arrayOfDrivers = [Driver]()
    init(data:Any) {
        if let data = data as? [String:Any], let drivers = data["availableDriver"] as? [[String:Any]] {
            arrayOfDrivers = drivers.map {
                Driver.init(driver: $0)
            }
        }
    }
}






