//
//  Item.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 15/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation

class Item {
    
    var itemName:String = ""
    var itemQnty:Int = 0
    var unitPrice:Float = 0.0
    var currencySymbol:String = ""
    var unitName:String = ""
    var addOnAvailable = 0
    var productId = ""
    var addOnsArray = [AddOn]()
    var images : [String] = []
    var storeType = 0
    init(data:[String:Any]) {
        
        
        if let productName = data["itemName"] as? String {
            itemName = productName
        }
        
        
        if let qauntity = data["quantity"] as? Int {
            itemQnty = qauntity
        }
        
        
        if let productId = data["productId"] as? String{
            self.productId = productId
        }
        
        if let productUnitPrice = data["finalPrice"] as? NSNumber {  //unitPrice
            unitPrice = Float(truncating: productUnitPrice)
        }
        
        if let unit = data["unitName"] as? String {
            unitName = unit
        }
        
        
        if let images = data["images"] as? [String]{
            self.images = images
        }
        
        if let currency = data["currencySymbol"] as? String {
            currencySymbol = currency
        }
        
        if currencySymbol == ""{
            currencySymbol =   UserDefaults.standard.value(forKey: "CurrencySymbol") as? String ?? ""
        }
        
        if let addOnAvailabl = data["addOnAvailable"]  as? Int{
            addOnAvailable = addOnAvailabl
            if(addOnAvailable != 0)
            {
                
            }
        }
        
        if let addOns = data["addOns"] as?  [[String:Any]] {
            if addOns.count > 0{
                for adds in addOns{
                    if adds.count > 0 {
                        if let addOnGroups = adds["addOnGroup"] as?  [[String:Any]] {
                            if addOnGroups.count > 0{
                                for singleAddon in   addOnGroups {
                                    addOnsArray.append(AddOn.init(addOnDetails: singleAddon))
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    deinit {
        print("Item deinit")
    }
}


/// add on class for item addon's
class AddOn {
    var name: String? = ""
    var price: String? = ""
    var id: String? = ""
    
    init(addOnDetails:[String:Any]) {
        
        if let name = addOnDetails["name"] as? String {
            self.name = name
        }
        if let price = addOnDetails["price"] as? String {
            self.price = price
        }
        if let id = addOnDetails["id"] as? String {
            self.id = id
        }
    }
}


