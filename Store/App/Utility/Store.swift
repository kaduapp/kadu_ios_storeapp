//
//  Store.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 14/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation


class Store : NSObject{
    
   var authToken = ""
   var bufferTime = 0
   var cityId = ""
   var cityName = ""
   var countryCode = ""
   var currency = ""
   var currencySymbol = ""
   var defaultBankAccount = ""
   var deviceId = ""
   var dispatcherUserType = 0
   var driverType = 0
   var enableBankAccount = false
   var fcmCityTopic = ""
   var fcmManagerTopic = ""
   var fcmStoreTopic = ""
   var fcmUserTopic = ""
   var franchiseId = "0"
   var franchiseName = ""
   var googleMapKey = ""
   var googleMapKeyMqtt = ""
   var ManagerId = ""
   var isAutoDispatch = false
   var isForcedAccept = false
   var profileName = ""
   var scheduledBookingEnable = 0
   var status = 0
   var storeId = "0"
   var storeName = ""
   var storeType = 0
   var storeTypeMsg = ""
   var userType = 0
   var mqttManagerTopic = ""
   var name = ""
    
    init(data:[String:Any]) {
        super.init()
    
        if let titleTemp = data["name"] as? String{
            name = titleTemp
        }
        
        if let titleTemp = data["cityId"] as? String{
            cityId = titleTemp
        }
        if let titleTemp = data["fcmStoreTopic"] as? String{
            fcmStoreTopic = titleTemp
        }
        
        if let titleTemp = data["fcmManagerTopic"] as? String{
            fcmManagerTopic = titleTemp
        }
        
        if let titleTemp = data["fcmUserTopic"] as? String{
            fcmUserTopic = titleTemp
        }
        
        if let titleTemp = data["franchiseId"] as? String{
            franchiseId = titleTemp
        }
        
        if let titleTemp = data["countryCode"] as? String{
            countryCode = titleTemp
        }
        if let titleTemp = data["enableBankAccount"] as? Bool{
            enableBankAccount = titleTemp
        }
        
        if let titleTemp = data["storeName"] as? String{
            storeName = titleTemp
        }
        
        if let titleTemp = data["currency"] as? String{
            currency = titleTemp
        }
        
        if let titleTemp = data["userType"] as? Int{
            userType = titleTemp
        }
        
        if let titleTemp = data["defaultBankAccount"] as? String{
            defaultBankAccount = titleTemp
        }
        
        if let titleTemp = data["cityName"] as? String{
            cityName = titleTemp
        }
        
        if let titleTemp = data["franchiseName"] as? String{
            franchiseName = titleTemp
        }
        
        if let titleTemp = data["profileName"] as? String{
            profileName = titleTemp
        }
        
        if let titleTemp = data["currencySymbol"] as? String{
            currencySymbol = titleTemp
        }
        
        if let titleTemp = data["authToken"] as? String{
            authToken = titleTemp
        }
        
        if let titleTemp = data["storeId"] as? String{
            storeId = titleTemp
        }
        
        
        if let titleTemp = data["id"] as? String{
            ManagerId = titleTemp
        }
        
        if let titleTemp = data["mqttManagerTopic"] as? String{
            mqttManagerTopic = titleTemp
        }
        
        if let titleTemp = data["storeTypeMsg"] as? String{
            storeTypeMsg = titleTemp
        }
    
        
        if let titleTemp = data["driverType"] as? Int{
            driverType = titleTemp
        }
        if let titleTemp = data["storeType"] as? Int{
           storeType = titleTemp
        }
        if let titleTemp = data["isAutoDispatch"] as? Int{
            
            let number = NSNumber.init(value: titleTemp)
            isAutoDispatch = number.boolValue
        }
        if let titleTemp = data["isForcedAccept"] as? Int{
            let number = NSNumber.init(value: titleTemp)
            isForcedAccept = number.boolValue
        }
        
    }
}
