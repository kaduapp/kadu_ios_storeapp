//
//  StoreUtility.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 14/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import FirebaseMessaging

class StoreUtility: NSObject {

class func getStore() -> Store {
    if let data     = UserDefaults.standard.object(forKey: UserDefaultConstants.StoreData) as? [String : Any]{
        let storeData = Store.init(data: data)
        return storeData
    }
    return Store.init(data: [:])
    }
    
    
    
    /// Remove user data, but store topics
    /// saved topics will get removed on successful unsubscription.
    class func removeUserData(){
    let store = StoreUtility.getStore()
     if store.userType == 0{
    
         UserDefaults.standard.set(store.fcmCityTopic, forKey: UserDefaultConstants.fcmCityTopic)
        }
      else{
        UserDefaults.standard.set(store.fcmStoreTopic,  forKey: UserDefaultConstants.fcmStoreTopic)
        }

      UserDefaults.standard.removeObject(forKey: UserDefaultConstants.StoreData)
    }

    class func isLogin() -> Bool {
        if StoreUtility.getStore().authToken.count > 0{
            return true
        }else {
            return false
        }
    }
 class func getHomeTabs() -> [String] {
    
    let store = StoreUtility.getStore()
    
    // Launder Only
    if store.storeType == 5 {
        return [ HomeTabs.New.title,HomeTabs.Washing.title,HomeTabs.In_Dispatch.title,HomeTabs.Assign.title]
    }
    
    if store.isForcedAccept && store.isAutoDispatch{
      
        if store.driverType == 2{ // Store Drivers Condition
          return [HomeTabs.Accepted.title,HomeTabs.Order_Pickup.title,HomeTabs.In_Dispatch.title,HomeTabs.Pickup_Ready.title]
        }
        else{// FreeLance Driver Condition
          return [HomeTabs.Accepted.title,HomeTabs.Order_Pickup.title,HomeTabs.Pickup_Ready.title]
        }
    }
    else if !store.isForcedAccept && store.isAutoDispatch {
        if store.driverType == 2{// Store Drivers Condition
           return [HomeTabs.New.title,HomeTabs.Accepted.title,HomeTabs.Order_Pickup.title,HomeTabs.In_Dispatch.title,HomeTabs.Pickup_Ready.title]
        }
        else{// FreeLance Driver Condition
            
             return [HomeTabs.New.title, HomeTabs.Accepted.title,HomeTabs.Order_Pickup.title,HomeTabs.Pickup_Ready.title]
        }
    }
    else if store.isForcedAccept && !store.isAutoDispatch{
        if store.driverType == 2{// Store Drivers Condition
             return [HomeTabs.New.title, HomeTabs.Accepted.title,HomeTabs.Order_Pickup.title,HomeTabs.Pickup_Ready.title]
        }
        else{// FreeLance Driver Condition
                return [HomeTabs.New.title, HomeTabs.Accepted.title,HomeTabs.Order_Pickup.title,HomeTabs.Pickup_Ready.title]
        }
    }
       return [HomeTabs.New.title, HomeTabs.Accepted.title,HomeTabs.Order_Pickup.title,HomeTabs.In_Dispatch.title, HomeTabs.Pickup_Ready.title]
 }
    
    
    class func subscribeFCMTopic() {
        if APIUtility.getLogin() {
            let store = StoreUtility.getStore()
            if store.userType == 0 {
                Messaging.messaging().subscribe(toTopic: store.fcmCityTopic)
            }
            else{
                Messaging.messaging().subscribe(toTopic: store.fcmStoreTopic)
            }
            Messaging.messaging().subscribe(toTopic: store.fcmManagerTopic)
            Messaging.messaging().subscribe(toTopic: store.fcmUserTopic)
            Messaging.messaging().subscribe(toTopic: "storeManager_" + store.cityId)
            
        }
    }
    
    class func unsubscribeFCMTopic() {
        let store = StoreUtility.getStore()
        if let fcmStoreTopic     = UserDefaults.standard.object(forKey: UserDefaultConstants.fcmStoreTopic) as? String, fcmStoreTopic.count > 0{
       Messaging.messaging().unsubscribe(fromTopic:fcmStoreTopic ) { (Error) in
        if Error == nil{
           UserDefaults.standard.removeObject(forKey: UserDefaultConstants.fcmStoreTopic)
        }
          }
        }
        if let fcmCityTopic  = UserDefaults.standard.object(forKey: UserDefaultConstants.fcmCityTopic) as? String, fcmCityTopic.count > 0{
            Messaging.messaging().unsubscribe(fromTopic:fcmCityTopic ) { (Error) in
                 if Error == nil{
                UserDefaults.standard.removeObject(forKey: UserDefaultConstants.fcmCityTopic)
                }
            }
        }

        Messaging.messaging().unsubscribe(fromTopic: store.fcmUserTopic)
        Messaging.messaging().unsubscribe(fromTopic: "storeManager_" + store.cityId)
        Messaging.messaging().unsubscribe(fromTopic: store.fcmManagerTopic)
    }
    
}

   enum HomeTabs:String {
    case New
    case Accepted
    case Order_Pickup
    case In_Dispatch
    case Pickup_Ready
    case Washing
    case Assign
    
    var title : String {
        switch self {
        case .New : return StringConstants.new()
        case .Accepted: return StringConstants.accepted()
        case .Order_Pickup: return StringConstants.orderPickup()
        case .In_Dispatch: return StringConstants.inDispatch()
        case .Pickup_Ready: return StringConstants.pickupReady()
        case .Washing: return StringConstants.washing()
        case .Assign: return StringConstants.assign()
        }
    }
}
