//
//  HomeViewModel.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 14/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import RxSwift



public enum PublishType:Int {
    case ServiceType
    case OrderDetails
    case MQTTType
}




public enum MQTTMessageType:Int {
    case MQTTNewOrder
    case MQTTSTAFFStatus
    case MQTTWebDispatcher
    case MQTTDriverOnlineStatus
    case MQTTCancelOrderStatus
    case MQTTSyncingData
}

public enum ServiceType:String {
    case None
    case Delivery = "Delivery"
    case Pickup = "Pickup"
}

enum BookingType:Int {
    case Now
    case Later
}

enum DeviceType:String {
    case None = ""
    case AndroidDevice = "device_android"
    case AppleDevice   = "device_ios"
}

enum PaymentType:Int {
    case Cash = 2
    case Card = 1
    case Wallet = 3
    case Ideal = 4
    case None = 0
}
class HomeViewModel : NSObject {
    
    
    var newOrder = [HomeOrder]()
    var acceptedOrder = [HomeOrder]()
    var inDispatchOrder = [HomeOrder]()
    var inProgress = [HomeOrder]() // Washing
    var pickUpReadyOrder = [HomeOrder]() // Order Ready
    var pickedUpOrder = [HomeOrder]() // Pickup Orders
    
    
    var newOrderSections = [[HomeOrder]]()
    var acceptedOrderSections = [[HomeOrder]]()
    var inDispatchOrderSections = [[HomeOrder]]()
    var inProgressSections = [[HomeOrder]]() // Washing
    var pickUpReadyOrderSections = [[HomeOrder]]() // Order Ready
    var pickedUpOrderSections = [[HomeOrder]]() // Pickup Orders
    
    var searchNewOrder = [HomeOrder]()
    var searchAcceptedOrder = [HomeOrder]()
    var searchInDispatchOrder = [HomeOrder]()
    var searchInProgress = [HomeOrder]() // Washing
    var searchPickUpReadyOrder = [HomeOrder]() // Order Ready
    var searchPickedUpOrder = [HomeOrder]() // Pickup Orders
    
    var searchNewOrderSections = [[HomeOrder]]()
    var searchAcceptedOrderSections = [[HomeOrder]]()
    var searchInDispatchOrderSections = [[HomeOrder]]()
    var searchInProgressSections = [[HomeOrder]]() // Washing
    var searchPickUpReadyOrderSections = [[HomeOrder]]() // Order Ready
    var searchPickedUpOrderSections = [[HomeOrder]]() // Pickup Orders
    
    
    let home_Response = PublishSubject<(Bool)>()
    
    // Get all orders
    func getAllOrders() {
        let store = StoreUtility.getStore()
        let cityID = store.cityId
        let header = APIUtility.getHeaderForOrders()
        let franchiseID = store.franchiseId
        let Index = 0
        let startDate = 0
        let fromDate = 0
        let isSearch = 0
        
        let getOrdersUrl = "\(APIEndTails.OrderList)\(cityID)/\(Index)/\(franchiseID)/\(store.storeId)/\(startDate)/\(fromDate)/\(isSearch)"

        
        APIHandler.getRequest(header:header, url:getOrdersUrl).subscribe(onNext: {[weak self]  success, data in
            if success == true {
                
                self?.handleOrders(data: data)
                self?.home_Response.onNext(true)
            }else {
    
            }
            
            }, onError: { Error in
                print(Error)
        }).disposed(by: disposeBag)
    }
    
    
    func handleOrders(data:Any) {
        if let response = data as? [String:Any]{
            
      if let orders = response["newOrder"] as? [[String:Any]] {
            self.newOrder = orders.map {
                HomeOrder.init(data: $0)
            }
         self.newOrderSections = self.makeDateSections(newOrder)
         }
    
     if let orders = response["acceptedOrder"] as? [[String:Any]] {
          self.acceptedOrder = orders.map {
                    HomeOrder.init(data: $0)
         }
           self.acceptedOrderSections = self.makeDateSections(acceptedOrder)
     }
            
     if let orders = response["inDispatchOrder"] as? [[String:Any]] {
                self.inDispatchOrder = orders.map {
                    HomeOrder.init(data: $0)
                }
            self.inDispatchOrderSections = self.makeDispatcherSections(inDispatchOrder)
     }
            
     if let orders = response["inProgress"] as? [[String:Any]] {
            self.inProgress = orders.map {
                    HomeOrder.init(data: $0)
                }
         self.inProgressSections = self.makeDateSections(inProgress)
        }
    
    if let orders = response["pickUpReadyOrder"] as? [[String:Any]] {
            self.pickUpReadyOrder = orders.map {
                HomeOrder.init(data: $0)
            }
         self.pickUpReadyOrderSections = self.makeDateSections(pickUpReadyOrder)
     }
    
    if let orders = response["pickedUpOrder"] as? [[String:Any]] {
            self.pickedUpOrder = orders.map {
                HomeOrder.init(data: $0)
            }
         self.pickedUpOrderSections = self.makeDateSections(pickedUpOrder)
        }
    }
  }
    
    
    func makeDateSections(_ ordersArray: [HomeOrder]) -> [[HomeOrder]] {
        
        var sectionsArray = [[HomeOrder]]()
        let datesArray = ordersArray.compactMap { $0.date } // return array of date
        
        let result = datesArray.unique()
        
        for date in result {
            let dateKey = date
            let filterArray = ordersArray.filter { $0.date == dateKey }
            sectionsArray.append(filterArray)
        }
        
        return sectionsArray
    }
    
   // Create Section for Dispathing (Auto dispatch InProgress & Auto dispatch Failed)
    func makeDispatcherSections(_ ordersArray: [HomeOrder]) -> [[HomeOrder]] {
        
        var sectionsArray = [[HomeOrder]]()
        
        let autoDispatch = ordersArray.filter { $0.indispatch == true }
            if autoDispatch.count > 0{
            sectionsArray.append(autoDispatch)
        }
        let autoDispatchFailed = ordersArray.filter { $0.indispatch == false }
        if autoDispatchFailed.count > 0{
        sectionsArray.append(autoDispatchFailed)
        }
        
        return sectionsArray
    }

    func getOrderModel(_ tabName :String) -> [HomeOrder]{
        
        switch tabName {
        case HomeTabs.New.title:
            //In case of Laundery new orders will be the accepted ones
            if StoreUtility.getStore().storeType == 5{
              return acceptedOrder
            }
           return newOrder
            
        case HomeTabs.Accepted.title:
            return acceptedOrder
            
        case HomeTabs.Order_Pickup.title,
             HomeTabs.Assign.title:
            return pickedUpOrder
            
        case HomeTabs.In_Dispatch.title:
            return inDispatchOrder
            
        case HomeTabs.Pickup_Ready.title:
            return pickUpReadyOrder
            
        case HomeTabs.Washing.title:
            return inProgress
            
        default:
            return []
        }
    }
    
    func getOrderModelSections(_ tabName :String) -> [[HomeOrder]]{
        
        switch tabName {
        case HomeTabs.New.title:
            //In case of Laundery new orders will be the accepted ones
            if StoreUtility.getStore().storeType == 5{
               
                return acceptedOrderSections
            }
            return newOrderSections
            
        case HomeTabs.Accepted.title:
            return acceptedOrderSections
            
        case HomeTabs.Order_Pickup.title,
             HomeTabs.Assign.title:
            return pickedUpOrderSections
            
        case HomeTabs.In_Dispatch.title:
            return inDispatchOrderSections
            
        case HomeTabs.Pickup_Ready.title:
            return pickUpReadyOrderSections
            
        case HomeTabs.Washing.title:
            return inProgressSections
            
        default:
            return []
        }
    }
    
    
    func getSearchedModel(_ tabName :String) -> [HomeOrder]{
        
        switch tabName {
        case HomeTabs.New.title:
            //In case of Laundery new orders will be the accepted ones
            if StoreUtility.getStore().storeType == 5{
                
                 return searchAcceptedOrder
            }
            return searchNewOrder
            
        case HomeTabs.Accepted.title:
            return searchAcceptedOrder
            
        case HomeTabs.Order_Pickup.title,
             HomeTabs.Assign.title:
            return searchPickedUpOrder
            
        case HomeTabs.In_Dispatch.title:
            return searchInDispatchOrder
            
        case HomeTabs.Pickup_Ready.title:
            return searchPickUpReadyOrder
            
        case HomeTabs.Washing.title:
            return searchInProgress
            
        default:
            return []
        }
    }
    
    func getSearchedModelSections(_ tabName :String) -> [[HomeOrder]]{
        
        switch tabName {
        case HomeTabs.New.title:
            //In case of Laundery new orders will be the accepted ones
            if StoreUtility.getStore().storeType == 5{
                
              return searchAcceptedOrderSections
            }
            return searchNewOrderSections
            
        case HomeTabs.Accepted.title:
            return searchAcceptedOrderSections
            
        case HomeTabs.Order_Pickup.title,
             HomeTabs.Assign.title:
            return searchPickedUpOrderSections
            
        case HomeTabs.In_Dispatch.title:
            return searchInDispatchOrderSections
            
        case HomeTabs.Pickup_Ready.title:
            return searchPickUpReadyOrderSections
            
        case HomeTabs.Washing.title:
            return searchInProgressSections
            
        default:
            return []
        }
    }
    
    func createSearchedModel(_ tabName :String,_ model :[HomeOrder]){
        
        switch tabName {
        case HomeTabs.New.title:
            //In case of Laundery new orders will be the accepted ones
            if StoreUtility.getStore().storeType == 5{
                
                searchAcceptedOrder.removeAll()
                searchAcceptedOrder.append(contentsOf: model)
                searchAcceptedOrderSections = self.makeDateSections(searchAcceptedOrder)
            }
            else{
            searchNewOrder.removeAll()
            searchNewOrder.append(contentsOf: model)
            searchNewOrderSections = self.makeDateSections(searchNewOrder)
            }
            
        case HomeTabs.Accepted.title:
            searchAcceptedOrder.removeAll()
           searchAcceptedOrder.append(contentsOf: model)
         searchAcceptedOrderSections = self.makeDateSections(searchAcceptedOrder)
            
        case HomeTabs.Order_Pickup.title,
             HomeTabs.Assign.title:
            searchPickedUpOrder.removeAll()
          searchPickedUpOrder.append(contentsOf: model)
         searchPickedUpOrderSections = self.makeDateSections(searchPickedUpOrder)
            
        case HomeTabs.In_Dispatch.title:
         searchInDispatchOrder.removeAll()
          searchInDispatchOrder.append(contentsOf: model)
         searchInDispatchOrderSections = self.makeDispatcherSections(searchInDispatchOrder)
            
        case HomeTabs.Pickup_Ready.title:
        searchPickUpReadyOrder.removeAll()
          searchPickUpReadyOrder.append(contentsOf: model)
         searchPickUpReadyOrderSections = self.makeDateSections(searchPickUpReadyOrder)
            
        case HomeTabs.Washing.title:
        searchInProgress.removeAll()
          searchInProgress.append(contentsOf: model)
         searchInProgressSections = self.makeDateSections(searchInProgress)
            
        default:
           print("End")
        }
    }
    
    func isDispatcherTab(_ tabName:String) -> Bool{
        
        if tabName == HomeTabs.In_Dispatch.title && self.inDispatchOrderSections.count > 0{
           return true
        }
        return false
    }
    
    
    
   // Update Orders Real Time
//    func changeOrderPosition(homeOrderM:HomeOrder) {
//        switch (orderStatus) {
//        case OrderStatus.NewOrder.rawValue:
//          
//        case OrderStatus.PreparingOrder.rawValue :
//          
//        case OrderStatus.OrderReady.rawValue,  OrderStatus.DriverAccepted.rawValue :
//         
//        case OrderStatus.OrderCompleted.rawValue,OrderStatus.DriverDeliverOrder.rawValue, OrderStatus.DriverJobFinished.rawValue,OrderStatus.ManagerCancelled.rawValue,OrderStatus.ManagerRejected.rawValue,OrderStatus.OrderExpired.rawValue,OrderStatus.CancelledByCustomer.rawValue:
//            
//        // if driver on delivery location
//        case OrderStatus.DriverAtDeliveryLocation.rawValue :
//            
//        case OrderStatus.DriverToCustomer.rawValue:
//         
//        case OrderStatus.DriverToStore.rawValue , OrderStatus.DriverAtStore.rawValue:
//          
//        case OrderStatus.Dispatch.rawValue:
//            
//        default:
//            break
//        }
//        
//     self.order_response.onNext((false,PublishType.MQTTType))
//    }
    
}


extension Sequence where Iterator.Element: Hashable {
    func unique() -> [Iterator.Element] {
        var alreadyAdded = Set<Iterator.Element>()
        return self.filter { alreadyAdded.insert($0).inserted }
    }
}




