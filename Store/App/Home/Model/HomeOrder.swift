//
//  HomeOrder.swift
//  Ufy_Store
//
//  Created by Rahul Sharma on 21/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

struct HomeOrder:Equatable {
    
    
    var orderID:Int = 0
    var customerName = ""
    var orderAmount:Float = 0.0
    var bookingDateTime = ""
    var dueDateTime = ""
    var bookingDateTimeStamp:Int64 = 0
    var date = Date()
    var dueTimeStamp:Int64 = 0
    var currencySymbol = ""
    var orderStatus:Int = 1
    var serviceType:Int = 1
    var bookingType:Int = 1
    var forcedAccept:Bool = false
    var indispatch:Bool = false
    var statusMessage = ""
    var storeTypeMsg = ""
    var storeType = 0
    var displayAmount : Float = 0.0
    var discount : Float = 0.0
    var tax : Float = 0.0
    
    
    init(data:[String:Any]) {
        
        if let customerDetails = data["customerDetails"] as? [String:Any], let name = customerDetails["name"] as? String  {
            customerName = name
        }
        
        if let storeType  =  data["storeType"] as?  Int{
            self.storeType = storeType
        }
        
        
        if Utility.getStoreType() == 5{
            
            if let tax = data["excTax"] as? NSNumber{
                self.tax = Float(truncating: tax)
            }
            if let discount = data["discount"] as? Float {
                self.discount = discount
            }
            
            if let subTotal = data["subTotalAmount"] as? NSNumber {
                displayAmount = Float(truncating: subTotal) - discount + tax
            }
            
        }else{
            if storeType == 5{
                if let tax = data["excTax"] as? NSNumber{
                    self.tax = Float(truncating: tax)
                }
                
                if let discount = data["discount"] as? Float {
                    self.discount = discount
                }
                
                if let subTotal = data["subTotalAmount"] as? NSNumber {
                    displayAmount = Float(truncating: subTotal) - discount + tax
                }
                
                
            }else{
                if let tax = data["excTax"] as? NSNumber{
                    self.tax = Float(truncating: tax)
                }
                if let discount = data["cartDiscount"] as? Float {
                    self.discount = discount
                }
                
                if let subTotal = data["cartTotal"] as? NSNumber {
                    displayAmount = Float(truncating: subTotal) - discount + tax
                }
                
            }
            
        }
        
        
        
        
        if let service = data["serviceType"] as? Int { // 1 for delivery , 2 for pickup
            serviceType = service
        }
        
        if let bookType = data["bookingType"] as? Int {  // 1 for now , 2 later
            bookingType = bookType
        }
        
        if let currentOrderId = data["orderId"] as? Int {
            orderID = currentOrderId
        }
        
        if let status = data["statusMsg"] as? String{
            statusMessage = status
            //            OrderCollectionCell.statusMsg = status
        }
        
        
        
        if let bookingType = data["bookingType"] as? NSNumber{
            if bookingType == 2{
                if let bookingDate = data["dueDatetime"] as? String {
                    bookingDateTime = bookingDate
                    let dateString = Date.getDateFromString(value: bookingDate, format:DateFormat.DateFormatServer )
                    date  = Date.getStringToDate(value: dateString, format: DateFormat.DateFormatServer)
                }
                
                if let bookingTimeStamp = data["dueDatetimeTimeStamp"] as? NSNumber{
                    bookingDateTimeStamp = Int64(truncating: bookingTimeStamp)
                }
                
            }else{
                if let bookingDate = data["bookingDate"] as? String {
                    bookingDateTime = bookingDate
                    let dateString = Date.getDateFromString(value: bookingDate, format:DateFormat.DateFormatServer )
                    date  = Date.getStringToDate(value: dateString, format: DateFormat.DateFormatServer)
                }
                
                if let bookingTimeStamp = data["bookingDateTimeStamp"] as? NSNumber{
                    bookingDateTimeStamp = Int64(truncating: bookingTimeStamp)
                    
                }
            }
        }
        
        if let dueStamp = data["dueDatetimeTimeStamp"] as? NSNumber{
            dueTimeStamp = Int64(truncating: dueStamp)
        }
        
        
        if let dueDate = data["dueDatetime"] as? String {
            dueDateTime = dueDate
        }
        
        if let totalCharge =  data["totalAmount"] as? NSNumber {
            orderAmount =  Float(truncating: totalCharge)
        }
        
        if let currency = data["currencySymbol"] as? String {
            currencySymbol = (currency as NSString) as String
        }
        
        if let status = data["status"] as? Int {
            orderStatus = status
        }
        
        if let forced = data["forcedAccept"] as? Int {
            forcedAccept = forced == 2 ? false : true
        }
        
        if let dispatch = data["inDispatch"] as? Int{
            indispatch = dispatch == 0 ? false : true
        }
        if let storeTypeMsg = data["storeTypeMsg"] as? String{
            self.storeTypeMsg = storeTypeMsg
        }
        
    }
    
    
    static func ==(lhs: HomeOrder, rhs: HomeOrder) -> Bool {
        return lhs.orderID == rhs.orderID
    }
}
