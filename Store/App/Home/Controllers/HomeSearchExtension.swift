//
//  HomeSearchExtension.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 18/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Search bar observers
extension HomeSlidesViewController:UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
       let orders = self.viewModel.getOrderModel(self.homeTabs[searchBar.tag])
    
    let orderId = searchText.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator:"")
        
        if orderId.count > 0{
        let IdFiltered = orders.filter {"\($0.orderID)".contains(searchText)}
            
        self.viewModel.createSearchedModel(self.homeTabs[searchBar.tag], IdFiltered)
        }
        else{
        let NameFiltered = orders.filter {"\($0.customerName.capitalized)".contains(searchText)}
        self.viewModel.createSearchedModel(self.homeTabs[searchBar.tag], NameFiltered)
        }
    
        for slide in slides{
            if slide.orderCollectionView.tag == searchBar.tag{
               slide.orderCollectionView.reloadData()
             break
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    self.viewModel.createSearchedModel(self.homeTabs[searchBar.tag], [HomeOrder]())
    }
}
