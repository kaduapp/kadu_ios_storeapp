//
//  HomeViewController.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 08/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit
import RxSwift
import CocoaLumberjack


class HomeSlidesViewController: UIViewController {

    @IBOutlet weak var orderStatusView: OrderStatusView!
    @IBOutlet weak var slidesScrollViewBottom: NSLayoutConstraint!
    @IBOutlet weak var slideScrollView : UIScrollView!{
      didSet{
            slideScrollView.delegate = self
        }
    }
    @IBOutlet weak var tabsScrollView : UIScrollView!{
        didSet{
            tabsScrollView.delegate = self
        }
    }
    var progressHUD :ProgressHUD!
   var homeTabs :[String] = StoreUtility.getHomeTabs()
   var slides:[HomeSlidesView] = []
   var tabs : [HomeTabsView] = []
   let storeDriverVM = StoreDriverViewModel()
   var selectedHomeTab : Int = 0
   var viewModel = HomeViewModel()
    
    /// Width of title tabs in iphone
    var titleTabsWidth : CGFloat = 150
    
    override func viewDidLoad() {
      super.viewDidLoad()
      self.setNavigationBarItem()
      addShadowToBar()
      self.createTitleTabs()
      setupHomeTabsScrollView(slides: self.tabs)
      self.createSlides()
      setupSlideScrollView(slides: slides)
      self.setSubscriberForResponse()
        Helper.showPI(string: StringConstants.ProgressIndicator())
      self.viewModel.getAllOrders()
      self.setStoreName()
      setRefreshAction()
      self.addObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.removeObserver()
    }
    
    func setSubscriberForResponse(){
        viewModel.home_Response.subscribe(onNext: {success in
            self.reloadData()
        }, onError: {error in
            print(error)
        }).disposed(by: disposeBag)
        
        _ = MQTT.sharedInstance.publishSubject.subscribe(onNext: {[weak self] response, messageType in
            if  messageType == .MQTTNewOrder
            {
              if response.count > 0{
              let homeOrder = HomeOrder.init(data: response)
              self?.updateOrderPosition(order:homeOrder )
              }
              else{
              self?.viewModel.getAllOrders()
              }
                
            }
            else if messageType == .MQTTSyncingData{
              Helper.showPI(string: "")
            }
            else if messageType == .MQTTDriverOnlineStatus {
                //  self?.driverData(data: response)
            }
            }).disposed(by: disposeBag)
    }
    
    
    /// Handle the position of order for real time update
    ///
    /// - Parameter order: Home order object
    func updateOrderPosition(order: HomeOrder){
        
        switch order.orderStatus {
        case 1:
            if homeTabs.contains(HomeTabs.New.title) && !viewModel.getOrderModel(HomeTabs.New.title).contains(order){
               viewModel.newOrder.insert(order,at: 0)
               viewModel.newOrderSections =  viewModel.makeDateSections(viewModel.newOrder)
                self.reloadData()
            }
        case 4:
            if homeTabs.contains(HomeTabs.New.title) && viewModel.getOrderModel(HomeTabs.New.title).contains(order){
                if let index = viewModel.newOrder.firstIndex(of: order) {
                viewModel.newOrder.remove(at: index)
                viewModel.newOrderSections =  viewModel.makeDateSections(viewModel.newOrder)
                }
            }
            if homeTabs.contains(HomeTabs.Accepted.title) && !viewModel.getOrderModel(HomeTabs.Accepted.title).contains(order){
                viewModel.acceptedOrder.insert(order,at: 0)
                viewModel.acceptedOrderSections =  viewModel.makeDateSections(viewModel.acceptedOrder)
                 self.reloadData()
            }
        
        default:
              self.viewModel.getAllOrders()
        }
        
    }

    
    /// Online and offline status of driver
    ///
    /// - Parameter data: MQTT message
//    func driverData(data:Any) {
//        if let data = data as? [String:Any], let driverStatus = data["status"] as? Int {
//
//            let driver = Driver.init(driver: data)
//
//            if selectedId == "0"{
//                if driverStatus == 3  {
//                    if !self.storeDriverVM.storeDriverDetails.driverDetails.contains(driver) {
//                        self.storeDriverVM.storeDriverDetails.driverDetails.insert(driver, at: 0)
//                    }
//                }else {
//                    self.storeDriverVM.storeDriverDetails.driverDetails = self.storeDriverVM.storeDriverDetails.driverDetails.filter{$0 != driver}
//                }
//            }else if driver.storeId == selectedId{
//                if driverStatus == 3  {
//                    if !self.storeDriverVM.storeDriverDetails.driverDetails.contains(driver) {
//                        self.storeDriverVM.storeDriverDetails.driverDetails.insert(driver, at: 0)
//                    }
//                }else {
//                    self.storeDriverVM.storeDriverDetails.driverDetails = self.storeDriverVM.storeDriverDetails.driverDetails.filter{$0 != driver}
//                }
//            }
//
//            self.storeDriverVM.storeDriverDetails.driverDetails.sort()
//            self.showErrorMessage()
//          //  self.storeDriverDetails.reloadData()
//        }
//    }
    
    func showErrorMessage() {
        if self.storeDriverVM.storeDriverDetails.driverDetails.count == 0 {
            //self.storeDriverDetails.showErrorMessage(message: StringConstants.noDrivers())
        }else {
          //  self.storeDriverDetails.hideErrorMessage()
        }
    }
    
    
    /// set store name on navigationcontroller title
    func setStoreName() {
        
        let store = StoreUtility.getStore()
        var storeName = ""
        switch store.userType {
        case 0: storeName = store.name
        case 1: storeName = store.franchiseName
        case 2: storeName = store.storeName
        default:
          storeName =   store.storeName
        }
        
        //storeName = removeSpecialCharsFromString(text: storeName)
        self.navigationItem.title = storeName
        
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890:".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
    
    
    func reloadData(){
        for (index, element) in self.tabs.enumerated(){
            Helper.hidePI()
            let orders = self.viewModel.getOrderModel(self.homeTabs[index])
            
            element.title.text = "\(self.homeTabs[index]) (\(orders.count))"
        }
        for slide in self.slides{
            slide.orderCollectionView.reloadData()
        }
    }
    
    
    /// This method will get called when we refresh the table view
    @objc func refreshNewOrders(_ rc:UIRefreshControl) {
        self.view.endEditing(true)
        rc.endRefreshing()
        self.viewModel.getAllOrders()
    }
    /// Setting refresh methods
    func setRefreshAction()  {
        for slideView in slides{
    slideView.orderCollectionView.refreshControl?.addTarget(self, action: #selector(refreshNewOrders(_:)), for: .valueChanged)
        }
        
    }
    
    @objc func homeTabsButtonClicked(sender : UIButton){
        
       let contentOffset = slideScrollView.contentOffset;
       let point = CGPoint(x: CGFloat(sender.tag * Int(self.view.frame.width)) , y: contentOffset.y)
        UIView.animate(withDuration: 0.2) { () -> Void in
           self.slideScrollView.contentOffset = point
            if self.homeTabs.indices.contains(sender.tag){
                let previousView = self.tabs[self.selectedHomeTab]
                 previousView.tabIsUnselected()
                let view = self.tabs[sender.tag]
                view.tabIsSelected()
                self.selectedHomeTab = sender.tag
            }
        }
       
    }
    
    //Mark:- Keyboard ANimation
    override func keyboardWillShow(_ notification: NSNotification){
        let kbSize = notification.userInfo?["UIKeyboardFrameEndUserInfoKey"] as! CGRect
        self.slidesScrollViewBottom.constant = kbSize.size.height
    }
    
    override func keyboardWillHide(_ notification: NSNotification){
        self.slidesScrollViewBottom.constant = 0
    }
    
}


extension HomeSlidesViewController : UIScrollViewDelegate{
    func createSlides(){
        for i in 0 ..< homeTabs.count{
            let slide:HomeSlidesView = Bundle.main.loadNibNamed("HomeSlidesView", owner: self, options: nil)?.first as! HomeSlidesView
            slide.orderCollectionView.tag = i
            slide.searchBar.tag = i
            slide.orderCollectionView.dataSource = self
            slide.orderCollectionView.delegate = self
            slide.searchBar.delegate = self
            self.slides.append(slide)
        }
    }
    
    func setupSlideScrollView(slides : [HomeSlidesView]) {
        slideScrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        slideScrollView.contentSize = CGSize(width:slideScrollView.frame.width * CGFloat(slides.count), height: view.frame.height - (140 + UIApplication.shared.statusBarFrame.height))
        
        slideScrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height - (140 + UIApplication.shared.statusBarFrame.height))
            slideScrollView.addSubview(slides[i])
        }
    }
    
    func createTitleTabs(){
        for i in 0 ..< homeTabs.count{
        let tab:HomeTabsView = Bundle.main.loadNibNamed("HomeTabsView", owner: self, options: nil)?.first as! HomeTabsView
        tab.title.text = homeTabs[i]
        tab.homeTabButton.tag = i
            if i == 0{
              tab.tabIsSelected()
            }
    tab.homeTabButton.addTarget(self,action:#selector(homeTabsButtonClicked),for:.touchUpInside)
        self.tabs.append(tab)
        }
    }
    
    func setupHomeTabsScrollView(slides : [HomeTabsView]) {
        tabsScrollView.frame = CGRect(x: 0, y: 0, width:view.frame.width, height:60)
        var width : CGFloat = 0
        
        if isIphone(){
          width = titleTabsWidth
        }
        else{
          // Width for title tabs in ipad
         width = CGFloat(self.view.bounds.size.width)/CGFloat(homeTabs.count)
        }
        
        tabsScrollView.contentSize = CGSize(width:width * CGFloat(tabs.count), height: 60)
        
        for i in 0 ..< self.tabs.count {
            tabs[i].frame = CGRect(x: width * CGFloat(i), y: 0, width: width, height: 60)
            tabsScrollView.addSubview(tabs[i])
        }
    }
    
    
    /*
     * default function called when view is scolled. In order to enable callback
     * when scrollview is scrolled, the below code needs to be called:
     * slideScrollView.delegate = self or
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == slideScrollView {
            let contentOffset = tabsScrollView.contentOffset;
            let point = CGPoint(x: (scrollView.contentOffset.x/self.view.frame.width) , y: contentOffset.y)
            UIView.animate(withDuration: 0.2) { () -> Void in
                if self.homeTabs.indices.contains(Int(point.x)){
                    let previousView = self.tabs[self.selectedHomeTab]
                     previousView.tabIsUnselected()
                    let view = self.tabs[Int(point.x)]
                    view.tabIsSelected()
                    if (view.frame.origin.x + self.titleTabsWidth) > self.view.frame.width {
                        self.tabsScrollView.contentOffset.x = (view.frame.origin.x - self.view.frame.width) + self.titleTabsWidth
                    }
                    self.selectedHomeTab = Int(point.x)
                }
            }
        }
    }
    
}


