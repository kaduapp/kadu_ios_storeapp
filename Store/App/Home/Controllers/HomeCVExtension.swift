//
//  Home+CVExtensionViewController.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 09/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class OrdersCollectionView: UICollectionView {
    
    override func awakeFromNib() {
        
        let refreshControl = UIRefreshControl()
        self.refreshControl = refreshControl
        self.register(UINib.init(nibName: "SectionHeader", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "SectionHeader")
        self.register(OrderCollectionCell.nib, forCellWithReuseIdentifier: OrderCollectionCell.identifier)
         self.register(SectionHeader.nib, forCellWithReuseIdentifier: SectionHeader.identifier)
    }
    
    
    func emptyErrorMessage() {
        if self.numberOfItems(inSection: 0) == 0{
            self.showErrorMessage(message: StringConstants.noCurrentOrders(),textColor: UIColor.white)
        }else {
            self.hideErrorMessage()
        }
    }
}

extension HomeSlidesViewController :UICollectionViewDataSource,UICollectionViewDelegate  {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
         let searchOrderSections  = self.viewModel.getSearchedModelSections(self.homeTabs[collectionView.tag])
        let orderSections  = self.viewModel.getOrderModelSections(self.homeTabs[collectionView.tag])
        if searchOrderSections.count > 0{
        return searchOrderSections.count
        }
        return orderSections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let searchOrderSections = self.viewModel.getSearchedModelSections(self.homeTabs[collectionView.tag])
        let orderSections  = self.viewModel.getOrderModelSections(self.homeTabs[collectionView.tag])
        if searchOrderSections.count > 0{
        return searchOrderSections[section].count
        }
         return orderSections[section].count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeader", for: indexPath) as! SectionHeader
        
        if viewModel.isDispatcherTab(self.homeTabs[collectionView.tag]){
            
            if self.viewModel.inDispatchOrderSections[indexPath.section][0].indispatch{
               sectionHeader.sectionHeaderlabel.text = "Auto-Dispatch Inprogress"
            }
            else{
               sectionHeader.sectionHeaderlabel.text = "Auto-Dispatch Failed"
            }
        }
        else{
            let searchOrderSections = self.viewModel.getSearchedModelSections(self.homeTabs[collectionView.tag])
            let orderSections  = self.viewModel.getOrderModelSections(self.homeTabs[collectionView.tag])
            var stringHead = Date()
                
            if searchOrderSections.count > 0{
                    stringHead =  searchOrderSections[indexPath.section][0].date
            }else{
                    stringHead =  orderSections[indexPath.section][0].date
            }
            sectionHeader.sectionHeaderlabel.text =  Helper.getDayOfWeek(stringHead)
                
        }
        return sectionHeader
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return  CGSize(width: collectionView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let ordersSection = self.viewModel.getOrderModelSections(self.homeTabs[collectionView.tag])
        let searchOrdersSection = self.viewModel.getSearchedModelSections(self.homeTabs[collectionView.tag])


        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OrderCollectionCell.identifier, for: indexPath) as? OrderCollectionCell {
            if searchOrdersSection.count > 0{
            cell.item = searchOrdersSection[indexPath.section][indexPath.row]
            }else{
             cell.item = ordersSection[indexPath.section][indexPath.row]
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         let ordersSection = self.viewModel.getOrderModelSections(self.homeTabs[collectionView.tag])
         let searchOrdersSection = self.viewModel.getSearchedModelSections(self.homeTabs[collectionView.tag])
        
        let orderVC = self.storyboard?.instantiateViewController(withIdentifier: String(describing: ManageOrderVC.self)) as! ManageOrderVC
        if searchOrdersSection.count > 0{
          orderVC.order = searchOrdersSection[indexPath.section][indexPath.row]
        }
        else{
         orderVC.order = ordersSection[indexPath.section][indexPath.row]
        }
        
        if (self.isIphone()) {
            self.navigationController?.pushViewController(orderVC, animated: true)
        }else {
            self.present(orderVC, animated: false, completion: nil)
        }
    }
    
}


// MARK: - UICollectionViewDelegateFlowLayout Methods
extension HomeSlidesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width:CGFloat!
        var height:CGFloat!
        if  !self.isIphone() { //!Utility.isForcedAccept() ||
            width = (collectionView.bounds.width - 100)/6
            height = width + 45
        }else {
            width = (collectionView.bounds.width - 32)/2
            height = width + 20
        }
        return  CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(10, 10, 10, 10)
    }
}
