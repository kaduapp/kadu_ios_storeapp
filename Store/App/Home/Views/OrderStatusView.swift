//
//  OrderStatusView.swift
//  DelivX Store
//
//  Created by Raghavendra Shedole on 01/02/18.
//  Copyright © 2018 Nabeel Gulzar. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class OrderStatusView: UIView {
    
    @IBOutlet weak var orderIDLabel: UILabel!
    @IBOutlet weak var orderStatusLabel:UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    var disposeBag = DisposeBag()
    var spinCount:Int = 0
    var timer:Timer!
    
       var isControllerDismiss = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        
        self.dropShadow(color: UIColor.colorWithHex(color: "6f6f6f"), opacity: 1, offSet: CGSize(width: 2, height: 2), radius: 5, scale: true)
      
    }
    
    func listenMqtt() {
       
        MQTT.sharedInstance.publishSubject.subscribe(onNext: { response, messageType in
            if messageType == MQTTMessageType.MQTTDriverOnlineStatus {
                return
            }
            if let status = response["statusMsg"] as? String, let orderID = response["orderId"] as? Int  , let orderType = response["status"] as? Int  {
                self.orderIDLabel.text = String(format:"%@ %@",NSLocalizedString("Order ID", comment: "Order ID"),"\(orderID)")
                self.orderStatusLabel.text = status
                
                if orderType == OrderStatus.NewOrder.rawValue {
                    if self.bottomConstraint.constant <= 0 && !(self.orderStatusLabel.text?.isEmpty)! {
                   // self.present()
                    }
                }
            }
        }, onError: { Error in
        },onDisposed: {
            print("MQTT Observer Removed")
        }).disposed(by: disposeBag)
    }
    
    deinit {
        disposeBag = DisposeBag()
        print("Order Status View Removed from Stack")
    }
    
    func removeObservers() {
        disposeBag = DisposeBag()
        print("Order Status View Removed from Stack")
    }
    
    func present() {
        UIView.animate(withDuration: 0.2) {
            self.bottomConstraint.constant = 50
            self.superview?.layoutIfNeeded()
        }
        
        DispatchQueue.main.async {
            self.timer  = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(self.rotate), userInfo: nil, repeats: false)
        }
    }
    
    @objc func rotate() {
        if spinCount == 2 {
        
            self.dismissView()
            return
        }
        
    UIView.animate(withDuration: 0.2, animations: {
            self.transform = CGAffineTransform(scaleX: 1, y: -1)
        }) { success in
            
            UIView.animate(withDuration: 0.2, animations: {
                self.transform = CGAffineTransform(scaleX: 1, y: 1)
            }, completion: { _ in
                self.spinCount += 1
                self.rotate()
            })
        }
    }
    
    
    func dismissView() {
      spinCount = 0
      UIView.animate(withDuration: 0.2) {
                self.timer.invalidate()
                
                self.bottomConstraint.constant = -100
                
                self.superview?.layoutIfNeeded()
            }
    }
    
    func getTopViewController() -> UIViewController? {
        let appDelegate = UIApplication.shared.delegate
        if let window = appDelegate!.window {
            return window?.rootViewController
        }
        return nil
    }
}


