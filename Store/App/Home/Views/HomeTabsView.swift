//
//  HomeSlidesView.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 09/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class HomeTabsView: UIView {
    
    @IBOutlet weak var title :UILabel!
    @IBOutlet weak var bottomView :UIView!
    @IBOutlet weak var homeTabButton : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tabIsUnselected()
    }
    
    func tabIsSelected(){
        Fonts.setPrimaryBold(title,size:18)
    }
    func tabIsUnselected(){
        Fonts.setPrimaryRegular(title,size:15)
    }
}
