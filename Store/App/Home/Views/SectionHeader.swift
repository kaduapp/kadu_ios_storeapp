//
//  SectionHeader.swift
//  DelivX Store
//
//  Created by 3Embed on 28/05/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class SectionHeader: UICollectionReusableView {
    @IBOutlet weak var sectionHeaderlabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .red
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    /// Method to register cell
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    /// Cell Identifier used for reusing cells
    static var identifier: String {
        return String(describing: self)
    }
   
}
