//
//  HomeView.swift
//  DelivX Store
//
//  Created by Rahul Sharma on 08/08/19.
//  Copyright © 2019 Nabeel Gulzar. All rights reserved.
//

import UIKit

class HomeSlidesView: UIView {

    @IBOutlet weak var orderCollectionView:OrdersCollectionView!
    
      @IBOutlet weak var searchBar:UISearchBar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
}

class OrderSearchBar: UISearchBar {
    
    override func awakeFromNib() {
        
        //SearchBar Text
        let textFieldInsideUISearchBar = self.value(forKey: "searchField") as? UITextField
        textFieldInsideUISearchBar?.textColor = ColorConstants.Color_searchBarText
        
        self.layer.cornerRadius = 8
        self.layer.masksToBounds = true
        self.layer.borderWidth = 2
        self.layer.borderColor = ColorConstants.backgroundColor.cgColor
        textFieldInsideUISearchBar?.font = UIFont(name: RobotoCondensed.Regular, size: (textFieldInsideUISearchBar?.font?.pointSize)!)
        
        //SearchBar Placeholder
        let textFieldInsideUISearchBarLabel = textFieldInsideUISearchBar!.value(forKey: "placeholderLabel") as? UILabel
        textFieldInsideUISearchBarLabel?.textColor = ColorConstants.Color_SerchBarPlaceholder
        textFieldInsideUISearchBarLabel?.text = "  \(StringConstants.searchPreOrders())"
    }
}
