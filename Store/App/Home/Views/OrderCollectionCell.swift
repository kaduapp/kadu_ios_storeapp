//
//  OrderCollectionCell.swift
//  DelivX Store
//
//  Created by 3 Embed on 18/12/17.
//  Copyright © 2017 3 Embed. All rights reserved.
//

import UIKit
import CircleProgressView

class OrderCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var cellContentView: UIView!
    @IBOutlet weak var orderIDLabel: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var orderAmount: UILabel!
    @IBOutlet weak var nowLabel: UILabel!
    @IBOutlet weak var storeType: UILabel!
    
    @IBOutlet var circularProgressView: CircleProgressView!
    @IBOutlet weak var timeView: UIView!
//    @IBOutlet weak var dueTime: UILabel!
    @IBOutlet weak var laterLabel: UILabel!
    @IBOutlet weak var circleDot: UIView!;
    @IBOutlet weak var bookingTimeLabel: UILabel!
    @IBOutlet weak var date: RobotoMediumLable!
    
    @IBOutlet weak var pulsatorWidthConstraints: NSLayoutConstraint!
    @IBOutlet weak var pulsatorHeightconstraints: NSLayoutConstraint!
    @IBOutlet weak var pulsator : UIImageView!
    @IBOutlet weak var orderType: UILabel!
    @IBOutlet weak var orderTypeView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    private var dateString  = ""
    var timeEnd : Date?
    var totalMin:Int = 1
    static var statusMsg = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetup()
       
    }  //bookingType
    /// INITIAL VIEW SETUP
    func initialSetup(){
        let userType = UserDefaults.standard.integer(forKey: "user_Type")
        
        if self.isIphone(){
              self.updateTheShadowToTheView(width:(UIScreen.main.bounds.size.width - 32)/2, height:(UIScreen.main.bounds.size.width - 32)/2 + 10)
        }else{
            self.updateTheShadowToTheView(width:(UIScreen.main.bounds.size.width - 100)/6, height:(UIScreen.main.bounds.size.width - 100)/6 + 35)
        }
        //   }
    }
    
    func updateTheShadowToTheView(width:CGFloat, height:CGFloat){
        
        self.contentView.layer.cornerRadius = 2.0
        self.contentView.layer.borderWidth = 0.5
        self.contentView.layer.borderColor = UIColor.clear.cgColor
        self.contentView.layer.masksToBounds = true
        
        self.layer.shadowColor = Helper.getUIColor(color:Colors.AppBaseColor).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.1
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(rect:CGRect.init(x: 0, y:0 , width: width, height: height)).cgPath
    }
    
    /// Method to register cell
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    /// Cell Identifier used for reusing cells
    static var identifier: String {
        return String(describing: self)
    }
    
    public func isIphone() -> Bool {
        return UIDevice.current.userInterfaceIdiom == .phone ? true : false
    }
    
    
    /// Setting cell data
    var item: HomeOrder? {
        didSet {
            guard let item = item else {
                return
            }
            
            storeType.text = item.bookingType == 1 ? StringConstants.ASAP() : StringConstants.later()
            storeType.textColor = .white
            storeType.textAlignment = item.bookingType == 1 ? .right : .center
            
            if item.bookingType == 2{
                if item.serviceType == 1{
                    dateString = item.dueDateTime
                    self.timeEnd = Date.getStringToDateFromTimeStamp(value: item.dueTimeStamp, format: DateFormat.DateAndTimeFormatServer)
                    self.dsiplayTimer()
                }
            }
            
            
            orderIDLabel.text = "\(item.orderID)"
            customerName.text = item.customerName
            orderAmount.text = "\(item.currencySymbol) \(String(format:"%0.2f",item.displayAmount))"
            
            if item.dueTimeStamp == 0{
                timeEnd = Date.getStringToDate(value: item.dueDateTime, format: DateFormat.DateAndTimeFormatServer)
            }else{
                timeEnd = Date.getStringToDateFromTimeStamp(value: item.dueTimeStamp, format: DateFormat.DateAndTimeFormatServer)
            }
            
            if(Utility.langCode == "ar"){
                if (item.statusMessage == "Time Updated"){
                    statusLabel.text = "الوقت المحدث"
                }else {
                    
                    switch item.statusMessage {
                     case "New Order" :
                        statusLabel.text = StringConstants.new()
                        break
                        
                    case "Accepted" :
                        statusLabel.text = StringConstants.accepted()
                        break
                        
                    case "Driver is at the store." :
                        statusLabel.text = "السائق هو في المتجر."
                        break
                    case "Order Delivered.":
                        statusLabel.text = "أجل تسليم."
                        break
                    default:
                        break
                    }
                }
            } else {
                 statusLabel.text = item.statusMessage
                if (item.bookingType == 1 && item.orderStatus == OrderStatus.Dispatch.rawValue  && item.serviceType == 1   )  || item.bookingType == 1 && item.orderStatus == OrderStatus.OrderUnassigned.rawValue  && item.serviceType == 1 || item.bookingType == 1 && item.orderStatus == OrderStatus.PreparingOrder.rawValue && item.serviceType == 1 {
                
                        statusLabel.text = StringConstants.waitingFroDriver()
                        laterLabel.text = " "
                        
                        pulsator.isHidden = false
                        pulsator.image = UIImage.gifImageWithName("pulsator1")
                        pulsatorWidthConstraints.constant = 27.0
                        pulsatorHeightconstraints.constant = 27.0
                }else{
                        pulsatorWidthConstraints.constant = 0.0
                        pulsatorHeightconstraints.constant = 0.0
                    }
                }
        
            laterLabel.text = " "
            
            totalMin = Date.differenceDate(date: timeEnd!)
            
            if item.bookingType == 1{   //totalMin
                laterLabel.isHidden = true
            }else{
                
                if item.dueTimeStamp == 0{
                    laterLabel.text = Date.getDateFromString(value: item.dueDateTime, format: DateFormat.onlyTime)
                }else{
                   laterLabel.text = Date.getTheDataFromTimeStamp(value: item.dueTimeStamp, format: DateFormat.TimeAndDateFormatServer)
                }
            }
            
            laterLabel.text = " " // 9023585002
            if item.bookingDateTimeStamp == 0{
                bookingTimeLabel.text = Date.getDateFromString(value: item.bookingDateTime, format: DateFormat.onlyTime)
                  date.text = Date.getDateFromString(value: item.bookingDateTime, format: DateFormat.onlyDate)
            }else{
                bookingTimeLabel.text = Date.getTheDataFromTimeStamp(value: item.bookingDateTimeStamp, format: DateFormat.onlyTime)
                  date.text = Date.getDateFromString(value: item.bookingDateTime, format: DateFormat.onlyDate)
            }
            
            if item.serviceType == 1{
                orderTypeView.backgroundColor = UIColor.colorWithHex(color: "4F5C90")
                orderType.text = StringConstants.Delivery()
            }else{
                orderTypeView.backgroundColor = UIColor.colorWithHex(color: "3CADA9")
                orderType.text = StringConstants.PickUp()
            }
            
            self.setCellSelected()
        }
    }
    func setCellSelected() {

    }

    
    deinit {
        print("Order Cell deinited")
        timer?.invalidate()
    }
    
    weak var timer : Timer?
    func dsiplayTimer(){
        self.setTimeLeft()
        
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.setTimeLeft), userInfo: nil, repeats: true)
        
    }
    @objc func setTimeLeft( ) {
        
        
        
        let timeNow = Date.getStringToDate(value:Date.getCurrentDateAndTime() , format: DateFormat.DateAndTimeFormatServer )
        
        if timeEnd?.compare(timeNow) == ComparisonResult.orderedDescending {
            
            let interval = timeEnd?.timeIntervalSince(timeNow)
            
            let days =  (interval! / (60*60*24)).rounded(.down)
            
            let daysRemainder = interval?.truncatingRemainder(dividingBy: 60*60*24)
            
            let hours = (daysRemainder! / (60 * 60)).rounded(.down)
            
            let hoursRemainder = daysRemainder?.truncatingRemainder(dividingBy: 60 * 60).rounded(.down)
            
            let minites  = (hoursRemainder! / 60).rounded(.down)
            
            let formatter = NumberFormatter()
            formatter.minimumIntegerDigits = 2
            
            let timeLeft = "\( ( formatter.string(from: NSNumber(value:days)) == "00" ? "" : formatter.string(from: NSNumber(value:days))  )  ?? "" )\(formatter.string(from: NSNumber(value:days)) == "00" ? "" : "D:" )\(formatter.string(from: NSNumber(value:hours)) ?? "")H:\(formatter.string(from: NSNumber(value:minites)) ?? "" )M"
        
                storeType.text = timeLeft
                storeType.textColor = .white
            
        }else{
            
            if (storeType.text?.contains("H:"))!  || (storeType.text?.contains(StringConstants.later()))!{
                
                
                let date = dateString
                let dateFormatter = DateFormatter()
                
                dateFormatter.dateFormat = DateFormat.DateAndTimeFormatServer
                let s = dateFormatter.date(from: date)
                
                let components = Calendar.current.dateComponents([.day, .hour, .minute], from: (s!).toLocalTime(), to:timeNow)
                
                guard   let day = components.day , let hoursPortion =  components.hour, let minutesPortion = components.minute
                else {
                    preconditionFailure("Should not happen")
                }
            
                var timeOverLay = "\(hoursPortion)H:\(minutesPortion)M"
                if timeOverLay.contains("-"){
                    timeOverLay =  timeOverLay.replacingOccurrences(of: "-", with: "")
                }
            
                storeType.text = timeOverLay
                storeType.textColor = .red
                storeType.textAlignment = .right
            }
        }
    }
}

extension String{
    func toDate(format : String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)!
    }
}





    


