//
//  MQTTModule.swift
//  MQTT Chat Module
//
//  Created by Rahul Sharma on 11/07/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

import UIKit
import MQTTClient
import Foundation
import CocoaLumberjack
import RxSwift


class MQTT : NSObject{
    struct Constants {
        
        fileprivate static let host = AppConfig.MQTTHost
        fileprivate  static let port:Int = AppConfig.MQTTPort
        fileprivate   static let userID = StoreUtility.getStore().storeId
        fileprivate   static let userName =  AppConfig.MQTTUsername
        fileprivate   static let password =  AppConfig.MQTTPassword
    }
    
    /// Shared instance object for gettting the singleton object
    static let sharedInstance = MQTT()
    var manager : MQTTSessionManager!
    var publishSubject = PublishSubject<([String:Any], MQTTMessageType)>()
    
    ///This flag will tell you that you are connected or not.
    var isConnected : Bool = false
    
    
    /// Check the MQTT Event trigger frequency
    var eventFrequency = 0
    var startedSyncData = 0
    var eventOrderId = 0
    var newEventOrderId = 0
    var orderIdTriggered = 0
    var timer : Timer?
    var rotation = 0
    var orderTriggeredTimer : Timer?
    /// Used for creating the initial connection.
    func createConnection() {
        ///creating connection with the proper client ID.
        if let userId = self.userID {
            self.connect(withClientId: userId)
        }
    }
    
    func disconnectMQTTConnection() {
        if self.isConnected {
            self.isConnected = false
            manager.disconnect(disconnectHandler: { (error) in
                print("Disconnected From MQTT")
            })
        }
    }

    /// Used for subscribing the channel
    ///
    /// - Parameters:
    ///   - topic: name of the current topic (It should contain the name of the topic with saperators)
    ///
    /// eg- Message/UserName
    ///   - Delivering: Type of QOS // can be 0,1 or 2.
    fileprivate func subscribe(topic : String, withDelivering Delivering : MQTTQosLevel) {
        if (manager != nil) {
            if (manager.subscriptions != nil) {
                var subscribeDict = manager.subscriptions!
                if (subscribeDict[topic] == nil) {
                    subscribeDict[topic] = Delivering.rawValue as NSNumber
                }
                self.manager.subscriptions = subscribeDict
            } else {
                let subcription = [topic : Delivering.rawValue as NSNumber]
                self.manager.subscriptions = subcription
            }
            
            self.manager.connect(toLast: nil)
         //   self.manager.connect(toLast: nil)
        }
    }
    /// Used for subscribing the channel
    ///
    /// - Parameter channelName: current channel which you want to subscribe.
    func subscribeTopic(withTopicName topicName : String,withDelivering delivering:MQTTQosLevel ) {
        let topicToSubscribe = topicName
        self.subscribe(topic: "\(topicToSubscribe)", withDelivering: delivering)
    }
    
    /// Used for Unsubscribing the channel
    ///
    /// - Parameter channelName: current channel which you want to Unsubscribing.
    
    func unsubscribeTopic(topic : String) {
        var unsubscribeDict = manager.subscriptions
        if unsubscribeDict?[topic] != nil {
            unsubscribeDict?.removeValue(forKey:topic)
        }
        self.manager.subscriptions = unsubscribeDict
          self.manager.connect(toLast: nil)
       // self.manager.connect(toLast: nil)
    }
    
    /// Used for connecting with the server.
    ///
    /// - Parameter clientId: current Client ID.
    func connect(withClientId clientId :String) {
        if (self.manager == nil) {
            self.manager = MQTTSessionManager()
            let host = Constants.host
            let port: Int = Constants.port
            manager.delegate = self
            manager.connect(to: host, port: port, tls: false, keepalive: 60, clean: false, auth: true, user: Constants.userName, pass: Constants.password, will: false, willTopic: nil, willMsg: nil, willQos: .atMostOnce, willRetainFlag: false, withClientId: "Ufly_iOS_Staff\(clientId)", securityPolicy: MQTTSSLSecurityPolicy.default(), certificates: [], protocolLevel: MQTTProtocolVersion.version31) { (Error) in
                if let error = Error?.localizedDescription {
                    DDLogDebug(error)
                }
            }
        } else {
            self.manager.connect(toLast: nil)
            //   self.manager.connect(toLast: nil)
        }
    }
}

// MARK: - Channels
extension MQTT {
    /// Connection ID
    var userID: String? {
        get {
            return StoreUtility.getStore().ManagerId + UIDevice.current.identifierForVendor!.uuidString
        }
    }
    
    /// This topic is for getting new orders
    var OrdersTopic: String? {
        get {
            let store = StoreUtility.getStore()
            let userType = store.userType
            switch userType{
            case 1: // frachise
                return "storeManager/\(store.cityId)/\(store.franchiseId)/#"
            case 2:  // store
                return "storeManager/\(store.cityId)/\(store.franchiseId)/\(store.storeId)/\(store.ManagerId)"
            default: // central
                return "cityManager/\(store.cityId)/\(store.ManagerId)"
            }
        }
    }
    
    
    /// Driver statuses channel
    var driverOnlineStatus:String? {
        get {
       return "dispatcher/\(UserDefaults.standard.object(forKey: "city_ID") ?? String.self)/\(StoreUtility.getStore().storeId)/0"
        }
    }
    
    /// Session Channel
    var sessionTopic:String {
        get {
            return "mqttManagerTopic/\(StoreUtility.getStore().mqttManagerTopic)"
        }
    }
}

// MARK: - Mqtt Delegates for getting messages
extension MQTT:MQTTSessionManagerDelegate {
    /// Will get mqtt messages here with the topic name on which we get that one
    ///
    /// - Parameters:
    ///   - data: MQTT Payload
    ///   - topic: subscribe topic on which we got the message
    ///   - retained: retained message of not
    func handleMessage(_ data: Data!, onTopic topic: String!, retained: Bool) {
        // self.mqttHandler.gotResponeFromMqtt(responseData: data, topicChannel: topic)
        do {
            guard let dataObj = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else { return }
            let mqtt = MQTT.sharedInstance
            if let destination = dataObj["destinationName"] as? String{
                if destination == "orderUpdate"{
                    if let orderId = dataObj["orderId"] as? Int{
                        newEventOrderId = orderId
                    }
                    if let orderId = dataObj["orderId"] as? Int, eventOrderId == 0{
                       eventOrderId = orderId
                    }
                    eventFrequency = eventFrequency + 1
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    if self.eventFrequency == 1{
                        self.eventFrequency = 0
                        self.startedSyncData = 0
                        self.newEventOrderId = 0
                        self.eventOrderId = 0
                        self.orderIdTriggered = 0
                    mqtt.publishSubject.onNext((dataObj, .MQTTNewOrder))
                    }
                 else if self.eventOrderId != self.newEventOrderId {
                 self.startedSyncData = self.eventFrequency
                 self.scheduledTimerWithTimeInterval()
                 mqtt.publishSubject.onNext((dataObj, .MQTTSyncingData))
                    }
                    else if self.orderIdTriggered != self.newEventOrderId  {
                     self.orderIdTriggered = self.eventOrderId
                    self.scheduledTimerForOrderTrighgerred()
                     mqtt.publishSubject.onNext((dataObj, .MQTTNewOrder))
                     }
                    })
                   
                }else if destination == "driverStatus"{
                    //mqtt.publishSubject.onNext((dataObj, .MQTTDriverOnlineStatus))
                }
            }
            
           if topic == mqtt.driverOnlineStatus {
                //driver statuses
//                mqtt.publishSubject.onNext((dataObj, .MQTTDriverOnlineStatus))
            }
            else if topic == mqtt.sessionTopic {
                //session mqtt messages
                Helper.showAlert(message: dataObj["message"] as! String, head: "Session Expired", type: 0)
                Logout.logout()
            }
            
        } catch let jsonError {
            DDLogDebug("Error !!!\(jsonError)")
        }
    }
    
    func scheduledTimerForOrderTrighgerred(){
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        guard orderTriggeredTimer == nil else { return }
        orderTriggeredTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.resetData), userInfo: nil, repeats: true)
    }
    
    
    @objc func resetData(){
        rotation = rotation + 1
        if self.orderIdTriggered != newEventOrderId || rotation == 6 {
            orderTriggeredTimer?.invalidate()
            orderTriggeredTimer = nil
            self.eventFrequency = 0
            self.startedSyncData = 0
            self.newEventOrderId = 0
            self.eventOrderId = 0
            self.orderIdTriggered = 0
            rotation = 0
            let mqtt = MQTT.sharedInstance
            mqtt.publishSubject.onNext(([:], .MQTTNewOrder))
        }
    }
    
    func scheduledTimerWithTimeInterval(){
        // Scheduling timer to Call the function "updateCounting" with the interval of 1 seconds
        guard timer == nil else { return }
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.checkSyncingDataVariable), userInfo: nil, repeats: true)
    }
    
    @objc func checkSyncingDataVariable(){
        if eventFrequency == startedSyncData{
        timer?.invalidate()
        timer = nil
        self.eventFrequency = 0
        self.startedSyncData = 0
        self.newEventOrderId = 0
        self.eventOrderId = 0
        self.orderIdTriggered = 0
        let mqtt = MQTT.sharedInstance
        mqtt.publishSubject.onNext(([:], .MQTTNewOrder))
        }
    }
    
    func sessionManager(_ sessionManager: MQTTSessionManager!, didChange newState: MQTTSessionManagerState) {
        switch newState {
        case .connected:
             self.isConnected = true
            let mqtt = MQTT.sharedInstance
            if !StoreUtility.isLogin(){
                mqtt.disconnectMQTTConnection()
                return
            }
            mqtt.subscribeTopic(withTopicName: mqtt.OrdersTopic!, withDelivering: .exactlyOnce)
            mqtt.subscribeTopic(withTopicName: mqtt.sessionTopic, withDelivering: .exactlyOnce)
            mqtt.subscribeTopic(withTopicName: mqtt.driverOnlineStatus!, withDelivering: .exactlyOnce)
        
        default:
            //other statuses
            break
            
        }
    }
}

